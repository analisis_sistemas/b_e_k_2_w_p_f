﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace DAL_B_I_T_S.Correo
{
  public  class cls_Correo_DAL
  {
      #region Var Prv

      
      
      //"b.i.t.s.bek2cr@gmail.com"  //"BITSBK2CR"
      private string _sFrom, _sTo, _sMessage, _sSubject, _sDE, _sPASS, _sError;

     

      private List<string> _lsArchivo;

 
      #endregion

      #region Var Pub

      public string sFrom
      {
          get { return _sFrom; }
          set { _sFrom = value; }
      }

      public string sTo
      {
          get { return _sTo; }
          set { _sTo = value; }
      }

      public string sMessage
      {
          get { return _sMessage; }
          set { _sMessage = value; }
      }

      public string sSubject
      {
          get { return _sSubject; }
          set { _sSubject = value; }
      }

      public string sDE
      {
          get { return _sDE; }
          set { _sDE = value; }
      }

      public string sPASS
      {
          get { return _sPASS; }
          set { _sPASS = value; }
      }

      public string sError
      {
          get { return _sError; }
          set { _sError = value; }
      }
      public List<string> lsArchivo
      {
          get { return _lsArchivo; }
          set { _lsArchivo = value; }
      }

     
      #endregion
  }
}
