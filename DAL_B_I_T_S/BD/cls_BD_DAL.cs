﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DAL_B_I_T_S.BD
{
   public class cls_BD_DAL
    {

        #region Variables Privadas

        private string _sMsjError, _sCadenaCNX, _sResulId;

        #endregion

        #region Variables Públicas & Constructores

        public string sMsjError
        {
            get { return _sMsjError; }
            set { _sMsjError = value; }
        }

        public string sCadenaCNX
        {
            get { return _sCadenaCNX; }
            set { _sCadenaCNX = value; }
        }

        public string sResulId
        {
            get { return _sResulId; }
            set { _sResulId = value; }
        }
        #endregion

        #region Objetos

        public SqlConnection SQL_CNX;

        public SqlCommand SQL_CMD;

        public SqlDataAdapter SQL_DA;

        public DataSet DS = new DataSet();

        #endregion
    }
}
