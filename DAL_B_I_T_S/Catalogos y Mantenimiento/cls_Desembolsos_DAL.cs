﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DAL_B_I_T_S.Catalogos_y_Mantenimiento
{
  public class cls_Desembolsos_DAL
  {
      #region Variables Privadas

      private char _cBandAxion, _cBandEstAxion;

      private int _iIdEstudiante, _iIdDesembolso, _iIdFactura, _iIdEstado;

      private double _dMonto_Desembolsado, _dMonto_Subvencion, _dMonto_Estudiante;

      private DateTime _dtFecha_Desembolso;

      #endregion

        #region Variables Publicas

        public DateTime dtFecha_Desembolso
        {
            get { return _dtFecha_Desembolso; }
            set { _dtFecha_Desembolso = value; }
        }

        public double dMonto_Desembolsado
        {
            get { return _dMonto_Desembolsado; }
            set { _dMonto_Desembolsado = value; }
        }

        public double dMonto_Subvencion
        {
            get { return _dMonto_Subvencion; }
            set { _dMonto_Subvencion = value; }
        }

        public double dMonto_Estudiante
        {
            get { return _dMonto_Estudiante; }
            set { _dMonto_Estudiante = value; }
        }


        public int iIdEstudiante
        {
            get { return _iIdEstudiante; }
            set { _iIdEstudiante = value; }
        }

        public int iIdDesembolso
        {
            get { return _iIdDesembolso; }
            set { _iIdDesembolso = value; }
        }

        public int iIdFactura
        {
            get { return _iIdFactura; }
            set { _iIdFactura = value; }
        }

        public int iIdEstado
        {
            get { return _iIdEstado; }
            set { _iIdEstado = value; }
        }

        public char cBandAxion
        {
            get { return _cBandAxion; }
            set { _cBandAxion = value; }
        }

        public char cBandEstAxion
        {
            get { return _cBandEstAxion; }
            set { _cBandEstAxion = value; }
        }

        #endregion
    }
}
