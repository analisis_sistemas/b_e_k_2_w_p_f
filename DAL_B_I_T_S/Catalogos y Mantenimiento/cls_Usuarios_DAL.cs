﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_B_I_T_S.Catalogos_y_Mantenimiento
{
   public class cls_Usuarios_DAL
   {

       #region Variables Privadas
       private string _sIdUsuario, _sNombre,  _sPassword;

       private int _iIdRol, _iIdEstado;

       private char _cBandAxion, _cBandEstAxion;      

          
       #endregion

       #region Variables Públicas

       public char cBandAxion
       {
           get { return _cBandAxion; }
           set { _cBandAxion = value; }
       }

       public char cBandEstAxion
       {
           get { return _cBandEstAxion; }
           set { _cBandEstAxion = value; }
       }

       public string sIdUsuario
       {
           get { return _sIdUsuario; }
           set { _sIdUsuario = value; }
       }

       public string sNombre
       {
           get { return _sNombre; }
           set { _sNombre = value; }
       }

    

       public string sPassword
       {
           get { return _sPassword; }
           set { _sPassword = value; }
       }
       

          public int iIdRol
          {
              get { return _iIdRol; }
              set { _iIdRol = value; }
          }

          public int iIdEstado
          {
              get { return _iIdEstado; }
              set { _iIdEstado = value; }
          }
       #endregion


   }
}
