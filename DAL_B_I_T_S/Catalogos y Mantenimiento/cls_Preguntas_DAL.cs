﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_B_I_T_S.Catalogos_y_Mantenimiento
{
   public class cls_Preguntas_DAL
   {
       #region Var Prv
        private int _iIdPregunta;

       
       private string _sPregunta;

       private char _cBandAxion, _cBandEstAxion;
       #endregion

       #region Var Pub
       public char cBandAxion
       {
           get { return _cBandAxion; }
           set { _cBandAxion = value; }
       }

       public char cBandEstAxion
       {
           get { return _cBandEstAxion; }
           set { _cBandEstAxion = value; }
       }

       public int iIdPregunta
       {
           get { return _iIdPregunta; }
           set { _iIdPregunta = value; }
       }

       public string sPregunta
       {
           get { return _sPregunta; }
           set { _sPregunta = value; }
       }
        #endregion
    }
}
