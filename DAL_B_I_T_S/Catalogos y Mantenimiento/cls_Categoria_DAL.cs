﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_B_I_T_S.Catalogos_y_Mantenimiento
{
   public class cls_Categoria_DAL
   {

       #region Variables Privadas


       private int _iIdCategoria, _iMonto;


       private string _sNombre;

       private char _cBandAxion, _cBandEstAxion;

       #endregion

       #region Variables Publicas

       public int iIdCategoria
       {
           get { return _iIdCategoria; }
           set { _iIdCategoria = value; }
       }


       public string sNombre
       {
           get { return _sNombre; }
           set { _sNombre = value; }
       }

       public int iMonto
       {
           get { return _iMonto; }
           set { _iMonto = value; }
       }      


       public char cBandAxion
       {
           get { return _cBandAxion; }
           set { _cBandAxion = value; }
       }

       public char cBandEstAxion
       {
           get { return _cBandEstAxion; }
           set { _cBandEstAxion = value; }
       }

       #endregion
    }
}
