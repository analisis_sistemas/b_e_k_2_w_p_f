﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_B_I_T_S.Catalogos_y_Mantenimiento
{
   public class cls_Cursos_DAL
   {
       #region Var Private

       private char _cBandAxion, _cBandEstAxion;

       private int _iIdEstado, _iId_Estudiante;

       private decimal _dNota;

       private String _sNombre;


       #endregion

       #region Var Public

       public int iIdEstado
       {
           get { return _iIdEstado; }
           set { _iIdEstado = value; }
       }

       public int iId_Estudiante
       {
           get { return _iId_Estudiante; }
           set { _iId_Estudiante = value; }
       }

       public decimal dNota
       {
           get { return _dNota; }
           set { _dNota = value; }
       }

       public char cBandAxion
       {
           get { return _cBandAxion; }
           set { _cBandAxion = value; }
       }

       public char cBandEstAxion
       {
           get { return _cBandEstAxion; }
           set { _cBandEstAxion = value; }
       }
       public String sNombre
       {
           get { return _sNombre; }
           set { _sNombre = value; }
       }
       #endregion
   }
}
