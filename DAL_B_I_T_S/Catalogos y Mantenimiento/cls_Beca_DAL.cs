﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_B_I_T_S.Catalogos_y_Mantenimiento
{
    public class cls_Beca_DAL
    {
        #region VarPr

        private int _iIdBeca, _iIdPatrocinador, _iIdCategoria, _iIdSubcategoria, _iIdEstado;

        private char _cBandAxion, _cBandEstAxion;

        private double _dPorcentajeDescuento, _dNotaMinima;

      

        private char _cAplicaDesembolso;




       
      
     

        #endregion

        #region Var Publi

        public char cBandAxion
        {
            get { return _cBandAxion; }
            set { _cBandAxion = value; }
        }

        public char cBandEstAxion
        {
            get { return _cBandEstAxion; }
            set { _cBandEstAxion = value; }
        }

        private string _sNombre;

        public string sNombre
        {
            get { return _sNombre; }
            set { _sNombre = value; }
        }

        public int iIdBeca
        {
            get { return _iIdBeca; }
            set { _iIdBeca = value; }
        }

        public int iIdPatrocinador
        {
            get { return _iIdPatrocinador; }
            set { _iIdPatrocinador = value; }
        }

        public int iIdCategoria
        {
            get { return _iIdCategoria; }
            set { _iIdCategoria = value; }
        }

        public int iIdSubcategoria
        {
            get { return _iIdSubcategoria; }
            set { _iIdSubcategoria = value; }
        }

        public int iIdEstado
        {
            get { return _iIdEstado; }
            set { _iIdEstado = value; }
        }

        public double dPorcentajeDescuento
        {
            get { return _dPorcentajeDescuento; }
            set { _dPorcentajeDescuento = value; }
        }

        public double dNotaMinima
        {
            get { return _dNotaMinima; }
            set { _dNotaMinima = value; }
        }
       

        public char cAplicaDesembolso
        {
            get { return _cAplicaDesembolso; }
            set { _cAplicaDesembolso = value; }
        }
        #endregion
    }
}
