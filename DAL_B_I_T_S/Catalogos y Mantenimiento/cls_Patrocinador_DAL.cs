﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_B_I_T_S.Catalogos_y_Mantenimiento
{
    public class cls_Patrocinador_DAL
    {
        #region Var Priv

        private int _iIdPatrocinador, _iIdPeriodo, _iIdEstado;

        private double _dMontoMaximo;

        private string _sNombre, _sCorreo;

       

        private char _cBandAxion, _cBandEstAxion;     


        #endregion

        #region Var Pub
        public char cBandAxion
        {
            get { return _cBandAxion; }
            set { _cBandAxion = value; }
        }
        public string sCorreo
        {
            get { return _sCorreo; }
            set { _sCorreo = value; }
        }

        public char cBandEstAxion
        {
            get { return _cBandEstAxion; }
            set { _cBandEstAxion = value; }
        }

        public string sNombre
        {
            get { return _sNombre; }
            set { _sNombre = value; }
        }


        public double dMontoMaximo
        {
            get { return _dMontoMaximo; }
            set { _dMontoMaximo = value; }
        }

        public int iIdPatrocinador
        {
            get { return _iIdPatrocinador; }
            set { _iIdPatrocinador = value; }
        }

        public int iIdPeriodo
        {
            get { return _iIdPeriodo; }
            set { _iIdPeriodo = value; }
        }

        public int iIdEstado
        {
            get { return _iIdEstado; }
            set { _iIdEstado = value; }
        }
        #endregion
    }
}
