﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_B_I_T_S.Catalogos_y_Mantenimiento
{
   public class cls_Empleados_DAL
   {
       #region Var Pr

       private int _iIdEmpleado, _iIdCampus;

       private string _sCedula, _sNombre, _sPrimerApellido, _sSegundoApellido, _sIdUsuario, _sCorreoElectronico;

       private DateTime _dtFechaNacimiento;

       private char _cBandAxion, _cBandEstAxion;

       
         
       #endregion

       #region Var Pub

       public int iIdEmpleado
       {
           get { return _iIdEmpleado; }
           set { _iIdEmpleado = value; }
       }

       public int iIdCampus
       {
           get { return _iIdCampus; }
           set { _iIdCampus = value; }
       }
       public string sCedula
       {
           get { return _sCedula; }
           set { _sCedula = value; }
       }

       public string sNombre
       {
           get { return _sNombre; }
           set { _sNombre = value; }
       }

       public string sPrimerApellido
       {
           get { return _sPrimerApellido; }
           set { _sPrimerApellido = value; }
       }

       public string sSegundoApellido
       {
           get { return _sSegundoApellido; }
           set { _sSegundoApellido = value; }
       }

       public string sIdUsuario
       {
           get { return _sIdUsuario; }
           set { _sIdUsuario = value; }
       }

       public string sCorreoElectronico
       {
           get { return _sCorreoElectronico; }
           set { _sCorreoElectronico = value; }
       }
       public DateTime dtFechaNacimiento
       {
           get { return _dtFechaNacimiento; }
           set { _dtFechaNacimiento = value; }
       }

       public char cBandAxion
       {
           get { return _cBandAxion; }
           set { _cBandAxion = value; }
       }

       public char cBandEstAxion
       {
           get { return _cBandEstAxion; }
           set { _cBandEstAxion = value; }
       }
       #endregion
   }
}
