﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_B_I_T_S.Catalogos_y_Mantenimiento
{
  public  class cls_Sub_Categoria_DAL
  {

      #region Var Pr
      private char _cBandAxion, _cBandEstAxion;

      private int _iIdSubcategoria, _iPorcentaje;

      private string _sNombre;

      
     
      #endregion

      #region Var Pub
      public int iIdSubcategoria
      {
          get { return _iIdSubcategoria; }
          set { _iIdSubcategoria = value; }
      }
      public int iPorcentaje
      {
          get { return _iPorcentaje; }
          set { _iPorcentaje = value; }
      }

      public string sNombre
      {
          get { return _sNombre; }
          set { _sNombre = value; }
      }

      public char cBandAxion
      {
          get { return _cBandAxion; }
          set { _cBandAxion = value; }
      }

      public char cBandEstAxion
      {
          get { return _cBandEstAxion; }
          set { _cBandEstAxion = value; }
      }
      #endregion
  }
}
