﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_B_I_T_S.Catalogos_y_Mantenimiento
{
   public class cls_Campus_DAL
   {
       #region Variables Privadas

       private int _iIdCampus;

       private Int16 _iIDInstitucion;

       private char _cBandAxion, _cBandEstAxion;

       private string _sNombre;

       
       #endregion

       #region Variables Publicas

       public char cBandAxion
       {
           get { return _cBandAxion; }
           set { _cBandAxion = value; }
       }

       public char cBandEstAxion
       {
           get { return _cBandEstAxion; }
           set { _cBandEstAxion = value; }
       }
       public int iIdCampus
       {
           get { return _iIdCampus; }
           set { _iIdCampus = value; }
       }

       public string sNombre
       {
           get { return _sNombre; }
           set { _sNombre = value; }
       }

       public Int16 sIDInstitucion
       {
           get { return _iIDInstitucion; }
           set { _iIDInstitucion = value; }
       }
       #endregion
   }
}
