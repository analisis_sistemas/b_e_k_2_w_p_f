﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_B_I_T_S.Catalogos_y_Mantenimiento
{
   public class cls_Materias_DAL
   {

       #region Var Priv

       private int _iIdMateria;

      

        private string _sNombre;

        private double _dCosto;

        private char _cBandAxion, _cBandEstAxion;

       
          
       #endregion

       #region Var Pub

        public char cBandAxion
        {
            get { return _cBandAxion; }
            set { _cBandAxion = value; }
        }

        public char cBandEstAxion
        {
            get { return _cBandEstAxion; }
            set { _cBandEstAxion = value; }
        }

        public double dCosto
        {
            get { return _dCosto; }
            set { _dCosto = value; }
        }
        public string sNombre
        {
            get { return _sNombre; }
            set { _sNombre = value; }
        }


        public int iIdMateria
        {
            get { return _iIdMateria; }
            set { _iIdMateria = value; }
        }
       #endregion
   }
}
