﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_B_I_T_S.Catalogos_y_Mantenimiento
{
   public class cls_Facturas_DAL
   {

       #region Var Priv

       private int _iIdFactura, _iCantidadEstudiantes, _iComprobantePago, _iIdEstado, _iIdPatrocinador;
        
       private double _dMonto;
        
       private DateTime _dtfechaCobro, _dtfechaPago;

       private char _cBandAxion, _cBandEstAxion;

       

       #endregion


        #region Var Pub

        public char cBandAxion
        {
            get { return _cBandAxion; }
            set { _cBandAxion = value; }
        }

        public char cBandEstAxion
        {
            get { return _cBandEstAxion; }
            set { _cBandEstAxion = value; }
        }

        public DateTime dtfechaCobro
        {
            get { return _dtfechaCobro; }
            set { _dtfechaCobro = value; }
        }

        public DateTime dtfechaPago
        {
            get { return _dtfechaPago; }
            set { _dtfechaPago = value; }
        }

public double dMonto
{
  get { return _dMonto; }
  set { _dMonto = value; }
}
     

public int iIdFactura
{
  get { return _iIdFactura; }
  set { _iIdFactura = value; }
}

public int iCantidadEstudiantes
{
  get { return _iCantidadEstudiantes; }
  set { _iCantidadEstudiantes = value; }
}


public int iComprobantePago
{
  get { return _iComprobantePago; }
  set { _iComprobantePago = value; }
}

public int iIdEstado
{
  get { return _iIdEstado; }
  set { _iIdEstado = value; }
}

public int iIdPatrocinador
{
  get { return _iIdPatrocinador; }
  set { _iIdPatrocinador = value; }
}


#endregion
  

    }
}
