﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_B_I_T_S.Catalogos_y_Mantenimiento
{
   public class cls_Respuestas_DAL
   {

       #region Var Prv
       private int _iIdRespuesta, _iIdPregunta;


       private List<string> _ls_lista = new List<string>();

         private string _sIdUsuario;

        

         private char _cBandAxion, _cBandEstAxion;

         public List<string> ls_Lista
         {
             get { return _ls_lista; }
             set { _ls_lista = value; }
         }
           
       #endregion

       #region Var Pub
         public int iIdRespuesta
         {
             get { return _iIdRespuesta; }
             set { _iIdRespuesta = value; }
         }

         public int iIdPregunta
         {
             get { return _iIdPregunta; }
             set { _iIdPregunta = value; }
         }

         public string sIdUsuario
         {
             get { return _sIdUsuario; }
             set { _sIdUsuario = value; }
         }
         public char cBandAxion
         {
             get { return _cBandAxion; }
             set { _cBandAxion = value; }
         }

         public char cBandEstAxion
         {
             get { return _cBandEstAxion; }
             set { _cBandEstAxion = value; }
         }
       #endregion
   }
}
