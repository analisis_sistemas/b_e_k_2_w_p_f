﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_B_I_T_S.Catalogos_y_Mantenimiento
{
   public class cls_Institucion_DAL
   {
       #region Variables Priv
       private string _sNombre;

       private Int16 _sIDInstitucion;

       private char _cBandAxion, _cBandEstAxion;
       #endregion


       #region Var Pub
       public char cBandAxion
       {
           get { return _cBandAxion; }
           set { _cBandAxion = value; }
       }

       public char cBandEstAxion
       {
           get { return _cBandEstAxion; }
           set { _cBandEstAxion = value; }
       }


       public Int16 sIDInstitucion
       {
           get { return _sIDInstitucion; }
           set { _sIDInstitucion = value; }
       }

       public string sNombre
       {
           get { return _sNombre; }
           set { _sNombre = value; }
       }
       #endregion
   }
}
