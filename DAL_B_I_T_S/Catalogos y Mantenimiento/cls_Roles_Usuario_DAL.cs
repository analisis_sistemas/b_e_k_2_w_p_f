﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_B_I_T_S.Catalogos_y_Mantenimiento
{
  public  class cls_Roles_Usuario_DAL
  {
      #region Var Prv

      private int _iIdRol;
     
    private string _sNombre;

    private char _cBandAxion, _cBandEstAxion;

    

   
      #endregion

      #region Var Pub

    public char cBandAxion
    {
        get { return _cBandAxion; }
        set { _cBandAxion = value; }
    }

    public char cBandEstAxion
    {
        get { return _cBandEstAxion; }
        set { _cBandEstAxion = value; }
    }

    public int iIdRol
    {
        get { return _iIdRol; }
        set { _iIdRol = value; }
    }

    public string sNombre
    {
        get { return _sNombre; }
        set { _sNombre = value; }
    }
      #endregion
  }
}
