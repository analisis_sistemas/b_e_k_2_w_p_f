﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace DAL_B_I_T_S.Catalogos_y_Mantenimiento
{
  public  class cls_Estudiante_DAL
  {

      #region Variables Privadas


      private char _cBandAxion, _cBandEstAxion;

      private int _iIdEstudiante, _iTelefono
          , _iIdCampus, _iIdBeca,
          _iIdEstado, _iGrado,
          _iCantNucleoFam, _iCantTrabajador;


      private decimal _dIngresoMesNeto, _dGastoMensual, _dTotalCosto;

    

     

    

     



      private string _sNombre, _sPrimerApellido
          , _sSegundoApellido, _sDireccion
          , _sNacionalidad, _sIdentificacion
          , _sCorreo, _sNivel_Academico, _sTipoCasa, _sIngresoMesBruto;

      

    
      private DateTime _dFechaNacimiento;

      private char _cGenero;     


      #endregion

      #region Variables Públicas

      public decimal dTotalCosto
      {
          get { return _dTotalCosto; }
          set { _dTotalCosto = value; }
      }

      public string sIngresoMesBruto
      {
          get { return _sIngresoMesBruto; }
          set { _sIngresoMesBruto = value; }
      }

      public decimal dGastoMensual
      {
          get { return _dGastoMensual; }
          set { _dGastoMensual = value; }
      }

      public decimal dIngresoMesNeto
      {
          get { return _dIngresoMesNeto; }
          set { _dIngresoMesNeto = value; }
      }
      public string sTipoCasa
      {
          get { return _sTipoCasa; }
          set { _sTipoCasa = value; }
      }
      public int iCantNucleoFam
      {
          get { return _iCantNucleoFam; }
          set { _iCantNucleoFam = value; }
      }

      public int iCantTrabajador
      {
          get { return _iCantTrabajador; }
          set { _iCantTrabajador = value; }
      }

     
      
      public char cBandAxion
      {
          get { return _cBandAxion; }
          set { _cBandAxion = value; }
      }

      public char cBandEstAxion
      {
          get { return _cBandEstAxion; }
          set { _cBandEstAxion = value; }
      }

      public string sNivel_Academico
      {
          get { return _sNivel_Academico; }
          set { _sNivel_Academico = value; }
      }

      public int iGrado
      {
          get { return _iGrado; }
          set { _iGrado = value; }
      }

      public string sIdentificacion
      {
          get { return _sIdentificacion; }
          set { _sIdentificacion = value; }
      }

      public int iTelefono
      {
          get { return _iTelefono; }
          set { _iTelefono = value; }
      }

      public int iIdEstudiante
      {
          get { return _iIdEstudiante; }
          set { _iIdEstudiante = value; }
      }
 
      public string sNombre
      {
          get { return _sNombre; }
          set { _sNombre = value; }
      }

      public string sPrimerApellido
      {
          get { return _sPrimerApellido; }
          set { _sPrimerApellido = value; }
      }

      public string sSegundoApellido
      {
          get { return _sSegundoApellido; }
          set { _sSegundoApellido = value; }
      }

      public string sDireccion
      {
          get { return _sDireccion; }
          set { _sDireccion = value; }
      }

      public string sNacionalidad
      {
          get { return _sNacionalidad; }
          set { _sNacionalidad = value; }
      }
      public DateTime dFechaNacimiento
      {
          get { return _dFechaNacimiento; }
          set { _dFechaNacimiento = value; }
      }
      public char cGenero
      {
          get { return _cGenero; }
          set { _cGenero = value; }
      }

      public int iIdEstado
      {
          get { return _iIdEstado; }
          set { _iIdEstado = value; }
      }

      public int iIdBeca
      {
          get { return _iIdBeca; }
          set { _iIdBeca = value; }
      }

      public int iIdCampus
      {
          get { return _iIdCampus; }
          set { _iIdCampus = value; }
      }

      public string sCorreo
      {
          get { return _sCorreo; }
          set { _sCorreo = value; }
      } 
      #endregion
  }
}
