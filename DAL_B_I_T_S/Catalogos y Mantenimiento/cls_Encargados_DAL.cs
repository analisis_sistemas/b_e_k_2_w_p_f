﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_B_I_T_S.Catalogos_y_Mantenimiento
{
    public class cls_Encargados_DAL
    {

        #region Var Privados

        private char _cBandAxion, _cBandEstAxion;

        private string _sIdEncargado, _sNombre, _sApellido1, _sApellido2, _sParentesco;

        private int _iIdEstudiante;

        #endregion

        #region Var Pub

        public int iIdEstudiante
        {
            get { return _iIdEstudiante; }
            set { _iIdEstudiante = value; }
        }

        public string sIdEncargado
        {
            get { return _sIdEncargado; }
            set { _sIdEncargado = value; }
        }

        public string sNombre
        {
            get { return _sNombre; }
            set { _sNombre = value; }
        }

        public string sApellido1
        {
            get { return _sApellido1; }
            set { _sApellido1 = value; }
        }

        public string sApellido2
        {
            get { return _sApellido2; }
            set { _sApellido2 = value; }
        }

        public string sParentesco
        {
            get { return _sParentesco; }
            set { _sParentesco = value; }
        }


        public char cBandAxion
        {
            get { return _cBandAxion; }
            set { _cBandAxion = value; }
        }

        public char cBandEstAxion
        {
            get { return _cBandEstAxion; }
            set { _cBandEstAxion = value; }
        }

        #endregion

    }
}
