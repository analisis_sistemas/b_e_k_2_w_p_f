CREATE DATABASE [DB_Beca2]
GO

USE [DB_Beca2]
GO

--DROP DATABASE [DB_Beca2]
GO
/*
Schemas
*/

CREATE SCHEMA [SCH_GENERAL]
GO
CREATE SCHEMA [SCH_USUARIO]
GO


/**************************************************
                     TABLAS
**************************************************/


/*Tabla Institucion*/

CREATE TABLE [SCH_GENERAL].[TBL_INSTITUCION]
(
	 [IDInstitucion] SMALLINT NOT NULL UNIQUE, --PK
	 [Nombre] NVARCHAR (200) NOT NULL

  CONSTRAINT PK_INSTITUCION PRIMARY KEY CLUSTERED 
  (
     [IDInstitucion] ASC
  )
)
GO

/*Tabla Campus*/

CREATE TABLE [SCH_GENERAL].[TBL_CAMPUS]
(
 
	 [idCampus] INT NOT NULL UNIQUE, -- PK
	 [nombre] NVARCHAR (200) NOT NULL,
	 [IDInstitucion] SMALLINT NOT NULL --FK Institucion 

  CONSTRAINT PK_CAMPUS PRIMARY KEY CLUSTERED 
  (
     [idCampus] ASC
  )
)
GO

/* Foreing Key Institucion para Tabla Campus */

ALTER TABLE [SCH_GENERAL].[TBL_CAMPUS]  WITH NOCHECK
   ADD CONSTRAINT [FK_CAMPUS_INSTITUCION] FOREIGN KEY
     ([IDInstitucion])
   REFERENCES [SCH_GENERAL].[TBL_INSTITUCION]
     ([IDInstitucion])
GO

ALTER TABLE [SCH_GENERAL].[TBL_CAMPUS]
	CHECK CONSTRAINT  [FK_CAMPUS_INSTITUCION] 

GO

/*Tabla Empleados*/

CREATE TABLE [SCH_USUARIO].[TBL_EMPLEADOS]
(
	[idEmpleado] INT NOT NULL UNIQUE, --PK
	[cedula] NVARCHAR (30) NOT NULL,
	[nombre] NVARCHAR (30) NOT NULL,
	[primerApellido] NVARCHAR (30) NOT NULL,
	[segundoApellido] NVARCHAR (30) NULL,
	[idUsuario] NVARCHAR (15) NULL,
	[correoElectronico] NVARCHAR (100) NULL,
	[fechaNacimiento] DATETIME NOT NULL,
	[idCampus] INT NOT NULL --FK

   CONSTRAINT PK_EMPLEADOS PRIMARY KEY CLUSTERED
   (
     [idEmpleado] ASC
   )
)
GO

/*Foreign Key Campus para tabla Empleados */

ALTER TABLE [SCH_USUARIO].[TBL_EMPLEADOS] WITH NOCHECK
   ADD CONSTRAINT [FK_EMPLEADOS_CAMPUS] FOREIGN KEY
	([idCampus])
   REFERENCES [SCH_GENERAL].[TBL_CAMPUS]
	([idCampus])
GO

ALTER TABLE [SCH_USUARIO].[TBL_EMPLEADOS]
	CHECK CONSTRAINT [FK_EMPLEADOS_CAMPUS]
GO

/*Tabla Roles de Usuario*/

CREATE TABLE [SCH_USUARIO].[TBL_ROLESDEUSUARIOS]
(
	[idRol] INT NOT NULL UNIQUE,
	[nombre] NVARCHAR (25) NOT NULL

  CONSTRAINT PK_ROLESDEUSUARIOS PRIMARY KEY CLUSTERED 
  (
    [idRol] ASC
  )
)
GO

/*Tabla de Estado para Usuarios*/

CREATE TABLE [SCH_USUARIO].[TBL_ESTADOUSUARIO]
(
	[idEstado] INT NOT NULL UNIQUE, --PK
	[nombre] NVARCHAR (20) NULL

  CONSTRAINT PK_ESTADO PRIMARY KEY CLUSTERED
  (
     [idEstado] ASC
  )
)
GO

/*Tabla Usuarios*/

CREATE TABLE [SCH_USUARIO].[TBL_USUARIOS]
(
	[idUsuario] NVARCHAR (15) NOT NULL UNIQUE, --PK
	[nombre] NVARCHAR (20) NOT NULL,
	[password] NVARCHAR (20) NOT NULL,
	[idRol] INT NULL, --FK
	[idEstado] INT NOT NULL --FK

  CONSTRAINT PK_USUARIOS PRIMARY KEY CLUSTERED 
  (
    [idUsuario] ASC
  )
)
GO

/*Foreign Keys para la tabla Usuarios*/

/*Foreign Key Roles de usuario para tabla Usuarios*/
ALTER TABLE [SCH_USUARIO].[TBL_USUARIOS] WITH NOCHECK
   ADD CONSTRAINT [FK_USUARIOS_ROLESDEUSUARIOS] FOREIGN KEY
	([idRol])
   REFERENCES [SCH_USUARIO].[TBL_ROLESDEUSUARIOS]
	([idRol])
GO
ALTER TABLE [SCH_USUARIO].[TBL_USUARIOS]
	CHECK CONSTRAINT [FK_USUARIOS_ROLESDEUSUARIOS]
GO

/*Foreign Key usuarios para tabla Empleados*/
ALTER TABLE [SCH_USUARIO].[TBL_EMPLEADOS] WITH NOCHECK
   ADD CONSTRAINT [FK_EMPLEADOS_USUARIOS] FOREIGN KEY
	([idUsuario])
   REFERENCES [SCH_USUARIO].[TBL_USUARIOS]
	([idUsuario])
GO

ALTER TABLE [SCH_USUARIO].[TBL_EMPLEADOS]
	CHECK CONSTRAINT [FK_EMPLEADOS_USUARIOS]
GO

/*Foreign key estado para tabla usuarios*/
ALTER TABLE [SCH_USUARIO].[TBL_USUARIOS] WITH NOCHECK
   ADD CONSTRAINT [FK_USUARIOS_ESTADO] FOREIGN KEY
	([idEstado])
   REFERENCES [SCH_USUARIO].[TBL_ESTADOUSUARIO]
	([idEstado])
GO
ALTER TABLE [SCH_USUARIO].[TBL_USUARIOS]
	CHECK CONSTRAINT [FK_USUARIOS_ESTADO]
GO

/*Tabla Preguntas*/

CREATE TABLE [SCH_USUARIO].[TBL_PREGUNTAS]
(
	[idPregunta] INT NOT NULL UNIQUE,
	[Pregunta] NVARCHAR (60) NULL

  CONSTRAINT PK_PREGUNTAS PRIMARY KEY CLUSTERED
  (
    [idPregunta] ASC
  )
)
GO

/*Tabla Respuestas*/

CREATE TABLE [SCH_USUARIO].[TBL_RESPUESTAS]
(
	[idRespuesta] INT NOT NULL UNIQUE identity(1,1), --PK
	[respuesta] NVARCHAR (30) NOT NULL,
	[idUsuario] NVARCHAR (15) NOT NULL,
	[idPregunta] INT NOT NULL --FK

  CONSTRAINT PK_RESPUESTAS PRIMARY KEY CLUSTERED
  (
    [idREspuesta] ASC
  )
)
GO

/*Foreign Key Preguntas para Tabla Respuestas*/

ALTER TABLE [SCH_USUARIO].[TBL_RESPUESTAS] WITH NOCHECK
   ADD CONSTRAINT [FK_RESPUESTAS_PREGUNTAS] FOREIGN KEY
   ([idPregunta])
   REFERENCES [SCH_USUARIO].[TBL_PREGUNTAS]
   ([idPregunta])
GO
ALTER TABLE [SCH_USUARIO].[TBL_RESPUESTAS]
	CHECK CONSTRAINT [FK_RESPUESTAS_PREGUNTAS]
GO

/*Tabla Estado Factura*/

CREATE TABLE [SCH_GENERAL].[TBL_ESTADOFACTURA]
(
	[idEstado] INT NOT NULL UNIQUE,
	[nombre] NVARCHAR(20)

 CONSTRAINT PK_ESTADOFACTURA PRIMARY KEY CLUSTERED
 (
   [idEstado] ASC
 )
)
GO


/*Tabla Estado Patrocinador*/

CREATE TABLE [SCH_GENERAL].[TBL_ESTADOPATROCINADOR]
(
	[idEstado] INT NOT NULL UNIQUE,
	[nombre] NVARCHAR (200) NOT NULL

  CONSTRAINT PK_ESTADOPATROCINADOR PRIMARY KEY CLUSTERED
  (
    [idEstado] ASC
  )
)
GO

/*Tabla Periodo*/

CREATE TABLE [SCH_GENERAL].[TBL_PERIODO]
(
	[idPeriodo] INT NOT NULL UNIQUE,
	[nombre] NVARCHAR (15) NOT NULL

  CONSTRAINT PK_PERIODO PRIMARY KEY CLUSTERED
  (
    [idPeriodo] ASC
  )
)
GO


/*Tabla Patrocinador*/

CREATE TABLE [SCH_GENERAL].[TBL_PATROCINADOR]
(
	[idPatrocinador] INT NOT NULL, --PK
	[nombre] NVARCHAR (30) NOT NULL,
	[montoMaximo] DECIMAL NOT NULL,
	[correoElectronico] NVARCHAR (100) NULL,
	[idPeriodo] INT NOT NULL, --FK
	[idEstado] INT NOT NULL, --FK

  CONSTRAINT PK_PATROCINADOR PRIMARY KEY CLUSTERED
  (
     [idPatrocinador] ASC
  )
)
GO

/*Foreign Key periodo para tabla Patrocinador*/
ALTER TABLE [SCH_GENERAL].[TBL_PATROCINADOR] WITH NOCHECK
   ADD CONSTRAINT [FK_PATROCINADOR_PERIODO] FOREIGN KEY
   ([idPeriodo])
   REFERENCES [SCH_GENERAL].[TBL_PERIODO]
   ([idPeriodo])
GO
ALTER TABLE [SCH_GENERAL].[TBL_PATROCINADOR]
	CHECK CONSTRAINT [FK_PATROCINADOR_PERIODO]
GO

/*Foreign key Estado Patrocinador para tabla Patrocinador*/
ALTER TABLE [SCH_GENERAL].[TBL_PATROCINADOR] WITH NOCHECK
  ADD CONSTRAINT [FK_PATROCINADOR_ESTADOPATROCINADOR] FOREIGN KEY
  ([idEstado])
  REFERENCES [SCH_GENERAL].[TBL_ESTADOPATROCINADOR]
  ([idEstado])
GO
ALTER TABLE [SCH_GENERAL].[TBL_PATROCINADOR]
	CHECK CONSTRAINT [FK_PATROCINADOR_ESTADOPATROCINADOR]
GO

/*Tabla Facturas*/
CREATE TABLE [SCH_GENERAL].[TBL_FACTURAS]
(
	[idFactura] INT NOT NULL UNIQUE,
	[cantidadEstudiantes] INT NOT NULL,
	[monto] DECIMAL NOT NULL,
	[fechaCobro] DATETIME NOT NULL,
	[fechaPago] DATETIME NUll,
	[comprobantePago] INT NULL,
	[idEstado] INT NULL,
	[idPatrocinador] INT NOT NULL,
	
  CONSTRAINT PK_FACTURAS PRIMARY KEY CLUSTERED
  (
    [idFactura] ASC
  )
)
GO

/*Foreign keys para la tabla Facturas*/
/*Foreign key estado para tabla facturas*/

ALTER TABLE [SCH_GENERAL].[TBL_FACTURAS] WITH NOCHECK
  ADD CONSTRAINT [FK_FACTURAS_ESTADOFACTURA] FOREIGN KEY 
	([idEstado])
  REFERENCES [SCH_GENERAL].[TBL_ESTADOFACTURA]
	([idEstado])
GO
ALTER TABLE [SCH_GENERAL].[TBL_FACTURAS]
	CHECK CONSTRAINT [FK_FACTURAS_ESTADOFACTURA]

/*Foreign key patrocinador para tabla facturas*/


ALTER TABLE [SCH_GENERAL].[TBL_FACTURAS] WITH NOCHECK
  ADD CONSTRAINT [FK_FACTURAS_PATROCINADOR] FOREIGN KEY 
	([idPatrocinador])
  REFERENCES [SCH_GENERAL].[TBL_PATROCINADOR]
	([idPatrocinador])
GO
ALTER TABLE [SCH_GENERAL].[TBL_FACTURAS]
	CHECK CONSTRAINT [FK_FACTURAS_PATROCINADOR]
GO

/*Tabla Estado Becas*/

CREATE TABLE [SCH_GENERAL].[TBL_ESTADOBECAS]
(
	[idEstado] INT NOT NULL UNIQUE,
	[nombre] nvarchar(20)

  CONSTRAINT PK_ESTADOBECAS PRIMARY KEY CLUSTERED
  (
    [idEstado] ASC
  )
)
GO

/*Tabla Categoria*/ 

CREATE TABLE [SCH_GENERAL].[TBL_CATEGORIA]
(
	[idCategoria] INT NOT NULL UNIQUE,
	[nombre] NVARCHAR (20) NOT NULL,
	[monto] INT NOT NULL

  CONSTRAINT PK_CATEGORIA PRIMARY KEY CLUSTERED
  (
    [idCategoria] ASC
  )
)
GO

/*Tabla SubCategoria*/

CREATE TABLE [SCH_GENERAL].[TBL_SUBCATEGORIA]
(
	[idSubcategoria] INT NOT NULL UNIQUE,
	[nombre] NVARCHAR (20) NOT NULL,
	[porcentaje] DECIMAL NOT NULL

  CONSTRAINT PK_SUBCATEGORIA PRIMARY KEY CLUSTERED
  (
    [idSubcategoria] ASC
  )
)
GO

/*Tabla Becas*/

CREATE TABLE [SCH_GENERAL].[TBL_BECAS]
(
	[idBeca] INT NOT NULL UNIQUE,
	[porcentajeDescuento] DECIMAL NOT NULL,
	[nombre] NVARCHAR(50) NOT NULL,
	[notaMinima] DECIMAL NULL,
	[idPatrocinador] INT NOT NULL,
	[idCategoria] INT NOT NULL,
	[idSubcategoria] INT NOT NULL,
	[idEstado] INT NOT NULL,

  CONSTRAINT PK_BECAS PRIMARY KEY CLUSTERED
  (
    [idBeca] ASC
  )
  )
GO

/*Foreign keys para la tabla becas*/

/*Foregin key categoria para becas*/

ALTER TABLE [SCH_GENERAL].[TBL_BECAS] WITH NOCHECK
   ADD CONSTRAINT [FK_BECAS_CATEGORIA] FOREIGN KEY
   ([idCategoria])
   REFERENCES [SCH_GENERAL].[TBL_CATEGORIA]
   ([idCategoria])
GO
ALTER TABLE [SCH_GENERAL].[TBL_BECAS]
	CHECK CONSTRAINT [FK_BECAS_CATEGORIA]
GO

/*Foreign key Sub categoria para la tabla becas*/
ALTER TABLE [SCH_GENERAL].[TBL_BECAS] WITH NOCHECK
   ADD CONSTRAINT [FK_BECAS_SUBCATEGORIA] FOREIGN KEY
   ([idSubcategoria])
   REFERENCES [SCH_GENERAL].[TBL_SUBCATEGORIA]
   ([idSubcategoria])
GO
ALTER TABLE [SCH_GENERAL].[TBL_BECAS]
	CHECK CONSTRAINT [FK_BECAS_SUBCATEGORIA]
GO

/*Foreign key Patrocinador para la tabla becas*/
ALTER TABLE [SCH_GENERAL].[TBL_BECAS] WITH NOCHECK
   ADD CONSTRAINT [FK_BECAS_PATROCINADOR] FOREIGN KEY
   ([idPatrocinador])
   REFERENCES [SCH_GENERAL].[TBL_PATROCINADOR]
   ([idPatrocinador])
GO
ALTER TABLE [SCH_GENERAL].[TBL_BECAS]
	CHECK CONSTRAINT [FK_BECAS_PATROCINADOR]
GO

/*Foreign key Estado para la tabla becas*/

ALTER TABLE [SCH_GENERAL].[TBL_BECAS] WITH NOCHECK
   ADD CONSTRAINT [FK_BECAS_ESTADOBECAS] FOREIGN KEY
   ([idEstado])
   REFERENCES  [SCH_GENERAL].[TBL_ESTADOBECAS]
   ([idEstado])
GO


/*Tabla Estado para expediente estudiante*/

CREATE TABLE [SCH_GENERAL].[TBL_ESTADOEXPEDIENTE]
(
	[idEstado] INT NOT NULL UNIQUE,
	[nombre] NVARCHAR (100)
 
  CONSTRAINT PK_ESTADOEXPEDIENTE PRIMARY KEY CLUSTERED
  (
    [idEstado] ASC
  )
)
GO



/*Tabla Expediente Estudiantes */

CREATE TABLE [SCH_GENERAL].[TBL_EXPEDIENTE_ESTUDIANTES]
(
	[idEstudiante] INT NOT NULL UNIQUE, --PK
	[nombre] NVARCHAR (30) NOT NULL,
	[primerApellido] NVARCHAR (30) NOT NULL,
	[segundoApellido] NVARCHAR (30) NULL,
	[identificacion] NVARCHAR (30) NOT NULL,
	[direccion] NVARCHAR (250) NULL,
	[telefono] INT NOT NULL,
	[fechaNacimiento] DATETIME NOT NULL,
	[genero] CHAR(1) NULL,
	[nacionalidad] NVARCHAR (30) NOT NULL,
	[correoElectronico] NVARCHAR (100) NULL,
	[nivel_academico] NVARCHAR (15) NOT NULL,
	[grado] INT NOT NULL,
	[total_costo] DECIMAL NOT NULL, 
	[idCampus] INT NOT NULL, --FK
	[idBeca] INT NULL, --FK
	[idEstado] INT NOT NULL, --FK
	[condicionCasa] NVARCHAR(15) NOT NULL,
    [personasNucleo] INT NOT NULL,
    [cantidadPersonasLaboran] INT NOT NULL,
    [rangoSalario] NVARCHAR(25) NOT NULL,
    [ingresos] DECIMAL NOT NULL,
    [gastos] DECIMAL NOT NULL


  CONSTRAINT PK_EXPEDIENTE_ESTUDIANTES PRIMARY KEY CLUSTERED
  (
    [idEstudiante] ASC
  )
)
GO


/*Foreign Keys para tabla Expediente estudiantes*/

/*Foreign key campus para tabla expediente estudiante*/

ALTER TABLE [SCH_GENERAL].[TBL_EXPEDIENTE_ESTUDIANTES] WITH NOCHECK
   ADD CONSTRAINT [FK_EXPEDIENTE_ESTUDIANTES_CAMPUS] FOREIGN KEY
	([idCampus])
   REFERENCES [SCH_GENERAL].[TBL_Campus]
	([idCampus])
GO
ALTER TABLE [SCH_GENERAL].[TBL_EXPEDIENTE_ESTUDIANTES]
	CHECK CONSTRAINT [FK_EXPEDIENTE_ESTUDIANTES_CAMPUS]
GO

/*Foreign key Estado para tabla Expediente estudiante*/

ALTER TABLE [SCH_GENERAL].[TBL_EXPEDIENTE_ESTUDIANTES] WITH NOCHECK
   ADD CONSTRAINT [FK_EXPEDIENTE_ESTUDIANTES_ESTADOESTUDIANTE] FOREIGN KEY
   ([idEstado])
   REFERENCES [SCH_GENERAL].[TBL_ESTADOEXPEDIENTE]
   ([idEstado])
GO
ALTER TABLE [SCH_GENERAL].[TBL_EXPEDIENTE_ESTUDIANTES]
	CHECK CONSTRAINT [FK_EXPEDIENTE_ESTUDIANTES_ESTADOESTUDIANTE]
GO


/*Foreign key Becas para tabla expediente*/

ALTER TABLE [SCH_GENERAL].[TBL_EXPEDIENTE_ESTUDIANTES] WITH NOCHECK
   ADD CONSTRAINT [FK_EXPEDIENTE_ESTUDIANTES_BECAS] FOREIGN KEY
   ([idBeca])
   REFERENCES [SCH_GENERAL].[TBL_BECAS]
   ([idBeca])
GO
ALTER TABLE [SCH_GENERAL].[TBL_EXPEDIENTE_ESTUDIANTES]
	CHECK CONSTRAINT [FK_EXPEDIENTE_ESTUDIANTES_BECAS]
GO


/*Tabla Encargados*/

CREATE TABLE [SCH_GENERAL].[TBL_ENCARGADOS]
(
	[idEncargado] NVARCHAR (50) NOT NULL UNIQUE,
	[nombre] NVARCHAR (30) NOT NULL,
	[primerApellido] NVARCHAR (30) NOT NULL,
	[segundoApellido] NVARCHAR (30) NULL,
	[identificacion] NVARCHAR (30) NOT NULL,
	[idEstudiante] INT NOT NULL,
	[parentesco] NVARCHAR (30) NOT NULL

  CONSTRAINT PK_ENCARGADOS PRIMARY KEY CLUSTERED
  ( 
    [idEncargado] ASC
  )
)
GO

/*Foreign key expediente para la tabla encargados*/

ALTER TABLE [SCH_GENERAL].[TBL_ENCARGADOS] WITH NOCHECK
  ADD CONSTRAINT [FK_ENCARGADOS_EXPEDIENTE_ESTUDIANTE] FOREIGN KEY
  ([idEstudiante])
  REFERENCES [SCH_GENERAL].[TBL_EXPEDIENTE_ESTUDIANTES]
  ([idEstudiante])

GO
ALTER TABLE [SCH_GENERAL].[TBL_ENCARGADOS]
	CHECK CONSTRAINT [FK_ENCARGADOS_EXPEDIENTE_ESTUDIANTE]

GO

/* Tabla Estado Desembolsos*/

CREATE TABLE [SCH_GENERAL].[TBL_ESTADO_DESEMBOLSOS]
(
	[idEstado] INT NOT NULL UNIQUE,
	[nombre] NVARCHAR(20) NOT NULL

	CONSTRAINT PK_ESTADO_DESEMBOLSO PRIMARY KEY CLUSTERED
	(
		[idEstado] ASC
	)
)

GO

/*Tabla Desembolsos*/

CREATE TABLE [SCH_GENERAL].[TBL_DESEMBOLSOS]
(
	[idDesembolso] INT NOT NULL UNIQUE,
	[idEstudiante] INT NOT NULL, --FK
	[montoDesembolsado] DECIMAL NULL,
	[montoSubvencion] DECIMAL NULL,
	[montoEstudiante] DECIMAL NULL,
	[fechaDesembolso] DATETIME NULL,
	[idEstado] INT NOT NULL, --FK
	[idFactura] INT NOT NULL, --FK

  CONSTRAINT PK_DESEMBOLSO PRIMARY KEY CLUSTERED
  (
    [idDesembolso] ASC
  )
)
GO

/*Foreign key Expediente estudiante para tabla desembolsos*/

ALTER TABLE [SCH_GENERAL].[TBL_DESEMBOLSOS] WITH NOCHECK
  ADD CONSTRAINT [FK_DESEMBOLSOS_EXPEDIENTE_ESTUDIANTE] FOREIGN KEY
  ([idEstudiante])
  REFERENCES [SCH_GENERAL].[TBL_EXPEDIENTE_ESTUDIANTES]
  ([idEstudiante])

GO
ALTER TABLE [SCH_GENERAL].[TBL_DESEMBOLSOS]
	CHECK CONSTRAINT [FK_DESEMBOLSOS_EXPEDIENTE_ESTUDIANTE]
GO

/*Foreign key Estado para tabla desembolsos*/

ALTER TABLE [SCH_GENERAL].[TBL_DESEMBOLSOS] WITH NOCHECK
  ADD CONSTRAINT [FK_DESEMBOLSOS_ESTADO_DESEMBOLSO] FOREIGN KEY
  ([idEstado])
  REFERENCES  [SCH_GENERAL].[TBL_ESTADO_DESEMBOLSOS]
  ([idEstado])

GO
ALTER TABLE [SCH_GENERAL].[TBL_DESEMBOLSOS]
	CHECK CONSTRAINT [FK_DESEMBOLSOS_ESTADO_DESEMBOLSO]
GO

/*Foreign key Factura para tabla desembolsos*/

ALTER TABLE [SCH_GENERAL].[TBL_DESEMBOLSOS] WITH NOCHECK
  ADD CONSTRAINT [FK_DESEMBOLSOS_FACTURA] FOREIGN KEY
  ([idFactura])
  REFERENCES  [SCH_GENERAL].[TBL_FACTURAS]
  ([idFactura])

GO
ALTER TABLE [SCH_GENERAL].[TBL_DESEMBOLSOS]
	CHECK CONSTRAINT [FK_DESEMBOLSOS_FACTURA]
GO

--*****************************************************************************************************--


/*Tabla Estado Cursos*/

CREATE TABLE [SCH_GENERAL].[TBL_ESTADOCURSO]
(
	[idEstado] INT NOT NULL UNIQUE,
	[nombre] nvarchar(20)

 CONSTRAINT PK_ESTADOCURSO PRIMARY KEY CLUSTERED
 (
   [idEstado] ASC
 )
)
GO

/*Tabla Materias*/
 
CREATE TABLE [SCH_GENERAL].[TBL_MATERIAS]
(
    [idMateria] INT NOT NULL UNIQUE,
    [nombre] NVARCHAR (20) NOT NULL,
    [costo] DECIMAL NOT NULL
 
  CONSTRAINT PK_MATERIAS PRIMARY KEY CLUSTERED
  (
    [idMateria] ASC
  )
)
 
/*Foreign key Expediente estudiante para tabla materias*/
 
--ALTER TABLE [SCH_GENERAL].[TBL_MATERIAS] WITH NOCHECK
--  ADD CONSTRAINT [FK_MATERIAS_EXPEDIENTE_ESTUDIANTE] FOREIGN KEY
--    ([idEstudiante])
--  REFERENCES [SCH_GENERAL].[TBL_EXPEDIENTE_ESTUDIANTES]
--    ([idEstudiante])
--GO
--ALTER TABLE [SCH_GENERAL].[TBL_MATERIAS]
--    CHECK CONSTRAINT [FK_MATERIAS_EXPEDIENTE_ESTUDIANTE]
--GO
 
 
/*Tabla Cursos*/
 
CREATE TABLE [SCH_GENERAL].[TBL_CURSOS]
(
    [idCurso] INT NOT NULL UNIQUE, --PK
    [idMateria] INT NOT NULL, --FK
    [idEstado] INT NOT NULL, --FK
    [idEstudiante] INT NOT NULL, --FK
    [montoMatriculado] Decimal NOT NULL,
    [fechaMatricula] DATETIME NULL,
    [idFactura] INT NULL, --FK
    [nota] DECIMAL NOT NULL 
 
  CONSTRAINT PK_CURSOS PRIMARY KEY CLUSTERED
  (
    [idCurso] ASC
  )
)
GO
 
/*Foreign Key materias para tabla cursos*/
 
ALTER TABLE [SCH_GENERAL].[TBL_CURSOS] WITH NOCHECK
  ADD CONSTRAINT [FK_CURSOS_MATERIAS] FOREIGN KEY
    ([idMateria])
  REFERENCES [SCH_GENERAL].[TBL_MATERIAS]
    ([idMateria])
GO
ALTER TABLE [SCH_GENERAL].[TBL_CURSOS]
    CHECK CONSTRAINT [FK_CURSOS_MATERIAS]
GO 
 
/*Foreign Key Factura para tabla cursos*/
 
ALTER TABLE [SCH_GENERAL].[TBL_CURSOS] WITH NOCHECK
  ADD CONSTRAINT [FK_CURSOS_FACTURAS] FOREIGN KEY
    ([idFactura])
  REFERENCES [SCH_GENERAL].[TBL_FACTURAS]
    ([idFactura])
GO
ALTER TABLE [SCH_GENERAL].[TBL_CURSOS]
    CHECK CONSTRAINT [FK_CURSOS_FACTURAS]
GO
 
/*Foreign key Expediente estudiante para tabla cursos*/
 
ALTER TABLE [SCH_GENERAL].[TBL_CURSOS] WITH NOCHECK
  ADD CONSTRAINT [FK_CURSOS_EXPEDIENTE_ESTUDIANTE] FOREIGN KEY
    ([idEstudiante])
  REFERENCES [SCH_GENERAL].[TBL_EXPEDIENTE_ESTUDIANTES]
    ([idEstudiante])
GO
ALTER TABLE [SCH_GENERAL].[TBL_CURSOS]
    CHECK CONSTRAINT [FK_CURSOS_EXPEDIENTE_ESTUDIANTE]
GO
 
/*Foreign key estado para tabla cursos*/
 
ALTER TABLE [SCH_GENERAL].[TBL_CURSOS] WITH NOCHECK
  ADD CONSTRAINT [FK_CURSOS_ESTADOCURSO] FOREIGN KEY
    ([idEstado])
  REFERENCES [SCH_GENERAL].[TBL_ESTADOCURSO]
    ([idEstado])
 
GO
ALTER TABLE [SCH_GENERAL].[TBL_CURSOS]
    CHECK CONSTRAINT [FK_CURSOS_ESTADOCURSO]
GO
------- SP'S --------------

----------------------------------------------------------------------
--------------------		  Becas     ------------------------------
----------------------------------------------------------------------

----------- INSERTAR ---------
CREATE PROCEDURE [dbo].[SP_Becas_Insertar]
(
	   @idBeca int,
       @porcentajeDescuento decimal(18,0),
       @nombre nvarchar(50),
       @notaMinima decimal(18,0),
       @idPatrocinador int,
       @idCategoria int,
	   @idSubcategoria int,
       @idEstado int
)

AS
BEGIN

INSERT INTO SCH_GENERAL.TBL_BECAS
           (idBeca,
			porcentajeDescuento,
			nombre,
			notaMinima,
			idPatrocinador,
			idCategoria,
			idSubcategoria,
			idEstado)

     VALUES
           (@idBeca,
			@porcentajeDescuento ,
			@nombre,
			@notaMinima,
			@idPatrocinador,
			@idCategoria,
			@idSubcategoria,
			@idEstado)

END

GO

----------- Modificar ---------

CREATE PROCEDURE [dbo].[SP_Becas_Modificar]
(
	   @idBeca int,
       @porcentajeDescuento decimal(18,0),
       @nombre nvarchar(50),
       @notaMinima decimal(18,0),
       @idPatrocinador int,
       @idCategoria int,
	   @idSubcategoria int,
       @idEstado int
)

AS
BEGIN

UPDATE SCH_GENERAL.TBL_BECAS
   SET idBeca = @idBeca
      ,porcentajeDescuento = @porcentajeDescuento
      ,nombre = @nombre
      ,notaMinima = @notaMinima
      ,idPatrocinador = @idPatrocinador
	  ,idCategoria = @idCategoria
	  ,idSubcategoria = @idSubcategoria
      ,idEstado = @idEstado
 WHERE idBeca = @idBeca

END

GO

----------Listar-----------
create procedure SP_Becas_Listar

as begin

SELECT idBeca
      ,porcentajeDescuento
      ,nombre
      ,notaMinima
      ,idPatrocinador
      ,idCategoria
      ,idSubcategoria
      ,idEstado
  FROM [SCH_GENERAL].[TBL_BECAS]

end

go
----------------------------------------------------------------------
--------------------		  Campus     ------------------------------
----------------------------------------------------------------------

----------- LISTAR -----------

CREATE PROCEDURE [dbo].[SP_Campus_Listar]

AS
BEGIN

SELECT idCampus,
       nombre,
       IDInstitucion
       

  FROM [SCH_GENERAL].[TBL_CAMPUS]

END

GO

----------- INSERTAR ---------

CREATE PROCEDURE [dbo].[SP_Campus_Insertar]
(
       @idCampus int,
       @nombre nvarchar(200),
       @IDInstitucion SMALLINT
       
)

AS
BEGIN

INSERT INTO [SCH_GENERAL].[TBL_CAMPUS]
           (idCampus,
			nombre,
			IDInstitucion)

     VALUES
           (@idCampus,
       		@nombre,
      		@IDInstitucion)

END

GO

----------- Modificar ---------

CREATE PROCEDURE [dbo].[SP_Campus_Modificar]

(
	       @idCampus int,
           @nombre nvarchar(200),
           @IDInstitucion SMALLINT
)

AS
BEGIN

UPDATE [SCH_GENERAL].[TBL_CAMPUS]
   SET  idCampus = @idCampus 
      , nombre = @nombre 
      , IDInstitucion = @IDInstitucion 
      
 WHERE idCampus=@idCampus

END
GO
-------- Eliminar ---------

CREATE PROCEDURE [dbo].[SP_Campus_Eliminar]
(
	@idCampus int
)

AS
BEGIN

DELETE FROM [SCH_GENERAL].[TBL_CAMPUS]
      WHERE idCampus = @idCampus 
END


GO

------- Filtrar ----------

CREATE PROCEDURE [dbo].[SP_Campus_Filtrar_X_ID]
(
	@idCampus int
)

AS
BEGIN

SELECT idCampus,
	nombre,
	IDInstitucion

  FROM [SCH_GENERAL].[TBL_CAMPUS]
  WHERE idCampus  = @idCampus

END

GO
----------------------------------------------------------------------
--------------------		  Categoria     ------------------------------
----------------------------------------------------------------------

----------- LISTAR -----------

CREATE PROCEDURE [dbo].[SP_Categoria_Listar]

AS
BEGIN

SELECT [idCategoria],
       [nombre],
	   [monto]  

  FROM [SCH_GENERAL].[TBL_CATEGORIA]

END

GO

----------- INSERTAR ---------

CREATE PROCEDURE [dbo].[SP_Categoria_Insertar]
(
       @idCategoria int,
       @nombre nvarchar(20),
	   @monto int       
)

AS
BEGIN

INSERT INTO [SCH_GENERAL].[TBL_CATEGORIA]
           (idCategoria,
			nombre, 
			monto)

     VALUES
           (@idCategoria,
       		@nombre, 
			@monto)

END

GO

----------- Modificar ---------

CREATE PROCEDURE [dbo].[SP_Categoria_Modificar]
(
	       @idCategoria int,
           @nombre nvarchar(20),
		   @monto int  
)

AS
BEGIN

UPDATE [SCH_GENERAL].[TBL_CATEGORIA]
   SET  idCategoria = @idCategoria 
      , nombre = @nombre 
	  , monto = @monto
      
 WHERE idCategoria = @idCategoria 

END

GO

-------- Eliminar ---------

CREATE PROCEDURE [dbo].[SP_Categoria_Eliminar]
(
	  @idCategoria int
)

AS
BEGIN

DELETE FROM [SCH_GENERAL].[TBL_CATEGORIA]
      WHERE idCategoria = @idCategoria 
END


GO

------- Filtrar ----------

CREATE PROCEDURE [dbo].[SP_Categoria_Filtrar_X_ID]
(
	@idCategoria int
)

AS
BEGIN

SELECT idCategoria,
	nombre, monto

  FROM [SCH_GENERAL].[TBL_CATEGORIA]
  WHERE idCategoria = @idCategoria

END

GO
----------------------------------------------------------------------
--------------------		  Cursos     ------------------------------
----------------------------------------------------------------------

----------- LISTAR -----------
 
CREATE PROCEDURE [dbo].[SP_Cursos_Listar]
 
AS
BEGIN
 
SELECT  [idCurso] ,
    [idMateria],
    [idEstado],
    [idEstudiante],
    [montoMatriculado] ,
    [fechaMatricula],
    [idFactura] ,
    [nota]
        
 
  FROM [SCH_GENERAL].[TBL_CURSOS]
 
END
 
GO
 
----------- INSERTAR ---------
 
CREATE PROCEDURE [dbo].[SP_Cursos_Insertar]
(
       @idCurso INT ,
    @idMateria INT,
    @idEstado INT,
    @idEstudiante INT,
    @montoMatriculado  DECIMAL,
    @fechaMatricula DATETIME,
    @idFactura INT,
    @nota DECIMAL,
    @promedio decimal
        
)
 
AS
BEGIN
 
INSERT INTO [SCH_GENERAL].[TBL_CURSOS]
           ( [idCurso] ,
         [idMateria] ,
         [idEstado] ,
         [idEstudiante] ,
         [montoMatriculado] ,
         [fechaMatricula] ,
         [idFactura] ,
         [nota] )
 
     VALUES
           (@idCurso ,
                @idMateria,
                @idEstado,
                @idEstudiante,
                @montoMatriculado,
                @fechaMatricula,
                @idFactura ,
                @nota)
 
END
 
GO
 
----------- Modificar ---------
 
CREATE PROCEDURE [dbo].[SP_Cursos_Modificar]
(
       @idCurso INT ,
    @idMateria INT,
    @idEstado INT,
    @idEstudiante INT,
    @montoMatriculado DECIMAL,
    @fechaMatricula DATETIME,
    @idFactura INT,
    @nota DECIMAL
)
 
AS
BEGIN
 
UPDATE [SCH_GENERAL].[TBL_CURSOS]
   SET  [idCurso] = @idCurso 
      , [idMateria] = @idMateria
      , [idEstado] = @idEstado
      , [idEstudiante] = @idEstudiante
      , [montoMatriculado]  = @montoMatriculado 
      , [fechaMatricula] = @fechaMatricula
      , [idFactura] = @idFactura
      , [nota]= @nota
      
       
       
 WHERE [idCurso] = @idCurso 
 
END
 
GO
 
-------- Eliminar ---------
 
CREATE PROCEDURE [dbo].[SP_Cursos_Eliminar]
(
    @idCurso INT
)
 
AS
BEGIN
 
DELETE FROM [SCH_GENERAL].[TBL_CURSOS]
      WHERE [idCurso] = @idCurso 
END
 
 
GO
 
-------- Filtrar(RQ 23 - Control de notas) --------
 
--CREATE PROCEDURE [dbo].[SP_Cursos_Filtrar_X_ID]
--(
--    @idEstudiante INT
--)
--AS
--BEGIN
--    SELECT idEstado, idEstudiante, nombre, nota, promedio FROM [SCH_GENERAL].[TBL_CURSOS] 
--    WHERE idEstudiante = @idEstudiante
--END
 
--GO
----------------------------------------------------------------------
--------------------		  Desembolsos     ------------------------------
----------------------------------------------------------------------

----------- LISTAR -----------

CREATE PROCEDURE [dbo].[SP_Desembolsos_Listar]

AS
BEGIN

SELECT  [idDesembolso] ,
	[idEstudiante] ,
	[montoDesembolsado] ,
	[montoSubvencion],
	[montoEstudiante]
	[fechaDesembolso] ,
	[idEstado] ,
	[idFactura] 
       

  FROM [SCH_GENERAL].[TBL_DESEMBOLSOS]

END

GO
-------- Filtrar --------

CREATE PROCEDURE [dbo].[SP_Desembolsos_Filtrar]
(
	@id_Desembolso INT
)
AS
BEGIN
	SELECT * FROM [SCH_GENERAL].[TBL_DESEMBOLSOS] WHERE idDesembolso = @id_Desembolso
END

GO
----------- INSERTAR ---------

CREATE PROCEDURE [dbo].[SP_Desembolsos_Insertar]
(
       @idDesembolso INT ,
	@idEstudiante INT ,
	@montoDesembolsado DECIMAL ,
	@montoSubvencion DECIMAL ,
	@montoEstudiante DECIMAL ,
	@fechaDesembolso DATETIME ,
	@idEstado INT ,
	@idFactura INT 
       
)

AS
BEGIN

INSERT INTO [SCH_GENERAL].[TBL_DESEMBOLSOS]
           ( [idDesembolso] ,
	[idEstudiante] ,
	[montoDesembolsado] ,
	[montoSubvencion],
	montoEstudiante,
	[fechaDesembolso] ,
	[idEstado] ,
	[idFactura] )

     VALUES
           ( @idDesembolso,
	@idEstudiante ,
	@montoDesembolsado ,
	@montoSubvencion,
	@montoEstudiante,
	@fechaDesembolso ,
	@idEstado ,
	@idFactura  )

END

GO

----------- Modificar ---------

CREATE PROCEDURE [dbo].[SP_Desembolsos_Modificar]
(
	   @idDesembolso INT ,
	@idEstudiante INT ,
	@montoDesembolsado DECIMAL ,
	@montoSubvencion DECIMAL ,
	@montoEstudiante DECIMAL ,
	@fechaDesembolso DATETIME ,
	@idEstado INT ,
	@idFactura INT 
)

AS
BEGIN

UPDATE [SCH_GENERAL].[TBL_DESEMBOLSOS]
   SET  [idDesembolso] = @idDesembolso 
      , [idEstudiante] = @idEstudiante 
      , [montoDesembolsado] = @montoDesembolsado 
	  , [montoSubvencion] = @montoSubvencion 
	  , [montoEstudiante] = @montoEstudiante
      , [fechaDesembolso] = @fechaDesembolso 
      , [idEstado] = @idEstado 
      , [idFactura] = @idFactura 
      
      
      
 WHERE [idDesembolso] = @idDesembolso

END

GO

-------- Eliminar ---------

CREATE PROCEDURE [dbo].[SP_Desembolsos_Eliminar]
(
	 @idDesembolso INT 
)

AS
BEGIN

DELETE FROM [SCH_GENERAL].[TBL_DESEMBOLSOS]
      WHERE idDesembolso = @idDesembolso  
END


GO
----------------------------------------------------------------------
--------------------		  Empleados     ------------------------------
----------------------------------------------------------------------

----------- LISTAR -----------

CREATE PROCEDURE [dbo].[SP_Empleados_Listar]

AS
BEGIN

SELECT [idEmpleado]  ,
	[cedula] ,
	[nombre] ,
	[primerApellido] ,
	[segundoApellido] ,
	[idUsuario] ,
	[correoElectronico] ,
	[fechaNacimiento] ,
	[idCampus] 
       

  FROM [SCH_USUARIO].[TBL_EMPLEADOS]

END

GO

----------- INSERTAR ---------

CREATE PROCEDURE [dbo].[SP_Empleados_Insertar]
(
       @idEmpleado INT ,
	@cedula NVARCHAR (30) ,
	@nombre NVARCHAR (30) ,
	@primerApellido NVARCHAR (30) ,
	@segundoApellido NVARCHAR (30) ,
	@idUsuario NVARCHAR (15) ,
	@correoElectronico NVARCHAR (50) ,
	@fechaNacimiento DATETIME ,
	@idCampus INT 
       
)

AS
BEGIN

INSERT INTO [SCH_USUARIO].[TBL_EMPLEADOS]
           (            [idEmpleado] ,
	[cedula] ,
	[nombre] ,
	[primerApellido] ,
	[segundoApellido] ,
	[idUsuario] ,
	[correoElectronico] ,
	[fechaNacimiento] ,
	[idCampus] )

     VALUES
           (	@idEmpleado  ,
	@cedula  ,
	@nombre  ,
	@primerApellido  ,
	@segundoApellido  ,
	@idUsuario  ,
	@correoElectronico  ,
	@fechaNacimiento  ,
	@idCampus )

END

GO

----------- Modificar ---------

CREATE PROCEDURE [dbo].[SP_Empleados_Modificar]
(
	   @idEmpleado INT ,
	@cedula NVARCHAR (30) ,
	@nombre NVARCHAR (30) ,
	@primerApellido NVARCHAR (30) ,
	@segundoApellido NVARCHAR (30) ,
	@idUsuario NVARCHAR (15) ,
	@correoElectronico NVARCHAR (50) ,
	@fechaNacimiento DATETIME ,
	@idCampus INT  
)

AS
BEGIN

UPDATE [SCH_USUARIO].[TBL_EMPLEADOS]
   SET  [idEmpleado] = @idEmpleado
      , [cedula] = @cedula
      , [nombre] = @nombre
      , [primerApellido] = @primerApellido
      , [segundoApellido] = @segundoApellido
      , [idUsuario] = @idUsuario
      , [correoElectronico] = @correoElectronico
      , [fechaNacimiento] = @fechaNacimiento
      , [idCampus] = @idCampus
       
 WHERE [idEmpleado] = @idEmpleado

END

GO

-------- Filtrar --------

CREATE PROCEDURE [dbo.SP_Empleados_Filtrar_X_ID]
(
	@idEmpleado INT
)
AS
BEGIN
	SELECT * FROM [SCH_USUARIO].[TBL_EMPLEADOS] WHERE idEmpleado = @idEmpleado
END

GO
----------------------------------------------------------------------
--------------------		  Encargados     ------------------------------
----------------------------------------------------------------------

----------- LISTAR -----------

CREATE PROCEDURE [dbo].[SP_Encargados_Listar]

AS
BEGIN

SELECT  [idEncargado] ,
	[nombre] ,
	[primerApellido] ,
	[segundoApellido] ,
	[identificacion] ,
	[idEstudiante] ,
	[parentesco]
       

  FROM [SCH_GENERAL].[TBL_ENCARGADOS]

END

GO

----------- INSERTAR ---------

CREATE PROCEDURE [dbo].[SP_Encargados_Insertar]
(
        @idEncargado NVARCHAR (50) ,
	@nombre NVARCHAR (400) ,
	@primerApellido NVARCHAR (30) ,
	@segundoApellido NVARCHAR (30) ,
	@identificacion NVARCHAR (30) ,
	@idEstudiante INT ,
	@parentesco NVARCHAR (50) 
       
)

AS
BEGIN

INSERT INTO [SCH_GENERAL].[TBL_ENCARGADOS]
           (  [idEncargado] ,
	[nombre] ,
	[primerApellido],
	[segundoApellido] ,
	[identificacion] ,
	[idEstudiante] ,
	[parentesco] )

     VALUES
           (	@idEncargado ,
	@nombre  ,
	@primerApellido ,
	@segundoApellido ,
	@identificacion ,
	@idEstudiante  ,
	@parentesco  )

END

GO

----------- Modificar ---------

CREATE PROCEDURE [dbo].[SP_Encargados_Modificar]
(
	   @idEncargado NVARCHAR (50) ,
	@nombre NVARCHAR (400) ,
	@primerApellido NVARCHAR (30) ,
	@segundoApellido NVARCHAR (30) ,
	@identificacion NVARCHAR (30) ,
	@idEstudiante INT ,
	@parentesco NVARCHAR (50)  
)

AS
BEGIN

UPDATE [SCH_GENERAL].[TBL_ENCARGADOS]
   SET  [idEncargado] = @idEncargado 
      , [nombre] = @nombre 
	  ,	[primerApellido] = @primerApellido
	  ,	[segundoApellido] = @segundoApellido
	  ,	[identificacion] = @identificacion
      , [idEstudiante] = @idEstudiante 
      , [parentesco] = @parentesco 
      
       
 WHERE [idEncargado] = @idEncargado 

END

GO

-------- Eliminar ---------

CREATE PROCEDURE [dbo].[SP_Encargados_Eliminar]
(
	@idEncargado INT
)

AS
BEGIN

DELETE FROM [SCH_GENERAL].TBL_ENCARGADOS
      WHERE [idEncargado] = @idEncargado 
END


GO
-------- Filtrar --------

CREATE PROCEDURE [dbo].[SP_Encargagos_Filtrar_X_ID]
(
	@idEncargado NVARCHAR (50)
)
AS
BEGIN
	SELECT * FROM [SCH_GENERAL].[TBL_ENCARGADOS] WHERE [idEncargado] = @idEncargado
END

GO
----------------------------------------------------------------------
--------------------		  Estado_Becas    ------------------------------
----------------------------------------------------------------------

----------- LISTAR -----------

CREATE PROCEDURE [dbo].[SP_Estado_Becas_Listar]

AS
BEGIN

SELECT [idEstado] ,
       nombre
       

  FROM [SCH_GENERAL].[TBL_ESTADOBECAS]
  END

GO

----------- INSERTAR ---------

CREATE PROCEDURE [dbo].[SP_Estado_Becas_Insertar]
(
       @idEstado INT ,
	@nombre NVARCHAR (20)
       
)

AS
BEGIN

INSERT INTO [SCH_GENERAL].[TBL_ESTADOBECAS]
           (        [idEstado] ,
					[nombre] )

     VALUES
           (	@idEstado ,
	@nombre  )

END

GO

----------- Modificar ---------

CREATE PROCEDURE [dbo].[SP_Estado_Becas_Modificar]
(
	   @idEstado INT ,
	@nombre NVARCHAR (20)
)

AS
BEGIN

UPDATE [SCH_GENERAL].[TBL_ESTADOBECAS]
   SET  [idEstado] = @idEstado 
      , nombre  = @nombre  
      
      
 WHERE [idEstado] = @idEstado

END

GO

------- Filtrar ----------

CREATE PROCEDURE [dbo].[SP_Estado_Becas_Filtrar_X_Nombre]
(
	@idEstado INT
)

AS
BEGIN

SELECT [idEstado] ,
	[nombre] 

  FROM [SCH_GENERAL].[TBL_ESTADOBECAS]
  WHERE [idEstado] = @idEstado

END

GO
----------------------------------------------------------------------
--------------------		  Estado_Curso    ------------------------------
----------------------------------------------------------------------

----------- LISTAR -----------

CREATE PROCEDURE [dbo].[SP_Estado_Curso_Listar]

AS
BEGIN

SELECT [idEstado] ,
	[nombre] 
       

  FROM [SCH_GENERAL].[TBL_ESTADOCURSO]
END

GO

----------- INSERTAR ---------

CREATE PROCEDURE [dbo].[SP_Estado_Curso_Insertar]
(
       @idEstado INT ,
	@nombre NVARCHAR (20)
       
)

AS
BEGIN

INSERT INTO [SCH_GENERAL].[TBL_ESTADOCURSO]
           (            [idEstado] ,
	[nombre] )

     VALUES
           (	@idEstado ,
				@nombre  )

END

GO

----------- Modificar ---------

CREATE PROCEDURE [dbo].[SP_Estado_Curso_Modificar]
(
	   @idEstado INT ,
	   @nombre NVARCHAR (20)
)

AS
BEGIN

UPDATE [SCH_GENERAL].[TBL_ESTADOCURSO]
   SET  [idEstado] = @idEstado 
      , [nombre]  = @nombre  
      
      
 WHERE [idEstado] = @idEstado

END

GO

------- Filtrar ----------

CREATE PROCEDURE [dbo].[SP_Estado_Curso_Filtrar_X_Nombre]
(
	@idEstado INT
)

AS
BEGIN

SELECT [idEstado] ,
	[nombre] 

  FROM [SCH_GENERAL].[TBL_ESTADOCURSO]
  WHERE [idEstado] = @idEstado

END

GO
----------------------------------------------------------------------
--------------------	  Estado_Desembolso   ------------------------------
----------------------------------------------------------------------

----------- LISTAR -----------

CREATE PROCEDURE [dbo].[SP_Estado_Desembolso_Listar]

AS
BEGIN

SELECT [idEstado] ,
	[nombre] 
       

  FROM [SCH_GENERAL].[TBL_ESTADO_DESEMBOLSOS]
END

GO

----------- INSERTAR ---------

CREATE PROCEDURE [dbo].[SP_Estado_Desembolso_Insertar]
(
       @idEstado INT ,
	@nombre NVARCHAR (20)
       
)

AS
BEGIN

INSERT INTO [SCH_GENERAL].[TBL_ESTADO_DESEMBOLSOS]
           (            [idEstado] ,
	[nombre] )

     VALUES
           (	@idEstado ,
	@nombre  )

END

GO

----------- Modificar ---------

CREATE PROCEDURE [dbo].[SP_Estado_Desembolso_Modificar]
(
	   @idEstado INT ,
	@nombre NVARCHAR (20)
)

AS
BEGIN

UPDATE [SCH_GENERAL].[TBL_ESTADO_DESEMBOLSOS]
   SET  [idEstado] = @idEstado 
      , [nombre]  = @nombre 
      
      
 WHERE [idEstado] = @idEstado

END

GO

------- Filtrar ----------

CREATE PROCEDURE [dbo].[SP_Estado_Desembolso_Filtrar_X_Nombre]
(
	@idEstado INT
)

AS
BEGIN

SELECT [idEstado] ,
	[nombre] 

  FROM [SCH_GENERAL].[TBL_ESTADO_DESEMBOLSOS]
  WHERE [idEstado] = @idEstado

END

GO
----------------------------------------------------------------------
--------------------	  Estado_Expediente   ------------------------------
----------------------------------------------------------------------

----------- LISTAR -----------

CREATE PROCEDURE [dbo].[SP_Estado_Expediente_Listar]

AS
BEGIN

SELECT [idEstado] ,
	[nombre] 
       

  FROM [SCH_GENERAL].[TBL_ESTADOEXPEDIENTE]
END

GO

----------- INSERTAR ---------

CREATE PROCEDURE [dbo].[SP_Estado_Expediente_Insertar]
(
       @idEstado INT ,
	@nombre NVARCHAR (100)
       
)

AS
BEGIN

INSERT INTO [SCH_GENERAL].[TBL_ESTADOEXPEDIENTE]
           (            [idEstado] ,
	[nombre] )

     VALUES
           (	@idEstado ,
				@nombre  )

END

GO

----------- Modificar ---------

CREATE PROCEDURE [dbo].[SP_Estado_Expediente_Modificar]
(
	   @idEstado INT ,
	   @nombre NVARCHAR (100)
)

AS
BEGIN

UPDATE [SCH_GENERAL].[TBL_ESTADOEXPEDIENTE]
   SET  [idEstado] = @idEstado 
      , nombre  = @nombre  
      
      
 WHERE [idEstado] = @idEstado

END

GO

------- Filtrar ----------

CREATE PROCEDURE [dbo].[SP_Estado_Expediente_Filtrar_X_Nombre]
(
	@idEstado INT
)

AS
BEGIN

SELECT [idEstado] ,
	[nombre] 

  FROM [SCH_GENERAL].[TBL_ESTADOEXPEDIENTE]
  WHERE [idEstado] = @idEstado

END

GO
----------------------------------------------------------------------
--------------------	  Estado_Facturas   ------------------------------
----------------------------------------------------------------------

----------- LISTAR -----------

CREATE PROCEDURE [dbo].[SP_Estado_Facturas_Listar]

AS
BEGIN

SELECT [idEstado] ,
	[nombre] 
       

  FROM [SCH_GENERAL].[TBL_ESTADOFACTURA]
END

GO

----------- INSERTAR ---------

CREATE PROCEDURE [dbo].[SP_Estado_Facturas_Insertar]
(
       @idEstado INT ,
	   @nombre NVARCHAR (20)
       
)

AS
BEGIN

INSERT INTO [SCH_GENERAL].[TBL_ESTADOFACTURA]
           (            [idEstado] ,
	[nombre] )

     VALUES
           (	@idEstado ,
				@nombre  )

END

GO

----------- Modificar ---------

CREATE PROCEDURE [dbo].[SP_Estado_Facturas_Modificar]
(
	   @idEstado INT ,
	   @nombre NVARCHAR (20)
)

AS
BEGIN

UPDATE [SCH_GENERAL].[TBL_ESTADOFACTURA]
   SET  [idEstado] = @idEstado 
      , [nombre]  = @nombre  
      
      
 WHERE [idEstado] = @idEstado

END

GO

------- Filtrar ----------

CREATE PROCEDURE [dbo].[SP_Estado_Facturas_Filtrar_X_Nombre]
(
	@idEstado INT
)

AS
BEGIN

SELECT [idEstado] ,
	[nombre] 

  FROM [SCH_GENERAL].[TBL_ESTADOFACTURA]
  WHERE [idEstado] = @idEstado

END

GO
---------------------------------------------------------------------
--------------------		  Estado_Patrocinador    ------------------------------
----------------------------------------------------------------------

----------- LISTAR -----------

CREATE PROCEDURE [dbo].[SP_Estado_Patrocinador_Listar]

AS
BEGIN

SELECT [idEstado] ,
	[nombre] 
       

  FROM [SCH_GENERAL].[TBL_ESTADOPATROCINADOR]
END

GO

----------- INSERTAR ---------

CREATE PROCEDURE [dbo].[SP_Estado_Patrocinador_Insertar]
(
       @idEstado INT ,
	   @nombre NVARCHAR (200)
       
)

AS
BEGIN

INSERT INTO [SCH_GENERAL].[TBL_ESTADOPATROCINADOR]
           (            [idEstado] ,
	[nombre] )

     VALUES
           (	@idEstado ,
				@nombre  )

END

GO

----------- Modificar ---------

CREATE PROCEDURE [dbo].[SP_Estado_Patrocinador_Modificar]
(
	   @idEstado INT ,
	@nombre NVARCHAR (200)
)

AS
BEGIN

UPDATE [SCH_GENERAL].[TBL_ESTADOPATROCINADOR]
   SET  [idEstado] = @idEstado
      , [nombre]  = @nombre  
      
      
 WHERE [idEstado] = @idEstado

END

GO

------- Filtrar ----------

CREATE PROCEDURE [dbo].[SP_Estado_Patrocinador_Filtrar_X_Nombre]
(
	@idEstado INT
)

AS
BEGIN

SELECT [idEstado] ,
	[nombre] 

  FROM [SCH_GENERAL].[TBL_ESTADOPATROCINADOR]
  WHERE [idEstado] = @idEstado

END

GO
----------------------------------------------------------------------
--------------------		  Estado_Usuario    ------------------------------
----------------------------------------------------------------------

----------- LISTAR -----------

CREATE PROCEDURE [dbo].[SP_Estado_Usuario_Listar]

AS
BEGIN

SELECT [idEstado] ,
	[nombre] 
       

  FROM [SCH_USUARIO].[TBL_ESTADOUSUARIO]
END

GO

----------- INSERTAR ---------

CREATE PROCEDURE [dbo].[SP_Estado_Usuario_Insertar]
(
       @idEstado INT ,
	@nombre NVARCHAR (20)
       
)

AS
BEGIN

INSERT INTO [SCH_USUARIO].[TBL_ESTADOUSUARIO]
           (            [idEstado] ,
	[nombre] )

     VALUES
           (	@idEstado ,
				@nombre  )

END

GO

----------- Modificar ---------

CREATE PROCEDURE [dbo].[SP_Estado_Usuario_Modificar]
(
	   @idEstado INT ,
		@nombre NVARCHAR (20)
)

AS
BEGIN

UPDATE [SCH_USUARIO].[TBL_ESTADOUSUARIO]
   SET  [idEstado] = @idEstado
      , nombre  = @nombre 
      
      
 WHERE idEstado = @idEstado

END

GO

------- Filtrar ----------

CREATE PROCEDURE [dbo].[SP_Estado_Usuario_Filtrar_X_Nombre]
(
	@idEstado INT
)

AS
BEGIN

SELECT [idEstado] ,
	[nombre] 

  FROM [SCH_USUARIO].[TBL_ESTADOUSUARIO]
  WHERE [idEstado] = @idEstado

END

GO
----------------------------------------------------------------------
--------------------		 Expediente Estudiante    ------------------------------
----------------------------------------------------------------------

----------- LISTAR -----------

CREATE PROCEDURE [dbo].[SP_Expediente_Estudiante_Listar]

AS
BEGIN

SELECT  [idEstudiante] ,
	[nombre] ,
	[primerApellido] ,
	[segundoApellido] ,
	[identificacion] ,
	[direccion] ,
	[telefono] ,
	[fechaNacimiento] ,
	[genero] ,
	[nacionalidad] ,
	[correoElectronico] ,
	[nivel_academico] ,
	[grado] ,
	[total_costo] ,
	[idCampus] ,
	[idBeca] ,
	[idEstado] ,
	[condicionCasa] ,
    [personasNucleo] ,
    [cantidadPersonasLaboran],
    [rangoSalario] ,
    [ingresos] ,
    [gastos] 
       

  FROM [SCH_GENERAL].[TBL_EXPEDIENTE_ESTUDIANTES]
  
END

GO

----------- INSERTAR ---------

CREATE PROCEDURE [dbo].[SP_Expediente_Estudiante_Insertar]
(
    @idEstudiante INT ,
	@nombre NVARCHAR (30) ,
	@primerApellido NVARCHAR (30) ,
	@segundoApellido NVARCHAR (30) ,
	@identificacion NVARCHAR (30) ,
	@direccion NVARCHAR (250) ,
	@telefono INT ,
	@fechaNacimiento DATETIME ,
	@genero CHAR(1) ,
	@nacionalidad NVARCHAR (30) ,
	@correoElectronico NVARCHAR (30) ,
	@nivel_academico NVARCHAR (15) ,
	@grado INT ,
	@total_costo DECIMAL,
	@idCampus INT ,
	@idBeca INT ,
	@idEstado INT ,
	@condicionCasa NVARCHAR(15) ,
    @personasNucleo INT ,
    @cantidadPersonasLaboran INT ,
    @rangoSalario NVARCHAR(25) ,
    @ingresos DECIMAL ,
    @gastos DECIMAL 
)

AS
BEGIN

INSERT INTO [SCH_GENERAL].[TBL_EXPEDIENTE_ESTUDIANTES]
(
            [idEstudiante] ,
	[nombre] ,
	[primerApellido] ,
	[segundoApellido] ,
	[identificacion] ,
	[direccion] ,
	[telefono] ,
	[fechaNacimiento] ,
	[genero] ,
	[nacionalidad] ,
	[correoElectronico] ,
	[nivel_academico] ,
	[grado] ,
	[total_costo] ,
	[idCampus] ,
	[idBeca] ,
	[idEstado] ,
	[condicionCasa] ,
    [personasNucleo] ,
    [cantidadPersonasLaboran],
    [rangoSalario] ,
    [ingresos] ,
    [gastos] )

     VALUES
         (  @idEstudiante ,
			@nombre  ,
			@primerApellido ,
			@segundoApellido  ,
			@identificacion  ,
			@direccion  ,
			@telefono ,
			@fechaNacimiento ,
			@genero  ,
			@nacionalidad   ,
			@correoElectronico ,
			@nivel_academico ,
			@grado ,
			@total_costo,
			@idCampus ,
			@idBeca ,
			@idEstado ,
			@condicionCasa,
			@personasNucleo,
			@cantidadPersonasLaboran,
			@rangoSalario,
			@ingresos,
			@gastos  )

END

GO

----------- Modificar ---------

CREATE PROCEDURE [dbo].[SP_Expediente_Estudiante_Modificar]
(
	   @idEstudiante INT ,
	@nombre NVARCHAR (30) ,
	@primerApellido NVARCHAR (30) ,
	@segundoApellido NVARCHAR (30) ,
	@identificacion NVARCHAR (30) ,
	@direccion NVARCHAR (250) ,
	@telefono INT ,
	@fechaNacimiento DATETIME ,
	@genero CHAR(1) ,
	@nacionalidad NVARCHAR (30) ,
	@correoElectronico NVARCHAR (30) ,
	@nivel_academico NVARCHAR (15) ,
	@grado INT ,
	@total_costo DECIMAL,
	@idCampus INT ,
	@idBeca INT ,
	@idEstado INT ,
	@condicionCasa NVARCHAR(15) ,
    @personasNucleo INT ,
    @cantidadPersonasLaboran INT ,
    @rangoSalario NVARCHAR(25) ,
    @ingresos DECIMAL ,
    @gastos DECIMAL 
)

AS
BEGIN

UPDATE [SCH_GENERAL].[TBL_EXPEDIENTE_ESTUDIANTES]
   SET  [idEstudiante] = @idEstudiante 
      , [nombre] = @nombre
      , [primerApellido] = @primerApellido
      , [segundoApellido] = @segundoApellido
      , [identificacion] = @identificacion
      , [direccion] = @direccion
      , [telefono] = @telefono
      , [fechaNacimiento] = @fechaNacimiento
      , [genero] = @genero
      , [nacionalidad] = @nacionalidad
	  , [correoElectronico] = @correoElectronico
	  ,	[nivel_academico] = @nivel_academico
	  ,	[grado] = @grado
	  , [total_costo] = @total_costo
      , [idCampus] = @idCampus
      , [idBeca] = @idBeca
      , [idEstado]  = @idEstado
	  , [condicionCasa] = @condicionCasa
	  ,	[personasNucleo] = @personasNucleo
      ,	[cantidadPersonasLaboran] = @cantidadPersonasLaboran
	  ,	[rangoSalario] = @rangoSalario
	  ,	[ingresos] = @ingresos
	  ,	[gastos] = @gastos
      
      
 WHERE [idEstudiante] = @idEstudiante
END

GO
-------- Filtrar --------

CREATE PROCEDURE [SP_Expediente_Estudiante_Filtrar_X_ID]
(
	@idEstudiante INT
)
AS
BEGIN
	SELECT * FROM [SCH_GENERAL].[TBL_EXPEDIENTE_ESTUDIANTES] WHERE idEstudiante = @idEstudiante
END

GO 
----------------------------------------------------------------------
--------------------		 Facturas    ------------------------------
----------------------------------------------------------------------

----------- LISTAR -----------

CREATE PROCEDURE [dbo].[SP_Facturas_Listar]

AS
BEGIN

SELECT  [idFactura]
      ,[cantidadEstudiantes]
      ,[monto]
      ,[fechaCobro]
      ,[fechaPago]
      ,[comprobantePago]
      ,[idEstado]
      ,[idPatrocinador]
       

  FROM [SCH_GENERAL].[TBL_FACTURAS]
  
END

GO

----------- INSERTAR ---------

CREATE PROCEDURE [dbo].[SP_Facturas_Insertar]
(
            @idFactura int
           ,@cantidadEstudiantes int
           ,@monto decimal(18,0)
           ,@fechaCobro datetime
           ,@fechaPago datetime
           ,@comprobantePago int
           ,@idEstado int
           ,@idPatrocinador int
)

AS
BEGIN

INSERT INTO [SCH_GENERAL].[TBL_FACTURAS]
(
            [idFactura]
           ,[cantidadEstudiantes]
           ,[monto]
           ,[fechaCobro]
           ,[fechaPago]
           ,[comprobantePago]
           ,[idEstado]
           ,[idPatrocinador]  )

     VALUES
         (   @idFactura 
           ,@cantidadEstudiantes
           ,@monto 
           ,@fechaCobro
           ,@fechaPago
           ,@comprobantePago 
           ,@idEstado
           ,@idPatrocinador   )

END

GO

----------- Modificar ---------

CREATE PROCEDURE [dbo].[SP_Facturas_Modificar]
(
			@idFactura int
           ,@cantidadEstudiantes int
           ,@monto decimal(18,0)
           ,@fechaCobro datetime
           ,@fechaPago datetime
           ,@comprobantePago int
           ,@idEstado int
           ,@idPatrocinador int
)

AS
BEGIN

UPDATE [SCH_GENERAL].[TBL_FACTURAS]
   SET  idFactura = @idFactura 
      , cantidadEstudiantes = @cantidadEstudiantes
      , monto = @monto
      , fechaCobro = @fechaCobro
      , fechaPago = @fechaPago
      , comprobantePago = @comprobantePago
      , idEstado = @idEstado
      , idPatrocinador = @idPatrocinador
      
      
      
 WHERE idFactura = @idFactura 
END

GO

----eliminar factura---
CREATE PROCEDURE [dbo].[SP_Facturas_Eliminar]
(
	@idFactura INT
)

AS
BEGIN

DELETE FROM [SCH_GENERAL].[TBL_FACTURAS]
      WHERE [idFactura] = @idFactura 
END


GO
----------- Filtrar -----------
CREATE PROCEDURE [dbo].[SP_Facturas_Filtrar_X_ID]
(
	@idFactura int
)
AS
BEGIN
	SELECT * FROM [SCH_GENERAL].[TBL_FACTURAS] WHERE idFactura = @idFactura
END

GO

----------------------------------------------------------------------
--------------------		Institucion  ------------------
----------------------------------------------------------------------

----------- LISTAR -----------

CREATE PROCEDURE [dbo].[SP_Institucion_Listar]

AS
BEGIN

SELECT  [IDInstitucion]
      ,[Nombre] 

  FROM [SCH_GENERAL].[TBL_INSTITUCION]
  
END

GO

----------- INSERTAR ---------

CREATE PROCEDURE [dbo].[SP_Institucion_Insertar]
(
            
	@IDInstitucion SMALLINT ,
	@Nombre NVARCHAR (200)
)

AS
BEGIN

INSERT INTO [SCH_GENERAL].[TBL_INSTITUCION]
(
    	[IDInstitucion]
      ,[Nombre]  )

     VALUES
         (   @IDInstitucion  ,
			 @Nombre  )

END

GO

----------- Modificar ---------

CREATE PROCEDURE [dbo].[SP_Institucion_Modificar]
(
			@IDInstitucion SMALLINT ,
	@Nombre NVARCHAR (200)
)

AS
BEGIN

UPDATE [SCH_GENERAL].[TBL_INSTITUCION]
   SET  [IDInstitucion] = @IDInstitucion 
      , [Nombre] = @Nombre
      
      
 WHERE [IDInstitucion] = @IDInstitucion  
END

GO

-------- Eliminar ---------

CREATE PROCEDURE [dbo].[SP_Institucion_Eliminar]
(
	@IDInstitucion SMALLINT
)

AS
BEGIN

DELETE FROM [SCH_GENERAL].[TBL_INSTITUCION]
      WHERE [IDInstitucion] = @IDInstitucion 
END


GO

-----Filtrar institucion por id----------------
CREATE PROCEDURE [dbo].[SP_Institucion_Filtrar]
(
	@IDinstitucion INT
)
AS
BEGIN
	SELECT IDInstitucion,Nombre FROM [SCH_GENERAL].[TBL_INSTITUCION] WHERE IDInstitucion = @IDinstitucion
END

GO
----------------------------------------------------------------------
--------------------		Materias  ------------------
----------------------------------------------------------------------

----------- LISTAR -----------
 
CREATE PROCEDURE [dbo].[SP_Materias_Listar]
 
AS
BEGIN
 
SELECT  [idMateria] ,
    [nombre] ,
    [costo] 
    
 
  FROM [SCH_GENERAL].[TBL_MATERIAS]
   
END
 
GO
 
----------- INSERTAR ---------
 
CREATE PROCEDURE [dbo].[SP_Materias_Insertar]
(
             
    @idMateria INT ,
    @nombre NVARCHAR (20) ,
    @costo DECIMAL 
)
 
AS
BEGIN
 
INSERT INTO [SCH_GENERAL].[TBL_MATERIAS]
(       [idMateria] ,
    [nombre] ,
    [costo]  )
 
     VALUES
         (   @idMateria ,
    @nombre ,
    @costo )
 
END
 
GO
 
----------- Modificar ---------
 
CREATE PROCEDURE [dbo].[SP_Materias_Modificar]
(   @idMateria INT ,
    @nombre NVARCHAR (20) ,
    @costo DECIMAL 
)
 
AS
BEGIN
 
UPDATE [SCH_GENERAL].[TBL_MATERIAS]
   SET  [idMateria] = @idMateria 
      , [nombre] = @nombre 
      , [costo]  = @costo 
       
       
       
 WHERE [idMateria] = @idMateria 
END
 
GO
 
-------- Eliminar ---------
 
CREATE PROCEDURE [dbo].[SP_Materias_Eliminar]
(
    @idMateria INT
)
 
AS
BEGIN
 
DELETE FROM [SCH_GENERAL].[TBL_MATERIAS]
      WHERE [idMateria] = @idMateria 
END
 
 
GO
 
-------- Filtrar --------
 
CREATE PROCEDURE [dbo].[SP_Materias_Filtrar_X_ID]
(
    @idMateria INT
)
AS
BEGIN
    SELECT idMateria,nombre,
    costo
 
    FROM [SCH_GENERAL].[TBL_MATERIAS] WHERE idMateria = @idMateria
END
 
GO
----------------------------------------------------------------------
--------------------		Patrocinador  ------------------
----------------------------------------------------------------------

----------- LISTAR -----------

CREATE PROCEDURE [dbo].[SP_Patrocinador_Listar]

AS
BEGIN

SELECT  [idPatrocinador] ,
	[nombre] ,
	[montoMaximo] ,
	[correoElectronico],
	[idPeriodo] ,
	[idEstado] 

  FROM [SCH_GENERAL].[TBL_PATROCINADOR]
  
END

GO

CREATE PROCEDURE [dbo].[SP_Patrocinador_Listar_Activo]

AS
BEGIN

SELECT  P.[idPatrocinador] ,
	P.[nombre] ,
	P.[montoMaximo] ,
	P.[correoElectronico],
	P.[idPeriodo] ,
	P.[idEstado] 

  FROM [SCH_GENERAL].[TBL_PATROCINADOR] P,[SCH_GENERAL].[TBL_ESTADOPATROCINADOR] EP
  WHERE P.[idEstado] =EP.[idEstado] AND EP.[nombre]='Activo'
  
END

GO

----------- INSERTAR ---------

CREATE PROCEDURE [dbo].[SP_Patrocinador_Insertar]
(
            
	@idPatrocinador INT ,
	@nombre NVARCHAR (30) ,
	@montoMaximo DECIMAL ,
	@correoElectronico NVARCHAR(100),
	@idPeriodo INT ,
	@idEstado INT 
)

AS
BEGIN

INSERT INTO [SCH_GENERAL].[TBL_PATROCINADOR]
(       [idPatrocinador] ,
	[nombre] ,
	[montoMaximo] ,
	[correoElectronico],
	[idPeriodo] ,
	[idEstado]   )

     VALUES
         (   @idPatrocinador ,
	@nombre  ,
	@montoMaximo ,
	@correoElectronico,
	@idPeriodo ,
	@idEstado )

END

GO

----------- Modificar ---------

CREATE PROCEDURE [dbo].[SP_Patrocinador_Modificar]
(	@idPatrocinador INT ,
	@nombre NVARCHAR (30) ,
	@montoMaximo DECIMAL ,
	@correoElectronico NVARCHAR(100),
	@idPeriodo INT ,
	@idEstado INT 
)

AS
BEGIN

UPDATE [SCH_GENERAL].[TBL_PATROCINADOR]
   SET  [idPatrocinador] = @idPatrocinador 
      , [nombre] = @nombre 
      , [montoMaximo] = @montoMaximo 
	  ,[correoElectronico]=@correoElectronico
      , [idPeriodo] = @idPeriodo 
      , [idEstado]   = @idEstado 
      
 WHERE [idPatrocinador] = @idPatrocinador 
END

GO

-------- Eliminar ---------

CREATE PROCEDURE [dbo].[SP_Patrocinador_Eliminar]
(
	@idPatrocinador INT
)

AS
BEGIN

DELETE FROM [SCH_GENERAL].[TBL_PATROCINADOR]
      WHERE [idPatrocinador] = @idPatrocinador
END


GO

-------- Filtrar --------

CREATE PROCEDURE [dbo].[SP_Patrocinador_Filtrar_X_ID]
(
	@idPatrocinador INT
)
AS
BEGIN
	SELECT * FROM [SCH_GENERAL].[TBL_PATROCINADOR] WHERE idPatrocinador = @idPatrocinador
END

GO
----------------------------------------------------------------------
--------------------		Periodo  ------------------
----------------------------------------------------------------------

----------- LISTAR -----------

CREATE PROCEDURE [dbo].[SP_Periodo_Listar]

AS
BEGIN

SELECT  [idPeriodo] ,
	[nombre] 

  FROM [SCH_GENERAL].[TBL_PERIODO]
  
END

GO

----------- INSERTAR ---------

CREATE PROCEDURE [dbo].[SP_Periodo_Insertar]
(
            
	@idPeriodo INT ,
	@nombre NVARCHAR (15)
)

AS
BEGIN

INSERT INTO [SCH_GENERAL].[TBL_PERIODO]
(       [idPeriodo] ,
	[nombre] 
  )

     VALUES
         (   @idPeriodo ,
	@nombre  )

END

GO

----------- Modificar ---------

CREATE PROCEDURE [dbo].[SP_Periodo_Modificar]
(	@idPeriodo INT ,
	@nombre NVARCHAR (15)
)

AS
BEGIN

UPDATE [SCH_GENERAL].[TBL_PERIODO]
   SET  [idPeriodo] = @idPeriodo 
      , [nombre] = @nombre 
 
      
 WHERE [idPeriodo] = @idPeriodo  
END

GO

-------- Eliminar ---------

CREATE PROCEDURE [dbo].[SP_Periodo_Eliminar]
(
	@idPeriodo INT 
)

AS
BEGIN

DELETE FROM [SCH_GENERAL].[TBL_PERIODO]
      WHERE [idPeriodo] = @idPeriodo 
END


GO

-------- Filtrar --------

CREATE PROCEDURE [dbo].[SP_Periodo_Filtrar_X_ID]
(
	@idPeriodo INT
)
AS
BEGIN
	SELECT * FROM [SCH_GENERAL].[TBL_PERIODO] WHERE idPeriodo = @idPeriodo
END

GO
----------------------------------------------------------------------
--------------------		Preguntas   ------------------
----------------------------------------------------------------------

----------- LISTAR -----------

CREATE PROCEDURE [dbo].[SP_Preguntas_Listar]

AS
BEGIN

SELECT  [idPregunta] ,
	[Pregunta] 

  FROM [SCH_USUARIO].[TBL_PREGUNTAS]
  
END

GO

----------- INSERTAR ---------

CREATE PROCEDURE [dbo].[SP_Preguntas_Insertar]
(
            
	@idPregunta INT ,
	@Pregunta NVARCHAR (60) 
)

AS
BEGIN

INSERT INTO [SCH_USUARIO].[TBL_PREGUNTAS]
(       [idPregunta] ,
	[Pregunta] 
  )

     VALUES
         (   @idPregunta ,
	@Pregunta )

END

GO

----------- Modificar ---------

CREATE PROCEDURE [dbo].[SP_Preguntas_Modificar]
(	@idPregunta INT ,
	@Pregunta NVARCHAR (60)
)

AS
BEGIN

UPDATE [SCH_USUARIO].[TBL_PREGUNTAS]
   SET  [idPregunta] = @idPregunta 
      , [Pregunta] = @Pregunta 
 
      
 WHERE [idPregunta] = @idPregunta  
END

GO

-------- Filtrar --------

CREATE PROCEDURE [dbo].[SP_Preguntas_Filtrar_X_ID]
(
	@idPregunta INT
)
AS
BEGIN
	SELECT * FROM [SCH_USUARIO].[TBL_PREGUNTAS] WHERE idPregunta = @idPregunta
END

GO
----------------------------------------------------------------------
--------------------		Respuestas  ------------------
----------------------------------------------------------------------

----------- LISTAR -----------

CREATE PROCEDURE [dbo].[SP_Respuestas_Listar]

AS
BEGIN

SELECT  [idRespuesta] ,
		[respuesta] ,
		[idUsuario] ,
		[idPregunta] 

  FROM [SCH_USUARIO].[TBL_RESPUESTAS]
  
END

GO

----------- INSERTAR ---------

CREATE PROCEDURE [dbo].[SP_Respuestas_Insertar]
(
	@respuesta NVARCHAR (100) ,
	@idUsuario NVARCHAR (15) ,
	@idPregunta INT 
)

AS
BEGIN

INSERT INTO [SCH_USUARIO].[TBL_RESPUESTAS]
(    	[respuesta] ,
		[idUsuario] ,
		[idPregunta] 
  )

     VALUES
         (  @respuesta ,
			@idUsuario  ,
			@idPregunta )

		SELECT MAX (idRespuesta) FROM SCH_USUARIO.TBL_RESPUESTAS

END



GO

----------- Modificar ---------

CREATE PROCEDURE [dbo].[SP_Respuestas_Modificar]
(	@idRespuesta INT ,
	@respuesta NVARCHAR (100) ,
	@idUsuario NVARCHAR (15) ,
	@idPregunta INT
)

AS
BEGIN

UPDATE [SCH_USUARIO].[TBL_RESPUESTAS]
   SET  
       respuesta = @respuesta 
      , idUsuario = @idUsuario 
      , idPregunta = @idPregunta 
 
      
 WHERE idRespuesta = @idRespuesta   
END

GO

-------- Eliminar ---------

CREATE PROCEDURE [dbo].[SP_Respuestas_Eliminar]
(
	@idRespuesta INT
)

AS
BEGIN

DELETE FROM [SCH_USUARIO].[TBL_RESPUESTAS]
      WHERE idRespuesta = @idRespuesta  
END


GO
----------------------------------------------------------------------
--------------------		Roles_Usuarios  ------------------
----------------------------------------------------------------------

----------- LISTAR -----------

CREATE PROCEDURE [dbo].[SP_Roles_Usuarios_Listar]

AS
BEGIN

SELECT  [idRol] ,
	[nombre] 

  FROM [SCH_USUARIO].[TBL_ROLESDEUSUARIOS]
  
END

GO

----------- INSERTAR ---------

CREATE PROCEDURE [dbo].[SP_Roles_Usuarios_Insertar]
(
            
	@idRol INT ,
	@nombre NVARCHAR (25) 
)

AS
BEGIN

INSERT INTO [SCH_USUARIO].[TBL_ROLESDEUSUARIOS]
(       [idRol] ,
		[nombre]
  )

     VALUES
         (   @idRol  ,
			 @nombre   )

END

GO

----------- Modificar ---------

CREATE PROCEDURE [dbo].[SP_Roles_Usuarios_Modificar]
(	@idRol INT ,
	@nombre NVARCHAR (25)
)

AS
BEGIN

UPDATE [SCH_USUARIO].[TBL_ROLESDEUSUARIOS]
   SET  idRol = @idRol 
      , nombre = @nombre 
      
 
      
 WHERE idRol = @idRol   
END

GO

-------- Filtrar --------

CREATE PROCEDURE [dbo].[SP_Roles_Usuarios_Filtrar_X_ID]
(
	@idRol INT
)
AS
BEGIN
	SELECT * FROM [SCH_USUARIO].[TBL_ROLESDEUSUARIOS] WHERE idRol = @idRol
END

GO
----------------------------------------------------------------------
--------------------		SubCategorias  ------------------
----------------------------------------------------------------------

----------- LISTAR -----------

CREATE PROCEDURE [dbo].[SP_SubCategorias_Listar]

AS
BEGIN

SELECT  [idSubcategoria] ,
	[nombre] ,
	[porcentaje] 

  FROM [SCH_GENERAL].[TBL_SUBCATEGORIA]
  
END

GO

----------- INSERTAR ---------

CREATE PROCEDURE [dbo].[SP_SubCategorias_Insertar]
(
            
	@idSubcategoria INT ,
	@nombre NVARCHAR (20) ,
	@porcentaje DECIMAL
)

AS
BEGIN

INSERT INTO [SCH_GENERAL].[TBL_SUBCATEGORIA]
(       [idSubcategoria] ,
		[nombre] ,
		[porcentaje] 
  )

     VALUES
         (   @idSubcategoria  ,
			 @nombre,
			 @porcentaje )

END

GO

----------- Modificar ---------

CREATE PROCEDURE [dbo].[SP_SubCategorias_Modificar]
(	@idSubcategoria INT ,
	@nombre NVARCHAR (20) ,
	@porcentaje DECIMAL
)

AS
BEGIN

UPDATE [SCH_GENERAL].[TBL_SUBCATEGORIA]
   SET  idSubcategoria = @idSubcategoria 
      , nombre = @nombre 
	  , porcentaje = @porcentaje
      
 
      
 WHERE idSubcategoria = @idSubcategoria   
END

GO

-------- Eliminar ---------

CREATE PROCEDURE [dbo].[SP_SubCategorias_Eliminar]
(
	@idSubcategoria INT
)

AS
BEGIN

DELETE FROM [SCH_GENERAL].[TBL_SUBCATEGORIA]
      WHERE idSubcategoria = @idSubcategoria
END


GO

------- Filtrar ----------

CREATE PROCEDURE [dbo].[SP_SubCategorias_Filtrar_X_ID]
(
	@idSubcategoria INT
)

AS
BEGIN

SELECT idSubcategoria,
	nombre,
	porcentaje

  FROM [SCH_GENERAL].[TBL_SUBCATEGORIA]
  WHERE idSubcategoria = @idSubcategoria

END

GO
----------------------------------------------------------------------
--------------------		Usuarios     ------------------------------
----------------------------------------------------------------------

----------- LISTAR -----------

CREATE PROCEDURE [dbo].[SP_Usuarios_Listar]

AS
BEGIN

SELECT [idUsuario] ,
	[nombre]   ,
	[password]  ,
	[idRol]  ,
	[idEstado]  
       

  FROM [SCH_USUARIO].[TBL_USUARIOS]

END

GO

----------- INSERTAR ---------

CREATE PROCEDURE [dbo].[SP_Usuarios_Insertar]
(
       @idUsuario NVARCHAR (15) ,
	@nombre NVARCHAR (20) ,
    @password NVARCHAR (8) ,
	@idRol INT ,
	@idEstado INT 
       
)

AS
BEGIN

INSERT INTO [SCH_USUARIO].[TBL_USUARIOS]
           (        [idUsuario] ,
					[nombre]   ,
					[password]  ,
					[idRol]  ,
					[idEstado])

     VALUES
           (	@idUsuario  ,
				@nombre  ,
				@password  ,
				@idRol  ,
				@idEstado  )

END

GO

----------- Modificar ---------

CREATE PROCEDURE [dbo].[SP_Usuarios_Modificar]
(
	   @idUsuario NVARCHAR (15) ,
	@nombre NVARCHAR (20) ,
    @password NVARCHAR (8) ,
	@idRol INT ,
	@idEstado INT 
)

AS
BEGIN

UPDATE [SCH_USUARIO].[TBL_USUARIOS]
   SET  idUsuario = @idUsuario
      , nombre = @nombre
      , password = @password
      , idRol = @idRol
      , idEstado = @idEstado
      
       
 WHERE idUsuario = @idUsuario

END

GO
-------- Filtrar --------

CREATE PROCEDURE [dbo].[SP_Usuarios_Filtrar_X_ID]
(
	@idUsuario NVARCHAR (15)
)
AS
BEGIN
	SELECT * FROM [SCH_USUARIO].[TBL_USUARIOS] WHERE idUsuario = @idUsuario
END
GO
------------------------------------------------------------------------------------------
------------------------------DATOS---PRUEBAS---------------------------------------------
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
--------Inserts Roles-------
INSERT INTO [SCH_USUARIO].[TBL_ROLESDEUSUARIOS]
           ([idRol]
           ,[nombre])
     VALUES
           (1,'Administrador')
GO

INSERT INTO [SCH_USUARIO].[TBL_ROLESDEUSUARIOS]
           ([idRol]
           ,[nombre])
     VALUES
           (2,'Consulta')
GO

INSERT INTO [SCH_USUARIO].[TBL_ROLESDEUSUARIOS]
           ([idRol]
           ,[nombre])
     VALUES
           (3,'BackOffice')
GO

INSERT INTO [SCH_USUARIO].[TBL_ROLESDEUSUARIOS]
           ([idRol]
           ,[nombre])
     VALUES
           (4,'Jefe BackOffice')
GO

INSERT INTO [SCH_USUARIO].[TBL_ROLESDEUSUARIOS]
           ([idRol]
           ,[nombre])
     VALUES
           (5,'Plataforma')
GO

INSERT INTO [SCH_USUARIO].[TBL_ROLESDEUSUARIOS]
           ([idRol]
           ,[nombre])
     VALUES
           (6,'Jefe Plataforma')
GO

INSERT INTO [SCH_USUARIO].[TBL_ROLESDEUSUARIOS]
           ([idRol]
           ,[nombre])
     VALUES
           (7,'Analista')
GO
--------Fin Insert Roles----

--------Inserts Instituciones-------

INSERT INTO [SCH_GENERAL].[TBL_INSTITUCION] 
([IDInstitucion],[Nombre])
VALUES (1,'Universidad Americana')
GO

INSERT INTO [SCH_GENERAL].[TBL_INSTITUCION] 
([IDInstitucion],[Nombre])
VALUES (2,'Universidad Latina')
GO
--------Fin Instituciones----

--------Inserts Campus-------

INSERT INTO [SCH_GENERAL].[TBL_CAMPUS]
([idCampus] ,[nombre] ,[IDInstitucion])
VALUES (1,'UAM-San Pedro', 1)
GO

INSERT INTO [SCH_GENERAL].[TBL_CAMPUS]
([idCampus] ,[nombre] ,[IDInstitucion])
VALUES (2,'UAM-Heredia', 1)
GO

INSERT INTO [SCH_GENERAL].[TBL_CAMPUS]
([idCampus] ,[nombre] ,[IDInstitucion])
VALUES (3,'UAM-Cartago', 1)
GO

INSERT INTO [SCH_GENERAL].[TBL_CAMPUS]
([idCampus] ,[nombre] ,[IDInstitucion])
VALUES (4,'San Pedro', 2)
GO

INSERT INTO [SCH_GENERAL].[TBL_CAMPUS]
([idCampus] ,[nombre] ,[IDInstitucion])
VALUES (5,'Heredia', 2)
GO
--------Fin Campus----



--------Inserts Estado_Usuarios-------

INSERT INTO [SCH_USUARIO].[TBL_ESTADOUSUARIO]
([idEstado],[nombre])
VALUES (1,'Activo')
GO

INSERT INTO [SCH_USUARIO].[TBL_ESTADOUSUARIO]
([idEstado],[nombre])
VALUES (2,'Inactivo')
GO
--------Fin Estado_Usuarios-------

--------Inserts Usuarios-------

INSERT INTO [SCH_USUARIO].[TBL_USUARIOS]
([idUsuario],[nombre],[password],[idRol],[idEstado])
VALUES ('1','mrohmans','12345678',1,1)
GO

INSERT INTO [SCH_USUARIO].[TBL_USUARIOS]
([idUsuario],[nombre],[password],[idRol],[idEstado])
VALUES ('2','manuel','manuel01',1,1)
GO
--------Fin Usuarios-------
--------Inserts Empleados-------

INSERT INTO [SCH_USUARIO].[TBL_EMPLEADOS]
([idEmpleado],[cedula],[nombre],[primerApellido],[segundoApellido],
[idUsuario],[correoElectronico],[fechaNacimiento],[idCampus])
VALUES (1,'112990963','Moises','Rohman','Sol�s',1,'mrohmans@gmail.com',
'11-12-1986',1)
GO

INSERT INTO [SCH_USUARIO].[TBL_EMPLEADOS]
([idEmpleado],[cedula],[nombre],[primerApellido],[segundoApellido],
[idUsuario],[correoElectronico],[fechaNacimiento],[idCampus])
VALUES (2,'112660686','David','Martinez','Herrera',1,'davidmartinezherrera@gmail.com',
'19680101',1)
GO

--------Fin Empleados----
--------Inserts Preguntas-------

INSERT INTO [SCH_USUARIO].[TBL_PREGUNTAS]
( [idPregunta],[Pregunta] )
VALUES (1, '�Cu�l es el nombre de su primera mascota?')
GO

INSERT INTO [SCH_USUARIO].[TBL_PREGUNTAS]
( [idPregunta],[Pregunta] )
VALUES (2, '�Cu�l es la fecha de nacimiento de su madre?')
GO

INSERT INTO [SCH_USUARIO].[TBL_PREGUNTAS]
( [idPregunta],[Pregunta] )
VALUES (3, '�Cu�l fue su primer n�mero telef�nico m�vil?')
GO

INSERT INTO [SCH_USUARIO].[TBL_PREGUNTAS]
( [idPregunta],[Pregunta] )
VALUES (4, '�Cu�l es la ciudad de nacimiento de su madre?')
GO

INSERT INTO [SCH_USUARIO].[TBL_PREGUNTAS]
( [idPregunta],[Pregunta] )
VALUES (5, '�Cu�l es el nombre de la escuela a la que usted asisti�?')
GO
--------Fin Preguntas-------

--------Inserts Respuestas-------


INSERT INTO [SCH_USUARIO].[TBL_RESPUESTAS]
([respuesta],[idUsuario] ,[idPregunta])
VALUES ('Firulais',1,1)
GO

INSERT INTO [SCH_USUARIO].[TBL_RESPUESTAS]
([respuesta],[idUsuario] ,[idPregunta])
VALUES ('1952-11-18',1,2)
GO

INSERT INTO [SCH_USUARIO].[TBL_RESPUESTAS]
([respuesta],[idUsuario] ,[idPregunta])
VALUES ('88888888',1,3)
GO

INSERT INTO [SCH_USUARIO].[TBL_RESPUESTAS]
([respuesta],[idUsuario] ,[idPregunta])
VALUES ('Guanacaste',1,4)
GO

INSERT INTO [SCH_USUARIO].[TBL_RESPUESTAS]
([respuesta],[idUsuario] ,[idPregunta])
VALUES ('Garcia',1,5)
GO

--------Fin Respuestas-------

--------Inserts Estado_Factura-------


INSERT INTO [SCH_GENERAL].[TBL_ESTADOFACTURA]
([idEstado],[nombre])
VALUES (1, 'Pendiente de pago')
GO

INSERT INTO [SCH_GENERAL].[TBL_ESTADOFACTURA]
([idEstado],[nombre])
VALUES (2, 'Cancelado')
GO
--------Fin Estado_Factura-------

--------Inserts Estado_Patrocinador-------


INSERT INTO [SCH_GENERAL].[TBL_ESTADOPATROCINADOR]
([idEstado],[nombre])
VALUES (1, 'Pendiente Aprobacion')
GO

INSERT INTO [SCH_GENERAL].[TBL_ESTADOPATROCINADOR]
([idEstado],[nombre])
VALUES (2, 'Activo')
GO

INSERT INTO [SCH_GENERAL].[TBL_ESTADOPATROCINADOR]
([idEstado],[nombre])
VALUES (3, 'Cerrado')
GO
--------Fin Estado_Patrocinador-------

--------Inserts Periodo-------


INSERT INTO [SCH_GENERAL].[TBL_PERIODO]
([idPeriodo],[nombre])
VALUES(1,'Mensual')
GO
--------Fin Periodo-------

--------Inserts Patrocinador-------


INSERT INTO [SCH_GENERAL].[TBL_PATROCINADOR]
([idPatrocinador],[nombre],[montoMaximo],[correoElectronico],
[idPeriodo],[idEstado])
VALUES (1,'FONABE','1000000',' mrohmans@gmail.com',1,1)
GO

--------Fin Patrocinador-------

--------Inserts Patrocinador-------


INSERT INTO [SCH_GENERAL].[TBL_FACTURAS]
([idFactura],[cantidadEstudiantes],[monto],[fechaCobro],[fechaPago],
[comprobantePago],[idEstado],[idPatrocinador])
VALUES (1,'2','1000000','20170727','20170728',
'1',1,1)
GO
--------Fin Patrocinador-------

--------Inserts Estado_Becas-------


INSERT INTO [SCH_GENERAL].[TBL_ESTADOBECAS]
([idEstado], [nombre])
VALUES (1, 'Pendiente Aprobacion')
GO

INSERT INTO [SCH_GENERAL].[TBL_ESTADOBECAS]
([idEstado], [nombre])
VALUES (2, 'Activo')
GO

INSERT INTO [SCH_GENERAL].[TBL_ESTADOBECAS]
([idEstado], [nombre])
VALUES (3, 'Cerrada')
GO
--------Fin Estado_Becas-------
--Estado Curso
USE [DB_Beca2]
GO

DECLARE	@return_value int

EXEC	@return_value = [dbo].[SP_Estado_Curso_Insertar]
		@idEstado = 1,
		@nombre = N'Aprobado'

SELECT	'Return Value' = @return_value

GO

USE [DB_Beca2]
GO

DECLARE	@return_value int

EXEC	@return_value = [dbo].[SP_Estado_Curso_Insertar]
		@idEstado = 2,
		@nombre = N'Reprobado'

SELECT	'Return Value' = @return_value

GO

USE [DB_Beca2]
GO

DECLARE	@return_value int

EXEC	@return_value = [dbo].[SP_Estado_Curso_Insertar]
		@idEstado = 3,
		@nombre = N'En Curso'

SELECT	'Return Value' = @return_value

GO

--estado Expediente
USE [DB_Beca2]
GO

DECLARE	@return_value int

EXEC	@return_value = [dbo].[SP_Estado_Expediente_Insertar]
		@idEstado = 1,
		@nombre = N'Pendiente aprobaci�n'

SELECT	'Return Value' = @return_value

GO

USE [DB_Beca2]
GO

DECLARE	@return_value int

EXEC	@return_value = [dbo].[SP_Estado_Expediente_Insertar]
		@idEstado = 2,
		@nombre = N'Activo'

SELECT	'Return Value' = @return_value

GO

USE [DB_Beca2]
GO

DECLARE	@return_value int

EXEC	@return_value = [dbo].[SP_Estado_Expediente_Insertar]
		@idEstado = 3,
		@nombre = N'Rechazado'

SELECT	'Return Value' = @return_value

GO

USE [DB_Beca2]
GO

DECLARE	@return_value int

EXEC	@return_value = [dbo].[SP_Estado_Expediente_Insertar]
		@idEstado = 4,
		@nombre = N'Congelado'

SELECT	'Return Value' = @return_value

GO
USE [DB_Beca2]
GO

DECLARE	@return_value int

EXEC	@return_value = [dbo].[SP_Estado_Expediente_Insertar]
		@idEstado = 5,
		@nombre = N'Cancelado'

SELECT	'Return Value' = @return_value

GO

-- Estados Desembolsos

USE [DB_Beca2]
GO

DECLARE	@return_value int

EXEC	@return_value = [dbo].[SP_Estado_Desembolso_Insertar]
		@idEstado = 1,
		@nombre = N'Facturado'

SELECT	'Return Value' = @return_value

GO

USE [DB_Beca2]
GO

DECLARE	@return_value int

EXEC	@return_value = [dbo].[SP_Estado_Desembolso_Insertar]
		@idEstado = 2,
		@nombre = N'Cancelado'

SELECT	'Return Value' = @return_value

GO


--------Inserts CategoriaBeca-------


--INSERT INTO [SCH_GENERAL].[TBL_CATEGORIA]
--([idCategoria],[nombre],[monto])
--VALUES (1,'Excelencia Academica','125000')
--GO
----------Fin CategoriaBeca-------

----------Inserts subCategoriaBeca-------


INSERT INTO [SCH_GENERAL].[TBL_SUBCATEGORIA]
([idSubcategoria],[nombre],[porcentaje])
VALUES (1, 'A', '100')
GO

INSERT INTO [SCH_GENERAL].[TBL_SUBCATEGORIA]
([idSubcategoria],[nombre],[porcentaje])
VALUES (2, 'B', '90')
GO

INSERT INTO [SCH_GENERAL].[TBL_SUBCATEGORIA]
([idSubcategoria],[nombre],[porcentaje])
VALUES (3, 'C', '80')
GO
----------Fin CategoriaBeca-------

----------Inserts Beca-------

--GO
--INSERT INTO [SCH_GENERAL].[TBL_BECAS]
--([idBeca],[porcentajeDescuento],[nombre],[notaMinima],
--[idPatrocinador],[idCategoria],[idSubcategoria],[idEstado])
--VALUES (1,'100','Beca Academica', '70',1,1,1,1)

----------Fin Beca-------

----------Inserts Estado_Expediente-------

--GO
--INSERT INTO [SCH_GENERAL].[TBL_ESTADOEXPEDIENTE]
--([idEstado],[nombre])
--VALUES (1,'Pendiente')

----------Fin Estado_Expediente-------

----------Inserts ExpedienteEst-------

--GO
--INSERT INTO [SCH_GENERAL].[TBL_EXPEDIENTE_ESTUDIANTES]
--([idEstudiante],
--	[nombre],
--	[primerApellido],
--	[segundoApellido],
--	[identificacion],
--	[direccion],
--	[telefono],
--	[fechaNacimiento],
--	[genero],
--	[nacionalidad],
--	[correoElectronico],
--	[nivel_academico],
--	[grado],
--	[total_costo], 
--	[idCampus], 
--	[idBeca],
--	[idEstado], 
--	[condicionCasa],
--    [personasNucleo],
--    [cantidadPersonasLaboran] ,
--    [rangoSalario] ,
--    [ingresos],
--    [gastos])
--VALUES (1,'Maria','Herrera','Piedra','11112222','300 sur del OutletMall, San Pedro',
--22502250,'19970305','F','costarricense','mariaherrera@gmail.com',
--'Universidad',1,500000,1,1,1,'Propia',3,1,800000,800000,
--700000)
----------Fin ExpedienteEst-------

----------Inserts Estado_Desembolso-------

--GO
--INSERT INTO [SCH_GENERAL].[TBL_ESTADO_DESEMBOLSOS]
--([idEstado],[nombre])
--VALUES (1,'Facturado')

----------Fin Estado_Desembolso-------

----------Inserts Desembolso-------

--GO
--INSERT INTO [SCH_GENERAL].[TBL_DESEMBOLSOS]
--([idDesembolso],[idEstudiante],[montoDesembolsado],
--[fechaDesembolso],[idEstado],[idFactura])
--VALUES (1,1,'500000','20170727',1,1)

----------Fin Desembolso-------

----------Inserts Estado_Curso-------


--INSERT INTO [SCH_GENERAL].[TBL_ESTADOCURSO]
--([idEstado],[nombre])
--VALUES (1,'Aprobado')

----------Fin Estado_Curso-------

----------Inserts Materias-------

--GO 
--INSERT INTO [SCH_GENERAL].[TBL_MATERIAS]
--([nombre],[costo],[idEstudiante])
--VALUES ('Ingl�s','50000',1)

----------Fin Materias-------

----------Inserts Cursos-------

--GO
--INSERT INTO [SCH_GENERAL].[TBL_CURSOS]
--([idEstado],[idEstudiante],[nombre],[nota],[promedio],[idFactura])
--VALUES (1,1,'Ingles','90','90',1)

--------Fin Cursos-------