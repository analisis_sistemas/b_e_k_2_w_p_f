﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;

namespace PL_B_I_T_S
{
    class GlassHelper
    {
        [DllImport("DwmApi.dll")]
        public static extern int DwmExtendFrameIntoClientArea
            (IntPtr hwmnd, ref Margins pMarInset);

        [StructLayout(LayoutKind.Sequential)]
        public struct Margins
        {
            public int cxLeftWidth;
            public int cxRightWidth;
            public int cyTopHeight;
            public int cyBottomHeight;
        }

        public static Margins GetDpiAdjustedMargins(IntPtr windowHandle,
            int left, int right, int top, int bottom)
        {

            System.Drawing.Graphics g = System.Drawing.Graphics.FromHwnd(windowHandle);
            float desktopDpiX = g.DpiX;
            float desktopDpiY = g.DpiY;

            GlassHelper.Margins margins = new GlassHelper.Margins();
            margins.cxLeftWidth = Convert.ToInt32(left * (desktopDpiX / 96));
            margins.cxRightWidth = Convert.ToInt32(right * (desktopDpiX / 96));
            margins.cyTopHeight = Convert.ToInt32(top * (desktopDpiX / 96));
            margins.cyBottomHeight = Convert.ToInt32(right * (desktopDpiX / 96));

            return margins;
        }

        public static void ExtendGlass(Window win, int left, int right, int top, int bottom)
        {
            WindowInteropHelper windowInterop = new WindowInteropHelper(win);
            IntPtr windowHandle = windowInterop.Handle;
            HwndSource mainWindowScr = HwndSource.FromHwnd(windowHandle);
            mainWindowScr.CompositionTarget.BackgroundColor = Colors.Transparent;

            GlassHelper.Margins margins =
                GlassHelper.GetDpiAdjustedMargins(windowHandle, left, right, top, bottom);

            int returnVal =
                GlassHelper.DwmExtendFrameIntoClientArea
                (mainWindowScr.Handle, ref margins);

            if (returnVal < 0)
            {
                throw new NotSupportedException("Operación fallida.");
            }
        }
    }
}
