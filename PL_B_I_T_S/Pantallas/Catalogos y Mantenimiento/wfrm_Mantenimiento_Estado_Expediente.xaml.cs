﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;
using BLL_B_I_T_S.Catalogos_y_Mantenimiento;
using System.Data;

namespace PL_B_I_T_S.Pantallas.Catalogos_y_Mantenimineto
{
    /// <summary>
    /// Lógica de interacción para wfrm_Mantenimiento_Estado_Expediente.xaml
    /// </summary>
    public partial class wfrm_Mantenimiento_Estado_Expediente: MetroWindow
    {
        public wfrm_Mantenimiento_Estado_Expediente()
        {
            InitializeComponent();
        }


        #region Variables Globales
        cls_Estado_Expediente_DAL obj_Estado_DAL = new cls_Estado_Expediente_DAL();
        cls_EstadoExpediente_BLL obj_Estado_BLL = new cls_EstadoExpediente_BLL();
        #endregion
        #region Métodos

        #region Cargar
        private void CargarEstados()
        {


            #region Variables
            string sMsjError = string.Empty;
            DataTable dtEstado = new DataTable();
            #endregion

            obj_Estado_BLL.Listar_EstadoExpediente(ref dtEstado, ref sMsjError);

            if (sMsjError == string.Empty)
            {
                dgv_EstadoExpediente.ItemsSource = null;
                dgv_EstadoExpediente.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtEstado });

            }
            else
            {
                dgv_EstadoExpediente.ItemsSource = null;
                MessageBox.Show("Se presento un error al listar Estados.\n\nDesc.Error ="
                                 + sMsjError, "Error al Listar", MessageBoxButton.OK, MessageBoxImage.Error);

                dgv_EstadoExpediente.ItemsSource = null;
            }
        }
        #endregion
        #endregion



        private void btn_ListarExpediente_Click(object sender, RoutedEventArgs e)
        {
            CargarEstados();
        }
    }
}
