﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;
using BLL_B_I_T_S.Catalogos_y_Mantenimiento;
using System.Data;

namespace PL_B_I_T_S.Pantallas.Catalogos_y_Mantenimineto
{
    /// <summary>
    /// Interaction logic for wfrm_Mantenimiento_Estado_Curso.xaml
    /// </summary>
    public partial class wfrm_Mantenimiento_Estado_Curso : MetroWindow
    {
        public wfrm_Mantenimiento_Estado_Curso()
        {
            InitializeComponent();
        }

        #region Variables Globales
        cls_Estado_Curso_DAL obj_EstadoCurso_DAL = new cls_Estado_Curso_DAL();
        cls_EstadoCurso_BLL obj_EstadoCurso_BLL = new cls_EstadoCurso_BLL();
        #endregion
        #region Métodos

        #region Cargar
        private void CargarEstadosCursos()
        {


            #region Variables
            string sMsjError = string.Empty;
            DataTable dtEstadoCurso = new DataTable();
            #endregion

            obj_EstadoCurso_BLL.Listar_EstadoCurso(ref dtEstadoCurso, ref sMsjError);

            if (sMsjError == string.Empty)
            {
                dgv_EstadoCurso.ItemsSource = null;
                dgv_EstadoCurso.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtEstadoCurso });

            }
            else
            {
                dgv_EstadoCurso.ItemsSource = null;
                MessageBox.Show("Se presento un error al listar Estados.\n\nDesc.Error ="
                                 + sMsjError, "Error al Listar", MessageBoxButton.OK, MessageBoxImage.Error);

                dgv_EstadoCurso.ItemsSource = null;
            }
        }
        #endregion
        #endregion


        private void btn_Listar_Click(object sender, RoutedEventArgs e)
        {
            CargarEstadosCursos();
        }
    }
}
