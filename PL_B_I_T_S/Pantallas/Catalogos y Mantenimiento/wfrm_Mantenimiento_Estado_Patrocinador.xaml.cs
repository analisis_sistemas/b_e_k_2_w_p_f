﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;
using BLL_B_I_T_S.Catalogos_y_Mantenimiento;
using System.Data;

namespace PL_B_I_T_S.Pantallas.Catalogos_y_Mantenimineto
{
    /// <summary>
    /// Lógica de interacción para wfrm_Mantenimiento_Estado_Patrocinador.xaml
    /// </summary>
    public partial class wfrm_Mantenimiento_Estado_Patrocinador : MetroWindow
    {
        public wfrm_Mantenimiento_Estado_Patrocinador()
        {
            InitializeComponent();
        }

        #region Variables Globales
        cls_Estado_Patrocinador_DAL obj_Estado_DAL = new cls_Estado_Patrocinador_DAL();
        cls_EstadoPatrocinador_BLL obj_Estado_BLL = new cls_EstadoPatrocinador_BLL();
        #endregion
        #region Métodos

        #region Cargar
        private void CargarEstados()
        {


            #region Variables
            string sMsjError = string.Empty;
            DataTable dtEstado = new DataTable();
            #endregion

            obj_Estado_BLL.Listar_EstadoPatrocinado(ref dtEstado, ref sMsjError);

            if (sMsjError == string.Empty)
            {
                dgv_EstadoPatrocinador.ItemsSource = null;
                dgv_EstadoPatrocinador.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtEstado });

            }
            else
            {
                dgv_EstadoPatrocinador.ItemsSource = null;
                MessageBox.Show("Se presento un error al listar Estados.\n\nDesc.Error ="
                                 + sMsjError, "Error al Listar", MessageBoxButton.OK, MessageBoxImage.Error);

                dgv_EstadoPatrocinador.ItemsSource = null;
            }
        }
        #endregion
        #endregion

        private void btn_Listar_Click(object sender, RoutedEventArgs e)
        {
            CargarEstados();
        }
    }
}
