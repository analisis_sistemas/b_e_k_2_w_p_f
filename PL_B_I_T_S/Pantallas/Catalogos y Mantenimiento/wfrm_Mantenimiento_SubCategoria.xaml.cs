﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using System.Data;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;
using BLL_B_I_T_S.Catalogos_y_Mantenimiento;


namespace PL_B_I_T_S.Pantallas.Catalogos_y_Mantenimineto
{
    /// <summary>
    /// Lógica de interacción para wfrm_Mantenimiento_SubCategoria.xaml
    /// </summary>
    public partial class wfrm_Mantenimiento_SubCategoria : MetroWindow
    {

        cls_Sub_Categoria_DAL obj_SubCategoria_DAL = new cls_Sub_Categoria_DAL();
        cls_SubCategoria_BLL obj_SubCategoria_BLL = new cls_SubCategoria_BLL();

        public wfrm_Mantenimiento_SubCategoria()
        {
            InitializeComponent();
        }



        private void btn_Listar_Click(object sender, RoutedEventArgs e)
        {
            CargarCategoria();
        }

        private void btn_Eliminar_Click(object sender, RoutedEventArgs e)
        {
            Eliminar_Categoria();
        }

        private void btn_Modificar_Click(object sender, RoutedEventArgs e)
        {
            Modificar_Categoria();
        }

        private void btn_Nuevo_Click(object sender, RoutedEventArgs e)
        {
            Crear();
        }



        #region Métodos

        #region Cargar
        private void CargarCategoria()
        {


            #region Variables
            string sMsjError = string.Empty;
            DataTable dtSubCategoria = new DataTable();
            #endregion

            obj_SubCategoria_BLL.Listar_SubCategoria(ref dtSubCategoria, ref sMsjError);

            if (sMsjError == string.Empty)
            {
                dgv_SubCategoria.ItemsSource = null;
                dgv_SubCategoria.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtSubCategoria });

            }
            else
            {
                dgv_SubCategoria.ItemsSource = null;
                MessageBox.Show("Se presento un error al listar SubCategoria.\n\nDesc.Error ="
                                 + sMsjError, "Error al Listar", MessageBoxButton.OK, MessageBoxImage.Error);

                dgv_SubCategoria.ItemsSource = null;
            }
        }
        #endregion

        #region Filtrar
        public void Filtrar_Categoria()
        {
            if (txt_Filtro.Text.Trim() == string.Empty)
            {
                CargarCategoria();
            }
            else
            {
                #region Variables
                DataTable dtSubCategoria = new DataTable();
                string sMsjError = string.Empty;

                #endregion



                obj_SubCategoria_BLL.Filtrar_SubCategoria(ref dtSubCategoria, Convert.ToInt32(txt_Filtro.Text.Trim()), ref sMsjError);//Filtro se convierte en int solo cuando 

                if (sMsjError == string.Empty)
                {
                    dgv_SubCategoria.ItemsSource = null;
                    dgv_SubCategoria.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtSubCategoria });
                }
                else
                {
                    dgv_SubCategoria.ItemsSource = null;
                    MessageBox.Show("Se presento un error al filtrar Activos.\n\nDesc.Error ="
                                     + sMsjError, "Error al filtrar", MessageBoxButton.OK, MessageBoxImage.Error);

                    dgv_SubCategoria.ItemsSource = null;
                }

            }
        }

        #endregion

        #region Eliminar

        public void Eliminar_Categoria()
        {
            #region Variables
            string sMjError = string.Empty;
            #endregion

            if (dgv_SubCategoria.SelectedItems.Count != 0)
            {
                if (MessageBox.Show("Desea Eliminar Realmente el Registro Seleccionado?", "Alerta", MessageBoxButton.YesNo,
                                      MessageBoxImage.Asterisk) == MessageBoxResult.Yes)
                {

                    DataRowView drv = (DataRowView)dgv_SubCategoria.SelectedItems[0];
                    int index = dgv_SubCategoria.SelectedCells[0].Column.DisplayIndex;//4 xq en este caso el id esta en la celda 4
                    string sPK = drv.Row.ItemArray[index].ToString();

                    obj_SubCategoria_BLL.Eliminar_SubCategoria(sPK, ref sMjError);

                    if (sMjError != string.Empty)
                    {
                        MessageBox.Show("Se presentó un error al eliminar el registro.\n\nDesc.Error =" + sMjError, "Error al Eliminar",
                                        MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    else
                    {
                        CargarCategoria();

                        MessageBox.Show("Se eliminó correctamente la Categoría Seleccionada.",
                                        "Fin Éxitoso", MessageBoxButton.OK, MessageBoxImage.Information);

                    }
                }

            }
            else
            {
                MessageBox.Show("No se puede eliminar ninguna SubCategoría ya que no hay nada seleccionado.", "Alerta", MessageBoxButton.OK,
                                 MessageBoxImage.Asterisk);
            }
        }

        #endregion 

        #region Modificar

        public void Modificar_Categoria()
        {

            Pantallas.Modificar.wfrm_Modificar_Subcategorias pant_Modificar = new Modificar.wfrm_Modificar_Subcategorias();

            cls_Sub_Categoria_DAL obj_SubCategoria_DAL = new cls_Sub_Categoria_DAL();
            obj_SubCategoria_DAL.cBandAxion = 'U';

            if (dgv_SubCategoria.Items.Count > 0)
            {
                if (dgv_SubCategoria.SelectedIndex > -1)
                {
                    DataRowView drv = (DataRowView)dgv_SubCategoria.SelectedItems[0];

                    int IdSubCategoria = dgv_SubCategoria.SelectedCells[0].Column.DisplayIndex;
                    obj_SubCategoria_DAL.iIdSubcategoria = Convert.ToInt32(drv.Row.ItemArray[IdSubCategoria].ToString());

                    int NombreSubCategoria = dgv_SubCategoria.SelectedCells[1].Column.DisplayIndex;
                    obj_SubCategoria_DAL.sNombre = (drv.Row.ItemArray[NombreSubCategoria].ToString());

                    int Porcentaje = dgv_SubCategoria.SelectedCells[2].Column.DisplayIndex;
                    obj_SubCategoria_DAL.iPorcentaje = Convert.ToInt32(drv.Row.ItemArray[Porcentaje].ToString());

                    pant_Modificar.obj_SubCategoria_DAL = obj_SubCategoria_DAL;
                    pant_Modificar.ShowDialog();
                }
                else
                {
                    MessageBox.Show("No puede editar valores ya que no hay nada Seleccionado", "Alerta", MessageBoxButton.OK, MessageBoxImage.Asterisk);

                }
            }
            else
            {
                MessageBox.Show("No puede editar valores ya que no existen Datos", "Alerta", MessageBoxButton.OK, MessageBoxImage.Asterisk);
            }
        }

        #endregion

        #region Crear

        public void Crear()
        {
            Pantallas.Modificar.wfrm_Modificar_Subcategorias PantModificar = new Modificar.wfrm_Modificar_Subcategorias();


            cls_Sub_Categoria_DAL obj_SubCategoria_DAL = new cls_Sub_Categoria_DAL();
            obj_SubCategoria_DAL.cBandAxion = 'I';
            PantModificar.obj_SubCategoria_DAL = obj_SubCategoria_DAL;
            PantModificar.ShowDialog();
        }


        #endregion
        #endregion


    }
}
