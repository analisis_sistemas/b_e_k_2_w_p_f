﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using System.Data;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;
using BLL_B_I_T_S.Catalogos_y_Mantenimiento;
using PL_B_I_T_S.Pantallas.Módulos;

namespace PL_B_I_T_S.Pantallas.Catalogos_y_Mantenimineto
{
    /// <summary>
    /// Lógica de interacción para wfrm_Mantenimiento_Respuestas.xaml
    /// </summary>
    public partial class wfrm_Mantenimiento_Respuestas : MetroWindow
    {
        public wfrm_Mantenimiento_Respuestas()
        {
            InitializeComponent();
        }

        #region Variables Globales
        cls_Respuestas_DAL obj_Respuestas_DAL = new cls_Respuestas_DAL();
        cls_Respuestas_BLL obj_Respuestas_BLL = new cls_Respuestas_BLL();
        #endregion

        #region Métodos

        #region Cargar
        private void Cargar()
        {


            #region Variables
            string sMsjError = string.Empty;
            DataTable dtRespuestas = new DataTable();
            #endregion

            obj_Respuestas_BLL.Listar_Respuestas(ref dtRespuestas, ref sMsjError);

            if (sMsjError == string.Empty)
            {
                dgv_Respuestas.ItemsSource = null;
                dgv_Respuestas.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtRespuestas });

            }
            else
            {
                dgv_Respuestas.ItemsSource = null;
                MessageBox.Show("Se presento un error al listar Preguntas.\n\nDesc.Error ="
                                 + sMsjError, "Error al Listar", MessageBoxButton.OK, MessageBoxImage.Error);

                dgv_Respuestas.ItemsSource = null;
            }
        }
        #endregion

        #region Filtrar
        public void Filtrar()
        {
            if (txt_Filtro.Text.Trim() == string.Empty)
            {
                Cargar();
            }
            else
            {
                #region Variables
                DataTable dtRespuestas = new DataTable();
                string sMsjError = string.Empty;

                #endregion



                obj_Respuestas_BLL.Filtrar_Respuestas(ref dtRespuestas, Convert.ToInt32(txt_Filtro.Text.Trim()), ref sMsjError);//Filtro se convierte en int solo cuando 

                if (sMsjError == string.Empty)
                {
                    dgv_Respuestas.ItemsSource = null;
                    dgv_Respuestas.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtRespuestas });
                }
                else
                {
                    dgv_Respuestas.ItemsSource = null;
                    MessageBox.Show("Se presento un error al filtrar Cursos.\n\nDesc.Error ="
                                     + sMsjError, "Error al filtrar", MessageBoxButton.OK, MessageBoxImage.Error);

                    dgv_Respuestas.ItemsSource = null;
                }

            }
        }

        #endregion
        #endregion

      

        private void btn_Listar_Click(object sender, RoutedEventArgs e)
        {
            Cargar();
        }
    }
}
