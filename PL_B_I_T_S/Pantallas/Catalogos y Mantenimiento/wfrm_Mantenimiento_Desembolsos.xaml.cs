﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using System.Data;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;
using BLL_B_I_T_S.Catalogos_y_Mantenimiento;
using PL_B_I_T_S.Pantallas.Módulos;

namespace PL_B_I_T_S.Pantallas.Catalogos_y_Mantenimineto
{
    /// <summary>
    /// Interaction logic for wfrm_Mantenimiento_Desembolsos.xaml
    /// </summary>
    public partial class wfrm_Mantenimiento_Desembolsos : MetroWindow
    {
        public wfrm_Mantenimiento_Desembolsos()
        {
            InitializeComponent();
        }

        #region Variables Globales
        cls_Desembolsos_DAL obj_Desembolsos_DAL = new cls_Desembolsos_DAL();
        cls_Desembolsos_BLL obj_Desembolsos_BLL = new cls_Desembolsos_BLL();
        #endregion

        #region Métodos

        #region Cargar
        private void Cargar()
        {


            #region Variables
            string sMsjError = string.Empty;
            DataTable dtDesembolsos = new DataTable();
            #endregion

            obj_Desembolsos_BLL.Listar_Desembolsos(ref dtDesembolsos, ref sMsjError);

            if (sMsjError == string.Empty)
            {
                dgv_Beca.ItemsSource = null;
                dgv_Beca.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtDesembolsos });

            }
            else
            {
                dgv_Beca.ItemsSource = null;
                MessageBox.Show("Se presento un error al listar Desembolsos.\n\nDesc.Error ="
                                 + sMsjError, "Error al Listar", MessageBoxButton.OK, MessageBoxImage.Error);

                dgv_Beca.ItemsSource = null;
            }
        }
        #endregion

        #region Filtrar
        public void Filtrars()
        {
            if (txt_Filtro.Text.Trim() == string.Empty)
            {
                Cargar();
            }
            else
            {
                #region Variables
                DataTable dtDesembolsos = new DataTable();
                string sMsjError = string.Empty;

                #endregion



                obj_Desembolsos_BLL.Filtrar_Desembolsos(ref dtDesembolsos, Convert.ToInt32(txt_Filtro.Text.Trim()), ref sMsjError);//Filtro se convierte en int solo cuando 

                if (sMsjError == string.Empty)
                {
                    dgv_Beca.ItemsSource = null;
                    dgv_Beca.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtDesembolsos });
                }
                else
                {
                    dgv_Beca.ItemsSource = null;
                    MessageBox.Show("Se presento un error al filtrar Desembolsos.\n\nDesc.Error ="
                                     + sMsjError, "Error al filtrar", MessageBoxButton.OK, MessageBoxImage.Error);

                    dgv_Beca.ItemsSource = null;
                }

            }
        }

        #endregion

        #region Eliminar

        public void Eliminar()
        {
            #region Variables
            string sMjError = string.Empty;
            #endregion

            if (dgv_Beca.Items.Count >= 1)
            {
                if (MessageBox.Show("Desea Eliminar Realmente el Registro Seleccionado?", "Alerta", MessageBoxButton.YesNo,
                                      MessageBoxImage.Asterisk) == MessageBoxResult.Yes)
                {

                    DataRowView drv = (DataRowView)dgv_Beca.SelectedItems[0];
                    int index = dgv_Beca.SelectedCells[0].Column.DisplayIndex;//4 xq en este caso el id esta en la celda 4
                    string sPK = drv.Row.ItemArray[index].ToString();

                    obj_Desembolsos_BLL.Eliminar_Desembolso(sPK, ref sMjError);

                    if (sMjError != string.Empty)
                    {
                        MessageBox.Show("Se presentó un error al eliminar el registro.\n\nDesc.Error =" + sMjError, "Error al Eliminar",
                                        MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    else
                    {
                        Cargar();

                        MessageBox.Show("Se eliminó correctamente el regiatro Seleccionado.",
                                        "Fin Éxitoso", MessageBoxButton.OK, MessageBoxImage.Information);

                    }
                }

            }
            else
            {
                MessageBox.Show("No se puede eliminar ningún registro ya que no hay nada seleccionado.", "Alerta", MessageBoxButton.OK,
                                 MessageBoxImage.Asterisk);
            }
        }

        #endregion //No se va a eliminar Estudiantes

        #region Modificar

        public void Modificar()
        {

            Pantallas.Modificar.wfrm_Modificar_Desembolsos pant_Modificar = new Modificar.wfrm_Modificar_Desembolsos();

            cls_Desembolsos_DAL obj_Desembolsos_DAL = new cls_Desembolsos_DAL();
            obj_Desembolsos_DAL.cBandAxion = 'U';

            if (dgv_Beca.Items.Count > 0)
            {
                if (dgv_Beca.SelectedIndex > -1)
                {
                    DataRowView drv = (DataRowView)dgv_Beca.SelectedItems[0];

                    int IdDesembolso = dgv_Beca.SelectedCells[0].Column.DisplayIndex;
                    obj_Desembolsos_DAL.iIdDesembolso = Convert.ToInt32(drv.Row.ItemArray[IdDesembolso].ToString());

                    int IdEstudiante = dgv_Beca.SelectedCells[1].Column.DisplayIndex;
                    obj_Desembolsos_DAL.iIdEstudiante = Convert.ToInt32(drv.Row.ItemArray[IdEstudiante].ToString());

                    int MontoDesembolsado = dgv_Beca.SelectedCells[2].Column.DisplayIndex;
                    obj_Desembolsos_DAL.dMonto_Desembolsado = Convert.ToInt64(drv.Row.ItemArray[MontoDesembolsado].ToString());

                    int FechaDesembolso = dgv_Beca.SelectedCells[3].Column.DisplayIndex;
                    obj_Desembolsos_DAL.dtFecha_Desembolso = Convert.ToDateTime(drv.Row.ItemArray[FechaDesembolso].ToString());

                    int IdEstado = dgv_Beca.SelectedCells[4].Column.DisplayIndex;
                    obj_Desembolsos_DAL.iIdEstado = Convert.ToInt32(drv.Row.ItemArray[IdEstado].ToString());

                    int IdFactura = dgv_Beca.SelectedCells[5].Column.DisplayIndex;
                    obj_Desembolsos_DAL.iIdFactura = Convert.ToInt32(drv.Row.ItemArray[IdFactura].ToString());


                    pant_Modificar.obj_Desembolsos_DAL = obj_Desembolsos_DAL;

                    pant_Modificar.ShowDialog();
                }
                else
                {
                    MessageBox.Show("No puede editar valores ya que no hay nada Seleccionado", "Alerta", MessageBoxButton.OK, MessageBoxImage.Asterisk);

                }
            }
            else
            {
                MessageBox.Show("No puede editar valores ya que no existen Datos", "Alerta", MessageBoxButton.OK, MessageBoxImage.Asterisk);
            }
        }

        #endregion

        #region Crear

        public void Crear()
        {
            Pantallas.Modificar.wfrm_Modificar_Desembolsos PantModificar = new Modificar.wfrm_Modificar_Desembolsos();


            cls_Desembolsos_DAL obj_Desembolsos_DAL = new cls_Desembolsos_DAL();
            obj_Desembolsos_DAL.cBandAxion = 'I';
            PantModificar.obj_Desembolsos_DAL = obj_Desembolsos_DAL;
            PantModificar.ShowDialog();
        }


        #endregion

        #endregion




        private void btn_Listar_Click(object sender, RoutedEventArgs e)
        {
            Cargar();
        }

        private void btn_Eliminar_Click(object sender, RoutedEventArgs e)
        {
            Eliminar();
        }

        private void btn_Modificar_Click(object sender, RoutedEventArgs e)
        {
            Modificar();
        }

        private void btn_Nuevo_Click(object sender, RoutedEventArgs e)
        {
            Crear();
        }

        private void btn_Listar_Click_1(object sender, RoutedEventArgs e)
        {

        }


       
    }
}
