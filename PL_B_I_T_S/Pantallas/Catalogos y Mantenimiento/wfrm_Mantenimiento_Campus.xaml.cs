﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;
using BLL_B_I_T_S.Catalogos_y_Mantenimiento;
using System.Data;

namespace PL_B_I_T_S.Pantallas.Catalogos_y_Mantenimineto
{
    /// <summary>
    /// Interaction logic for wfrm_Mantenimiento_Campus.xaml
    /// </summary>
    public partial class wfrm_Mantenimiento_Campus : MetroWindow
    {


        #region Variables Globales
        cls_Campus_DAL obj_campus_DAL = new cls_Campus_DAL();
        cls_Campus_BLL obj_campus_BLL = new cls_Campus_BLL();
        #endregion
        
        public wfrm_Mantenimiento_Campus()
        {
            InitializeComponent();
        }


        #region Métodos

        #region Cargar
        private void Cargar()
        {


            #region Variables
            string sMsjError = string.Empty;
            DataTable dtCampus = new DataTable();
            #endregion

            obj_campus_BLL.Listar_Campus(ref dtCampus, ref sMsjError);

            if (sMsjError == string.Empty)
            {
                dgv_Campus.ItemsSource = null;
                dgv_Campus.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtCampus });

            }
            else
            {
                dgv_Campus.ItemsSource = null;
                MessageBox.Show("Se presento un error al listar los campus.\n\nDesc.Error ="
                                 + sMsjError, "Error al Listar", MessageBoxButton.OK, MessageBoxImage.Error);

                dgv_Campus.ItemsSource = null;
            }
        }
        #endregion



        #region Eliminar

        public void Eliminar()
        {
            #region Variables
            string sMjError = string.Empty;
            #endregion

            if (dgv_Campus.SelectedItems.Count != 0)
            {
                if (MessageBox.Show("Desea Eliminar Realmente el Registro Seleccionado?", "Alerta", MessageBoxButton.YesNo,
                                      MessageBoxImage.Asterisk) == MessageBoxResult.Yes)
                {

                    DataRowView drv = (DataRowView)dgv_Campus.SelectedItems[0];
                    int index = dgv_Campus.SelectedCells[0].Column.DisplayIndex;//0 porque el id esta en la celda 0
                    string sPK = drv.Row.ItemArray[index].ToString();

                    obj_campus_BLL.Eliminar_Campus(Convert.ToInt32(sPK), ref sMjError);

                    if (sMjError != string.Empty)
                    {
                        MessageBox.Show("Se presentó un error al eliminar el registro.\n\nDesc.Error =" + sMjError, "Error al Eliminar",
                                        MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    else
                    {
                        Cargar();

                        MessageBox.Show("Se eliminó correctamente el campus seleccionado.",
                                        "Fin Éxitoso", MessageBoxButton.OK, MessageBoxImage.Information);

                    }
                }

            }
            else
            {
                MessageBox.Show("No se puede eliminar el campus ya que no hay nada seleccionado.", "Alerta", MessageBoxButton.OK,
                                 MessageBoxImage.Asterisk);
            }
        }

        #endregion

        #region Modificar

        public void Modificar()
        {

            Pantallas.Modificar.wfrm_Modificar_Campus pant_Modificar = new Modificar.wfrm_Modificar_Campus();

            cls_Campus_DAL obj_campus_DAL = new cls_Campus_DAL();
            obj_campus_DAL.cBandAxion = 'U';

            if (dgv_Campus.Items.Count > 0)
            {
                if (dgv_Campus.SelectedIndex > -1)
                {
                    DataRowView drv = (DataRowView)dgv_Campus.SelectedItems[0];

                    int Idcampus = dgv_Campus.SelectedCells[0].Column.DisplayIndex;
                    obj_campus_DAL.iIdCampus = Convert.ToInt16(drv.Row.ItemArray[Idcampus].ToString());

                    int Nombre = dgv_Campus.SelectedCells[1].Column.DisplayIndex;
                    obj_campus_DAL.sNombre = drv.Row.ItemArray[Nombre].ToString();

                    int IdInstitucion = dgv_Campus.SelectedCells[2].Column.DisplayIndex;
                    obj_campus_DAL.sIDInstitucion = Convert.ToInt16(drv.Row.ItemArray[IdInstitucion].ToString());

                    pant_Modificar.obj_campus_DAL = obj_campus_DAL;

                    pant_Modificar.ShowDialog();
                }
                else
                {
                    MessageBox.Show("No puede editar valores ya que no hay nada Seleccionado", "Alerta", MessageBoxButton.OK, MessageBoxImage.Asterisk);

                }
            }
            else
            {
                MessageBox.Show("No puede editar valores ya que no existen Datos", "Alerta", MessageBoxButton.OK, MessageBoxImage.Asterisk);
            }
        }

        #endregion

        #region Crear

        public void Crear()
        {
            Pantallas.Modificar.wfrm_Modificar_Campus PantModificar = new Modificar.wfrm_Modificar_Campus();


            cls_Campus_DAL obj_campus_DAL = new cls_Campus_DAL();
            obj_campus_DAL.cBandAxion = 'I';
            PantModificar.obj_campus_DAL = obj_campus_DAL;
            PantModificar.ShowDialog();
        }


        #endregion

        private void btn_Nuevo_Click(object sender, RoutedEventArgs e)
        {
            Crear();
        }

        #endregion

        private void btn_Listar_Click(object sender, RoutedEventArgs e)
        {
            Cargar();
        }

        private void btn_Eliminar_Click(object sender, RoutedEventArgs e)
        {
            Eliminar();
        }

        private void btn_Modificar_Click(object sender, RoutedEventArgs e)
        {
            Modificar();
        }


    }
}
