﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using System.Data;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;
using BLL_B_I_T_S.Catalogos_y_Mantenimiento;
using PL_B_I_T_S.Pantallas.Módulos;

namespace PL_B_I_T_S.Pantallas.Catalogos_y_Mantenimineto
{
    /// <summary>
    /// Lógica de interacción para wfrm_Mantenimiento_Facturas.xaml
    /// </summary>
    public partial class wfrm_Mantenimiento_Facturas : MetroWindow
    {

        #region Variables Globales
        cls_Facturas_DAL obj_Facturas_DAL = new cls_Facturas_DAL();
        cls_Facturas_BLL obj_Facturas_BLL = new cls_Facturas_BLL();
        #endregion

        public wfrm_Mantenimiento_Facturas()
        {
            InitializeComponent();
        }


        #region Métodos

        #region Cargar
        private void Cargar()
        {


            #region Variables
            string sMsjError = string.Empty;
            DataTable dtFactura = new DataTable();
            #endregion

            obj_Facturas_BLL.Listar_Facturas(ref dtFactura, ref sMsjError);

            if (sMsjError == string.Empty)
            {
                dgv_Factura.ItemsSource = null;
                dgv_Factura.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtFactura });

            }
            else
            {
                dgv_Factura.ItemsSource = null;
                MessageBox.Show("Se presento un error al listar Facturas.\n\nDesc.Error ="
                                 + sMsjError, "Error al Listar", MessageBoxButton.OK, MessageBoxImage.Error);

                dgv_Factura.ItemsSource = null;
            }
        }
        #endregion

        #region Filtrar
        public void Filtrar()
        {
            if (txt_FiltroFactura.Text.Trim() == string.Empty)
            {
                Cargar();
            }
            else
            {
                #region Variables
                DataTable dtFactura = new DataTable();
                string sMsjError = string.Empty;

                #endregion



                obj_Facturas_BLL.Filtrar_Facturas(ref dtFactura, Convert.ToInt32(txt_FiltroFactura.Text.Trim()), ref sMsjError);//Filtro se convierte en int solo cuando 

                if (sMsjError == string.Empty)
                {
                    dgv_Factura.ItemsSource = null;
                    dgv_Factura.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtFactura });
                }
                else
                {
                    dgv_Factura.ItemsSource = null;
                    MessageBox.Show("Se presento un error al filtrar Facturas.\n\nDesc.Error ="
                                     + sMsjError, "Error al filtrar", MessageBoxButton.OK, MessageBoxImage.Error);

                    dgv_Factura.ItemsSource = null;
                }

            }
        }

        #endregion


        #region Modificar

        public void Modificar()
        {

            Pantallas.Modificar.wfrm_Modificar_Facturas pant_Modificar = new Modificar.wfrm_Modificar_Facturas();

            cls_Facturas_DAL obj_Facturas_DAL = new cls_Facturas_DAL();
            obj_Facturas_DAL.cBandAxion = 'U';

            if (dgv_Factura.Items.Count > 0)
            {
                if (dgv_Factura.SelectedIndex > -1)
                {
                    DataRowView drv = (DataRowView)dgv_Factura.SelectedItems[0];

                    int IdFactura = dgv_Factura.SelectedCells[0].Column.DisplayIndex;
                    obj_Facturas_DAL.iIdFactura = Convert.ToInt32(drv.Row.ItemArray[IdFactura].ToString());

                    int Cant_Estudiantes = dgv_Factura.SelectedCells[1].Column.DisplayIndex;
                    obj_Facturas_DAL.iCantidadEstudiantes = Convert.ToInt32(drv.Row.ItemArray[Cant_Estudiantes].ToString());

                    int Monto= dgv_Factura.SelectedCells[2].Column.DisplayIndex;
                    obj_Facturas_DAL.dMonto = Convert.ToDouble(drv.Row.ItemArray[Monto].ToString());

                    int FechaCobro = dgv_Factura.SelectedCells[3].Column.DisplayIndex;
                    obj_Facturas_DAL.dtfechaCobro = Convert.ToDateTime(drv.Row.ItemArray[FechaCobro].ToString());

                    int FechaPago = dgv_Factura.SelectedCells[4].Column.DisplayIndex;
                    obj_Facturas_DAL.dtfechaPago = Convert.ToDateTime(drv.Row.ItemArray[FechaPago].ToString());

                    int Compro_Pago = dgv_Factura.SelectedCells[5].Column.DisplayIndex;
                    obj_Facturas_DAL.iComprobantePago = Convert.ToInt32(drv.Row.ItemArray[Compro_Pago].ToString());

                    int IdEstado = dgv_Factura.SelectedCells[6].Column.DisplayIndex;
                    obj_Facturas_DAL.iIdEstado = Convert.ToInt32(drv.Row.ItemArray[IdEstado].ToString());

                    int id_Patrocinador = dgv_Factura.SelectedCells[7].Column.DisplayIndex;
                    obj_Facturas_DAL.iIdPatrocinador = Convert.ToInt32(drv.Row.ItemArray[id_Patrocinador].ToString());

                    

                    //pant_Modificar.obj_Facturas_DAL = obj_Facturas_DAL;    falta una pantalla

                    pant_Modificar.ShowDialog();
                }
                else
                {
                    MessageBox.Show("No puede editar valores ya que no hay nada Seleccionado", "Alerta", MessageBoxButton.OK, MessageBoxImage.Asterisk);

                }
            }
            else
            {
                MessageBox.Show("No puede editar valores ya que no existen Datos", "Alerta", MessageBoxButton.OK, MessageBoxImage.Asterisk);
            }
        }

        #endregion

        #region Crear

        public void Crear()
        {
            Pantallas.Modificar.wfrm_Modificar_Facturas PantModificar = new Modificar.wfrm_Modificar_Facturas();


            cls_Facturas_DAL obj_Facturas_DAL = new cls_Facturas_DAL();
            obj_Facturas_DAL.cBandAxion = 'I';
            //PantModificar.obj_Facturas_DAL = obj_Facturas_DAL;   Falta Pantalla Modificar.
            PantModificar.ShowDialog();
        }


        #endregion

        #endregion




        private void btn_ListarFactura_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btn_EliminarFactura_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btn_ModificarFactura_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btn_NuevoFactura_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
