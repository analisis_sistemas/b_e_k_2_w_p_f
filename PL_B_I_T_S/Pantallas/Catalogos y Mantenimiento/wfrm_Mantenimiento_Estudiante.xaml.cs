﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using System.Data;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;
using BLL_B_I_T_S.Catalogos_y_Mantenimiento;
using PL_B_I_T_S.Pantallas.Módulos;

namespace PL_B_I_T_S.Pantallas.Catalogos_y_Mantenimineto
{
    /// <summary>
    /// Interaction logic for wfrm_Mantenimiento_Estudiante.xaml
    /// </summary>
    public partial class wfrm_Mantenimiento_Estudiante : MetroWindow
    {

        #region Variables Globales
        cls_Estudiante_DAL obj_Estudiante_DAL = new cls_Estudiante_DAL();
        cls_Estudiante_BLL obj_Estudiante_BLL = new cls_Estudiante_BLL();
        #endregion

        public wfrm_Mantenimiento_Estudiante()
        {
            InitializeComponent();
        }

        #region Métodos

        #region Cargar
        private void CargarEstudiantes()
        {
            #region Variables
            string sMsjError = string.Empty;
            DataTable dtEstudiante = new DataTable();
            #endregion

            obj_Estudiante_BLL.Listar_Estudiante(ref dtEstudiante, ref sMsjError);

            if (sMsjError == string.Empty)
            {
                dgv_Estudiantes.ItemsSource = null;
                dgv_Estudiantes.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtEstudiante });

            }
            else
            {
                dgv_Estudiantes.ItemsSource = null;
                MessageBox.Show("Se presento un error al listar Activos.\n\nDesc.Error ="
                                 + sMsjError, "Error al Listar", MessageBoxButton.OK, MessageBoxImage.Error);

                dgv_Estudiantes.ItemsSource = null;
            }
        }
        #endregion

        #region Filtrar
        public void Filtrar_Estudiantes()
        {
            if (txt_FiltroEstudiante.Text.Trim() == string.Empty)
            {
                CargarEstudiantes();
            }
            else
            {
                #region Variables
                DataTable dtEstudiante = new DataTable();
                string sMsjError = string.Empty;

                #endregion



                obj_Estudiante_BLL.Filtrar_Estudiante(ref dtEstudiante, Convert.ToInt32(txt_FiltroEstudiante.Text.Trim()), ref sMsjError);//Filtro se convierte en int solo cuando 

                if (sMsjError == string.Empty)
                {
                    dgv_Estudiantes.ItemsSource = null;
                    dgv_Estudiantes.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtEstudiante });
                }
                else
                {
                    dgv_Estudiantes.ItemsSource = null;
                    MessageBox.Show("Se presento un error al filtrar Activos.\n\nDesc.Error ="
                                     + sMsjError, "Error al filtrar", MessageBoxButton.OK, MessageBoxImage.Error);

                    dgv_Estudiantes.ItemsSource = null;
                }

            }
        }

        #endregion

        #region Eliminar

        //public void Eliminar_Estudiantes()
        //{
        //    #region Variables
        //    string sMjError = string.Empty;
        //    #endregion

        //    if (dgv_Estudiantes.Items.Count >= 1)
        //    {
        //        if (MessageBox.Show("Desea Eliminar Realmente el Registro Seleccionado?", "Alerta", MessageBoxButton.YesNo,
        //                              MessageBoxImage.Asterisk) == MessageBoxResult.Yes)
        //        {

        //            DataRowView drv = (DataRowView)dgv_Estudiantes.SelectedItems[0];
        //            int index = dgv_Estudiantes.SelectedCells[4].Column.DisplayIndex;//4 xq en este caso el id esta en la celda 4
        //            string sPK = drv.Row.ItemArray[index].ToString();

        //            obj_Estudiante_BLL.Eliminar_Estudiante(sPK, ref sMjError);

        //            if (sMjError != string.Empty)
        //            {
        //                MessageBox.Show("Se presentó un error al eliminar el registro.\n\nDesc.Error =" + sMjError, "Error al Eliminar",
        //                                MessageBoxButton.OK, MessageBoxImage.Error);
        //            }
        //            else
        //            {
        //                CargarEstudiantes();

        //                MessageBox.Show("Se eliminó correctamente el Estudiante Seleccionado.",
        //                                "Fin Éxitoso", MessageBoxButton.OK, MessageBoxImage.Information);

        //            }
        //        }

        //    }
        //    else
        //    {
        //        MessageBox.Show("No se puede eliminar ningún Estudiante ya que no hay nada seleccionado.", "Alerta", MessageBoxButton.OK,
        //                         MessageBoxImage.Asterisk);
        //    }
        //}

        #endregion //No se va a eliminar Estudiantes

        #region Modificar

        public void Modificar_Estudiante()
        {

            Pantallas.Modificar.wfrm_Modificar_ExpEstudiante pant_Modificar = new Modificar.wfrm_Modificar_ExpEstudiante();

            cls_Estudiante_DAL obj_Estudiante_DAL = new cls_Estudiante_DAL();
            obj_Estudiante_DAL.cBandAxion = 'U';

            if (dgv_Estudiantes.Items.Count > 0)
            {
                if (dgv_Estudiantes.SelectedIndex > -1)
                {
                    DataRowView drv = (DataRowView)dgv_Estudiantes.SelectedItems[0];

                    int IdEstudiante = dgv_Estudiantes.SelectedCells[0].Column.DisplayIndex;
                    obj_Estudiante_DAL.iIdEstudiante = Convert.ToInt32(drv.Row.ItemArray[IdEstudiante].ToString());

                    int Nombre = dgv_Estudiantes.SelectedCells[1].Column.DisplayIndex;
                    obj_Estudiante_DAL.sNombre = drv.Row.ItemArray[Nombre].ToString();

                    int App1 = dgv_Estudiantes.SelectedCells[2].Column.DisplayIndex;
                    obj_Estudiante_DAL.sPrimerApellido = drv.Row.ItemArray[App1].ToString();

                    int App2 = dgv_Estudiantes.SelectedCells[3].Column.DisplayIndex;
                    obj_Estudiante_DAL.sSegundoApellido = drv.Row.ItemArray[App2].ToString();

                    int Identificacion = dgv_Estudiantes.SelectedCells[4].Column.DisplayIndex;
                    obj_Estudiante_DAL.sIdentificacion = drv.Row.ItemArray[Identificacion].ToString();

                    int Direccion = dgv_Estudiantes.SelectedCells[5].Column.DisplayIndex;
                    obj_Estudiante_DAL.sDireccion = drv.Row.ItemArray[Direccion].ToString();

                    int Telefono = dgv_Estudiantes.SelectedCells[6].Column.DisplayIndex;
                    obj_Estudiante_DAL.iTelefono = Convert.ToInt32(drv.Row.ItemArray[Telefono].ToString());

                    int FechaNacimiento = dgv_Estudiantes.SelectedCells[7].Column.DisplayIndex;
                    obj_Estudiante_DAL.dFechaNacimiento = Convert.ToDateTime(drv.Row.ItemArray[FechaNacimiento].ToString());

                    int Genero = dgv_Estudiantes.SelectedCells[8].Column.DisplayIndex;
                    obj_Estudiante_DAL.cGenero = Convert.ToChar(drv.Row.ItemArray[Genero].ToString().Trim());

                    int Nacionalidad = dgv_Estudiantes.SelectedCells[9].Column.DisplayIndex;
                    obj_Estudiante_DAL.sNacionalidad = drv.Row.ItemArray[Nacionalidad].ToString();

                    int Correo = dgv_Estudiantes.SelectedCells[10].Column.DisplayIndex;
                    obj_Estudiante_DAL.sCorreo = drv.Row.ItemArray[Correo].ToString();

                    int Nivel = dgv_Estudiantes.SelectedCells[11].Column.DisplayIndex;
                    obj_Estudiante_DAL.sNivel_Academico= drv.Row.ItemArray[Nivel].ToString();

                    int Grado = dgv_Estudiantes.SelectedCells[12].Column.DisplayIndex;
                    obj_Estudiante_DAL.iGrado = Convert.ToInt32(drv.Row.ItemArray[Grado].ToString());

                    int Campus = dgv_Estudiantes.SelectedCells[13].Column.DisplayIndex;
                    obj_Estudiante_DAL.iIdCampus = Convert.ToInt32(drv.Row.ItemArray[Campus].ToString());

                    int Beca = dgv_Estudiantes.SelectedCells[14].Column.DisplayIndex;
                    obj_Estudiante_DAL.iIdBeca = Convert.ToInt32(drv.Row.ItemArray[Beca].ToString());

                    int Estado = dgv_Estudiantes.SelectedCells[15].Column.DisplayIndex;
                    obj_Estudiante_DAL.iIdEstado = Convert.ToInt32(drv.Row.ItemArray[Estado].ToString());

                    pant_Modificar.obj_Estudiante_DAL = obj_Estudiante_DAL;

                    pant_Modificar.ShowDialog();
                }
                else
                {
                    MessageBox.Show("No puede editar valores ya que no hay nada Seleccionado", "Alerta", MessageBoxButton.OK, MessageBoxImage.Asterisk);

                }
            }
            else
            {
                MessageBox.Show("No puede editar valores ya que no existen Datos", "Alerta", MessageBoxButton.OK, MessageBoxImage.Asterisk);
            }
        }

        #endregion

        #region Crear

        public void Crear_Estudiante()
        {
            Pantallas.Modificar.wfrm_Modificar_ExpEstudiante PantModificar = new Modificar.wfrm_Modificar_ExpEstudiante();


            cls_Estudiante_DAL obj_Estudiante_DAL = new cls_Estudiante_DAL();
            obj_Estudiante_DAL.cBandAxion = 'I';
            PantModificar.obj_Estudiante_DAL = obj_Estudiante_DAL;
            PantModificar.ShowDialog();
        }


        #endregion

        #endregion

        #region Eventos

        private void dgv_Estudiantes_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = true;
        }

        private void txt_Filtro_TextChanged(object sender, TextChangedEventArgs e)
        {

            Filtrar_Estudiantes();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                GlassHelper.ExtendGlass(this, -1, -1, -1, -1);
            }
            catch (Exception)
            {

                this.Background = Brushes.White;
            }

            CargarEstudiantes();
        }

        private void txt_Filtro_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

            //Permite solo números en el txtbox

            int ascci = Convert.ToInt32(Convert.ToChar(e.Text));

            if (ascci >= 48 && ascci <= 57)
            {

                e.Handled = false;

            }
            else
            {

                e.Handled = true;
            }

        }

        #endregion

        #region Botones


        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            CargarEstudiantes();
        }

      

        private void MenuItem_Click_3(object sender, RoutedEventArgs e)
        {

            Modificar_Estudiante();



        }

        #endregion

        private void btn_Nuevo_Click(object sender, RoutedEventArgs e)
        {
            Crear_Estudiante();
        }











    }
}
