﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using System.Data;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;
using BLL_B_I_T_S.Catalogos_y_Mantenimiento;
using PL_B_I_T_S.Pantallas.Módulos;

namespace PL_B_I_T_S.Pantallas.Catalogos_y_Mantenimineto
{
    /// <summary>
    /// Lógica de interacción para wfrm_Mantenimiento_Materias.xaml
    /// </summary>
    public partial class wfrm_Mantenimiento_Materias : MetroWindow
    {
        public wfrm_Mantenimiento_Materias()
        {
            InitializeComponent();
        }
        #region Variables Globales
        cls_Materias_DAL obj_Materias_DAL = new cls_Materias_DAL();
        cls_Materias_BLL obj_Materias_BLL = new cls_Materias_BLL();
        #endregion

        #region Eventos
        private void txt_FiltroMaterias_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            //Permite solo números en el txtbox

            int numeros = Convert.ToInt32(Convert.ToChar(e.Text));

            if (numeros >= 48 && numeros <= 57)
            {

                e.Handled = false;

            }
            else
            {

                e.Handled = true;
            }

           
        }
        #endregion

        #region Métodos

        #region Crear

        public void Crear()
        {
            Pantallas.Modificar.wfrm_Modificar_Materia PantModificar = new Modificar.wfrm_Modificar_Materia();


            cls_Materias_DAL obj_Materias_DAL = new cls_Materias_DAL();
            obj_Materias_DAL.cBandAxion = 'I';
            PantModificar.obj_Materia_DAL = obj_Materias_DAL;
            PantModificar.ShowDialog();
        }


        #endregion

        #region Cargar
        private void Cargar()
        {


            #region Variables
            string sMsjError = string.Empty;
            DataTable dtMateria = new DataTable();
            #endregion

            obj_Materias_BLL.Listar_Materia(ref dtMateria, ref sMsjError);

            if (sMsjError == string.Empty)
            {
                dgv_Materias.ItemsSource = null;
                dgv_Materias.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtMateria });

            }
            else
            {
                dgv_Materias.ItemsSource = null;
                MessageBox.Show("Se presento un error al listar Desembolsos.\n\nDesc.Error ="
                                 + sMsjError, "Error al Listar", MessageBoxButton.OK, MessageBoxImage.Error);

                dgv_Materias.ItemsSource = null;
            }
        }
        #endregion

        #region Filtrar
        public void Filtrar()
        {
            if (txt_FiltroMaterias.Text.Trim() == string.Empty)
            {
                Cargar();
            }
            else
            {
                #region Variables
                DataTable dtMateria = new DataTable();
                string sMsjError = string.Empty;

                #endregion



                obj_Materias_BLL.Filtrar_Materia(ref dtMateria, Convert.ToInt32(txt_FiltroMaterias.Text.Trim()), ref sMsjError);//Filtro se convierte en int solo cuando 

                if (sMsjError == string.Empty)
                {
                    dgv_Materias.ItemsSource = null;
                    dgv_Materias.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtMateria });
                }
                else
                {
                    dgv_Materias.ItemsSource = null;
                    MessageBox.Show("Se presento un error al filtrar Desembolsos.\n\nDesc.Error ="
                                     + sMsjError, "Error al filtrar", MessageBoxButton.OK, MessageBoxImage.Error);

                    dgv_Materias.ItemsSource = null;
                }

            }
        }

        #endregion

        #region Eliminar

        public void Eliminar()
        {
            #region Variables
            string sMjError = string.Empty;
            #endregion

            if (dgv_Materias.SelectedItems.Count !=0)
            {
                if (MessageBox.Show("Desea Eliminar Realmente el Registro Seleccionado?", "Alerta", MessageBoxButton.YesNo,
                                      MessageBoxImage.Asterisk) == MessageBoxResult.Yes)
                {

                    DataRowView drv = (DataRowView)dgv_Materias.SelectedItems[0];
                    int index = dgv_Materias.SelectedCells[0].Column.DisplayIndex;//4 xq en este caso el id esta en la celda 4
                    string sPK = drv.Row.ItemArray[index].ToString();

                    obj_Materias_BLL.Eliminar_Materia(sPK, ref sMjError);

                    if (sMjError != string.Empty)
                    {
                        MessageBox.Show("Se presentó un error al eliminar el registro.\n\nDesc.Error =" + sMjError, "Error al Eliminar",
                                        MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    else
                    {
                        Cargar();

                        MessageBox.Show("Se eliminó correctamente la Categoría Seleccionada.",
                                        "Fin Éxitoso", MessageBoxButton.OK, MessageBoxImage.Information);

                    }
                }

            }
            else
            {
                MessageBox.Show("No se puede eliminar ninguna Materia ya que no hay nada seleccionado.", "Alerta", MessageBoxButton.OK,
                                 MessageBoxImage.Asterisk);
            }
        }

        #endregion //No se va a eliminar Estudiantes

        #region Modificar

        public void Modificar()
        {

            Pantallas.Modificar.wfrm_Modificar_Materia pant_Modificar = new Modificar.wfrm_Modificar_Materia();

            cls_Materias_DAL obj_Materia_DAL = new cls_Materias_DAL();
            obj_Materia_DAL.cBandAxion = 'U';

            if (dgv_Materias.Items.Count > 0)
            {
                if (dgv_Materias.SelectedIndex > -1)
                {
                    DataRowView drv = (DataRowView)dgv_Materias.SelectedItems[0];

                    int Nombre = dgv_Materias.SelectedCells[1].Column.DisplayIndex;
                    obj_Materia_DAL.sNombre = drv.Row.ItemArray[Nombre].ToString();

                    int Costo = dgv_Materias.SelectedCells[2].Column.DisplayIndex;
                    obj_Materia_DAL.dCosto = Convert.ToInt64(drv.Row.ItemArray[Costo].ToString());

                    int idMateria = dgv_Materias.SelectedCells[0].Column.DisplayIndex;
                    obj_Materia_DAL.iIdMateria= Convert.ToInt32(drv.Row.ItemArray[idMateria].ToString());


                   pant_Modificar.obj_Materia_DAL = obj_Materia_DAL;


                    pant_Modificar.ShowDialog();
                }
                else
                {
                    MessageBox.Show("No puede editar valores ya que no hay nada Seleccionado", "Alerta", MessageBoxButton.OK, MessageBoxImage.Asterisk);

                }
            }
            else
            {
                MessageBox.Show("No puede editar valores ya que no existen Datos", "Alerta", MessageBoxButton.OK, MessageBoxImage.Asterisk);
            }
        }

        #endregion

       

        #endregion


       

        private void btn_Listar_Click(object sender, RoutedEventArgs e)
        {
            Cargar();
        }

        private void btn_ModificarMaterias_Click(object sender, RoutedEventArgs e)
        {
            Modificar();
        }

        private void btn_EliminarMateriasMaterias_Click(object sender, RoutedEventArgs e)
        {
            Eliminar();
        }

        private void btn_Nuevo_Click(object sender, RoutedEventArgs e)
        {
            Crear();
        }

       

    }
}
