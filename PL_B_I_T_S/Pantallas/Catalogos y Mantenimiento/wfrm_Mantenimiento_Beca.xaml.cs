﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using System.Data;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;
using BLL_B_I_T_S.Catalogos_y_Mantenimiento;
using PL_B_I_T_S.Pantallas.Módulos;

namespace PL_B_I_T_S.Pantallas.Catalogos_y_Mantenimineto
{
    /// <summary>
    /// Interaction logic for wfrm_Mantenimiento_Beca.xaml
    /// </summary>
    public partial class wfrm_Mantenimiento_Beca : MetroWindow
    {  
        public wfrm_Mantenimiento_Beca()
        {
            InitializeComponent();
        }
        #region Variables Globales
        cls_Beca_DAL obj_Beca_DAL = new cls_Beca_DAL();
        cls_Beca_BLL obj_Beca_BLL = new cls_Beca_BLL();
        #endregion

        #region Métodos

        #region Cargar
        private void CargarBeca()
        {


            #region Variables
            string sMsjError = string.Empty;
            DataTable dtBeca = new DataTable();
            #endregion

            obj_Beca_BLL.Listar_Beca(ref dtBeca, ref sMsjError);

            if (sMsjError == string.Empty)
            {
                dgv_Beca.ItemsSource = null;
                dgv_Beca.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtBeca });

            }
            else
            {
                dgv_Beca.ItemsSource = null;
                MessageBox.Show("Se presento un error al listar Becas.\n\nDesc.Error ="
                                 + sMsjError, "Error al Listar", MessageBoxButton.OK, MessageBoxImage.Error);

                dgv_Beca.ItemsSource = null;
            }
        }
        #endregion


        #region Modificar

        public void Modificar_Beca()
        {

            Pantallas.Módulos.wfrm_Registro_Becas pant_Modificar_beca = new Módulos.wfrm_Registro_Becas();

            cls_Beca_DAL obj_Beca_DAL = new cls_Beca_DAL();
            obj_Beca_DAL.cBandAxion = 'U';

            if (dgv_Beca.Items.Count > 0)
            {
                if (dgv_Beca.SelectedIndex > -1)
                {
                    DataRowView drv = (DataRowView)dgv_Beca.SelectedItems[0];

                    int IdBeca = dgv_Beca.SelectedCells[0].Column.DisplayIndex;
                    obj_Beca_DAL.iIdBeca = Convert.ToInt32(drv.Row.ItemArray[IdBeca].ToString());

                    int Desc = dgv_Beca.SelectedCells[1].Column.DisplayIndex;
                    obj_Beca_DAL.dPorcentajeDescuento = Convert.ToDouble(drv.Row.ItemArray[Desc].ToString());

                    int Nombre = dgv_Beca.SelectedCells[2].Column.DisplayIndex;
                    obj_Beca_DAL.sNombre = drv.Row.ItemArray[Nombre].ToString();

                    int Nota = dgv_Beca.SelectedCells[3].Column.DisplayIndex;
                    obj_Beca_DAL.dNotaMinima = Convert.ToDouble(drv.Row.ItemArray[Nota].ToString());

                    int IdPat = dgv_Beca.SelectedCells[4].Column.DisplayIndex;
                    obj_Beca_DAL.iIdPatrocinador = Convert.ToInt32(drv.Row.ItemArray[IdPat].ToString());

                    int Catego = dgv_Beca.SelectedCells[5].Column.DisplayIndex;
                    obj_Beca_DAL.iIdCategoria =Convert.ToInt32( drv.Row.ItemArray[Catego].ToString());

                    int SubCat = dgv_Beca.SelectedCells[6].Column.DisplayIndex;
                    obj_Beca_DAL.iIdSubcategoria = Convert.ToInt32(drv.Row.ItemArray[SubCat].ToString());                   

                    int Estado = dgv_Beca.SelectedCells[7].Column.DisplayIndex;
                    obj_Beca_DAL.iIdEstado = Convert.ToInt32(drv.Row.ItemArray[Estado].ToString());

                    pant_Modificar_beca.obj_Beca_DAL = obj_Beca_DAL;

                    pant_Modificar_beca.ShowDialog();
                }
                else
                {
                    MessageBox.Show("No puede editar valores ya que no hay nada Seleccionado", "Alerta", MessageBoxButton.OK, MessageBoxImage.Asterisk);

                }
            }
            else
            {
                MessageBox.Show("No puede editar valores ya que no existen Datos", "Alerta", MessageBoxButton.OK, MessageBoxImage.Asterisk);
            }
        }

        #endregion

        #region Crear

        public void Crear_Beca()
        {
             Pantallas.Módulos.wfrm_Registro_Becas PantModificar = new Módulos.wfrm_Registro_Becas();


            cls_Beca_DAL obj_Beca_DAL = new cls_Beca_DAL();
            obj_Beca_DAL.cBandAxion = 'I';
            PantModificar.obj_Beca_DAL = obj_Beca_DAL;
            PantModificar.ShowDialog();
        }


        #endregion

     

        #endregion

        #region Eventos
        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                GlassHelper.ExtendGlass(this, -1, -1, -1, -1);
            }
            catch (Exception)
            {

                this.Background = Brushes.White;
            }

            CargarBeca();
        }
        private void btn_Nuevo_Click(object sender, RoutedEventArgs e)
        {
            Crear_Beca();
        }

        private void btn_Modificar_Click(object sender, RoutedEventArgs e)
        {
            Modificar_Beca();
        }

        private void btn_Listar_Click(object sender, RoutedEventArgs e)
        {
            CargarBeca();
        }

        #endregion
    }
}
