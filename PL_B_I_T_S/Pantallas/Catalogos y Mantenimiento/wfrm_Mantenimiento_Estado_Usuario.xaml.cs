﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;
using BLL_B_I_T_S.Catalogos_y_Mantenimiento;
using System.Data;

namespace PL_B_I_T_S.Pantallas.Catalogos_y_Mantenimineto
{
    /// <summary>
    /// Lógica de interacción para wfrm_Mantenimiento_Estado_Usuario.xaml
    /// </summary>
    public partial class wfrm_Mantenimiento_Estado_Usuario : MetroWindow
    {
        public wfrm_Mantenimiento_Estado_Usuario()
        {
            InitializeComponent();
        }

        #region Variables Globales
        cls_Estado_Usuario_DAL obj_Estado_DAL = new cls_Estado_Usuario_DAL();
        cls_EstadoUsuario_BLL obj_Estado_BLL = new cls_EstadoUsuario_BLL();
        #endregion
        #region Métodos

        #region Cargar
        private void CargarEstados()
        {


            #region Variables
            string sMsjError = string.Empty;
            DataTable dtEstado = new DataTable();
            #endregion

            obj_Estado_BLL.Listar_EstadoUsuario(ref dtEstado, ref sMsjError);

            if (sMsjError == string.Empty)
            {
                dgv_EstadoUsuario.ItemsSource = null;
                dgv_EstadoUsuario.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtEstado });

            }
            else
            {
                dgv_EstadoUsuario.ItemsSource = null;
                MessageBox.Show("Se presento un error al listar Estados.\n\nDesc.Error ="
                                 + sMsjError, "Error al Listar", MessageBoxButton.OK, MessageBoxImage.Error);

                dgv_EstadoUsuario.ItemsSource = null;
            }
        }
        #endregion
        #endregion

        private void btn_Listar_Click(object sender, RoutedEventArgs e)
        {
            CargarEstados();

        }
    }
}
