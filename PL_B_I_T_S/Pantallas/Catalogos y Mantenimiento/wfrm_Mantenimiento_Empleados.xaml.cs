﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using System.Data;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;
using BLL_B_I_T_S.Catalogos_y_Mantenimiento;
using PL_B_I_T_S.Pantallas.Módulos;

namespace PL_B_I_T_S.Pantallas.Catalogos_y_Mantenimineto

{
    /// <summary>
    /// Interaction logic for wfrm_Mantenimiento_Empleados.xaml
    /// </summary>
    public partial class wfrm_Mantenimiento_Empleados : MetroWindow
    {

        #region Variables Globales
        cls_Empleados_DAL obj_Empleados_DAL = new cls_Empleados_DAL();
        cls_Empleados_BLL obj_Empleados_BLL = new cls_Empleados_BLL();
        #endregion

        public wfrm_Mantenimiento_Empleados()
        {
            InitializeComponent();
        }

        #region Métodos

        #region Cargar
        private void Cargar()
        {


            #region Variables
            string sMsjError = string.Empty;
            DataTable dtEmpleado = new DataTable();
            #endregion

            obj_Empleados_BLL.Listar_Empleados(ref dtEmpleado, ref sMsjError);

            if (sMsjError == string.Empty)
            {
                dgv_Empleados.ItemsSource = null;
                dgv_Empleados.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtEmpleado });

            }
            else
            {
                dgv_Empleados.ItemsSource = null;
                MessageBox.Show("Se presento un error al listar Empleados.\n\nDesc.Error ="
                                 + sMsjError, "Error al Listar", MessageBoxButton.OK, MessageBoxImage.Error);

                dgv_Empleados.ItemsSource = null;
            }
        }
        #endregion

        #region Filtrar
        public void Filtrar()
        {
            if (txt_Filtro.Text.Trim() == string.Empty)
            {
                Cargar();
            }
            else
            {
                #region Variables
                DataTable dtEmpleados = new DataTable();
                string sMsjError = string.Empty;

                #endregion



                obj_Empleados_BLL.Filtrar_Empleados(ref dtEmpleados, Convert.ToInt32(txt_Filtro.Text.Trim()), ref sMsjError);//Filtro se convierte en int solo cuando 

                if (sMsjError == string.Empty)
                {
                    dgv_Empleados.ItemsSource = null;
                    dgv_Empleados.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtEmpleados });
                }
                else
                {
                    dgv_Empleados.ItemsSource = null;
                    MessageBox.Show("Se presento un error al filtrar Empleados.\n\nDesc.Error ="
                                     + sMsjError, "Error al filtrar", MessageBoxButton.OK, MessageBoxImage.Error);

                    dgv_Empleados.ItemsSource = null;
                }

            }
        }

        #endregion


        #region Modificar

        public void Modificar()
        {

            Pantallas.Modificar.wfrm_Modificar_Registro_Usuario pant_Modificar = new Modificar.wfrm_Modificar_Registro_Usuario();

            cls_Empleados_DAL obj_Empleados_DAL = new cls_Empleados_DAL();
            obj_Empleados_DAL.cBandAxion = 'U';

            if (dgv_Empleados.Items.Count > 0)
            {
                if (dgv_Empleados.SelectedIndex > -1)
                {
                    DataRowView drv = (DataRowView)dgv_Empleados.SelectedItems[0];

                    int IdEmpleado = dgv_Empleados.SelectedCells[0].Column.DisplayIndex;
                    obj_Empleados_DAL.iIdEmpleado = Convert.ToInt32(drv.Row.ItemArray[IdEmpleado].ToString());

                    int Cedula = dgv_Empleados.SelectedCells[1].Column.DisplayIndex;
                    obj_Empleados_DAL.sCedula = drv.Row.ItemArray[Cedula].ToString();

                    int Nombre = dgv_Empleados.SelectedCells[2].Column.DisplayIndex;
                    obj_Empleados_DAL.sNombre = drv.Row.ItemArray[Nombre].ToString();

                    int App1 = dgv_Empleados.SelectedCells[3].Column.DisplayIndex;
                    obj_Empleados_DAL.sPrimerApellido = drv.Row.ItemArray[App1].ToString();

                    int App2 = dgv_Empleados.SelectedCells[4].Column.DisplayIndex;
                    obj_Empleados_DAL.sSegundoApellido = drv.Row.ItemArray[App2].ToString();

                    int IdUsuario = dgv_Empleados.SelectedCells[5].Column.DisplayIndex;
                    obj_Empleados_DAL.sIdUsuario = (drv.Row.ItemArray[IdUsuario].ToString());

                    int correoElectronico = dgv_Empleados.SelectedCells[6].Column.DisplayIndex;
                    obj_Empleados_DAL.sCorreoElectronico = (drv.Row.ItemArray[correoElectronico].ToString());

                    int FechaNacimiento = dgv_Empleados.SelectedCells[7].Column.DisplayIndex;
                    obj_Empleados_DAL.dtFechaNacimiento = Convert.ToDateTime(drv.Row.ItemArray[FechaNacimiento].ToString());

                    int Campus= dgv_Empleados.SelectedCells[8].Column.DisplayIndex;
                    obj_Empleados_DAL.iIdCampus = Convert.ToInt32(drv.Row.ItemArray[Campus].ToString());

                    pant_Modificar.obj_Empleados_DAL = obj_Empleados_DAL;

                    pant_Modificar.ShowDialog();
                }
                else
                {
                    MessageBox.Show("No puede editar valores ya que no hay nada Seleccionado", "Alerta", MessageBoxButton.OK, MessageBoxImage.Asterisk);

                }
            }
            else
            {
                MessageBox.Show("No puede editar valores ya que no existen Datos", "Alerta", MessageBoxButton.OK, MessageBoxImage.Asterisk);
            }
        }

        #endregion

        #region Crear

        public void Crear()
        {
            Pantallas.Modificar.wfrm_Modificar_Registro_Usuario PantModificar = new Modificar.wfrm_Modificar_Registro_Usuario();


            cls_Empleados_DAL obj_Empleados_DAL = new cls_Empleados_DAL();
            obj_Empleados_DAL.cBandAxion = 'I';
            PantModificar.obj_Empleados_DAL = obj_Empleados_DAL;
            PantModificar.ShowDialog();
        }


        #endregion

        #endregion

        private void btn_Listar_Click(object sender, RoutedEventArgs e)
        {
            Cargar();
        }

       

        private void btn_Modificar_Click(object sender, RoutedEventArgs e)
        {
            Modificar();
        }

        private void btn_Nuevo_Click(object sender, RoutedEventArgs e)
        {
            Crear();
        }
       
    }
}
