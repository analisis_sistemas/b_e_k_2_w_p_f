﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;
using BLL_B_I_T_S.Catalogos_y_Mantenimiento;
using System.Data;

namespace PL_B_I_T_S.Pantallas.Catalogos_y_Mantenimineto
{
    /// <summary>
    /// Lógica de interacción para wfrm_Mantenimiento_Preguntas.xaml
    /// </summary>
    public partial class wfrm_Mantenimiento_Preguntas : MetroWindow
    {
        public wfrm_Mantenimiento_Preguntas()
        {
            InitializeComponent();
        }
        #region Variables Globales
        cls_Preguntas_DAL obj_Preguntas_DAL = new cls_Preguntas_DAL();
        cls_Preguntas_BLL obj_Preguntas_BLL = new cls_Preguntas_BLL();
        #endregion

        #region Métodos

        #region Cargar
        private void Cargar()
        {


            #region Variables
            string sMsjError = string.Empty;
            DataTable dtPreguntas = new DataTable();
            #endregion

            obj_Preguntas_BLL.Listar_Preguntas(ref dtPreguntas, ref sMsjError);

            if (sMsjError == string.Empty)
            {
                dgv_Preguntas.ItemsSource = null;
                dgv_Preguntas.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtPreguntas });

            }
            else
            {
                dgv_Preguntas.ItemsSource = null;
                MessageBox.Show("Se presento un error al listar Preguntas.\n\nDesc.Error ="
                                 + sMsjError, "Error al Listar", MessageBoxButton.OK, MessageBoxImage.Error);

                dgv_Preguntas.ItemsSource = null;
            }
        }
        #endregion


        #endregion
        

        private void btn_Listar_Click(object sender, RoutedEventArgs e)
        {
            Cargar();
        }
    }
}
