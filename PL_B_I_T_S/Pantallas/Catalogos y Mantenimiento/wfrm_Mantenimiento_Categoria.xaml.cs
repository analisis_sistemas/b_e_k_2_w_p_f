﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using System.Data;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;
using BLL_B_I_T_S.Catalogos_y_Mantenimiento;

namespace PL_B_I_T_S.Pantallas.Catalogos_y_Mantenimineto
{
    /// <summary>
    /// Interaction logic for wfrm_Mantenimiento_Categoria.xaml
    /// </summary>
    public partial class wfrm_Mantenimiento_Categoria : MetroWindow
    {
        cls_Categoria_DAL obj_Categoria_DAL = new cls_Categoria_DAL();
        cls_Categoria_BLL obj_Categoria_BLL = new cls_Categoria_BLL();

        public wfrm_Mantenimiento_Categoria()
        {
            InitializeComponent();
        }

        #region Métodos

        #region Cargar
        private void CargarCategoria()
        {


            #region Variables
            string sMsjError = string.Empty;
            DataTable dtCategoria = new DataTable();
            #endregion

            obj_Categoria_BLL.Listar_Categoria(ref dtCategoria, ref sMsjError);

            if (sMsjError == string.Empty)
            {
                dgv_Categoria.ItemsSource = null;
                dgv_Categoria.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtCategoria });

            }
            else
            {
                dgv_Categoria.ItemsSource = null;
                MessageBox.Show("Se presento un error al listar Activos.\n\nDesc.Error ="
                                 + sMsjError, "Error al Listar", MessageBoxButton.OK, MessageBoxImage.Error);

                dgv_Categoria.ItemsSource = null;
            }
        }
        #endregion

        #region Filtrar
        public void Filtrar_Categoria()
        {
            if (txt_Filtro.Text.Trim() == string.Empty)
            {
                CargarCategoria();
            }
            else
            {
                #region Variables
                DataTable dtCategoria = new DataTable();
                string sMsjError = string.Empty;

                #endregion



                obj_Categoria_BLL.Filtrar_Categoria(ref dtCategoria, Convert.ToInt32(txt_Filtro.Text.Trim()), ref sMsjError);//Filtro se convierte en int solo cuando 

                if (sMsjError == string.Empty)
                {
                    dgv_Categoria.ItemsSource = null;
                    dgv_Categoria.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtCategoria});
                }
                else
                {
                    dgv_Categoria.ItemsSource = null;
                    MessageBox.Show("Se presento un error al filtrar Activos.\n\nDesc.Error ="
                                     + sMsjError, "Error al filtrar", MessageBoxButton.OK, MessageBoxImage.Error);

                    dgv_Categoria.ItemsSource = null;
                }

            }
        }

        #endregion

        #region Eliminar

        public void Eliminar_Categoria()
        {
            #region Variables
            string sMjError = string.Empty;
            #endregion

            if (dgv_Categoria.SelectedItems.Count != 0)
            {
                if (MessageBox.Show("Desea Eliminar Realmente el Registro Seleccionado?", "Alerta", MessageBoxButton.YesNo,
                                      MessageBoxImage.Asterisk) == MessageBoxResult.Yes)
                {

                    DataRowView drv = (DataRowView)dgv_Categoria.SelectedItems[0];
                    int index = dgv_Categoria.SelectedCells[0].Column.DisplayIndex;//4 xq en este caso el id esta en la celda 4
                    string sPK = drv.Row.ItemArray[index].ToString();

                    obj_Categoria_BLL.Eliminar_Categoria(sPK, ref sMjError);

                    if (sMjError != string.Empty)
                    {
                        MessageBox.Show("Se presentó un error al eliminar el registro.\n\nDesc.Error =" + sMjError, "Error al Eliminar",
                                        MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    else
                    {
                        CargarCategoria();

                        MessageBox.Show("Se eliminó correctamente la Categoría Seleccionada.",
                                        "Fin Éxitoso", MessageBoxButton.OK, MessageBoxImage.Information);

                    }
                }

            }
            else
            {
                MessageBox.Show("No se puede eliminar ninguna Categoría ya que no hay nada seleccionado.", "Alerta", MessageBoxButton.OK,
                                 MessageBoxImage.Asterisk);
            }
        }

        #endregion //No se va a eliminar Estudiantes

        #region Modificar

        public void Modificar_Categoria()
        {

            Pantallas.Modificar.wfrm_Modificar_Categorias pant_Modificar = new Modificar.wfrm_Modificar_Categorias();

            cls_Categoria_DAL obj_Categoria_DAL = new cls_Categoria_DAL();
            obj_Categoria_DAL.cBandAxion = 'U';

            if (dgv_Categoria.Items.Count > 0)
            {
                if (dgv_Categoria.SelectedIndex > -1)
                {
                    DataRowView drv = (DataRowView)dgv_Categoria.SelectedItems[0];

                    int IdCategoria = dgv_Categoria.SelectedCells[0].Column.DisplayIndex;
                    obj_Categoria_DAL.iIdCategoria = Convert.ToInt32(drv.Row.ItemArray[IdCategoria].ToString());

                    int NombreCategoria = dgv_Categoria.SelectedCells[1].Column.DisplayIndex;
                    obj_Categoria_DAL.sNombre =(drv.Row.ItemArray[NombreCategoria].ToString());

                    int Monto = dgv_Categoria.SelectedCells[2].Column.DisplayIndex;
                    obj_Categoria_DAL.iMonto = Convert.ToInt32(drv.Row.ItemArray[Monto].ToString());

                    pant_Modificar.obj_Categoria_DAL = obj_Categoria_DAL;
                    pant_Modificar.ShowDialog();
                }
                else
                {
                    MessageBox.Show("No puede editar valores ya que no hay nada Seleccionado", "Alerta", MessageBoxButton.OK, MessageBoxImage.Asterisk);

                }
            }
            else
            {
                MessageBox.Show("No puede editar valores ya que no existen Datos", "Alerta", MessageBoxButton.OK, MessageBoxImage.Asterisk);
            }
        }

        #endregion

        #region Crear

        public void Crear_Estudiante()
        {
            Pantallas.Modificar.wfrm_Modificar_Categorias PantModificar = new Modificar.wfrm_Modificar_Categorias();


            cls_Categoria_DAL obj_Categoria_DAL = new cls_Categoria_DAL();
            obj_Categoria_DAL.cBandAxion = 'I';
            PantModificar.obj_Categoria_DAL = obj_Categoria_DAL;
            PantModificar.ShowDialog();

        }


        #endregion

        #endregion

        #region Eventos

        private void dgv_Categoria_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = true;
        }

        private void txt_Filtro_TextChanged(object sender, TextChangedEventArgs e)
        {

            Filtrar_Categoria();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                GlassHelper.ExtendGlass(this, -1, -1, -1, -1);
            }
            catch (Exception)
            {

                this.Background = Brushes.White;
            }

            CargarCategoria();
        }

        private void txt_Filtro_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

            //Permite solo números en el txtbox

            int ascci = Convert.ToInt32(Convert.ToChar(e.Text));

            if (ascci >= 48 && ascci <= 57)
            {

                e.Handled = false;

            }
            else
            {

                e.Handled = true;
            }

        }

        #endregion

        private void btn_Listar_Click(object sender, RoutedEventArgs e)
        {
            CargarCategoria();
        }

        private void btn_Eliminar_Click(object sender, RoutedEventArgs e)
        {
            Eliminar_Categoria();
        }

        private void btn_Modificar_Click(object sender, RoutedEventArgs e)
        {
            Modificar_Categoria();
        }

        private void btn_Nuevo_Click(object sender, RoutedEventArgs e)
        {
            Crear_Estudiante();
        }
    
    }
    
}
