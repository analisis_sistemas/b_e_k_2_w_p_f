﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using System.Data;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;
using BLL_B_I_T_S.Catalogos_y_Mantenimiento;
using PL_B_I_T_S.Pantallas.Módulos;

namespace PL_B_I_T_S.Pantallas.Catalogos_y_Mantenimineto
{
    /// <summary>
    /// Lógica de interacción para wfrm_Mantenimiento_Patrocinador.xaml
    /// </summary>
    public partial class wfrm_Mantenimiento_Patrocinador : MetroWindow
    {
        public wfrm_Mantenimiento_Patrocinador()
        {
            InitializeComponent();
        }
        #region Variables Globales
        cls_Patrocinador_DAL obj_Patrocinador_DAL = new cls_Patrocinador_DAL();
        cls_Patrocinador_BLL obj_Patrocinador_BLL = new cls_Patrocinador_BLL();
        #endregion

        #region Métodos

        #region Cargar
        private void Cargar()
        {


            #region Variables
            string sMsjError = string.Empty;
            DataTable dtPatrocinador = new DataTable();
            #endregion

            obj_Patrocinador_BLL.Listar_Patrocinador(ref dtPatrocinador, ref sMsjError);

            if (sMsjError == string.Empty)
            {
                dgv_Patrocinador.ItemsSource = null;
                dgv_Patrocinador.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtPatrocinador });

            }
            else
            {
                dgv_Patrocinador.ItemsSource = null;
                MessageBox.Show("Se presento un error al listar Desembolsos.\n\nDesc.Error ="
                                 + sMsjError, "Error al Listar", MessageBoxButton.OK, MessageBoxImage.Error);

                dgv_Patrocinador.ItemsSource = null;
            }
        }
        #endregion

        #region Filtrar
        public void Filtrar()
        {
            if (txt_Filtro.Text.Trim() == string.Empty)
            {
                Cargar();
            }
            else
            {
                #region Variables
                DataTable dtPatrocinador = new DataTable();
                string sMsjError = string.Empty;

                #endregion



                obj_Patrocinador_BLL.Filtrar_Patrocinador(ref dtPatrocinador, Convert.ToInt32(txt_Filtro.Text.Trim()), ref sMsjError);//Filtro se convierte en int solo cuando 

                if (sMsjError == string.Empty)
                {
                    dgv_Patrocinador.ItemsSource = null;
                    dgv_Patrocinador.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtPatrocinador });
                }
                else
                {
                    dgv_Patrocinador.ItemsSource = null;
                    MessageBox.Show("Se presento un error al filtrar Desembolsos.\n\nDesc.Error ="
                                     + sMsjError, "Error al filtrar", MessageBoxButton.OK, MessageBoxImage.Error);

                    dgv_Patrocinador.ItemsSource = null;
                }

            }
        }

        #endregion

        #region Modificar

        public void Modificar()
        {

            Pantallas.Módulos.wfrm_Patrocinadores pant_Modificar = new Módulos.wfrm_Patrocinadores();

            cls_Patrocinador_DAL obj_Patrocinador_DAL = new cls_Patrocinador_DAL();

            obj_Patrocinador_DAL.cBandAxion = 'U';

            if (dgv_Patrocinador.Items.Count > 0)
            {
                if (dgv_Patrocinador.SelectedIndex > -1)
                {
                    DataRowView drv = (DataRowView)dgv_Patrocinador.SelectedItems[0];

                    int IdPatrocinador = dgv_Patrocinador.SelectedCells[0].Column.DisplayIndex;
                    obj_Patrocinador_DAL.iIdPatrocinador = Convert.ToInt32(drv.Row.ItemArray[IdPatrocinador].ToString());

                    int Nombre = dgv_Patrocinador.SelectedCells[1].Column.DisplayIndex;
                    obj_Patrocinador_DAL.sNombre = drv.Row.ItemArray[Nombre].ToString();

                    int MontoMaximo = dgv_Patrocinador.SelectedCells[2].Column.DisplayIndex;
                    obj_Patrocinador_DAL.dMontoMaximo = Convert.ToInt64(drv.Row.ItemArray[MontoMaximo].ToString());

                    int Correo= dgv_Patrocinador.SelectedCells[3].Column.DisplayIndex;
                    obj_Patrocinador_DAL.sCorreo = drv.Row.ItemArray[Correo].ToString();
 
                    int IdPeriodo = dgv_Patrocinador.SelectedCells[4].Column.DisplayIndex;
                    obj_Patrocinador_DAL.iIdPeriodo = Convert.ToInt32(drv.Row.ItemArray[IdPeriodo].ToString());

                    int IdEstado = dgv_Patrocinador.SelectedCells[5].Column.DisplayIndex;
                    obj_Patrocinador_DAL.iIdEstado = Convert.ToInt32(drv.Row.ItemArray[IdEstado].ToString());


                    pant_Modificar.obj_Patrocinador_DAL = obj_Patrocinador_DAL;
                    pant_Modificar.ShowDialog();
                    
                    
                }
                else
                {
                    MessageBox.Show("No puede editar valores ya que no hay nada Seleccionado", "Alerta", MessageBoxButton.OK, MessageBoxImage.Asterisk);

                }
            }
            else
            {
                MessageBox.Show("No puede editar valores ya que no existen Datos", "Alerta", MessageBoxButton.OK, MessageBoxImage.Asterisk);
            }
        }

        #endregion

        #region Crear

        public void Crear()
        {
            Pantallas.Módulos.wfrm_Patrocinadores PantModificar = new Módulos.wfrm_Patrocinadores();


            cls_Patrocinador_DAL obj_Patrocinador_DAL = new cls_Patrocinador_DAL();
            obj_Patrocinador_DAL.cBandAxion = 'I';
            PantModificar.obj_Patrocinador_DAL = obj_Patrocinador_DAL;
            PantModificar.ShowDialog();
        }


        #endregion

        #endregion


        private void btn_Listar_Click(object sender, RoutedEventArgs e)
        {
            Cargar();
        }


        private void btn_Modificar_Click(object sender, RoutedEventArgs e)
        {
            Modificar();
        }

        private void btn_Nuevo_Click(object sender, RoutedEventArgs e)
        {
            Crear();
        }

        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            Cargar();

            try
            {
                GlassHelper.ExtendGlass(this, -1, -1, -1, -1);
            }
            catch (Exception)
            {

                this.Background = Brushes.White;
            }
        }

       
    }
}
