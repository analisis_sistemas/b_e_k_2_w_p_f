﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;
using BLL_B_I_T_S.Catalogos_y_Mantenimiento;
using System.Data;

namespace PL_B_I_T_S.Pantallas.Catalogos_y_Mantenimineto
{
    /// <summary>
    /// Interaction logic for wfrm_Mantenimiento_Estado_Beca.xaml
    /// </summary>
    public partial class wfrm_Mantenimiento_Estado_Beca : MetroWindow
    {
        public wfrm_Mantenimiento_Estado_Beca()
        {
            InitializeComponent();
        }

        #region Variables Globales
        cls_Estado_Beca_DAL obj_EstadoBeca_DAL = new cls_Estado_Beca_DAL();
        cls_EstadoBeca_BLL obj_EstadoBeca_BLL = new cls_EstadoBeca_BLL();
        #endregion
        #region Métodos

        #region Cargar
        private void CargarEstadosBeca()
        {


            #region Variables
            string sMsjError = string.Empty;
            DataTable dtEstadoBeca = new DataTable();
            #endregion

            obj_EstadoBeca_BLL.Listar_EstadoBeca(ref dtEstadoBeca, ref sMsjError);

            if (sMsjError == string.Empty)
            {
                dgv_EstadoBeca.ItemsSource = null;
                dgv_EstadoBeca.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtEstadoBeca });

            }
            else
            {
                dgv_EstadoBeca.ItemsSource = null;
                MessageBox.Show("Se presento un error al listar Estados.\n\nDesc.Error ="
                                 + sMsjError, "Error al Listar", MessageBoxButton.OK, MessageBoxImage.Error);

                dgv_EstadoBeca.ItemsSource = null;
            }
        }
        #endregion
        #endregion

        private void btn_Listar_Click(object sender, RoutedEventArgs e)
        {
            CargarEstadosBeca();
        }
    }
}
