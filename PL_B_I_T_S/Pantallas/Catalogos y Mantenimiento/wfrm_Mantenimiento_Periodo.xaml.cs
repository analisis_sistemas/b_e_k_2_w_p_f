﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using System.Data;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;
using BLL_B_I_T_S.Catalogos_y_Mantenimiento;
using PL_B_I_T_S.Pantallas.Módulos;



namespace PL_B_I_T_S.Pantallas.Catalogos_y_Mantenimineto
{
    /// <summary>
    /// Lógica de interacción para wfrm_Mantenimiento_Periodo.xaml
    /// </summary>
    public partial class wfrm_Mantenimiento_Periodo : MetroWindow
    {

        #region Variables Globales
        cls_Periodo_DAL obj_Periodo_DAL = new cls_Periodo_DAL();
        cls_Periodo_BLL obj_Periodo_BLL = new cls_Periodo_BLL();
        #endregion


        #region Métodos

        #region Cargar
        private void Cargar()
        {


            #region Variables
            string sMsjError = string.Empty;
            DataTable dtPeriodo = new DataTable();
            #endregion

            obj_Periodo_BLL.Listar_Periodo(ref dtPeriodo, ref sMsjError);

            if (sMsjError == string.Empty)
            {
                dgv_Periodo.ItemsSource = null;
                dgv_Periodo.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtPeriodo });

            }
            else
            {
                dgv_Periodo.ItemsSource = null;
                MessageBox.Show("Se presento un error al listar Periodos.\n\nDesc.Error ="
                                 + sMsjError, "Error al Listar", MessageBoxButton.OK, MessageBoxImage.Error);

                dgv_Periodo.ItemsSource = null;
            }
        }
        #endregion



        #region Eliminar

        public void Eliminar()
        {
            #region Variables
            string sMjError = string.Empty;
            #endregion

            if (dgv_Periodo.SelectedItems.Count != 0)
            {
                if (MessageBox.Show("Desea Eliminar Realmente el Registro Seleccionado?", "Alerta", MessageBoxButton.YesNo,
                                      MessageBoxImage.Asterisk) == MessageBoxResult.Yes)
                {

                    DataRowView drv = (DataRowView)dgv_Periodo.SelectedItems[0];
                    int index = dgv_Periodo.SelectedCells[0].Column.DisplayIndex;//4 xq en este caso el id esta en la celda 4
                    string sPK = drv.Row.ItemArray[index].ToString();

                    obj_Periodo_BLL.Eliminar_Periodo(sPK, ref sMjError);

                    if (sMjError != string.Empty)
                    {
                        MessageBox.Show("Se presentó un error al eliminar el registro.\n\nDesc.Error =" + sMjError, "Error al Eliminar",
                                        MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    else
                    {
                        Cargar();

                        MessageBox.Show("Se eliminó correctamente el Periodo Seleccionado.",
                                        "Fin Éxitoso", MessageBoxButton.OK, MessageBoxImage.Information);

                    }
                }

            }
            else
            {
                MessageBox.Show("No se puede eliminar ningún Periodo ya que no hay nada seleccionado.", "Alerta", MessageBoxButton.OK,
                                 MessageBoxImage.Asterisk);
            }
        }

        #endregion //No se va a eliminar Estudiantes

        #region Modificar

        public void Modificar()
        {

            Pantallas.Modificar.wfrm_Modificar_Periodo pant_Modificar = new Modificar.wfrm_Modificar_Periodo();

            cls_Periodo_DAL obj_Periodo_DAL = new cls_Periodo_DAL();
            obj_Periodo_DAL.cBandAxion = 'U';

            if (dgv_Periodo.Items.Count > 0)
            {
                if (dgv_Periodo.SelectedIndex > -1)
                {
                    DataRowView drv = (DataRowView)dgv_Periodo.SelectedItems[0];

                    int IdPeriodo = dgv_Periodo.SelectedCells[0].Column.DisplayIndex;
                    obj_Periodo_DAL.iIdPeriodo = Convert.ToInt32(drv.Row.ItemArray[IdPeriodo].ToString());

                    int Nombre = dgv_Periodo.SelectedCells[1].Column.DisplayIndex;
                    obj_Periodo_DAL.sNombre = drv.Row.ItemArray[Nombre].ToString();

                    pant_Modificar.obj_Periodo_DAL = obj_Periodo_DAL;

                    pant_Modificar.ShowDialog();
                }
                else
                {
                    MessageBox.Show("No puede editar valores ya que no hay nada Seleccionado", "Alerta", MessageBoxButton.OK, MessageBoxImage.Asterisk);

                }
            }
            else
            {
                MessageBox.Show("No puede editar valores ya que no existen Datos", "Alerta", MessageBoxButton.OK, MessageBoxImage.Asterisk);
            }
        }

        #endregion

        #region Crear

        public void Crear()
        {
            Pantallas.Modificar.wfrm_Modificar_Periodo PantModificar = new Modificar.wfrm_Modificar_Periodo();


            cls_Periodo_DAL obj_Periodo_DAL = new cls_Periodo_DAL();
            obj_Periodo_DAL.cBandAxion = 'I';
            PantModificar.obj_Periodo_DAL = obj_Periodo_DAL;
            PantModificar.ShowDialog();
        }


        #endregion

        #endregion






        public wfrm_Mantenimiento_Periodo()
        {
            InitializeComponent();
        }

        private void btn_Listar_Click(object sender, RoutedEventArgs e)
        {
            Cargar();
        }

        private void btn_Eliminar_Click(object sender, RoutedEventArgs e)
        {
            Eliminar();
        }

        private void btn_Modificar_Click(object sender, RoutedEventArgs e)
        {
            Modificar();
        }

        private void btn_Nuevo_Click(object sender, RoutedEventArgs e)
        {
            Crear();
        }
    }
}
