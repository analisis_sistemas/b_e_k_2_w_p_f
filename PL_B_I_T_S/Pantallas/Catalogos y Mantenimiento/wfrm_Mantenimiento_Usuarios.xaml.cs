﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using System.Data;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;
using BLL_B_I_T_S.Catalogos_y_Mantenimiento;
using PL_B_I_T_S.Pantallas.Módulos;

namespace PL_B_I_T_S.Pantallas.Catalogos_y_Mantenimiento
{
    /// <summary>
    /// Interaction logic for wfrm_Mantenimiento_Usuarios.xaml
    /// </summary>
    public partial class wfrm_Mantenimiento_Usuarios : MetroWindow
    {

        #region Variables Globales
        cls_Usuarios_DAL obj_Usuarios_DAL = new cls_Usuarios_DAL();
        cls_Usuarios_BLL obj_Usuarios_BLL = new cls_Usuarios_BLL();
        #endregion

        public wfrm_Mantenimiento_Usuarios()
        {
            InitializeComponent();
        }

        #region Métodos

        #region Cargar
        private void CargarUsuario()
        {
            #region Variables
            string sMsjError = string.Empty;
            DataTable dtUsuario = new DataTable();
            #endregion

            obj_Usuarios_BLL.Listar_Usuario(ref dtUsuario, ref sMsjError);

            if (sMsjError == string.Empty)
            {
                dgv_Usuario.ItemsSource = null;
                dgv_Usuario.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtUsuario });

            }
            else
            {
                dgv_Usuario.ItemsSource = null;
                MessageBox.Show("Se presento un error al listar Activos.\n\nDesc.Error ="
                                 + sMsjError, "Error al Listar", MessageBoxButton.OK, MessageBoxImage.Error);

                dgv_Usuario.ItemsSource = null;
            }
        }
        #endregion

        //#region Filtrar
        //public void Filtrar_Estudiantes()
        //{
        //    if (txt_FiltroEstudiante.Text.Trim() == string.Empty)
        //    {
        //        CargarUsuario();
        //    }
        //    else
        //    {
        //        #region Variables
        //        DataTable dtEstudiante = new DataTable();
        //        string sMsjError = string.Empty;

        //        #endregion



        //        obj_Estudiante_BLL.Filtrar_Estudiante(ref dtEstudiante, Convert.ToInt32(txt_FiltroEstudiante.Text.Trim()), ref sMsjError);//Filtro se convierte en int solo cuando 

        //        if (sMsjError == string.Empty)
        //        {
        //            dgv_Estudiantes.ItemsSource = null;
        //            dgv_Estudiantes.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtEstudiante });
        //        }
        //        else
        //        {
        //            dgv_Estudiantes.ItemsSource = null;
        //            MessageBox.Show("Se presento un error al filtrar Activos.\n\nDesc.Error ="
        //                             + sMsjError, "Error al filtrar", MessageBoxButton.OK, MessageBoxImage.Error);

        //            dgv_Estudiantes.ItemsSource = null;
        //        }

        //    }
        //}

        //#endregion

        #region Eliminar

        //public void Eliminar_Estudiantes()
        //{
        //    #region Variables
        //    string sMjError = string.Empty;
        //    #endregion

        //    if (dgv_Estudiantes.Items.Count >= 1)
        //    {
        //        if (MessageBox.Show("Desea Eliminar Realmente el Registro Seleccionado?", "Alerta", MessageBoxButton.YesNo,
        //                              MessageBoxImage.Asterisk) == MessageBoxResult.Yes)
        //        {

        //            DataRowView drv = (DataRowView)dgv_Estudiantes.SelectedItems[0];
        //            int index = dgv_Estudiantes.SelectedCells[4].Column.DisplayIndex;//4 xq en este caso el id esta en la celda 4
        //            string sPK = drv.Row.ItemArray[index].ToString();

        //            obj_Estudiante_BLL.Eliminar_Estudiante(sPK, ref sMjError);

        //            if (sMjError != string.Empty)
        //            {
        //                MessageBox.Show("Se presentó un error al eliminar el registro.\n\nDesc.Error =" + sMjError, "Error al Eliminar",
        //                                MessageBoxButton.OK, MessageBoxImage.Error);
        //            }
        //            else
        //            {
        //                CargarEstudiantes();

        //                MessageBox.Show("Se eliminó correctamente el Estudiante Seleccionado.",
        //                                "Fin Éxitoso", MessageBoxButton.OK, MessageBoxImage.Information);

        //            }
        //        }

        //    }
        //    else
        //    {
        //        MessageBox.Show("No se puede eliminar ningún Estudiante ya que no hay nada seleccionado.", "Alerta", MessageBoxButton.OK,
        //                         MessageBoxImage.Asterisk);
        //    }
        //}

        #endregion //No se va a eliminar Estudiantes

        #region Modificar

        public void Modificar_Usuario()
        {

            Pantallas.Modificar.wfrm_Modificar_Registro_Usuario pant_Modificar = new Modificar.wfrm_Modificar_Registro_Usuario();

            cls_Usuarios_DAL obj_Usuario_DAL = new cls_Usuarios_DAL();
            obj_Usuario_DAL.cBandAxion = 'U';

            if (dgv_Usuario.Items.Count > 0)
            {
                if (dgv_Usuario.SelectedIndex > -1)
                {
                    DataRowView drv = (DataRowView)dgv_Usuario.SelectedItems[0];

                    int IdUsuario = dgv_Usuario.SelectedCells[0].Column.DisplayIndex;
                    obj_Usuario_DAL.sIdUsuario = drv.Row.ItemArray[IdUsuario].ToString();

                    int Nombre = dgv_Usuario.SelectedCells[1].Column.DisplayIndex;
                    obj_Usuario_DAL.sNombre = drv.Row.ItemArray[Nombre].ToString();

                    int Password = dgv_Usuario.SelectedCells[2].Column.DisplayIndex;
                    obj_Usuario_DAL.sPassword = drv.Row.ItemArray[Password].ToString();

                    int IdRol = dgv_Usuario.SelectedCells[3].Column.DisplayIndex;
                    obj_Usuario_DAL.iIdRol = Convert.ToInt32(drv.Row.ItemArray[IdRol].ToString());

                    int idEstado = dgv_Usuario.SelectedCells[4].Column.DisplayIndex;
                    obj_Usuario_DAL.iIdEstado = Convert.ToInt32(drv.Row.ItemArray[idEstado].ToString());

                    pant_Modificar.obj_Usuarios_DAL = obj_Usuario_DAL;

                    pant_Modificar.ShowDialog();
                }
                else
                {
                    MessageBox.Show("No puede editar valores ya que no hay nada Seleccionado", "Alerta", MessageBoxButton.OK, MessageBoxImage.Asterisk);

                }
            }
            else
            {
                MessageBox.Show("No puede editar valores ya que no existen Datos", "Alerta", MessageBoxButton.OK, MessageBoxImage.Asterisk);
            }
        }

        #endregion

        #region Crear

        public void Crear_Usuario()
        {
            Pantallas.Modificar.wfrm_Modificar_Registro_Usuario PantModificar = new Modificar.wfrm_Modificar_Registro_Usuario();


            cls_Usuarios_DAL obj_Usuario_DAL = new cls_Usuarios_DAL();
            obj_Usuario_DAL.cBandAxion = 'I';
            PantModificar.obj_Usuarios_DAL = obj_Usuario_DAL;
            PantModificar.ShowDialog();
        }


        #endregion

        #endregion

        #region Eventos

        private void dgv_Estudiantes_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = true;
        }

        private void txt_Filtro_TextChanged(object sender, TextChangedEventArgs e)
        {

            //Filtrar_Estudiantes();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                GlassHelper.ExtendGlass(this, -1, -1, -1, -1);
            }
            catch (Exception)
            {

                this.Background = Brushes.White;
            }

            CargarUsuario();
        }

        private void txt_Filtro_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

            //Permite solo números en el txtbox

            int ascci = Convert.ToInt32(Convert.ToChar(e.Text));

            if (ascci >= 48 && ascci <= 57)
            {

                e.Handled = false;

            }
            else
            {

                e.Handled = true;
            }

        }

        #endregion

        #region Botones


        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            CargarUsuario();
        }



        private void MenuItem_Click_3(object sender, RoutedEventArgs e)
        {
            Modificar_Usuario();
        }

        #endregion

        private void dgv_Usuario_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

        }

        private void btn_Nuevo_Click(object sender, RoutedEventArgs e)
        {
            Crear_Usuario();
        }

    }
}
