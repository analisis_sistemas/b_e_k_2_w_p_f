﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using System.Data;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;
using BLL_B_I_T_S.Catalogos_y_Mantenimiento;
using PL_B_I_T_S.Pantallas.Módulos;

namespace PL_B_I_T_S.Pantallas.Catalogos_y_Mantenimineto
{
    /// <summary>
    /// Lógica de interacción para wfrm_Mantenimiento_Roles_Usuario.xaml
    /// </summary>
    public partial class wfrm_Mantenimiento_Roles_Usuario : MetroWindow
    {
        public wfrm_Mantenimiento_Roles_Usuario()
        {
            InitializeComponent();
        }
        #region Variables Globales
        cls_Roles_Usuario_DAL obj_RolesUsuarios_DAL= new cls_Roles_Usuario_DAL();
        cls_RolUsuario_BLL obj_RolesUsuarios_BLL = new cls_RolUsuario_BLL();
        #endregion
        #region Métodos

        #region Cargar
        private void CargarRoles()
        {


            #region Variables
            string sMsjError = string.Empty;
            DataTable dtRolUsuario = new DataTable();
            #endregion

            obj_RolesUsuarios_BLL.Listar_RolUsuario(ref dtRolUsuario, ref sMsjError);

            if (sMsjError == string.Empty)
            {
                dgv_RolesUsuario.ItemsSource = null;
                dgv_RolesUsuario.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtRolUsuario });

            }
            else
            {
                dgv_RolesUsuario.ItemsSource = null;
                MessageBox.Show("Se presento un error al listar Roles de Usuario.\n\nDesc.Error ="
                                 + sMsjError, "Error al Listar", MessageBoxButton.OK, MessageBoxImage.Error);

                dgv_RolesUsuario.ItemsSource = null;
            }
        }
        #endregion
        #endregion

        private void btn_Listar_Click(object sender, RoutedEventArgs e)
        {
            CargarRoles();
        }
       
     
}
}
       
        