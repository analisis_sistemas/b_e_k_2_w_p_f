﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using System.Data;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;
using BLL_B_I_T_S.Catalogos_y_Mantenimiento;
using PL_B_I_T_S.Pantallas.Módulos;

namespace PL_B_I_T_S.Pantallas.Catalogos_y_Mantenimineto
{
    /// <summary>
    /// Interaction logic for wfrm_Mantenimiento_Cursos.xaml
    /// </summary>
    public partial class wfrm_Mantenimiento_Cursos : MetroWindow
    {
        public wfrm_Mantenimiento_Cursos()
        {
            InitializeComponent();
        }

        #region Variables Globales
        cls_Cursos_DAL obj_Cursos_DAL = new cls_Cursos_DAL();
        cls_Cursos_BLL obj_Cursos_BLL = new cls_Cursos_BLL();
        #endregion

        #region Métodos

        #region Cargar
        private void Cargar()
        {


            #region Variables
            string sMsjError = string.Empty;
            DataTable dtCursos = new DataTable();
            #endregion

            obj_Cursos_BLL.Listar_Cursos(ref dtCursos, ref sMsjError);

            if (sMsjError == string.Empty)
            {
                dgv_Cursos.ItemsSource = null;
                dgv_Cursos.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtCursos });

            }
            else
            {
                dgv_Cursos.ItemsSource = null;
                MessageBox.Show("Se presento un error al listar Cursos.\n\nDesc.Error ="
                                 + sMsjError, "Error al Listar", MessageBoxButton.OK, MessageBoxImage.Error);

                dgv_Cursos.ItemsSource = null;
            }
        }
        #endregion

        #region Filtrar
        public void Filtrar()
        {
            if (txt_Filtro.Text.Trim() == string.Empty)
            {
                Cargar();
            }
            else
            {
                #region Variables
                DataTable dtCursos = new DataTable();
                string sMsjError = string.Empty;

                #endregion



                obj_Cursos_BLL.Filtrar_Cursos(ref dtCursos, Convert.ToInt32(txt_Filtro.Text.Trim()), ref sMsjError);//Filtro se convierte en int solo cuando 

                if (sMsjError == string.Empty)
                {
                    dgv_Cursos.ItemsSource = null;
                    dgv_Cursos.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtCursos });
                }
                else
                {
                    dgv_Cursos.ItemsSource = null;
                    MessageBox.Show("Se presento un error al filtrar Cursos.\n\nDesc.Error ="
                                     + sMsjError, "Error al filtrar", MessageBoxButton.OK, MessageBoxImage.Error);

                    dgv_Cursos.ItemsSource = null;
                }

            }
        }

        #endregion

        #region Eliminar

        public void Eliminar()
        {
            #region Variables
            string sMjError = string.Empty;
            #endregion

            if (dgv_Cursos.Items.Count >= 1)
            {
                if (MessageBox.Show("Desea Eliminar Realmente el Registro Seleccionado?", "Alerta", MessageBoxButton.YesNo,
                                      MessageBoxImage.Asterisk) == MessageBoxResult.Yes)
                {

                    DataRowView drv = (DataRowView)dgv_Cursos.SelectedItems[0];
                    int index = dgv_Cursos.SelectedCells[0].Column.DisplayIndex;//4 xq en este caso el id esta en la celda 4
                    string sPK = drv.Row.ItemArray[index].ToString();

                    obj_Cursos_BLL.Eliminar_Cursos(sPK, ref sMjError);

                    if (sMjError != string.Empty)
                    {
                        MessageBox.Show("Se presentó un error al eliminar el registro.\n\nDesc.Error =" + sMjError, "Error al Eliminar",
                                        MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    else
                    {
                        Cargar();

                        MessageBox.Show("Se eliminó correctamente el Estudiante Seleccionado.",
                                        "Fin Éxitoso", MessageBoxButton.OK, MessageBoxImage.Information);

                    }
                }

            }
            else
            {
                MessageBox.Show("No se puede eliminar ningún Estudiante ya que no hay nada seleccionado.", "Alerta", MessageBoxButton.OK,
                                 MessageBoxImage.Asterisk);
            }
        }

        #endregion //No se va a eliminar Estudiantes

        #region Modificar

        public void Modificar()
        {

            Pantallas.Módulos.wfrm_Control_Notas pant_Modificar = new Módulos.wfrm_Control_Notas();

            cls_Cursos_DAL obj_Cursos_DAL = new cls_Cursos_DAL();
            obj_Cursos_DAL.cBandAxion = 'U';

            if (dgv_Cursos.Items.Count > 0)
            {
                if (dgv_Cursos.SelectedIndex > -1)
                {
                    DataRowView drv = (DataRowView)dgv_Cursos.SelectedItems[0];

                    int IdEstudiante = dgv_Cursos.SelectedCells[0].Column.DisplayIndex;
                    obj_Cursos_DAL.iId_Estudiante = Convert.ToInt32(drv.Row.ItemArray[IdEstudiante].ToString());

                    int Nombre = dgv_Cursos.SelectedCells[1].Column.DisplayIndex;
                    obj_Cursos_DAL.sNombre = drv.Row.ItemArray[Nombre].ToString();

                    int IdEstado = dgv_Cursos.SelectedCells[2].Column.DisplayIndex;
                    obj_Cursos_DAL.iIdEstado = Convert.ToInt32(drv.Row.ItemArray[IdEstado].ToString());

                    int Nota = dgv_Cursos.SelectedCells[3].Column.DisplayIndex;
                    obj_Cursos_DAL.dNota = Convert.ToDecimal(drv.Row.ItemArray[Nota].ToString());

                    

                    pant_Modificar.obj_Cursos_DAL = obj_Cursos_DAL;

                    pant_Modificar.ShowDialog();
                }
                else
                {
                    MessageBox.Show("No puede editar valores ya que no hay nada Seleccionado", "Alerta", MessageBoxButton.OK, MessageBoxImage.Asterisk);

                }
            }
            else
            {
                MessageBox.Show("No puede editar valores ya que no existen Datos", "Alerta", MessageBoxButton.OK, MessageBoxImage.Asterisk);
            }
        }

        #endregion

       

        #endregion





        private void btn_Eliminar_Click(object sender, RoutedEventArgs e)
        {
            Eliminar();
        }

        private void btn_Modificar_Click(object sender, RoutedEventArgs e)
        {
            Modificar();
        }
    }
}
