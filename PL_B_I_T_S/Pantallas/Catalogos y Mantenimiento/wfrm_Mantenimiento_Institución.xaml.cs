﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;
using BLL_B_I_T_S.Catalogos_y_Mantenimiento;
using System.Data;

namespace PL_B_I_T_S.Pantallas.Catalogos_y_Mantenimiento
{
    /// <summary>
    /// Interaction logic for wfrm_Mantenimiento_Institución.xaml
    /// </summary>
    public partial class wfrm_Mantenimiento_Institución : MetroWindow
    {

        #region Variables Globales
        cls_Institucion_DAL obj_institucion_DAL = new cls_Institucion_DAL();
        cls_Institucion_BLL obj_institucion_BLL = new cls_Institucion_BLL();
        #endregion

        public wfrm_Mantenimiento_Institución()
        {
            InitializeComponent();
        }

        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                GlassHelper.ExtendGlass(this, -1, -1, -1, -1);
            }
            catch (Exception)
            {

                this.Background = Brushes.White;
            }
        }

        

        #region Métodos

        #region Cargar
        private void Cargar()
        {


            #region Variables
            string sMsjError = string.Empty;
            DataTable dtInstitucion = new DataTable();
            #endregion

            obj_institucion_BLL.Listar_Institucion(ref dtInstitucion, ref sMsjError);

            if (sMsjError == string.Empty)
            {
                dgv_Institucion.ItemsSource = null;
                dgv_Institucion.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtInstitucion });

            }
            else
            {
                dgv_Institucion.ItemsSource = null;
                MessageBox.Show("Se presento un error al listar Periodos.\n\nDesc.Error ="
                                 + sMsjError, "Error al Listar", MessageBoxButton.OK, MessageBoxImage.Error);

                dgv_Institucion.ItemsSource = null;
            }
        }
        #endregion



        #region Eliminar

        public void Eliminar()
        {
            #region Variables
            string sMjError = string.Empty;
            #endregion

            if (dgv_Institucion.SelectedItems.Count != 0)
            {
                if (MessageBox.Show("Desea Eliminar Realmente el Registro Seleccionado?", "Alerta", MessageBoxButton.YesNo,
                                      MessageBoxImage.Asterisk) == MessageBoxResult.Yes)
                {

                    DataRowView drv = (DataRowView)dgv_Institucion.SelectedItems[0];
                    int index = dgv_Institucion.SelectedCells[0].Column.DisplayIndex;//0 porque el id esta en la celda 0
                    string sPK = drv.Row.ItemArray[index].ToString();

                    obj_institucion_BLL.Eliminar_Institucion(sPK, ref sMjError);

                    if (sMjError != string.Empty)
                    {
                        MessageBox.Show("Se presentó un error al eliminar el registro.\n\nDesc.Error =" + sMjError, "Error al Eliminar",
                                        MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    else
                    {
                        Cargar();

                        MessageBox.Show("Se eliminó correctamente la Institucion seleccionada.",
                                        "Fin Éxitoso", MessageBoxButton.OK, MessageBoxImage.Information);

                    }
                }

            }
            else
            {
                MessageBox.Show("No se puede eliminar la Institucion ya que no hay nada seleccionado.", "Alerta", MessageBoxButton.OK,
                                 MessageBoxImage.Asterisk);
            }
        }

        #endregion 

        #region Modificar

        public void Modificar()
        {

            Pantallas.Modificar.wfrm_Modificar_Instituciones pant_Modificar = new Modificar.wfrm_Modificar_Instituciones();

            cls_Institucion_DAL obj_institucion_DAL = new cls_Institucion_DAL();
            obj_institucion_DAL.cBandAxion = 'U';

            if (dgv_Institucion.Items.Count > 0)
            {
                if (dgv_Institucion.SelectedIndex > -1)
                {
                    DataRowView drv = (DataRowView)dgv_Institucion.SelectedItems[0];

                    int IdInstitucion = dgv_Institucion.SelectedCells[0].Column.DisplayIndex;
                    obj_institucion_DAL.sIDInstitucion = Convert.ToInt16(drv.Row.ItemArray[IdInstitucion].ToString());

                    int Nombre = dgv_Institucion.SelectedCells[1].Column.DisplayIndex;
                    obj_institucion_DAL.sNombre = drv.Row.ItemArray[Nombre].ToString();

                    pant_Modificar.obj_institucion_DAL = obj_institucion_DAL;

                    pant_Modificar.ShowDialog();
                }
                else
                {
                    MessageBox.Show("No puede editar valores ya que no hay nada Seleccionado", "Alerta", MessageBoxButton.OK, MessageBoxImage.Asterisk);

                }
            }
            else
            {
                MessageBox.Show("No puede editar valores ya que no existen Datos", "Alerta", MessageBoxButton.OK, MessageBoxImage.Asterisk);
            }
        }

        #endregion

        #region Crear

        public void Crear()
        {
            Pantallas.Modificar.wfrm_Modificar_Instituciones PantModificar = new Modificar.wfrm_Modificar_Instituciones();


            cls_Institucion_DAL obj_Institucion_DAL = new cls_Institucion_DAL();
            obj_institucion_DAL.cBandAxion = 'I';
            PantModificar.obj_institucion_DAL = obj_institucion_DAL;
            PantModificar.ShowDialog();
        }


        #endregion

        private void btn_Nuevo_Click(object sender, RoutedEventArgs e)
        {
            Crear();
        }

        #endregion

        private void btn_Modificar_Click(object sender, RoutedEventArgs e)
        {
            Modificar();
        }

        private void btn_Eliminar_Click(object sender, RoutedEventArgs e)
        {
            Eliminar();
        }

        private void btn_Listar_Click(object sender, RoutedEventArgs e)
        {
            Cargar();
        }

    }
}
