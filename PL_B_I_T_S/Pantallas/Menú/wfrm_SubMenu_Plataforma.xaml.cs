﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PL_B_I_T_S.Pantallas;
using MahApps.Metro.Controls;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;

namespace PL_B_I_T_S.Pantallas.Menú
{
    /// <summary>
    /// Lógica de interacción para wfrm_SubMenu_Plataforma.xaml
    /// </summary>
    public partial class wfrm_SubMenu_Plataforma : MetroWindow
    {
        public wfrm_SubMenu_Plataforma()
        {
            InitializeComponent();
        }
        #region Variables Globales

        cls_Estudiante_DAL obj_Estudiante_DAL = new cls_Estudiante_DAL();
        cls_Encargados_DAL obj_Encargados_DAL = new cls_Encargados_DAL();
        cls_Cursos_DAL obj_Cursos_DAL = new cls_Cursos_DAL();
        cls_Materias_DAL obj_Materias_DAL = new cls_Materias_DAL();

        #endregion

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            CrearExpediente();
            this.Close();
            
        }

        private void btn_Control_Notas_Click(object sender, RoutedEventArgs e)
        {
            CrearCursos();
        }

        private void btn_Materias_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            CrearMaterias();
            this.Show();

        }


        #region Metodos
        #region Crear

        public void CrearExpediente()
        {
            Pantallas.Modificar.wfrm_Modificar_ExpEstudiante PantModificar = new Modificar.wfrm_Modificar_ExpEstudiante();

            cls_Estudiante_DAL obj_Estudiante_DAL = new cls_Estudiante_DAL();
            cls_Encargados_DAL obj_Encargados_DAL = new cls_Encargados_DAL();
            obj_Estudiante_DAL.cBandAxion = 'I';
            obj_Encargados_DAL.cBandAxion = 'I';
            PantModificar.obj_Estudiante_DAL = obj_Estudiante_DAL;
            PantModificar.obj_Encargados_DAL = obj_Encargados_DAL;
            PantModificar.ShowDialog();
        }

        public void CrearCursos()
        {
            Pantallas.Módulos.wfrm_Control_Notas PantModificar = new Módulos.wfrm_Control_Notas();

            cls_Cursos_DAL obj_Cursos_DAL = new cls_Cursos_DAL();
            obj_Cursos_DAL.cBandAxion = 'I';
            PantModificar.obj_Cursos_DAL = obj_Cursos_DAL;
            PantModificar.ShowDialog();
        }

        public void CrearMaterias()
        {

            Pantallas.Modificar.wfrm_Modificar_Materia PantModificar = new Modificar.wfrm_Modificar_Materia();

            cls_Materias_DAL obj_Materias_DAL = new cls_Materias_DAL();
            obj_Cursos_DAL.cBandAxion = 'I';
            //PantModificar.obj_Materias_DAL = obj_Materias_DAL;
            PantModificar.ShowDialog();
        }

        #endregion
        #endregion
    }
}
