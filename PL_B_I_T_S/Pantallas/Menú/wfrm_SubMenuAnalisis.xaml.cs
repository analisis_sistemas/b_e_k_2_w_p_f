﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PL_B_I_T_S.Pantallas;
using MahApps.Metro.Controls;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;

namespace PL_B_I_T_S.Pantallas.Menú
{
    /// <summary>
    /// Lógica de interacción para wfrm_SubMenuAnalisis.xaml
    /// </summary>
    public partial class wfrm_SubMenuAnalisis : MetroWindow
    {
        public wfrm_SubMenuAnalisis()
        {
            InitializeComponent();
        }
      #region Variables Globales 
       

      #endregion



        private void btn_GeneradorBecas_Click(object sender, RoutedEventArgs e)
        {
            Pantallas.Módulos.wfrm_Generador_Becas PantModi = new Pantallas.Módulos.wfrm_Generador_Becas();
            PantModi.ShowDialog();
        }

        private void btn_AprobacionPatrocinador_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
