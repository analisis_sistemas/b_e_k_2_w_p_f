﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;
using BLL_B_I_T_S.Catalogos_y_Mantenimiento;

namespace PL_B_I_T_S.Pantallas.Menú
{
    /// <summary>
    /// Interaction logic for wpfrm_Menu.xaml
    /// </summary>
    public partial class wpfrm_Menu : MetroWindow
    {
        public wpfrm_Menu()
        {
            InitializeComponent();
        }
        #region Variables Globales

        cls_Desembolsos_DAL obj_Desembolsos_DAL = new cls_Desembolsos_DAL();
        cls_Patrocinador_DAL obj_Patrocinador_DAL = new cls_Patrocinador_DAL();
        cls_Usuarios_DAL obj_Usuarios_DAL = new cls_Usuarios_DAL();
        cls_Empleados_DAL obj_Empleados_DAL = new cls_Empleados_DAL();
        cls_Respuestas_DAL obj_Respuestas_DAL = new cls_Respuestas_DAL();

        #endregion

        #region Events
        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {

            if (MessageBox.Show("Desea Salir del Sistema?"
                , "Cerrar y Salir", MessageBoxButton.YesNo
                , MessageBoxImage.Asterisk) == MessageBoxResult.Yes)
            {
                this.Close();
            }

        }

        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                GlassHelper.ExtendGlass(this, -1, -1, -1, -1);
            }
            catch (Exception)
            {

                this.Background = Brushes.White;
            }
        }
        #endregion

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {

            Pantallas.Menú.wfrm_SubMenu_Plataforma PantModificar = new wfrm_SubMenu_Plataforma();
            this.Hide();
            PantModificar.ShowDialog();
            this.Show();

        }

        private void Tile_Click_1(object sender, RoutedEventArgs e)
        {
            this.Hide();
            CrearUsuarios();
            this.Show();
        }

        private void Tile_Click(object sender, RoutedEventArgs e)
        {
           
            Pantallas.Menú.wfrm_SubMenuAnalisis pantModi = new Pantallas.Menú.wfrm_SubMenuAnalisis();
            this.Hide();
            pantModi.ShowDialog();
            this.Show();
        }

        private void Tile_Click_2(object sender, RoutedEventArgs e)
        {
            this.Hide();
            CrearDesembolsos();
            this.Show();

        }

        private void Tile_Click_3(object sender, RoutedEventArgs e)
        {
            this.Hide();
            CrearPatrocinador();            
            this.Show();

        }

        private void Tile_Click_4(object sender, RoutedEventArgs e)
        {
            Pantallas.Módulos.wfrm_Reportes PantModi = new Módulos.wfrm_Reportes();
            this.Hide();
            PantModi.ShowDialog();
            this.Show();
        }



        #region Metodos
        #region Crear

        public void CrearDesembolsos()
        {
            Pantallas.Modificar.wfrm_Modificar_Desembolsos PantModificar = new Modificar.wfrm_Modificar_Desembolsos();

            cls_Desembolsos_DAL obj_Desembolsos_DAL = new cls_Desembolsos_DAL();
            obj_Desembolsos_DAL.cBandAxion = 'I';
            PantModificar.obj_Desembolsos_DAL = obj_Desembolsos_DAL;
            PantModificar.ShowDialog();
        }

        public void CrearPatrocinador()
        {
            Pantallas.Módulos.wfrm_Patrocinadores PantModificar = new Módulos.wfrm_Patrocinadores();

            cls_Patrocinador_DAL obj_Patrocinador_DAL = new cls_Patrocinador_DAL();
            obj_Patrocinador_DAL.cBandAxion = 'I';
            PantModificar.obj_Patrocinador_DAL = obj_Patrocinador_DAL;
            PantModificar.ShowDialog();
        }

        public void CrearUsuarios()
        {
            Pantallas.Modificar.wfrm_Modificar_Registro_Usuario pant_Modi = new Pantallas.Modificar.wfrm_Modificar_Registro_Usuario();

            cls_Usuarios_DAL obj_Usuario_DAL = new cls_Usuarios_DAL();
            obj_Usuario_DAL.cBandAxion = 'I';
            cls_Empleados_DAL obj_Empleados_DAL = new cls_Empleados_DAL();
            obj_Empleados_DAL.cBandAxion = 'I';
            cls_Respuestas_DAL obj_Respuestas_DAL = new cls_Respuestas_DAL();
            obj_Respuestas_DAL.cBandAxion = 'I';

            pant_Modi.obj_Usuarios_DAL = obj_Usuarios_DAL;
            pant_Modi.obj_Respuestas_DAL = obj_Respuestas_DAL;
            pant_Modi.obj_Empleados_DAL = obj_Empleados_DAL;
            pant_Modi.ShowDialog();
        }

        #endregion

        private void Tile_Click_5(object sender, RoutedEventArgs e)
        {

            Pantallas.Menú.wfrmMenuMantenimientos pantMantenimientos = new wfrmMenuMantenimientos();
            this.Hide();
            pantMantenimientos.ShowDialog();
            this.Show();
        }
        #endregion




    }
}
