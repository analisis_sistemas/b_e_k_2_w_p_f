﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using PL_B_I_T_S.Pantallas.Catalogos_y_Mantenimineto;
namespace PL_B_I_T_S.Pantallas.Menú
{
    /// <summary>
    /// Interaction logic for wfrmMenuMantenimientos.xaml
    /// </summary>
    public partial class wfrmMenuMantenimientos : MetroWindow
    {
        public wfrmMenuMantenimientos()
        {
            InitializeComponent();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
           Pantallas.Catalogos_y_Mantenimiento.wfrm_Mantenimiento_Institución pantInstitucion = new Catalogos_y_Mantenimiento.wfrm_Mantenimiento_Institución() ;
            pantInstitucion.ShowDialog();
        }

        private void Tile_Click(object sender, RoutedEventArgs e)
        {
            Pantallas.Catalogos_y_Mantenimineto.wfrm_Mantenimiento_Campus pantCampus = new Catalogos_y_Mantenimineto.wfrm_Mantenimiento_Campus();
            pantCampus.ShowDialog();
        }

        private void Tile_Click_1(object sender, RoutedEventArgs e)
        {
            Pantallas.Catalogos_y_Mantenimineto.wfrm_Mantenimiento_Patrocinador pantPatrocinador = new Catalogos_y_Mantenimineto.wfrm_Mantenimiento_Patrocinador();
            pantPatrocinador.ShowDialog();
        }

        private void Tile_Click_2(object sender, RoutedEventArgs e)
        {
            Pantallas.Catalogos_y_Mantenimineto.wfrm_Mantenimiento_Materias pantMaterias = new Catalogos_y_Mantenimineto.wfrm_Mantenimiento_Materias();
            pantMaterias.ShowDialog();
        }

        private void Tile_Click_3(object sender, RoutedEventArgs e)
        {
            Pantallas.Catalogos_y_Mantenimineto.wfrm_Mantenimiento_Roles_Usuario pantRoles = new Catalogos_y_Mantenimineto.wfrm_Mantenimiento_Roles_Usuario();
            pantRoles.ShowDialog();
        }

        private void Tile_Click_4(object sender, RoutedEventArgs e)
        {
            Pantallas.Catalogos_y_Mantenimineto.wfrm_Mantenimiento_Preguntas pantPreguntas = new Catalogos_y_Mantenimineto.wfrm_Mantenimiento_Preguntas();
            pantPreguntas.ShowDialog();
        }

        private void Tile_Click_5(object sender, RoutedEventArgs e)
        {
            Pantallas.Catalogos_y_Mantenimineto.wfrm_Mantenimiento_Periodo pantPeriodo = new Catalogos_y_Mantenimineto.wfrm_Mantenimiento_Periodo();
            pantPeriodo.ShowDialog();
        }

        private void Tile_Click_6(object sender, RoutedEventArgs e)
        {
            Pantallas.Catalogos_y_Mantenimineto.wfrm_Mantenimiento_Estado_Beca pantEstadoBecas = new Catalogos_y_Mantenimineto.wfrm_Mantenimiento_Estado_Beca();
            pantEstadoBecas.ShowDialog();
        }

        private void Tile_Click_7(object sender, RoutedEventArgs e)
        {
            Pantallas.Catalogos_y_Mantenimineto.wfrm_Mantenimiento_Estado_Curso pantEstadoCurso = new Catalogos_y_Mantenimineto.wfrm_Mantenimiento_Estado_Curso();
            pantEstadoCurso.ShowDialog();
        }

        private void Tile_Click_8(object sender, RoutedEventArgs e)
        {
            Pantallas.Catalogos_y_Mantenimineto.wfrm_Mantenimiento_Estado_Expediente pantEstadoEspediente = new Catalogos_y_Mantenimineto.wfrm_Mantenimiento_Estado_Expediente();
            pantEstadoEspediente.ShowDialog();
        }

        private void Tile_Click_9(object sender, RoutedEventArgs e)
        {
            Pantallas.Catalogos_y_Mantenimineto.wfrm_Mantenimiento_Estado_Factura pantEstadoFactura = new Catalogos_y_Mantenimineto.wfrm_Mantenimiento_Estado_Factura();
            pantEstadoFactura.ShowDialog();
        }

        private void Tile_Click_10(object sender, RoutedEventArgs e)
        {
            Pantallas.Catalogos_y_Mantenimineto.wfrm_Mantenimiento_Estado_Desembolsos pantEstadoDesembolso = new Catalogos_y_Mantenimineto.wfrm_Mantenimiento_Estado_Desembolsos();
            pantEstadoDesembolso.ShowDialog();
        }

        private void Tile_Click_11(object sender, RoutedEventArgs e)
        {
            Pantallas.Catalogos_y_Mantenimineto.wfrm_Mantenimiento_Estado_Patrocinador pantEstadoPatrocinador = new Catalogos_y_Mantenimineto.wfrm_Mantenimiento_Estado_Patrocinador();
            pantEstadoPatrocinador.ShowDialog();
        }

        private void Tile_Click_12(object sender, RoutedEventArgs e)
        {
            Pantallas.Catalogos_y_Mantenimineto.wfrm_Mantenimiento_Estado_Usuario pantEstadoUsuario = new Catalogos_y_Mantenimineto.wfrm_Mantenimiento_Estado_Usuario();
            pantEstadoUsuario.ShowDialog();
        }

        private void Tile_Click_13(object sender, RoutedEventArgs e)
        {
            Pantallas.Catalogos_y_Mantenimineto.wfrm_Mantenimiento_Beca pantBecas = new Catalogos_y_Mantenimineto.wfrm_Mantenimiento_Beca();
            pantBecas.ShowDialog();
        }

        private void Tile_Click_14(object sender, RoutedEventArgs e)
        {
            Pantallas.Catalogos_y_Mantenimineto.wfrm_Mantenimiento_Categoria pantCategoria = new Catalogos_y_Mantenimineto.wfrm_Mantenimiento_Categoria();
            pantCategoria.ShowDialog();
        }

        private void Tile_Click_15(object sender, RoutedEventArgs e)
        {
            Pantallas.Catalogos_y_Mantenimineto.wfrm_Mantenimiento_SubCategoria pantSubCategorias = new Catalogos_y_Mantenimineto.wfrm_Mantenimiento_SubCategoria();
            pantSubCategorias.ShowDialog();
        }

        private void Tile_Click_16(object sender, RoutedEventArgs e)
        {
            Pantallas.Catalogos_y_Mantenimineto.wfrm_Mantenimiento_Estudiante pantExpediente = new Catalogos_y_Mantenimineto.wfrm_Mantenimiento_Estudiante();
            pantExpediente.ShowDialog();
        }

        private void Tile_Click_17(object sender, RoutedEventArgs e)
        {
            Pantallas.Catalogos_y_Mantenimiento.wfrm_Mantenimiento_Usuarios pantUsuarios = new Catalogos_y_Mantenimiento.wfrm_Mantenimiento_Usuarios();
            pantUsuarios.ShowDialog();
        }
    }
}
