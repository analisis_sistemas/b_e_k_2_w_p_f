﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;

using DAL_B_I_T_S.Catalogos_y_Mantenimiento;
using BLL_B_I_T_S.Catalogos_y_Mantenimiento;
using System.Data;


namespace PL_B_I_T_S.Pantallas.Modificar
{
    /// <summary>
    /// Interaction logic for wfrm_Modificar_Campus.xaml
    /// </summary>
    public partial class wfrm_Modificar_Campus : MetroWindow
    {
        public wfrm_Modificar_Campus()
        {
            InitializeComponent();
        }

        #region VarGlobales
        public cls_Campus_DAL obj_campus_DAL = new cls_Campus_DAL();
        cls_Campus_BLL obj_campus_BLL = new cls_Campus_BLL();

        #endregion

        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                GlassHelper.ExtendGlass(this, -1, -1, -1, -1);
            }
            catch (Exception)
            {

                this.Background = Brushes.White;
            }
            Cargar_Combo();
            PintarControles();
        }

        private void txt_Campus_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int letra = Convert.ToInt32(Convert.ToChar(e.Text));

            int numeros = Convert.ToInt32(Convert.ToChar(e.Text));

            if ((letra >= 65 && letra <= 90 || letra >= 97 && letra <= 122) || (numeros >= 48 && numeros <= 57))
            {
                e.Handled = false;
            }
            else e.Handled = true;
        }

        #region Guardar

        public void Guardar()
        {

            string sMsjError = string.Empty;

            if (obj_campus_DAL.cBandAxion == 'I')
            {
                if (txt_idCampus.Text == string.Empty ||
                    txt_idCampus.Text == string.Empty)
                {
                    MessageBox.Show("Debe completar los campos con la información solicitada", "Error"
                        , MessageBoxButton.OK, MessageBoxImage.Error);

                }
                else
                {


                    obj_campus_DAL.iIdCampus = Convert.ToInt16(txt_idCampus.Text.Trim());
                    obj_campus_DAL.sNombre = txt_Campus.Text.Trim();
                    obj_campus_DAL.sIDInstitucion = Convert.ToInt16( cmb_Institucion.SelectedValue.ToString());



                    obj_campus_BLL.Crear_Campus(ref obj_campus_DAL, ref sMsjError);

                    if (sMsjError != string.Empty)
                    {
                        MessageBox.Show("Se presento un error al Insertar un Nuevo Campus.\n\nDesc.Error="
                            + sMsjError, "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                    }
                    else
                    {
                        MessageBox.Show("Campus Insertado Exitosamente", "Insert Exitoso", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    PintarControles();
                }

            }
            else
            {
                if (txt_idCampus.Text == string.Empty ||
                    txt_idCampus.Text == string.Empty)
                {
                    MessageBox.Show("Debe rellenar lo campos con la información Solicitada", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {



                    obj_campus_DAL.sIDInstitucion = Convert.ToInt16(txt_idCampus.Text.Trim());
                    obj_campus_DAL.sNombre = txt_Campus.Text.Trim();
                    obj_campus_DAL.sIDInstitucion = Convert.ToInt16(cmb_Institucion.SelectedValue.ToString());
                    obj_campus_BLL.Modificar_Campus(ref obj_campus_DAL, ref sMsjError);

                    MessageBox.Show("Campus Modificado", "Insert Exitoso", MessageBoxButton.OK, MessageBoxImage.Information);


                }
            }

        }
        #endregion

        #region PintarControles

        private void PintarControles()
        {
            if (obj_campus_DAL.cBandAxion == 'I')
            {
                Cargar_Combo();
                txt_idCampus.Text = string.Empty;
                txt_Campus.Text = string.Empty;
                cmb_Institucion.SelectedIndex = 0;

            }
            else
            {

                Cargar_Combo();
                txt_idCampus.Text = obj_campus_DAL.sIDInstitucion.ToString();
                txt_Campus.Text = obj_campus_DAL.sNombre.ToString();
                cmb_Institucion.SelectedValue  = obj_campus_DAL.sIDInstitucion.ToString();

                txt_idCampus.IsEnabled = false;


                

            }
        }
        #endregion

        #region Cargar Combos

        private void Cargar_Combo()
        {
            #region Variables
            string sMsjError = string.Empty;
            DataTable dtInstitucion = new DataTable();
            cls_Institucion_BLL inst = new cls_Institucion_BLL();
            #endregion

            //Llamo los métodos de cada tabla q necesito


            inst.Listar_Institucion(ref dtInstitucion, ref sMsjError);
            

            if (sMsjError == string.Empty)
            {



                cmb_Institucion.ItemsSource = null;
                cmb_Institucion.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtInstitucion });

                //In WPF use DisplayMemberPath and SelectedValuePath.

                cmb_Institucion.SelectedValuePath = dtInstitucion.Columns[0].ToString();
                cmb_Institucion.DisplayMemberPath = dtInstitucion.Columns[1].ToString();

            }
            else
            {


                cmb_Institucion.ItemsSource = null;


                MessageBox.Show("Se presento un error al Listar Patrocinadores.\n\nDesc.Error=" + sMsjError, "Error al listar",
                                 MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        #endregion

        private void btn_Guardar_Click(object sender, RoutedEventArgs e)
        {
            Guardar();
        }
    }
}
