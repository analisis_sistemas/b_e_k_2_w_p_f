﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using System.Data;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;
using BLL_B_I_T_S.Catalogos_y_Mantenimiento;
using PL_B_I_T_S.Pantallas.Módulos;

namespace PL_B_I_T_S.Pantallas.Modificar
{
    public partial class wfrm_Modificar_Registro_Usuario : MetroWindow
    {
        #region Variables Globales

        public cls_Usuarios_DAL obj_Usuarios_DAL = new cls_Usuarios_DAL();
        public cls_Usuarios_BLL obj_Usuarios_BLL = new cls_Usuarios_BLL();

        public cls_Empleados_DAL obj_Empleados_DAL = new cls_Empleados_DAL();
        public cls_Empleados_BLL obj_Empleados_BLL = new cls_Empleados_BLL();

        public cls_Respuestas_DAL obj_Respuestas_DAL = new cls_Respuestas_DAL();
        cls_Respuestas_BLL obj_Respuestas_BLL = new cls_Respuestas_BLL();


        cls_EstadoUsuario_BLL obj_Estado_Usuario_BLL = new cls_EstadoUsuario_BLL();

        cls_RolUsuario_BLL obj_RolUsuario_BLL = new cls_RolUsuario_BLL();

        cls_Campus_BLL obj_Campus_BLL = new cls_Campus_BLL();

        #endregion

        public wfrm_Modificar_Registro_Usuario()
        {
            InitializeComponent();
        }

        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                GlassHelper.ExtendGlass(this, -1, -1, -1, -1);
                PintarControles();

            }
            catch (Exception)
            {

                this.Background = Brushes.White;
            }

        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            Guardar();
        }

        #region Métodos

        #region Guardar

        public void Guardar()
        {

            string sMsjError = string.Empty;
            obj_Usuarios_DAL.cBandAxion = 'I';
            if (obj_Usuarios_DAL.cBandAxion == 'I')
            {
                if (txt_NombreTrabajador.Text == string.Empty ||
                    txt_Apell1Trabajador.Text == string.Empty ||
                    txt_Apell2Trabajador.Text == string.Empty ||
                    txt_IdentificacionTrabajador.Text == string.Empty ||
                    dtp_Fecha_NacimientoTrabajador.Text == string.Empty ||
                    cbx_CampusTrabajador.Text == string.Empty ||
                    cbx_EstadoTrabajador.Text == string.Empty ||
                    cbx_TipoRol.Text == string.Empty ||
                    txt_CorreoTrabajador.Text == string.Empty ||
                    pwb_Contrasena.Password == string.Empty ||
                    txt_PrimeraMascota.Text == string.Empty ||
                    dtp_Fecha_NacimientoMadre.Text == string.Empty ||
                    txt_PrimerNumTele.Text == string.Empty ||
                    txt_CiudadNaciMadre.Text == string.Empty ||
                    txt_NombreEscuela.Text == string.Empty)
                {
                    MessageBox.Show("Debe rellenar los campos con la información solicitada", "Error"
                        , MessageBoxButton.OK, MessageBoxImage.Error);

                }
                else
                {

                    obj_Empleados_DAL.sNombre = txt_NombreTrabajador.Text.Trim();
                    obj_Empleados_DAL.sPrimerApellido = txt_Apell1Trabajador.Text.Trim();
                    obj_Empleados_DAL.sSegundoApellido = txt_Apell2Trabajador.Text.Trim();
                    obj_Empleados_DAL.iIdEmpleado = Convert.ToInt32(txt_IdentificacionTrabajador.Text.Trim());
                    obj_Empleados_DAL.dtFechaNacimiento = Convert.ToDateTime(dtp_Fecha_NacimientoTrabajador.SelectedDate);
                    obj_Empleados_DAL.iIdCampus = Convert.ToInt32(cbx_CampusTrabajador.SelectedValue.ToString());
                    obj_Usuarios_DAL.iIdEstado = Convert.ToInt32(cbx_EstadoTrabajador.SelectedValue.ToString());
                    obj_Usuarios_DAL.iIdRol = Convert.ToInt32(cbx_TipoRol.SelectedValue.ToString());
                    obj_Empleados_DAL.sCorreoElectronico = txt_CorreoTrabajador.Text.Trim();
                    obj_Usuarios_DAL.sPassword = pwb_Contrasena.Password;

                    obj_Respuestas_DAL.sIdUsuario = obj_Empleados_DAL.iIdEmpleado.ToString();
                    obj_Respuestas_DAL.ls_Lista.Add(txt_PrimeraMascota.Text.Trim()); 
                    obj_Respuestas_DAL.ls_Lista.Add((dtp_Fecha_NacimientoTrabajador.SelectedDate.ToString()));
                    obj_Respuestas_DAL.ls_Lista.Add(txt_PrimerNumTele.Text.Trim());
                    obj_Respuestas_DAL.ls_Lista.Add(txt_CiudadNaciMadre.Text.Trim());
                    obj_Respuestas_DAL.ls_Lista.Add(txt_NombreEscuela.Text.Trim());

                    obj_Empleados_DAL.sIdUsuario = obj_Empleados_DAL.iIdEmpleado.ToString();
                    obj_Usuarios_DAL.sIdUsuario = obj_Empleados_DAL.iIdEmpleado.ToString();
                    obj_Usuarios_DAL.sNombre = obj_Empleados_DAL.sNombre;
                    obj_Empleados_DAL.sCedula = obj_Empleados_DAL.iIdEmpleado.ToString();
                    obj_Respuestas_DAL.iIdPregunta = 1;
                    
                    obj_Usuarios_BLL.Crear_Usuarios(ref obj_Usuarios_DAL, ref sMsjError);
                    obj_Empleados_BLL.Crear_Empleados(ref obj_Empleados_DAL, ref sMsjError);
                    obj_Respuestas_BLL.Crear_Respuestas(ref obj_Respuestas_DAL, ref sMsjError);

                    if (sMsjError != string.Empty)
                    {
                        MessageBox.Show("Se presento un error al Insertar un nuevo USUARIO.\n\nDesc.Error="
                            + sMsjError, "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                    }
                    else
                    {
                        MessageBox.Show("Usuario Insertado Exitosamente", "Insert Exitoso", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }

            }
            else
            {
                if (txt_NombreTrabajador.Text == string.Empty ||
                    txt_Apell1Trabajador.Text == string.Empty ||
                    txt_Apell2Trabajador.Text == string.Empty ||
                    txt_IdentificacionTrabajador.Text == string.Empty ||
                    dtp_Fecha_NacimientoTrabajador.Text == string.Empty ||
                    cbx_CampusTrabajador.Text == string.Empty ||
                    cbx_EstadoTrabajador.Text == string.Empty ||
                    cbx_TipoRol.Text == string.Empty ||
                    txt_CorreoTrabajador.Text == string.Empty ||
                    pwb_Contrasena.Password == string.Empty ||
                    txt_PrimeraMascota.Text == string.Empty ||
                    dtp_Fecha_NacimientoMadre.Text == string.Empty ||
                    txt_PrimerNumTele.Text == string.Empty ||
                    txt_CiudadNaciMadre.Text == string.Empty ||
                    txt_NombreEscuela.Text == string.Empty)
                {
                    MessageBox.Show("Debe rellenar lo campos con la información Solicitada", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    //obj_Empleados_DAL.sNombre = txt_NombreTrabajador.Text.Trim();
                    //obj_Empleados_DAL.sPrimerApellido = txt_Apell1Trabajador.Text.Trim();
                    //obj_Empleados_DAL.sSegundoApellido = txt_Apell2Trabajador.Text.Trim();
                    //obj_Empleados_DAL.iIdEmpleado = Convert.ToInt32(txt_IdentificacionTrabajador.Text.Trim());
                    //obj_Empleados_DAL.dtFechaNacimiento = Convert.ToDateTime(dtp_Fecha_NacimientoTrabajador.SelectedDate);
                    //obj_Empleados_DAL.iIdCampus = Convert.ToInt32(cbx_CampusTrabajador.SelectedValuePath.ToString());
                    //obj_Usuarios_DAL.iIdEstado = Convert.ToInt32(cbx_EstadoTrabajador.SelectedValuePath.ToString());
                    //obj_Usuarios_DAL.iIdRol = Convert.ToInt32(cbx_TipoRol.SelectedValuePath.ToString());
                    //obj_Empleados_DAL.sCorreoElectronico = txt_CorreoTrabajador.Text.Trim();
                    //obj_Usuarios_DAL.sPassword = pwb_Contrasena.Password;
                    //obj_Respuestas_DAL.sIdUsuario = Convert.ToString(txt_IdentificacionTrabajador.Text.Trim()); //Hay que generar codigo para que cada una se guarde en una fila de la tabla respuestas
                    //obj_Respuestas_DAL.sRespuesta = txt_PrimeraMascota.Text.Trim(); //Falta ligar el id de pregunta
                    //obj_Respuestas_DAL.sRespuesta = (dtp_Fecha_NacimientoTrabajador.SelectedDate.ToString());
                    //obj_Respuestas_DAL.sRespuesta = txt_PrimerNumTele.Text.Trim();
                    //obj_Respuestas_DAL.sRespuesta = txt_CiudadNaciMadre.Text.Trim();
                    //obj_Respuestas_DAL.sRespuesta = txt_NombreEscuela.Text.Trim();


                }
            }

        }
        #endregion

        #region PintarControles

        private void PintarControles()
        {
            obj_Empleados_DAL.cBandAxion = 'I';
            if (obj_Empleados_DAL.cBandAxion == 'I')
            {
                Cargar_Combo();

                     txt_NombreTrabajador.Text = string.Empty;
                     txt_Apell1Trabajador.Text = string.Empty;
                     txt_Apell2Trabajador.Text = string.Empty;
                     txt_IdentificacionTrabajador.Text = string.Empty;
                     dtp_Fecha_NacimientoTrabajador.Text = string.Empty;
                     cbx_CampusTrabajador.Text = string.Empty;
                     cbx_EstadoTrabajador.Text = string.Empty;
                     cbx_TipoRol.Text = string.Empty;
                     txt_CorreoTrabajador.Text = string.Empty;
                     pwb_Contrasena.Password = string.Empty;
                     txt_PrimeraMascota.Text = string.Empty;
                     dtp_Fecha_NacimientoMadre.Text = string.Empty;
                     txt_PrimerNumTele.Text =string.Empty;
                     txt_CiudadNaciMadre.Text = string.Empty; 
                     txt_NombreEscuela.Text = string.Empty;

            }
            else
            {

                //Cargar_Combo();
                //txt_NombreTrabajador.Text = obj_Empleados_DAL.sNombre.ToString();
                //txt_Apell1Trabajador.Text = obj_Empleados_DAL.sPrimerApellido.ToString();
                //txt_Apell2Trabajador.Text = obj_Empleados_DAL.sSegundoApellido.ToString();
                //txt_IdentificacionTrabajador.Text = obj_Empleados_DAL.sCedula.ToString();
                //dtp_Fecha_NacimientoTrabajador.Text = obj_Empleados_DAL.dtFechaNacimiento.ToString();
                //cbx_CampusTrabajador.Text = obj_Empleados_DAL.iIdCampus.ToString();          
                //txt_CorreoTrabajador.Text = obj_Empleados_DAL.sCorreoElectronico.ToString();
                //pwb_Contrasena.Password = obj_Usuarios_DAL.sPassword.ToString();
                //txt_PrimeraMascota.Text = obj_Respuestas_DAL.sRespuesta.ToString();  //Buscar metodo para guardar todas las respuestas con el id.
                //dtp_Fecha_NacimientoMadre.Text = obj_Respuestas_DAL.sRespuesta.ToString();
                //txt_PrimerNumTele.Text = obj_Respuestas_DAL.sRespuesta.ToString();
                //txt_CiudadNaciMadre.Text = obj_Respuestas_DAL.sRespuesta.ToString();
                //txt_NombreEscuela.Text = obj_Respuestas_DAL.sRespuesta.ToString();




            }
        }
        #endregion

        #region Cargar Combos

        private void Cargar_Combo()
        {
            #region Variables
            string sMsjError = string.Empty;
            DataTable dtEstados = new DataTable();
            DataTable dtRol = new DataTable();
            DataTable dtCampus = new DataTable();

            #endregion

            //Llamo los métodos de cada tabla q necesito

            obj_RolUsuario_BLL.Listar_RolUsuario(ref dtRol, ref sMsjError);
            obj_Estado_Usuario_BLL.Listar_EstadoUsuario(ref dtEstados, ref sMsjError);
            obj_Campus_BLL.Listar_Campus(ref dtCampus, ref sMsjError);



            if (sMsjError == string.Empty)
            {

          

               cbx_CampusTrabajador.ItemsSource = null;
                cbx_CampusTrabajador.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtCampus });

                cbx_EstadoTrabajador.ItemsSource = null;
                cbx_EstadoTrabajador.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtEstados });

                cbx_TipoRol.ItemsSource = null;
                cbx_TipoRol.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtRol });

                //In WPF use DisplayMemberPath and SelectedValuePath.

                cbx_CampusTrabajador.SelectedValuePath = dtCampus.Columns[0].ToString();
                cbx_CampusTrabajador.DisplayMemberPath = dtCampus.Columns[1].ToString();

                cbx_TipoRol.SelectedValuePath = dtRol.Columns[0].ToString();
                cbx_TipoRol.DisplayMemberPath = dtRol.Columns[1].ToString();

                cbx_EstadoTrabajador.SelectedValuePath = dtEstados.Columns[0].ToString();
                cbx_EstadoTrabajador.DisplayMemberPath = dtEstados.Columns[1].ToString();

            }
            else
            {
              
                cbx_CampusTrabajador.ItemsSource = null;
                cbx_EstadoTrabajador.ItemsSource = null;
                cbx_TipoRol.ItemsSource = null;

                MessageBox.Show("Se presento un error al Listar los Usuarios.\n\nDesc.Error=" + sMsjError, "Error al listar",
                                 MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        #endregion

        #endregion

        #region eventos

        private void txt_NombreTrabajador_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int letra = Convert.ToInt32(Convert.ToChar(e.Text));

            if (letra >= 65 && letra <= 90 || letra >= 97 && letra <= 122)
            {
                e.Handled = false;
            }
            else e.Handled = true;
        }

        private void txt_Apell1Trabajador_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int letra = Convert.ToInt32(Convert.ToChar(e.Text));

            if (letra >= 65 && letra <= 90 || letra >= 97 && letra <= 122)
            {
                e.Handled = false;
            }
            else e.Handled = true;
        }

        private void txt_Apell2Trabajador_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int letra = Convert.ToInt32(Convert.ToChar(e.Text));

            if (letra >= 65 && letra <= 90 || letra >= 97 && letra <= 122)
            {
                e.Handled = false;
            }
            else e.Handled = true;
        }

        private void txt_IdentificacionTrabajador_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int letra = Convert.ToInt32(Convert.ToChar(e.Text));

            int numeros = Convert.ToInt32(Convert.ToChar(e.Text));

            if ((letra >= 65 && letra <= 90 || letra >= 97 && letra <= 122) || (numeros >= 48 && numeros <= 57))
            {
                e.Handled = false;
            }
            else e.Handled = true;

        }

        private void txt_PrimeraMascota_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int letra = Convert.ToInt32(Convert.ToChar(e.Text));

            if (letra >= 65 && letra <= 90 || letra >= 97 && letra <= 122)
            {
                e.Handled = false;
            }
            else e.Handled = true;
        }

        private void txt_PrimerNumTele_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int numeros = Convert.ToInt32(Convert.ToChar(e.Text));

            if (numeros >= 48 && numeros <= 57)
            {

                e.Handled = false;

            }
            else
            {

                e.Handled = true;
            }
        }

        private void txt_CiudadNaciMadre_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int letra = Convert.ToInt32(Convert.ToChar(e.Text));

            int numeros = Convert.ToInt32(Convert.ToChar(e.Text));

            if ((letra >= 65 && letra <= 90 || letra >= 97 && letra <= 122) || (numeros >= 48 && numeros <= 57))
            {
                e.Handled = false;
            }
            else e.Handled = true;
        }

        private void txt_NombreEscuela_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int letra = Convert.ToInt32(Convert.ToChar(e.Text));

            int numeros = Convert.ToInt32(Convert.ToChar(e.Text));

            if ((letra >= 65 && letra <= 90 || letra >= 97 && letra <= 122) || (numeros >= 48 && numeros <= 57))
            {
                e.Handled = false;
            }
            else e.Handled = true;
        }
        #endregion


    }
}
