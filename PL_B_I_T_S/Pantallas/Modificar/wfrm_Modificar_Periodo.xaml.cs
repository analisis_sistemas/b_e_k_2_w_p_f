﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;
using BLL_B_I_T_S.Catalogos_y_Mantenimiento;
using System.Data;

namespace PL_B_I_T_S.Pantallas.Modificar
{
    /// <summary>
    /// Lógica de interacción para wfrm_Modificar_Periodo.xaml
    /// </summary>
    public partial class wfrm_Modificar_Periodo : MetroWindow
    {
        public wfrm_Modificar_Periodo()
        {
            InitializeComponent();
        }
        #region eventos 
        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                GlassHelper.ExtendGlass(this, -1, -1, -1, -1);
            }
            catch (Exception)
            {

                this.Background = Brushes.White;
            }
            PintarControles();
        }

        private void txt_IdPeriodo_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int numeros = Convert.ToInt32(Convert.ToChar(e.Text));

            if (numeros >= 48 && numeros <= 57)
            {

                e.Handled = false;

            }
            else
            {

                e.Handled = true;
            }
        }

        private void txt_NombrePeriodo_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int letra = Convert.ToInt32(Convert.ToChar(e.Text));

            if (letra >= 65 && letra <= 90 || letra >= 97 && letra <= 122)
            {
                e.Handled = false;
            }
            else e.Handled = true;
        }
        #endregion

        #region Var Glob
        public cls_Periodo_DAL obj_Periodo_DAL = new cls_Periodo_DAL();
        cls_Periodo_BLL obj_Periodo_BLL = new cls_Periodo_BLL();
       
        #endregion

        #region Guardar

        public void Guardar()
        {

            string sMsjError = string.Empty;

            if (obj_Periodo_DAL.cBandAxion == 'I')
            {
                if (txt_IdPeriodo.Text == string.Empty ||
                    txt_NombrePeriodo.Text == string.Empty )
                {
                    MessageBox.Show("Debe completar los campos con la información solicitada", "Error"
                        , MessageBoxButton.OK, MessageBoxImage.Error);

                }
                else
                {


                    obj_Periodo_DAL.iIdPeriodo = Convert.ToInt32(txt_IdPeriodo.Text.Trim());
                    obj_Periodo_DAL.sNombre = txt_NombrePeriodo.Text.Trim();
               


                    obj_Periodo_BLL.Crear_Periodo(ref obj_Periodo_DAL, ref sMsjError);

                    if (sMsjError != string.Empty)
                    {
                        MessageBox.Show("Se presento un error al Insertar un Nuevo Periodo.\n\nDesc.Error="
                            + sMsjError, "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                    }
                    else
                    {
                        MessageBox.Show("Periodo Insertado Exitosamente", "Insert Exitoso", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    PintarControles();
                }

            }
            else
            {
                if (txt_IdPeriodo.Text == string.Empty ||
                    txt_NombrePeriodo.Text == string.Empty)
                {
                    MessageBox.Show("Debe rellenar lo campos con la información Solicitada", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {



                    obj_Periodo_DAL.iIdPeriodo = Convert.ToInt32(txt_IdPeriodo.Text.Trim());
                    obj_Periodo_DAL.sNombre = txt_NombrePeriodo.Text.Trim();
                    obj_Periodo_BLL.Modificar_Periodo(ref obj_Periodo_DAL, ref sMsjError);

                    MessageBox.Show("Periodo Modificado", "Insert exitoso", MessageBoxButton.OK, MessageBoxImage.Information);


                }
            }

        }
        #endregion

        #region PintarControles

        private void PintarControles()
        {
            if (obj_Periodo_DAL.cBandAxion == 'I')
            {

               txt_IdPeriodo.Text = string.Empty;
               txt_NombrePeriodo.Text = string.Empty;

            }
            else
            {
               

                txt_IdPeriodo.Text = obj_Periodo_DAL.iIdPeriodo.ToString();
                txt_NombrePeriodo.Text = obj_Periodo_DAL.sNombre.ToString();

                txt_IdPeriodo.IsEnabled = false;
              



            }
        }
        #endregion

        private void btn_Guardar_Periodo_Click(object sender, RoutedEventArgs e)
        {
            Guardar();
        }
    }
}
