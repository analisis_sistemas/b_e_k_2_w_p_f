﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Data;
using System.Text.RegularExpressions;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;
using BLL_B_I_T_S.Catalogos_y_Mantenimiento;
using PL_B_I_T_S.Pantallas.Menú;
using BLL_B_I_T_S.Logica_General;
using System.Collections.ObjectModel;


namespace PL_B_I_T_S.Pantallas.Modificar
{
    /// <summary>
    /// Interaction logic for wfrm_Modificar_ExpEstudiante.xaml
    /// </summary>
    public partial class wfrm_Modificar_ExpEstudiante : MetroWindow
    {
        public wfrm_Modificar_ExpEstudiante()
        {
            InitializeComponent();

        }

        #region Var Glob
        public cls_Estudiante_DAL obj_Estudiante_DAL = new cls_Estudiante_DAL();
        cls_Estudiante_BLL obj_Estudiante_BLL = new cls_Estudiante_BLL();
        public cls_Encargados_DAL obj_Encargados_DAL = new cls_Encargados_DAL();
        cls_Estado_Expediente_BLL obj_Estado_Estudiante_BLL = new cls_Estado_Expediente_BLL();
        cls_Beca_BLL obj_Beca_BLL = new cls_Beca_BLL();
        cls_Campus_BLL obj_Campus_BLL = new cls_Campus_BLL();
        bool Valmail;


        #endregion

        #region Botones


        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {

            Guardar();
        }
        #endregion

        #region Eventos
        private void wfrm_Modificar_Estudiante_Loaded(object sender, RoutedEventArgs e)
        {
            PintarControles();
            try
            {
                GlassHelper.ExtendGlass(this, -1, -1, -1, -1);
            }
            catch (Exception)
            {

                this.Background = Brushes.White;
            }
        }

        private void txt_Grado_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int numeros = Convert.ToInt32(Convert.ToChar(e.Text));

            if (numeros >= 48 && numeros <= 57)
            {

                e.Handled = false;

            }
            else
            {

                e.Handled = true;
            }
        }

        private void txt_id_Estudiante_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int numeros = Convert.ToInt32(Convert.ToChar(e.Text));

            if (numeros >= 48 && numeros <= 57)
            {

                e.Handled = false;

            }
            else
            {

                e.Handled = true;
            }
        }

        private void txt_Telefono_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int numeros = Convert.ToInt32(Convert.ToChar(e.Text));

            if (numeros >= 48 && numeros <= 57)
            {

                e.Handled = false;

            }
            else
            {

                e.Handled = true;
            }
        }

        private void txt_Nombre_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

            int letra = Convert.ToInt32(Convert.ToChar(e.Text));

            if (letra >= 65 && letra <= 90 || letra >= 97 || letra == 161 || letra == 164 && letra <= 122)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;

            }

        }

        private void txt_Apell1_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

            int letra = Convert.ToInt32(Convert.ToChar(e.Text));

            if (letra >= 65 && letra <= 90 || letra >= 97 || letra == 161 || letra == 164 && letra <= 122)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;

            }

        }

        private void txt_Apell2_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

            int letra = Convert.ToInt32(Convert.ToChar(e.Text));

            if (letra >= 65 && letra <= 90 || letra >= 97 || letra == 161 || letra == 164 && letra <= 122)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;

            }
        }

        private void txt_Identificacion_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int letra = Convert.ToInt32(Convert.ToChar(e.Text));

            if (letra >= 65 && letra <= 90 || letra >= 97 || letra == 164 && letra <= 122)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;

            }
        }



        private void txt_Nacionalidad_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int letra = Convert.ToInt32(Convert.ToChar(e.Text));

            if (letra >= 65 && letra <= 90 || letra >= 97 && letra <= 122)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;

            }
        }

        private void txt_Montogasto_Copy_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int numeros = Convert.ToInt32(Convert.ToChar(e.Text));

            if (numeros >= 48 && numeros <= 57)
            {

                e.Handled = false;

            }
            else
            {

                e.Handled = true;
            }
        }

        private void txt_MontoIngreso_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int numeros = Convert.ToInt32(Convert.ToChar(e.Text));

            if (numeros >= 48 && numeros <= 57)
            {

                e.Handled = false;

            }
            else
            {

                e.Handled = true;
            }
        }

        private void txt_NombreEncargado_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int letra = Convert.ToInt32(Convert.ToChar(e.Text));

            if (letra >= 65 && letra <= 90 || letra >= 97 && letra <= 122)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;

            }
        }

        private void txt_Apell1Encargado_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int letra = Convert.ToInt32(Convert.ToChar(e.Text));

            if (letra >= 65 && letra <= 90 || letra >= 97 && letra <= 122)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;

            }
        }

        private void txt_Apell2Encargado_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int letra = Convert.ToInt32(Convert.ToChar(e.Text));

            if (letra >= 65 && letra <= 90 || letra >= 97 && letra <= 122)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;

            }
        }

        private void txt_IdentificacionEncargado_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int letra = Convert.ToInt32(Convert.ToChar(e.Text));

            if (letra >= 65 && letra <= 90 || letra >= 97 && letra <= 122)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;

            }
        }

        private void txt_Parentezco_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int letra = Convert.ToInt32(Convert.ToChar(e.Text));

            if (letra >= 65 && letra <= 90 || letra >= 97 && letra <= 122)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;

            }
        }

        private void nmc_CantPersCasa_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int numeros = Convert.ToInt32(Convert.ToChar(e.Text));

            if (numeros >= 48 && numeros <= 57)
            {

                e.Handled = false;

            }
            else
            {

                e.Handled = true;
            }
        }

        private void nmc_CantPersTrabajan_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int numeros = Convert.ToInt32(Convert.ToChar(e.Text));

            if (numeros >= 48 && numeros <= 57)
            {

                e.Handled = false;

            }
            else
            {

                e.Handled = true;
            }
        }
        #endregion

        #region Métodos

        #region ValEmail

        private void ValEmail()
        {
            if (!Regex.IsMatch(txt_Correo.Text, @"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"))
            {
                Valmail = false;
                MessageBox.Show("Debe Ingresar una Direccion de Correo Electrónico Válida");
                txt_Correo.Select(0, txt_Correo.Text.Length);
                txt_Correo.Focus();
                
            }
            else
            {
                Valmail = true;
               
                
            }

        }
        #endregion

        #region Guardar

        public void Guardar()
        {

            string sMsjError = string.Empty;

            if (obj_Estudiante_DAL.cBandAxion == 'I')
            {
                if (txt_Nombre.Text == string.Empty ||
                    txt_Apell1.Text == string.Empty ||
                    txt_Apell2.Text == string.Empty ||
                    txt_Direccion.Text == string.Empty ||
                    txt_id_Estudiante.Text == string.Empty ||
                    txt_Nacionalidad.Text == string.Empty ||
                    txt_Telefono.Text == string.Empty ||
                    txt_Identificacion.Text == string.Empty)
                {
                    MessageBox.Show("Debe realizar los campos con la información solicitada", "Error"
                        , MessageBoxButton.OK, MessageBoxImage.Error);

                }
                else
                {
                    obj_Estudiante_DAL.iIdEstudiante = Convert.ToInt32(txt_id_Estudiante.Text.Trim());
                    obj_Estudiante_DAL.sNombre = txt_Nombre.Text.Trim();
                    obj_Estudiante_DAL.sPrimerApellido = txt_Apell1.Text.Trim();
                    obj_Estudiante_DAL.sSegundoApellido = txt_Apell2.Text.Trim();
                    obj_Estudiante_DAL.sIdentificacion = txt_Identificacion.Text.Trim();
                    obj_Estudiante_DAL.sDireccion = txt_Direccion.Text.Trim();
                    obj_Estudiante_DAL.iTelefono = Convert.ToInt32(txt_Telefono.Text.Trim());
                    obj_Estudiante_DAL.dFechaNacimiento = dtp_Fecha_Nacimiento.SelectedDate.Value.Date;
                    obj_Estudiante_DAL.cGenero = Convert.ToChar(cbx_Genero.SelectedValue);
                    obj_Estudiante_DAL.sNacionalidad = txt_Nacionalidad.Text.Trim();                  

                    obj_Estudiante_DAL.sNivel_Academico = cmb_NivelAcademico.SelectedValue.ToString();

                    obj_Estudiante_DAL.iGrado = Convert.ToInt32(txt_Grado.Text.Trim());
                    obj_Estudiante_DAL.dTotalCosto = Convert.ToDecimal(txt_TotalCosto.Text.Trim());


                    obj_Estudiante_DAL.iIdCampus = Convert.ToInt32(cbx_Campus.SelectedValue.ToString());

                    obj_Estudiante_DAL.iIdBeca = Convert.ToInt32(cbx_Beca.SelectedValue.ToString());
                    obj_Estudiante_DAL.iIdEstado = Convert.ToInt32(cbx_Estado.SelectedValue.ToString());

                    ComboBoxItem sTipocasa = (ComboBoxItem)cmb_TipoCasa.SelectedItem;
                    obj_Estudiante_DAL.sTipoCasa = sTipocasa.Content.ToString();

                    obj_Estudiante_DAL.iCantNucleoFam = Convert.ToInt32(nmc_CantPersCasa.Value.ToString());
                    obj_Estudiante_DAL.iCantTrabajador = Convert.ToInt32(nmc_CantPersTrabajan.Value.ToString());

                    ComboBoxItem IngresoMes = (ComboBoxItem)cmb_Ingreso.SelectedItem;
                    obj_Estudiante_DAL.sIngresoMesBruto = IngresoMes.Content.ToString();

                    obj_Estudiante_DAL.dIngresoMesNeto = Convert.ToDecimal(txt_MontoIngreso.Text.Trim());
                    obj_Estudiante_DAL.dGastoMensual = Convert.ToDecimal(txt_Montogasto.Text.Trim());

                    int Edad = DateTime.Today.AddTicks(-obj_Estudiante_DAL.dFechaNacimiento.Ticks).Year - 1;


                    if (Edad < 15 || obj_Estudiante_DAL.dFechaNacimiento >= DateTime.Now)
                    {

                        MessageBox.Show("La fecha de nacimiento no puede ser superior o igual al día de Hoy",
                               "Error", MessageBoxButton.OK, MessageBoxImage.Error);


                    }
                    else
                    {
                        if (txt_Correo.Text.Length == 0)
                        {
                            MessageBox.Show("Debe Ingresar una Direccion de Correo Electrónico");
                            txt_Correo.Focus();
                        }
                        else
                        {
                            ValEmail();

                            if (Valmail == true)
                            {
                                obj_Estudiante_DAL.sCorreo = txt_Correo.Text.Trim();
                                obj_Estudiante_BLL.Crear_Estudiante(ref obj_Estudiante_DAL, ref sMsjError);

                                if (sMsjError != string.Empty)
                                {
                                    MessageBox.Show("Se presento un error al Insertar un Nuevo Estudiante.\n\nDesc.Error="
                                        + sMsjError, "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                                }
                                else
                                {
                                    MessageBox.Show("Estudiante Insertado Exitosamente", "Insert Exitoso", MessageBoxButton.OK, MessageBoxImage.Information);
                                }
                                PintarControles();
                            }

                        }                  
    
                    }

                }

            }
            else
            {
                if (txt_Nombre.Text == string.Empty ||
                    txt_Apell1.Text == string.Empty ||
                    txt_Apell2.Text == string.Empty ||
                    txt_Direccion.Text == string.Empty ||
                    txt_id_Estudiante.Text == string.Empty ||
                    txt_Nacionalidad.Text == string.Empty ||
                    txt_Telefono.Text == string.Empty ||

                   txt_Identificacion.Text == string.Empty)
                {
                    MessageBox.Show("Debe rellenar lo campos con la información Solicitada", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    obj_Estudiante_DAL.iIdEstudiante = Convert.ToInt32(txt_id_Estudiante.Text.Trim());
                    obj_Estudiante_DAL.sNombre = txt_Nombre.Text.Trim();
                    obj_Estudiante_DAL.sPrimerApellido = txt_Apell1.Text.Trim();
                    obj_Estudiante_DAL.sSegundoApellido = txt_Apell2.Text.Trim();
                    obj_Estudiante_DAL.sIdentificacion = txt_Identificacion.Text.Trim();
                    obj_Estudiante_DAL.sDireccion = txt_Direccion.Text.Trim();
                    obj_Estudiante_DAL.iTelefono = Convert.ToInt32(txt_Telefono.Text.Trim());
                    obj_Estudiante_DAL.dFechaNacimiento = dtp_Fecha_Nacimiento.SelectedDate.Value.Date;


                    obj_Estudiante_DAL.cGenero = Convert.ToChar(cbx_Genero.SelectedValue);

                    obj_Estudiante_DAL.sNacionalidad = txt_Nacionalidad.Text.Trim();
                    obj_Estudiante_DAL.sCorreo = txt_Correo.Text.Trim();

                    ComboBoxItem sNivel = (ComboBoxItem)cmb_NivelAcademico.SelectedItem;
                    obj_Estudiante_DAL.sNivel_Academico = sNivel.Content.ToString();

                    obj_Estudiante_DAL.iGrado = Convert.ToInt32(txt_Grado.Text.Trim());
                    obj_Estudiante_DAL.dTotalCosto = Convert.ToDecimal(txt_TotalCosto.Text.Trim());


                    obj_Estudiante_DAL.iIdCampus = Convert.ToInt32(cbx_Campus.SelectedValue.ToString());

                    obj_Estudiante_DAL.iIdBeca = Convert.ToInt32(cbx_Beca.SelectedValue.ToString());
                    obj_Estudiante_DAL.iIdEstado = Convert.ToInt32(cbx_Estado.SelectedValue.ToString());

                    ComboBoxItem sTipocasa = (ComboBoxItem)cmb_TipoCasa.SelectedItem;
                    obj_Estudiante_DAL.sTipoCasa = sTipocasa.Content.ToString();

                    obj_Estudiante_DAL.iCantNucleoFam = Convert.ToInt32(nmc_CantPersCasa.Value.ToString());
                    obj_Estudiante_DAL.iCantTrabajador = Convert.ToInt32(nmc_CantPersTrabajan.Value.ToString());

                    ComboBoxItem IngresoMes = (ComboBoxItem)cmb_Ingreso.SelectedItem;
                    obj_Estudiante_DAL.sIngresoMesBruto = IngresoMes.Content.ToString();

                    obj_Estudiante_DAL.dIngresoMesNeto = Convert.ToDecimal(txt_MontoIngreso.Text.Trim());
                    obj_Estudiante_DAL.dGastoMensual = Convert.ToDecimal(txt_Montogasto.Text.Trim());

                    int Edad = DateTime.Today.AddTicks(-obj_Estudiante_DAL.dFechaNacimiento.Ticks).Year - 1;


                    if (Edad < 15 || obj_Estudiante_DAL.dFechaNacimiento >= DateTime.Now)
                    {

                        MessageBox.Show("La fecha de nacimiento no puede ser superior o igual al día de Hoy",
                               "Error", MessageBoxButton.OK, MessageBoxImage.Error);


                    }
                    else
                    {
                        if (txt_Correo.Text.Trim().Contains("@"))
                        {

                            obj_Estudiante_BLL.Modificar_Estudiante(ref obj_Estudiante_DAL, ref sMsjError);

                            if (sMsjError != string.Empty)
                            {
                                MessageBox.Show("Se presento un error al Insertar un Nuevo Estudiante.\n\nDesc.Error="
                                    + sMsjError, "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                            }
                            else
                            {
                                MessageBox.Show("Estudiante Insertado Exitosamente", "Insert Exitoso", MessageBoxButton.OK, MessageBoxImage.Information);
                            }
                            PintarControles();
                        }
                        else
                        {
                            MessageBox.Show("Formato de Correo Inválido", "Correo No Existente", MessageBoxButton.OK, MessageBoxImage.Warning);
                        }
                    }
                }
                this.Close();
            }

        }



        #endregion

        #region PintarControles

        private void PintarControles()
        {
            if (obj_Estudiante_DAL.cBandAxion == 'I')
            {

                Cargar_Combo();


                txt_id_Estudiante.Text = string.Empty;
                txt_Nombre.Text = string.Empty;
                txt_Apell1.Text = string.Empty;
                txt_Apell2.Text = string.Empty;
                txt_Identificacion.Text = string.Empty;
                txt_Direccion.Text = string.Empty;
                txt_Telefono.Text = string.Empty;
                dtp_Fecha_Nacimiento.Text = "";

                txt_Nacionalidad.Text = string.Empty;
                txt_Correo.Text = string.Empty;



            }
            else
            {
                Cargar_Combo();

                txt_id_Estudiante.Text = obj_Estudiante_DAL.iIdEstudiante.ToString();
                txt_Nombre.Text = obj_Estudiante_DAL.sNombre;
                txt_Apell1.Text = obj_Estudiante_DAL.sPrimerApellido;
                txt_Apell2.Text = obj_Estudiante_DAL.sSegundoApellido;
                txt_Identificacion.Text = obj_Estudiante_DAL.sIdentificacion;
                txt_Direccion.Text = obj_Estudiante_DAL.sDireccion;
                txt_Telefono.Text = obj_Estudiante_DAL.iTelefono.ToString();
                dtp_Fecha_Nacimiento.Text = obj_Estudiante_DAL.dFechaNacimiento.ToString();
                txt_Nacionalidad.Text = obj_Estudiante_DAL.sNacionalidad;
                txt_Correo.Text = obj_Estudiante_DAL.sCorreo;
                txt_MontoIngreso.Text = obj_Estudiante_DAL.dIngresoMesNeto.ToString();
                txt_Montogasto.Text = obj_Estudiante_DAL.dGastoMensual.ToString();
                nmc_CantPersCasa.Value = obj_Estudiante_DAL.iCantNucleoFam;
                nmc_CantPersTrabajan.Value = obj_Estudiante_DAL.iCantTrabajador;

            }
        }
        #endregion



        #region Cargar Combos
  

        private void Cargar_Combo()
        {
           

            #region Variables
            string sMsjError = string.Empty;
            DataTable dtEstados = new DataTable();
            //DataTable dtGenero = new DataTable();
            DataTable dtBeca = new DataTable();
            DataTable dtCampus = new DataTable();
            DataTable dtCasa = new DataTable();
            DataTable dtIngreso = new DataTable();
            DataTable dtNivel = new DataTable();

            #endregion

            //Llamo los métodos de cada tabla q necesito

           
            obj_Estado_Estudiante_BLL.Listar_Estado_Expediente(ref dtEstados, ref sMsjError);
            obj_Beca_BLL.Listar_Beca(ref dtBeca, ref sMsjError);
            obj_Campus_BLL.Listar_Campus(ref dtCampus, ref sMsjError);



            if (sMsjError == string.Empty)
            {


                //Cargo Combo genero
                ObservableCollection<string> Genero = new ObservableCollection<string>();
                Genero.Add("M");
                Genero.Add("F");
                this.cbx_Genero.ItemsSource = Genero;

                //Cargo Combo Tipo Vivienda
                ObservableCollection<string> Casa = new ObservableCollection<string>();
                Casa.Add("Propia");
                Casa.Add("Alquilada");
                Casa.Add("Prestada");
                this.cmb_TipoCasa.ItemsSource = Casa;

                //Cargo Combos Ingreso
                ObservableCollection<decimal> Ingreso = new ObservableCollection<decimal>();
                Ingreso.Add(200000);
                Ingreso.Add(500000);
                Ingreso.Add(1000000);
                this.cmb_TipoCasa.ItemsSource = Casa;

                cbx_Beca.ItemsSource = null;
                cbx_Beca.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtBeca });

                cbx_Campus.ItemsSource = null;
                cbx_Campus.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtCampus });

                cbx_Estado.ItemsSource = null;
                cbx_Estado.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtEstados });

             
                //In WPF use DisplayMemberPath and SelectedValuePath.



                cbx_Beca.SelectedValuePath = dtBeca.Columns[0].ToString();
                cbx_Beca.DisplayMemberPath = dtBeca.Columns[1].ToString();

                cbx_Campus.SelectedValuePath = dtCampus.Columns[0].ToString();
                cbx_Campus.DisplayMemberPath = dtCampus.Columns[1].ToString();


                cbx_Estado.SelectedValuePath = dtEstados.Columns[0].ToString();
                cbx_Estado.DisplayMemberPath = dtEstados.Columns[1].ToString();

             


            }
            else
            {
                cbx_Beca.ItemsSource = null;
                cbx_Campus.ItemsSource = null;
                cbx_Estado.ItemsSource = null;
                cbx_Genero.ItemsSource = null;

                MessageBox.Show("Se presento un error al Listar los Estudiantes.\n\nDesc.Error=" + sMsjError, "Error al listar",
                                 MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        #endregion

        private void wfrm_Modificar_Estudiante_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (MessageBox.Show("Desea Salir del Sistema", "Salir", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
            {
                e.Cancel = true;
            } 
            
        }

       





        #endregion

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Desea Regresar al Menú Anterior", "Salir", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                wfrm_SubMenu_Plataforma pantPlataforma = new wfrm_SubMenu_Plataforma();
                this.Hide();
                pantPlataforma.ShowDialog();
                this.Close();
            }
        }






    }
}
