﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;
using BLL_B_I_T_S.Catalogos_y_Mantenimiento;
using System.Data;

namespace PL_B_I_T_S.Pantallas.Modificar
{
    /// <summary>
    /// Interaction logic for wfrm_Modificar_Instituciones.xaml
    /// </summary>
    public partial class wfrm_Modificar_Instituciones : MetroWindow
    {
        public wfrm_Modificar_Instituciones()
        {
            InitializeComponent();
        }
        #region VarGlobales
        public cls_Institucion_DAL obj_institucion_DAL = new cls_Institucion_DAL();
        cls_Institucion_BLL obj_institucion_BLL = new cls_Institucion_BLL();

        #endregion

        #region eventos
        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                GlassHelper.ExtendGlass(this, -1, -1, -1, -1);
            }
            catch (Exception)
            {

                this.Background = Brushes.White;
            }
            PintarControles();
        }

        private void txt_Institucion_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int letra = Convert.ToInt32(Convert.ToChar(e.Text));

            int numeros = Convert.ToInt32(Convert.ToChar(e.Text));

            if ((letra >= 65 && letra <= 90 || letra >= 97 && letra <= 122) || (numeros >= 48 && numeros <= 57))
            {
                e.Handled = false;
            }
            else e.Handled = true;
        }

        private void btn_Guardar_Click(object sender, RoutedEventArgs e)
        {
            Guardar();
        }

        #endregion

        #region Guardar

        public void Guardar()
        {

            string sMsjError = string.Empty;

            if (obj_institucion_DAL.cBandAxion == 'I')
            {
                if (txt_Id.Text == string.Empty ||
                    txt_Institucion.Text == string.Empty)
                {
                    MessageBox.Show("Debe completar los campos con la información solicitada", "Error"
                        , MessageBoxButton.OK, MessageBoxImage.Error);

                }
                else
                {


                    obj_institucion_DAL.sIDInstitucion = Convert.ToInt16(txt_Id.Text.Trim());
                    obj_institucion_DAL.sNombre = txt_Institucion.Text.Trim();



                    obj_institucion_BLL.Crear_Institucion(ref obj_institucion_DAL, ref sMsjError);

                    if (sMsjError != string.Empty)
                    {
                        MessageBox.Show("Se presento un error al Insertar un Nuevo Periodo.\n\nDesc.Error="
                            + sMsjError, "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                    }
                    else
                    {
                        MessageBox.Show("Institucion Insertada Exitosamente", "Insert Exitoso", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    PintarControles();
                }

            }
            else
            {
                if (txt_Id.Text == string.Empty ||
                    txt_Institucion.Text == string.Empty)
                {
                    MessageBox.Show("Debe rellenar lo campos con la información Solicitada", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {



                    obj_institucion_DAL.sIDInstitucion = Convert.ToInt16(txt_Id.Text.Trim());
                    obj_institucion_DAL.sNombre = txt_Institucion.Text.Trim();
                    obj_institucion_BLL.Modificar_Institucion(ref obj_institucion_DAL, ref sMsjError);

                    MessageBox.Show("Institucion Modificada", "Insert Exitoso", MessageBoxButton.OK, MessageBoxImage.Information);


                }
            }

        }
        #endregion

        #region PintarControles

        private void PintarControles()
        {
            if (obj_institucion_DAL.cBandAxion == 'I')
            {

                txt_Id.Text = string.Empty;
                txt_Institucion.Text = string.Empty;

            }
            else
            {


                txt_Id.Text = obj_institucion_DAL.sIDInstitucion.ToString();
                txt_Institucion.Text = obj_institucion_DAL.sNombre.ToString();

                txt_Id.IsEnabled = false;




            }
        }
        #endregion
    }
}
