﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;
using BLL_B_I_T_S.Catalogos_y_Mantenimiento;
using System.Data;

namespace PL_B_I_T_S.Pantallas.Modificar
{
    /// <summary>
    /// Lógica de interacción para wfrm_Modificar_Desembolsos.xaml
    /// </summary>
    public partial class wfrm_Modificar_Desembolsos : MetroWindow
    {
        public wfrm_Modificar_Desembolsos()
        {
            InitializeComponent();
        }

        #region Var Glob
        public cls_Desembolsos_DAL obj_Desembolsos_DAL = new cls_Desembolsos_DAL();
        cls_Desembolsos_BLL obj_Desembolsos_BLL = new cls_Desembolsos_BLL();
        cls_EstadoDesembolsos_BLL obj_Estado_Desembolsos_BLL = new cls_EstadoDesembolsos_BLL();
        #endregion

        #region Guardar

        public void Guardar()
        {

            string sMsjError = string.Empty;

            if (obj_Desembolsos_DAL.cBandAxion == 'I')
            {
                if (txt_IdDesembolso.Text == string.Empty ||
                    txt_IdEstudiante.Text == string.Empty ||
                    txt_IdFactura.Text == string.Empty ||
                    txt_Monto_Desembolsado.Text == string.Empty )
                     
                    
                {
                    MessageBox.Show("Debe realizar los campos con la información solicitada", "Error"
                        , MessageBoxButton.OK, MessageBoxImage.Error);

                }
                else
                {
                    

                    obj_Desembolsos_DAL.iIdDesembolso = Convert.ToInt32(txt_IdDesembolso.Text.Trim());
                    obj_Desembolsos_DAL.iIdEstudiante = Convert.ToInt32(txt_IdEstudiante.Text.Trim());
                    obj_Desembolsos_DAL.iIdFactura = Convert.ToInt32(txt_IdFactura.Text.Trim());
                    obj_Desembolsos_DAL.dMonto_Desembolsado = Convert.ToDouble(txt_Monto_Desembolsado.Text.Trim());
                    obj_Desembolsos_DAL.dtFecha_Desembolso = Convert.ToDateTime(dtp_FechaDesembolso.SelectedDate);
                    obj_Desembolsos_DAL.iIdEstado = Convert.ToInt32(cmb_EstadoDesembolso.SelectedValue);
                    

                    obj_Desembolsos_BLL.Crear_Desembolso(ref obj_Desembolsos_DAL, ref sMsjError);

                    if (sMsjError != string.Empty)
                    {
                        MessageBox.Show("Se presento un error al Insertar un Nuevo Desembolso.\n\nDesc.Error="
                            + sMsjError, "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                    }
                    else
                    {
                        MessageBox.Show("Desembolso Insertado Exitosamente", "Insert Exitoso", MessageBoxButton.OK, MessageBoxImage.Information);
                        this.Close();
                    }
                    PintarControles();
                }

            }
            else
            {
                if (txt_IdDesembolso.Text == string.Empty ||
                    txt_IdEstudiante.Text == string.Empty ||
                    txt_IdFactura.Text == string.Empty ||
                    txt_Monto_Desembolsado.Text == string.Empty
                  
                    )
                {
                    MessageBox.Show("Debe rellenar lo campos con la información Solicitada", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {


                    obj_Desembolsos_DAL.iIdDesembolso = Convert.ToInt32(txt_IdDesembolso.Text.Trim());
                    obj_Desembolsos_DAL.iIdEstudiante = Convert.ToInt32(txt_IdEstudiante.Text.Trim());
                    obj_Desembolsos_DAL.iIdFactura = Convert.ToInt32(txt_IdFactura.Text.Trim());
                    obj_Desembolsos_DAL.dMonto_Desembolsado = Convert.ToDouble(txt_Monto_Desembolsado.Text.Trim());
                    obj_Desembolsos_DAL.dtFecha_Desembolso = Convert.ToDateTime(dtp_FechaDesembolso.SelectedDate);
                    obj_Desembolsos_DAL.iIdEstado = Convert.ToInt32(cmb_EstadoDesembolso.SelectedValue);

                    obj_Desembolsos_BLL.Modificar_Activos(ref obj_Desembolsos_DAL, ref sMsjError);



                    if (sMsjError != string.Empty)
                    {
                        MessageBox.Show("Se Presento un error al Modificar el Desembolso.\n\nDesc.Error = "
                            + sMsjError, " ERROR EN MODIFICAR ", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    else
                    {
                        MessageBox.Show("Desembolso Modificado Exitosamente...!!!", "UPDATE EXITOSO", MessageBoxButton.OK, MessageBoxImage.Information);
                    }

                    PintarControles();
                }
                this.Close();
            }

        }
        #endregion

        #region PintarControles

        private void PintarControles()
        {
            if (obj_Desembolsos_DAL.cBandAxion == 'I')
            {

                Cargar_Combo();


                txt_IdDesembolso.Text = string.Empty;
                txt_IdEstudiante.Text = string.Empty;
                txt_IdFactura.Text = string.Empty;
                txt_Monto_Desembolsado.Text = string.Empty;

            }
            else
            {
                Cargar_Combo();

                txt_IdDesembolso.Text = obj_Desembolsos_DAL.iIdDesembolso.ToString();
                txt_IdEstudiante.Text = obj_Desembolsos_DAL.iIdEstudiante.ToString();
                txt_Monto_Desembolsado.Text = obj_Desembolsos_DAL.dMonto_Desembolsado.ToString();
                txt_IdFactura.Text = obj_Desembolsos_DAL.iIdFactura.ToString();
                txt_IdDesembolso.IsEnabled = false;



            }
        }
        #endregion

        #region Cargar Combos

        private void Cargar_Combo()
        {
            #region Variables
            string sMsjError = string.Empty;
            DataTable dtEstado = new DataTable();
            

            #endregion

            //Llamo los métodos de cada tabla q necesito

           
            obj_Estado_Desembolsos_BLL.Listar_EstadoDesembolsos(ref dtEstado, ref sMsjError);
          



            if (sMsjError == string.Empty)
            {


                cmb_EstadoDesembolso.ItemsSource = null;
                cmb_EstadoDesembolso.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtEstado });

                //In WPF use DisplayMemberPath and SelectedValuePath.

                cmb_EstadoDesembolso.SelectedValuePath = dtEstado.Columns[0].ToString();
                cmb_EstadoDesembolso.DisplayMemberPath = dtEstado.Columns[1].ToString();




            }
            else
            {
         
                cmb_EstadoDesembolso.ItemsSource = null;
             

                MessageBox.Show("Se presento un error al Listar los Desembolsos.\n\nDesc.Error=" + sMsjError, "Error al listar",
                                 MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        #endregion

        #region Eventos
        private void btn_Guardar_Click(object sender, RoutedEventArgs e)
        {
            Guardar();
        }

        private void txt_IdDesembolso_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int numeros = Convert.ToInt32(Convert.ToChar(e.Text));

            if (numeros >= 48 && numeros <= 57)
            {

                e.Handled = false;

            }
            else
            {

                e.Handled = true;
            }
        }

        private void txt_IdEstudiante_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int numeros = Convert.ToInt32(Convert.ToChar(e.Text));

            if (numeros >= 48 && numeros <= 57)
            {

                e.Handled = false;

            }
            else
            {

                e.Handled = true;
            }
        }

        private void txt_Monto_Desembolsado_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int numeros = Convert.ToInt32(Convert.ToChar(e.Text));

            if (numeros >= 48 && numeros <= 57)
            {

                e.Handled = false;

            }
            else
            {

                e.Handled = true;
            }
        }

        private void txt_IdFactura_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int numeros = Convert.ToInt32(Convert.ToChar(e.Text));

            if (numeros >= 48 && numeros <= 57)
            {

                e.Handled = false;

            }
            else
            {

                e.Handled = true;
            }
        }

        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            Cargar_Combo();
        }

        #endregion
    }
}
