﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;
using BLL_B_I_T_S.Catalogos_y_Mantenimiento;
using System.Data;

namespace PL_B_I_T_S.Pantallas.Modificar
{
    /// <summary>
    /// Lógica de interacción para wfrm_Modificar_Materia.xaml
    /// </summary>
    public partial class wfrm_Modificar_Materia : MetroWindow
    {


        public wfrm_Modificar_Materia()
        {
            InitializeComponent();
        }

        #region Var Glob
 
        public cls_Materias_DAL obj_Materia_DAL = new cls_Materias_DAL();
        cls_Materias_BLL obj_Materia_BLL = new cls_Materias_BLL();

        #endregion

        #region evento
        private void txt_Costo_Materia_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int numeros = Convert.ToInt32(Convert.ToChar(e.Text));

            if (numeros >= 48 && numeros <= 57)
            {

                e.Handled = false;

            }
            else
            {

                e.Handled = true;
            }
        }

     

        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                GlassHelper.ExtendGlass(this, -1, -1, -1, -1);
            }
            catch (Exception)
            {

                this.Background = Brushes.White;
            }

            PintarControles();
        }

        private void txt_Materia_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int letra = Convert.ToInt32(Convert.ToChar(e.Text));

            if (letra >= 65 && letra <= 90 || letra >= 97 && letra <= 122)
            {
                e.Handled = false;
            }
            else e.Handled = true;
        }
        #endregion
      

        #region Metodos

        #region Guardar
        public void Guardar()
        {

            string sMsjError = string.Empty;

            if (obj_Materia_DAL.cBandAxion == 'I')
            {
                if (txt_IdMateria.Text == string.Empty ||
                    txt_Costo_Materia.Text == string.Empty ||
                    txt_Materia.Text == string.Empty)
                {
                    MessageBox.Show("Debe realizar los campos con la información solicitada", "Error"
                        , MessageBoxButton.OK, MessageBoxImage.Error);

                }
                else
                {

                    obj_Materia_DAL.sNombre = txt_Materia.Text.Trim();
                    obj_Materia_DAL.iIdMateria = Convert.ToInt32(txt_IdMateria.Text.Trim());
                    obj_Materia_DAL.dCosto = Convert.ToInt64(txt_Costo_Materia.Text.Trim());


                    obj_Materia_BLL.Crear_Materias(ref obj_Materia_DAL, ref sMsjError);

                    if (sMsjError != string.Empty)
                    {
                        MessageBox.Show("Se presento un error al Insertar la Materia.\n\nDesc.Error="
                            + sMsjError, "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                    }
                    else
                    {
                        MessageBox.Show("Materia Insertada Exitosamente", "Insert Exitoso", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    PintarControles();
                }

            }
            else
            {
                if (txt_IdMateria.Text == string.Empty ||
                    txt_Costo_Materia.Text == string.Empty ||
                    txt_Materia.Text == string.Empty)
                {
                    MessageBox.Show("Debe rellenar lo campos con la información Solicitada", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    obj_Materia_DAL.sNombre = txt_Materia.Text.Trim();
                    obj_Materia_DAL.iIdMateria = Convert.ToInt32(txt_IdMateria.Text.Trim());
                    obj_Materia_DAL.dCosto = Convert.ToInt64(txt_Costo_Materia.Text.Trim());
                    obj_Materia_BLL.Modificar_Materia(ref obj_Materia_DAL, ref sMsjError);

                    MessageBox.Show("Materia Modificada Exitosamente", "Modificacion Exitosa", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }

        }
        #endregion

        #region PintarControles

        private void PintarControles()
        {
            if (obj_Materia_DAL.cBandAxion == 'I')
            {

                txt_IdMateria.Text = string.Empty;
                txt_Materia.Text = string.Empty;
                txt_Costo_Materia.Text = string.Empty;

            }
            else
            {

                txt_IdMateria.Text = obj_Materia_DAL.iIdMateria.ToString();
                txt_IdMateria.IsEnabled = false;
                txt_Materia.Text = obj_Materia_DAL.sNombre;
                txt_Costo_Materia.Text = obj_Materia_DAL.dCosto.ToString();

            }
        }
        #endregion
        #endregion

        private void btn_Guardar_Materia_Click(object sender, RoutedEventArgs e)
        {
            Guardar();
        
            
        }

        private void txt_IdMateria_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int numeros = Convert.ToInt32(Convert.ToChar(e.Text));

            if (numeros >= 48 && numeros <= 57)
            {

                e.Handled = false;

            }
            else
            {

                e.Handled = true;
            }
        }

       

       

      
    }
}
