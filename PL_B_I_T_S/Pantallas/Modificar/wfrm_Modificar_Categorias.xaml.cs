﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;
using BLL_B_I_T_S.Catalogos_y_Mantenimiento;
using System.Data;

namespace PL_B_I_T_S.Pantallas.Modificar
{
    public partial class wfrm_Modificar_Categorias : MetroWindow
    {
        public wfrm_Modificar_Categorias()
        {
            InitializeComponent();
        }
        
        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {

                GlassHelper.ExtendGlass(this, -1, -1, -1, -1);

                PintarControles();

            }
            catch (Exception)
            {

                this.Background = Brushes.White;
            }
        }


        public cls_Categoria_DAL obj_Categoria_DAL = new cls_Categoria_DAL();
        cls_Categoria_BLL obj_Categoria_BLL = new cls_Categoria_BLL();
       

        #region Métodos

        #region Guardar

        public void Guardar()
        {

            string sMsjError = string.Empty;

            if (obj_Categoria_DAL.cBandAxion == 'I')
            {
                if (txt_NombreBeca.Text == string.Empty ||
                    txt_Id_Beca.Text == string.Empty ||
                    txt_MontoBeca.Text == string.Empty)
                {
                    MessageBox.Show("Debe rellenar los campos con la información solicitada", "Error"
                        , MessageBoxButton.OK, MessageBoxImage.Error);

                }
                else
                {

                    obj_Categoria_DAL.sNombre = txt_NombreBeca.Text.Trim();
                    obj_Categoria_DAL.iIdCategoria = Convert.ToInt32(txt_Id_Beca.Text.Trim());
                    obj_Categoria_DAL.iMonto = Convert.ToInt32(txt_MontoBeca.Text.Trim());


                    obj_Categoria_BLL.Crear_Categoria(ref obj_Categoria_DAL, ref sMsjError);

                    if (sMsjError != string.Empty)
                    {
                        MessageBox.Show("Se presento un error al Insertar una nueva Categoria.\n\nDesc.Error="
                            + sMsjError, "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                    }
                    else
                    {
                        MessageBox.Show("Categoria Insertado Exitosamente", "Insert Exitoso", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    PintarControles();
                }

            }
            else
            {
                if (txt_NombreBeca.Text == string.Empty ||
                    txt_Id_Beca.Text == string.Empty ||
                    txt_MontoBeca.Text == string.Empty)
                {
                    MessageBox.Show("Debe rellenar los campos con la información Solicitada", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    obj_Categoria_DAL.sNombre = txt_NombreBeca.Text.Trim();
                    obj_Categoria_DAL.iIdCategoria = Convert.ToInt32(txt_Id_Beca.Text.Trim());
                    obj_Categoria_DAL.iMonto = Convert.ToInt32(txt_MontoBeca.Text.Trim());
                    obj_Categoria_BLL.Modificar_Categoria(ref obj_Categoria_DAL, ref sMsjError);

                    MessageBox.Show("Categoria Modificada Exitosamente", "Update Exitoso", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }

        }
        #endregion

        #region PintarControles

        private void PintarControles()
        {
            if (obj_Categoria_DAL.cBandAxion == 'I')
            {

                txt_Id_Beca.Text = string.Empty;
                txt_MontoBeca.Text = string.Empty;
                txt_NombreBeca.Text = string.Empty;

            }
            else
            {

                txt_Id_Beca.Text = obj_Categoria_DAL.iIdCategoria.ToString();
                txt_Id_Beca.IsEnabled = false;
                txt_MontoBeca.Text = obj_Categoria_DAL.iMonto.ToString();
                txt_NombreBeca.Text = obj_Categoria_DAL.sNombre.ToString();

            }
        }
        #endregion




        #endregion

        #region eventos
        private void txt_Id_Beca_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int numeros = Convert.ToInt32(Convert.ToChar(e.Text));

            if (numeros >= 48 && numeros <= 57)
            {

                e.Handled = false;

            }
            else
            {

                e.Handled = true;
            }
        }

        private void txt_MontoBeca_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int numeros = Convert.ToInt32(Convert.ToChar(e.Text));

            if (numeros >= 48 && numeros <= 57)
            {

                e.Handled = false;

            }
            else
            {

                e.Handled = true;
            }
        }

        private void txt_NombreBeca_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int letra = Convert.ToInt32(Convert.ToChar(e.Text));

            if (letra >= 65 && letra <= 90 || letra >= 97 && letra <= 122)
            {
                e.Handled = false;
            }
            else e.Handled = true;
        }

        #endregion

        //Guardar
        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            Guardar();
        }

        //Salir
        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Desea Salir Realmente...???", "Salir", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                this.Close();
            }
        }
    }
}
