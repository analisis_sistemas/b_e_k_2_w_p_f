﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;

namespace PL_B_I_T_S.Pantallas.Modificar
{
    /// <summary>
    /// Lógica de interacción para wfrm_Modificar_Recuperar_Contrasena.xaml
    /// </summary>
    public partial class wfrm_Modificar_Recuperar_Contrasena : MetroWindow
    {
        public wfrm_Modificar_Recuperar_Contrasena()
        {
            InitializeComponent();
        }

        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                GlassHelper.ExtendGlass(this, -1, -1, -1, -1);
            }
            catch (Exception)
            {

                this.Background = Brushes.White;
            }
        }

        private void txt_NombreUsuario_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int letra = Convert.ToInt32(Convert.ToChar(e.Text));

            int numeros = Convert.ToInt32(Convert.ToChar(e.Text));

            if ((letra >= 65 && letra <= 90 || letra >= 97 && letra <= 122) || (numeros >= 48 && numeros <= 57))
            {
                e.Handled = false;
            }
            else e.Handled = true;
        }

        private void txt_RespPrimeraMascota_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int letra = Convert.ToInt32(Convert.ToChar(e.Text));

            if (letra >= 65 && letra <= 90 || letra >= 97 && letra <= 122)
            {
                e.Handled = false;
            }
            else e.Handled = true;
        }

        private void txt_RespPrimerNumTele_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int numeros = Convert.ToInt32(Convert.ToChar(e.Text));

            if (numeros >= 48 && numeros <= 57)
            {

                e.Handled = false;

            }
            else
            {

                e.Handled = true;
            }
        }

        private void txt_RespCiudadNaciMadre_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int letra = Convert.ToInt32(Convert.ToChar(e.Text));

            int numeros = Convert.ToInt32(Convert.ToChar(e.Text));

            if ((letra >= 65 && letra <= 90 || letra >= 97 && letra <= 122) || (numeros >= 48 && numeros <= 57))
            {
                e.Handled = false;
            }
            else e.Handled = true;
        }

        private void txt_RespNombreEscuela_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int letra = Convert.ToInt32(Convert.ToChar(e.Text));

            int numeros = Convert.ToInt32(Convert.ToChar(e.Text));

            if ((letra >= 65 && letra <= 90 || letra >= 97 && letra <= 122) || (numeros >= 48 && numeros <= 57))
            {
                e.Handled = false;
            }
            else e.Handled = true;
        }
    }
}
