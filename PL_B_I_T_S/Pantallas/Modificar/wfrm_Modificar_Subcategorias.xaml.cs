﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;
using BLL_B_I_T_S.Catalogos_y_Mantenimiento;
using System.Data;

namespace PL_B_I_T_S.Pantallas.Modificar
{
    /// <summary>
    /// Lógica de interacción para wfrm_Modificar_Subcategorias.xaml
    /// </summary>
    public partial class wfrm_Modificar_Subcategorias : MetroWindow
    {
        public wfrm_Modificar_Subcategorias()
        {
            InitializeComponent();
        }

        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                GlassHelper.ExtendGlass(this, -1, -1, -1, -1);
                
                PintarControles();
            
            }
            catch (Exception)
            {

                this.Background = Brushes.White;
            }
        }

        public cls_Sub_Categoria_DAL obj_SubCategoria_DAL = new cls_Sub_Categoria_DAL();
        cls_SubCategoria_BLL obj_SubCategoria_BLL = new cls_SubCategoria_BLL();

        #region Métodos

        #region Guardar

        public void Guardar()
        {

            string sMsjError = string.Empty;

            if (obj_SubCategoria_DAL.cBandAxion == 'I')
            {
                if (txt_NombreSubcategoria.Text == string.Empty ||
                    txt_IdSubcategoria.Text == string.Empty ||
                    txt_PorcentajeBeca.Text == string.Empty)
                {
                    MessageBox.Show("Debe rellenar los campos con la información solicitada", "Error"
                        , MessageBoxButton.OK, MessageBoxImage.Error);

                }
                else
                {

                    obj_SubCategoria_DAL.sNombre = txt_NombreSubcategoria.Text.Trim();
                    obj_SubCategoria_DAL.iIdSubcategoria = Convert.ToInt32(txt_IdSubcategoria.Text.Trim());
                    obj_SubCategoria_DAL.iPorcentaje = Convert.ToInt32(txt_PorcentajeBeca.Text.Trim());


                    obj_SubCategoria_BLL.Crear_SubCategoria(ref obj_SubCategoria_DAL, ref sMsjError);

                    if (sMsjError != string.Empty)
                    {
                        MessageBox.Show("Se presento un error al Insertar una nueva SubCategoria.\n\nDesc.Error="
                            + sMsjError, "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                    }
                    else
                    {
                        MessageBox.Show("SubCategoria Insertado Exitosamente", "Insert Exitoso", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    PintarControles();
                }

            }
            else
            {
                if (txt_NombreSubcategoria.Text == string.Empty ||
                    txt_IdSubcategoria.Text == string.Empty ||
                    txt_PorcentajeBeca.Text == string.Empty)
                {
                    MessageBox.Show("Debe rellenar los campos con la información Solicitada", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    obj_SubCategoria_DAL.sNombre = txt_NombreSubcategoria.Text.Trim();
                    obj_SubCategoria_DAL.iIdSubcategoria = Convert.ToInt32(txt_IdSubcategoria.Text.Trim());
                    obj_SubCategoria_DAL.iPorcentaje = Convert.ToInt32(txt_PorcentajeBeca.Text.Trim());
                    obj_SubCategoria_BLL.Modificar_SubCategoria(ref obj_SubCategoria_DAL, ref sMsjError);

                    MessageBox.Show("SubCategoria Modificada Exitosamente", "Update Exitoso", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }

        }
        #endregion

        #region PintarControles

        private void PintarControles()
        {
            if (obj_SubCategoria_DAL.cBandAxion == 'I')
            {

                txt_IdSubcategoria.Text = string.Empty;
                txt_PorcentajeBeca.Text = string.Empty;
                txt_NombreSubcategoria.Text = string.Empty;

            }
            else
            {

                txt_IdSubcategoria.Text = obj_SubCategoria_DAL.iIdSubcategoria.ToString();
                txt_IdSubcategoria.IsEnabled = false;
                txt_PorcentajeBeca.Text = obj_SubCategoria_DAL.iPorcentaje.ToString();
                txt_NombreSubcategoria.Text = obj_SubCategoria_DAL.sNombre.ToString();

            }
        }
        #endregion

        #endregion

        #region eventos
        private void txt_NombreBeca_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int letra = Convert.ToInt32(Convert.ToChar(e.Text));

            if (letra >= 65 && letra <= 90 || letra >= 97 && letra <= 122)
            {
                e.Handled = false;
            }
            else e.Handled = true;
        }

        private void txt_NombreSubcategoria_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int letra = Convert.ToInt32(Convert.ToChar(e.Text));

            if (letra >= 65 && letra <= 90 || letra >= 97 && letra <= 122)
            {
                e.Handled = false;
            }
            else e.Handled = true;
        }

        private void txt_IdSubcategoria_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int numeros = Convert.ToInt32(Convert.ToChar(e.Text));

            if (numeros >= 48 && numeros <= 57)
            {

                e.Handled = false;

            }
            else
            {

                e.Handled = true;
            }
        }

        private void txt_PorcentajeBeca_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int numeros = Convert.ToInt32(Convert.ToChar(e.Text));

            if (numeros >= 48 && numeros <= 57)
            {

                e.Handled = false;

            }
            else
            {

                e.Handled = true;
            }
        }
       #endregion


        private void Guardar_Click(object sender, RoutedEventArgs e)
        {
            Guardar();
        }


        private void Salir_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Desea Salir Realmente...???", "Salir", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                this.Close();
            }
        }

        
   
    }
}
