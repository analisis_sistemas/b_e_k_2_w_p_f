﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using System.ComponentModel;
using System.Threading;
using PL_B_I_T_S.Pantallas.Menú;

namespace PL_B_I_T_S.Pantallas.Login
{
    /// <summary>
    /// Interaction logic for wfrm_Bienvenida.xaml
    /// </summary>
    public partial class wfrm_Bienvenida : MetroWindow
    {
        public wfrm_Bienvenida()
        {
            InitializeComponent();
        }

      

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.DoWork += worker_DoWork;
            worker.ProgressChanged += worker_ProgressChanged;

            worker.RunWorkerAsync();
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            int j = 4;
            for (int i = j; i <= 100; i=i+j)
            {
                (sender as BackgroundWorker).ReportProgress(i);
                Thread.Sleep(100);              

            }

        
          
        }

        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            pb_Iniciar.Value = e.ProgressPercentage;

            if (e.ProgressPercentage == 100)
            {

                wpfrm_Menu ab_Menu = new Menú.wpfrm_Menu();
                this.Close();
                ab_Menu.ShowDialog();
            }
        }

        private void wfrm_Bienvenido_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                GlassHelper.ExtendGlass(this, -1, -1, -1, -1);
            }
            catch (Exception)
            {

                this.Background = Brushes.White;
            }
        }

       
    }
}
