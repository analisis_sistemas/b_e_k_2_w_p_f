﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using System.Diagnostics;
using PL_B_I_T_S.Pantallas.Login;
using PL_B_I_T_S.Pantallas.Modificar;
using PL_B_I_T_S.Pantallas.Catalogos_y_Mantenimineto;
using PL_B_I_T_S.Pantallas.Módulos;
using PL_B_I_T_S.Pantallas.Menú;
using BLL_B_I_T_S.Logica_General;
using DAL_B_I_T_S.BD;
using BLL_B_I_T_S.BD;
using System.Data.SqlClient;
using System.Data;



namespace PL_B_I_T_S.Pantallas
{
    /// <summary>
    /// Interaction logic for wpfrm_Login.xaml
    /// </summary>
    public partial class wpfrm_Login : MetroWindow
    {
        public wpfrm_Login()
        {
            InitializeComponent();

        }

        #region Variables
        cls_BD_DAL obj_BD_DAL = new cls_BD_DAL();
        cls_BD_BLL obj_BD_BLL = new cls_BD_BLL();
        string Rol_Usuario = "";

        #endregion

        #region Botones
        private void btn_Ingresar_Click(object sender, RoutedEventArgs e)
        {

            Login();

        }
        #endregion

        #region Métodos

        private void Login()
        {
            try
            {
                obj_BD_BLL.Traer_CNX(ref obj_BD_DAL);
                obj_BD_BLL.Abrir_CNX(ref obj_BD_DAL);
               

                if (txt_User.Text == string.Empty || txt_Pass.Password == string.Empty)
                {
                    MessageBox.Show("Debe Ingresar Credenciales para Ingresar", "Error", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                }
                else
                {


                    SqlCommand CMD =
                                       new SqlCommand("SELECT nombre,password,idRol FROM [SCH_USUARIO].[TBL_USUARIOS] WHERE [nombre] ='"
                                           + txt_User.Text.Trim() + "' AND [password]='"
                                           + txt_Pass.Password + "'", obj_BD_DAL.SQL_CNX);

                    CMD.ExecuteNonQuery();
                    DataSet DS = new DataSet();
                    SqlDataAdapter DA = new SqlDataAdapter(CMD);
                    DA.Fill(DS, "[SCH_USUARIO].[TBL_USUARIOS]");
                    DataRow DR;
                    DR = DS.Tables["[SCH_USUARIO].[TBL_USUARIOS]"].Rows[0];

                    #region Roles Usuario

                    if (DR["idRol"].ToString() == "1")
                    {
                        Rol_Usuario = "Administrador TI";
                    }
                    else if (DR["idRol"].ToString() == "2")
                    {
                        Rol_Usuario = "Consulta";
                    }
                    else if (DR["idRol"].ToString() == "3")
                    {
                        Rol_Usuario = "BackOffice";
                    }
                    else if (DR["idRol"].ToString() == "4")
                    {
                        Rol_Usuario = "Jefe BackOffice";
                    }
                    else if (DR["idRol"].ToString() == "5")
                    {
                        Rol_Usuario = "Plataforma";
                    }
                    else if (DR["idRol"].ToString() == "6")
                    {
                        Rol_Usuario = "Analista";
                    }
                    #endregion


                    if ((txt_User.Text == DR["nombre"].ToString()) || (txt_Pass.Password == DR["password"].ToString()))
                    {
                        this.Hide();
                        AccesoRol();
                        wfrm_Bienvenida ab_Bienvenida = new Login.wfrm_Bienvenida();
                        this.Close();
                        ab_Bienvenida.lbl_Nombre_User.Content = txt_User.Text;
                        ab_Bienvenida.lbl_Rol.Content = Rol_Usuario;
                        ab_Bienvenida.ShowDialog();
                       
                    }
                    else
                    {
                        MessageBox.Show("Usuario y/o Contraseña Incorrectos", "Error", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                        obj_BD_BLL.Cerrar_CNX(ref obj_BD_DAL);
                    }  
                   
                }
                
            }
            catch (Exception)
            {

                MessageBox.Show("Error al Intentar Conectar", "Error No Controlado", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void hyperlink_RequestNavigate(object sender, System.Windows.Navigation.RequestNavigateEventArgs e)
        {
            string uri = e.Uri.AbsoluteUri;
            Process.Start(new ProcessStartInfo(uri));
            e.Handled = true;
        }

        private void AccesoRol() 
        {
            if (Rol_Usuario=="Consulta")
            {
                wfrm_Mantenimiento_Categoria obj_Cat = new wfrm_Mantenimiento_Categoria();
                obj_Cat.btn_Nuevo.IsEnabled = false;
                obj_Cat.btn_Modificar.IsEnabled = false;
                obj_Cat.btn_Eliminar.IsEnabled = false;
                obj_Cat.dgv_Categoria.IsEnabled = false;
            }
            else if (Rol_Usuario=="BackOffice")
            {

            }
            else if (Rol_Usuario=="Jefe BackOffice")
            {
                
            } else if (Rol_Usuario=="Plataforma")
            {

            }
            else if (Rol_Usuario == "Analista")
            {

            }
        }
        #endregion

        #region Evevntos
        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                GlassHelper.ExtendGlass(this, -1, -1, -1, -1);
            }
            catch (Exception)
            {

                this.Background = Brushes.White;
            }
        }

        private void Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            wfrm_Modificar_Recuperar_Contrasena pant_Modificar = new wfrm_Modificar_Recuperar_Contrasena();
            this.Close();
            pant_Modificar.Show();
           
        }

        private void txt_Pass_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key != System.Windows.Input.Key.Enter) return;

            Login();
        }

        private void txt_User_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key != System.Windows.Input.Key.Enter) return;

            Login();
        }
        #endregion

       







    }
}
