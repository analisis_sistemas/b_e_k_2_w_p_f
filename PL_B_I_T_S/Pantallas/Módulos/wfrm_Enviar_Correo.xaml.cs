﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using Microsoft.Win32;
using System.Net.Mail;
using DAL_B_I_T_S.Correo;
using BLL_B_I_T_S.Correo;
using System.Net;

namespace PL_B_I_T_S.Pantallas.Módulos
{

    public partial class wfrm_Enviar_Correo : MetroWindow
    {
        cls_Correo_DAL obj_Correo_DAL = new cls_Correo_DAL();
        cls_Correo_BLL obj_Correo_BLL = new cls_Correo_BLL();

        public wfrm_Enviar_Correo()
        {
            InitializeComponent();
            obj_Correo_DAL.lsArchivo = new List<string>();
        }

        private void btn_Adjuntar_Click(object sender, RoutedEventArgs e)
        {

            OpenFileDialog file = new OpenFileDialog();
            file.Multiselect = true;
            String[] nombresArchivos = null;

            if (file.ShowDialog() == true)
            {
                nombresArchivos = file.SafeFileNames;
                obj_Correo_DAL.lsArchivo.AddRange(file.FileNames);
            }
            foreach (string adjunto in nombresArchivos)
            {
                txt_Adjunto.Text += adjunto + ",";
            }
        }

        private void btn_Enviar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                obj_Correo_DAL.sDE = "b.i.t.s.bek2cr@gmail.com";
                obj_Correo_DAL.sPASS = "BITSBK2CR";
                obj_Correo_DAL.sFrom = "b.i.t.s.bek2cr@gmail.com";
                obj_Correo_DAL.sTo = txt_Para.Text.Trim();
                obj_Correo_DAL.sSubject = txt_Asunto.Text.Trim();
                obj_Correo_DAL.sMessage = txt_Mensaje.Text.Trim();

                if (obj_Correo_BLL.enviaMail(ref obj_Correo_DAL) == true)
                {
                    MessageBox.Show("Mensaje Enviado : ", "Envío Exitoso", MessageBoxButton.OK, MessageBoxImage.Information);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Error al Enviar: " + obj_Correo_DAL.sError, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Error al intentar enviar : \n" + ex.Message.ToString(), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }


        }


    }
}


