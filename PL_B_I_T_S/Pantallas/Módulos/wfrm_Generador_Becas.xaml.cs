﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using System.Data;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;
using BLL_B_I_T_S.Catalogos_y_Mantenimiento;


namespace PL_B_I_T_S.Pantallas.Módulos
{
    /// <summary>
    /// Interaction logic for wfrm_Generador_Becas.xaml
    /// </summary>
    public partial class wfrm_Generador_Becas : MetroWindow
    {
        public wfrm_Generador_Becas()
        {
            InitializeComponent();
        }

        #region VarGlob
        
        public cls_Beca_DAL obj_Beca_DAL = new cls_Beca_DAL();
        cls_Beca_BLL obj_Beca_BLL = new cls_Beca_BLL();

        cls_EstadoExpediente_BLL obj_Estado_Expediente = new cls_EstadoExpediente_BLL();
        cls_SubCategoria_BLL obj_Sub_Cat = new cls_SubCategoria_BLL();
        #endregion


        #region Metodos
        #region Guardar

        //public void Guardar()
        //{

        //    string sMsjError = string.Empty;

        //    if (obj_Beca_DAL.cBandAxion == 'I')
        //    {
        //        if (txt_IdEstudiante.Text == string.Empty)
        //        {
        //            MessageBox.Show("Debe realizar los campos con la información solicitada", "Error"
        //                , MessageBoxButton.OK, MessageBoxImage.Error);

        //        }
        //        else
        //        {
        //            obj_Beca_DAL.iIdEstudiante = Convert.ToInt32(txt_id_Estudiante.Text.Trim());
        //            obj_Estudiante_DAL.sNombre = txt_Nombre.Text.Trim();
        //            obj_Estudiante_DAL.sPrimerApellido = txt_Apell1.Text.Trim();
        //            obj_Estudiante_DAL.sSegundoApellido = txt_Apell2.Text.Trim();
        //            obj_Estudiante_DAL.sIdentificacion = txt_Identificacion.Text.Trim();
        //            obj_Estudiante_DAL.sDireccion = txt_Direccion.Text.Trim();
        //            obj_Estudiante_DAL.iTelefono = Convert.ToInt32(txt_Telefono.Text.Trim());
        //            obj_Estudiante_DAL.dFechaNacimiento = dtp_Fecha_Nacimiento.SelectedDate.Value.Date;
        //            obj_Estudiante_DAL.cGenero = Convert.ToChar(cbx_Genero.SelectedValue);
        //            obj_Estudiante_DAL.sNacionalidad = txt_Nacionalidad.Text.Trim();

        //            obj_Estudiante_DAL.sNivel_Academico = cmb_NivelAcademico.SelectedValue.ToString();

        //            obj_Estudiante_DAL.iGrado = Convert.ToInt32(txt_Grado.Text.Trim());
        //            obj_Estudiante_DAL.dTotalCosto = Convert.ToDecimal(txt_TotalCosto.Text.Trim());


        //            obj_Estudiante_DAL.iIdCampus = Convert.ToInt32(cbx_Campus.SelectedValue.ToString());

        //            obj_Estudiante_DAL.iIdBeca = Convert.ToInt32(cbx_Beca.SelectedValue.ToString());
        //            obj_Estudiante_DAL.iIdEstado = Convert.ToInt32(cbx_Estado.SelectedValue.ToString());

        //            ComboBoxItem sTipocasa = (ComboBoxItem)cmb_TipoCasa.SelectedItem;
        //            obj_Estudiante_DAL.sTipoCasa = sTipocasa.Content.ToString();

        //            obj_Estudiante_DAL.iCantNucleoFam = Convert.ToInt32(nmc_CantPersCasa.Value.ToString());
        //            obj_Estudiante_DAL.iCantTrabajador = Convert.ToInt32(nmc_CantPersTrabajan.Value.ToString());

        //            ComboBoxItem IngresoMes = (ComboBoxItem)cmb_Ingreso.SelectedItem;
        //            obj_Estudiante_DAL.sIngresoMesBruto = IngresoMes.Content.ToString();

        //            obj_Estudiante_DAL.dIngresoMesNeto = Convert.ToDecimal(txt_MontoIngreso.Text.Trim());
        //            obj_Estudiante_DAL.dGastoMensual = Convert.ToDecimal(txt_Montogasto.Text.Trim());

        //            int Edad = DateTime.Today.AddTicks(-obj_Estudiante_DAL.dFechaNacimiento.Ticks).Year - 1;


        //            if (Edad < 15 || obj_Estudiante_DAL.dFechaNacimiento >= DateTime.Now)
        //            {

        //                MessageBox.Show("La fecha de nacimiento no puede ser superior o igual al día de Hoy",
        //                       "Error", MessageBoxButton.OK, MessageBoxImage.Error);


        //            }
        //            else
        //            {
        //                if (txt_Correo.Text.Length == 0)
        //                {
        //                    MessageBox.Show("Debe Ingresar una Direccion de Correo Electrónico");
        //                    txt_Correo.Focus();
        //                }
        //                else
        //                {
        //                    ValEmail();

        //                    if (Valmail == true)
        //                    {
        //                        obj_Estudiante_DAL.sCorreo = txt_Correo.Text.Trim();
        //                        obj_Estudiante_BLL.Crear_Estudiante(ref obj_Estudiante_DAL, ref sMsjError);

        //                        if (sMsjError != string.Empty)
        //                        {
        //                            MessageBox.Show("Se presento un error al Insertar un Nuevo Estudiante.\n\nDesc.Error="
        //                                + sMsjError, "Error", MessageBoxButton.OK, MessageBoxImage.Error);

        //                        }
        //                        else
        //                        {
        //                            MessageBox.Show("Estudiante Insertado Exitosamente", "Insert Exitoso", MessageBoxButton.OK, MessageBoxImage.Information);
        //                        }
        //                        PintarControles();
        //                    }


        //                }




        //            }

        //        }

        //    }
        //    else
        //    {
        //        if (txt_Nombre.Text == string.Empty ||
        //            txt_Apell1.Text == string.Empty ||
        //            txt_Apell2.Text == string.Empty ||
        //            txt_Direccion.Text == string.Empty ||
        //            txt_id_Estudiante.Text == string.Empty ||
        //            txt_Nacionalidad.Text == string.Empty ||
        //            txt_Telefono.Text == string.Empty ||

        //           txt_Identificacion.Text == string.Empty)
        //        {
        //            MessageBox.Show("Debe rellenar lo campos con la información Solicitada", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
        //        }
        //        else
        //        {
        //            obj_Estudiante_DAL.iIdEstudiante = Convert.ToInt32(txt_id_Estudiante.Text.Trim());
        //            obj_Estudiante_DAL.sNombre = txt_Nombre.Text.Trim();
        //            obj_Estudiante_DAL.sPrimerApellido = txt_Apell1.Text.Trim();
        //            obj_Estudiante_DAL.sSegundoApellido = txt_Apell2.Text.Trim();
        //            obj_Estudiante_DAL.sIdentificacion = txt_Identificacion.Text.Trim();
        //            obj_Estudiante_DAL.sDireccion = txt_Direccion.Text.Trim();
        //            obj_Estudiante_DAL.iTelefono = Convert.ToInt32(txt_Telefono.Text.Trim());
        //            obj_Estudiante_DAL.dFechaNacimiento = dtp_Fecha_Nacimiento.SelectedDate.Value.Date;


        //            obj_Estudiante_DAL.cGenero = Convert.ToChar(cbx_Genero.SelectedValue);

        //            obj_Estudiante_DAL.sNacionalidad = txt_Nacionalidad.Text.Trim();
        //            obj_Estudiante_DAL.sCorreo = txt_Correo.Text.Trim();

        //            ComboBoxItem sNivel = (ComboBoxItem)cmb_NivelAcademico.SelectedItem;
        //            obj_Estudiante_DAL.sNivel_Academico = sNivel.Content.ToString();

        //            obj_Estudiante_DAL.iGrado = Convert.ToInt32(txt_Grado.Text.Trim());
        //            obj_Estudiante_DAL.dTotalCosto = Convert.ToDecimal(txt_TotalCosto.Text.Trim());


        //            obj_Estudiante_DAL.iIdCampus = Convert.ToInt32(cbx_Campus.SelectedValue.ToString());

        //            obj_Estudiante_DAL.iIdBeca = Convert.ToInt32(cbx_Beca.SelectedValue.ToString());
        //            obj_Estudiante_DAL.iIdEstado = Convert.ToInt32(cbx_Estado.SelectedValue.ToString());

        //            ComboBoxItem sTipocasa = (ComboBoxItem)cmb_TipoCasa.SelectedItem;
        //            obj_Estudiante_DAL.sTipoCasa = sTipocasa.Content.ToString();

        //            obj_Estudiante_DAL.iCantNucleoFam = Convert.ToInt32(nmc_CantPersCasa.Value.ToString());
        //            obj_Estudiante_DAL.iCantTrabajador = Convert.ToInt32(nmc_CantPersTrabajan.Value.ToString());

        //            ComboBoxItem IngresoMes = (ComboBoxItem)cmb_Ingreso.SelectedItem;
        //            obj_Estudiante_DAL.sIngresoMesBruto = IngresoMes.Content.ToString();

        //            obj_Estudiante_DAL.dIngresoMesNeto = Convert.ToDecimal(txt_MontoIngreso.Text.Trim());
        //            obj_Estudiante_DAL.dGastoMensual = Convert.ToDecimal(txt_Montogasto.Text.Trim());

        //            int Edad = DateTime.Today.AddTicks(-obj_Estudiante_DAL.dFechaNacimiento.Ticks).Year - 1;


        //            if (Edad < 15 || obj_Estudiante_DAL.dFechaNacimiento >= DateTime.Now)
        //            {

        //                MessageBox.Show("La fecha de nacimiento no puede ser superior o igual al día de Hoy",
        //                       "Error", MessageBoxButton.OK, MessageBoxImage.Error);


        //            }
        //            else
        //            {
        //                if (txt_Correo.Text.Trim().Contains("@"))
        //                {

        //                    obj_Estudiante_BLL.Crear_Estudiante(ref obj_Estudiante_DAL, ref sMsjError);

        //                    if (sMsjError != string.Empty)
        //                    {
        //                        MessageBox.Show("Se presento un error al Insertar un Nuevo Estudiante.\n\nDesc.Error="
        //                            + sMsjError, "Error", MessageBoxButton.OK, MessageBoxImage.Error);

        //                    }
        //                    else
        //                    {
        //                        MessageBox.Show("Estudiante Insertado Exitosamente", "Insert Exitoso", MessageBoxButton.OK, MessageBoxImage.Information);
        //                    }
        //                    PintarControles();
        //                }
        //                else
        //                {
        //                    MessageBox.Show("Formato de Correo Inválido", "Correo No Existente", MessageBoxButton.OK, MessageBoxImage.Warning);
        //                }
        //            }
        //        }
        //        this.Close();
        //    }

        //}



        #endregion

        #region PintarControles

        //private void PintarControles()
        //{
        //    if (obj_Estudiante_DAL.cBandAxion == 'I')
        //    {

        //        Cargar_Combo();


        //        txt_id_Estudiante.Text = string.Empty;
        //        txt_Nombre.Text = string.Empty;
        //        txt_Apell1.Text = string.Empty;
        //        txt_Apell2.Text = string.Empty;
        //        txt_Identificacion.Text = string.Empty;
        //        txt_Direccion.Text = string.Empty;
        //        txt_Telefono.Text = string.Empty;
        //        dtp_Fecha_Nacimiento.Text = "";

        //        txt_Nacionalidad.Text = string.Empty;
        //        txt_Correo.Text = string.Empty;



        //    }
        //    else
        //    {
        //        Cargar_Combo();

        //        txt_id_Estudiante.Text = obj_Estudiante_DAL.iIdEstudiante.ToString();
        //        txt_Nombre.Text = obj_Estudiante_DAL.sNombre;
        //        txt_Apell1.Text = obj_Estudiante_DAL.sPrimerApellido;
        //        txt_Apell2.Text = obj_Estudiante_DAL.sSegundoApellido;
        //        txt_Identificacion.Text = obj_Estudiante_DAL.sIdentificacion;
        //        txt_Direccion.Text = obj_Estudiante_DAL.sDireccion;
        //        txt_Telefono.Text = obj_Estudiante_DAL.iTelefono.ToString();
        //        dtp_Fecha_Nacimiento.Text = obj_Estudiante_DAL.dFechaNacimiento.ToString();
        //        txt_Nacionalidad.Text = obj_Estudiante_DAL.sNacionalidad;
        //        txt_Correo.Text = obj_Estudiante_DAL.sCorreo;
        //        txt_MontoIngreso.Text = obj_Estudiante_DAL.dIngresoMesNeto.ToString();
        //        txt_Montogasto.Text = obj_Estudiante_DAL.dGastoMensual.ToString();
        //        nmc_CantPersCasa.Value = obj_Estudiante_DAL.iCantNucleoFam;
        //        nmc_CantPersTrabajan.Value = obj_Estudiante_DAL.iCantTrabajador;

        //    }
        //}
        #endregion



        #region Cargar Combos
  

        private void Cargar_Combo()
        {
           

            #region Variables
            string sMsjError = string.Empty;
            
            DataTable dtEstadoExp = new DataTable();
            DataTable dtSubCate = new DataTable();
           

            #endregion

            //Llamo los métodos de cada tabla q necesito

           
            obj_Estado_Expediente.Listar_EstadoExpediente(ref dtEstadoExp, ref sMsjError);
            obj_Sub_Cat.Listar_SubCategoria(ref dtSubCate, ref sMsjError);
            



            if (sMsjError == string.Empty)
            {


          

                cbx_EstadoExpediente.ItemsSource = null;
                cbx_EstadoExpediente.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtEstadoExp });

                cbx_SubCategoria.ItemsSource = null;
                cbx_SubCategoria.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtSubCate });

               

             
                //In WPF use DisplayMemberPath and SelectedValuePath.



                cbx_EstadoExpediente.SelectedValuePath = dtEstadoExp.Columns[0].ToString();
                cbx_EstadoExpediente.DisplayMemberPath = dtEstadoExp.Columns[1].ToString();

                cbx_SubCategoria.SelectedValuePath = dtSubCate.Columns[0].ToString();
                cbx_SubCategoria.DisplayMemberPath = dtSubCate.Columns[1].ToString();


            }
            else
            {
                cbx_EstadoExpediente.ItemsSource = null;
                cbx_SubCategoria.ItemsSource = null;
               

                MessageBox.Show("Se presento un error al Listar Becas.\n\nDesc.Error=" + sMsjError, "Error al listar",
                                 MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        #endregion
        #endregion

   


        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                GlassHelper.ExtendGlass(this, -1, -1, -1, -1);
            }
            catch (Exception)
            {

                this.Background = Brushes.White;
            }

            Cargar_Combo();
        }
    }
}
