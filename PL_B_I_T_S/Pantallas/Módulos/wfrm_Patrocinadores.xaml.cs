﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;
using BLL_B_I_T_S.Catalogos_y_Mantenimiento;
using System.Data;
using System.Text.RegularExpressions;

namespace PL_B_I_T_S.Pantallas.Módulos
{
    /// <summary>
    /// Lógica de interacción para wfrm_Patrocinadores.xaml
    /// </summary>
    public partial class wfrm_Patrocinadores : MetroWindow
    {
        #region Var Glob
        public cls_Patrocinador_DAL obj_Patrocinador_DAL = new cls_Patrocinador_DAL();
        cls_Patrocinador_BLL obj_Patrocinador_BLL = new cls_Patrocinador_BLL();

        cls_EstadoPatrocinador_BLL obj_EstaPatro_BLL = new cls_EstadoPatrocinador_BLL();
        cls_Periodo_BLL obj_Periodo_BLL = new cls_Periodo_BLL();
        

        

        bool Valmail;

        #endregion
        public wfrm_Patrocinadores()
        {
            InitializeComponent();
        }

        #region eventos

        private void txt_NombreBeca_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int letra = Convert.ToInt32(Convert.ToChar(e.Text));

            if (letra >= 65 && letra <= 90 || letra >= 97 && letra <= 122)
            {
                e.Handled = false;
            }
            else e.Handled = true;
        }
        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                GlassHelper.ExtendGlass(this, -1, -1, -1, -1);
            }
            catch (Exception)
            {

                this.Background = Brushes.White;
            }

            Cargar_Combo();
            PintarControles();
        }

        private void txt_IdPatrocinador_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int numeros = Convert.ToInt32(Convert.ToChar(e.Text));

            if (numeros >= 48 && numeros <= 57)
            {

                e.Handled = false;

            }
            else
            {

                e.Handled = true;
            }
        }

        private void txt_NombrePatrocinador_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int letra = Convert.ToInt32(Convert.ToChar(e.Text));

            if ((letra >= 65 && letra <= 90 || letra >= 97 && letra <= 122))
            {
                e.Handled = false;
            }
            else e.Handled = true;
        }

        private void txt_PerioricidadCobro_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int letra = Convert.ToInt32(Convert.ToChar(e.Text));

            if (letra >= 65 && letra <= 90 || letra >= 97 && letra <= 122)
            {
                e.Handled = false;
            }
            else e.Handled = true;
        }

        private void txt_MontoMaximo_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int numeros= Convert.ToInt32(Convert.ToChar(e.Text));
            
            if (numeros >= 48 && numeros <= 57)
            {

                e.Handled = false;

            }
            else
            {

                e.Handled = true;
            }
        }
        #endregion

        #region Métodos
        #region ValEmail

        private void ValEmail()
        {
            if (!Regex.IsMatch(txt_Correo.Text, @"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"))
            {
              Valmail = false;
                MessageBox.Show("Debe Ingresar una Direccion de Correo Electrónico Válida");
                txt_Correo.Select(0, txt_Correo.Text.Length);
                txt_Correo.Focus();

            }
            else
            {
                Valmail = true;


            }

        }
        #endregion

        #region Guardar

        public void Guardar()
        {

            string sMsjError = string.Empty;

            if (obj_Patrocinador_DAL.cBandAxion == 'I')
            {
                if (txt_NombrePatrocinador.Text == string.Empty ||
                    txt_MontoMaximo.Text == string.Empty ||
                    txt_Correo.Text == string.Empty||
                    txt_IdPatrocinador.Text== string.Empty
                    )
                {
                    MessageBox.Show("Debe realizar los campos con la información solicitada", "Error"
                        , MessageBoxButton.OK, MessageBoxImage.Error);

                }
                else
                {

                    obj_Patrocinador_DAL.sNombre = txt_NombrePatrocinador.Text.Trim();
                    obj_Patrocinador_DAL.iIdEstado = Convert.ToInt32(cmb_Estado.SelectedValue.ToString());
                    obj_Patrocinador_DAL.iIdPeriodo = Convert.ToInt32(cmb_Periodo.SelectedValue.ToString());
                    
                    obj_Patrocinador_DAL.dMontoMaximo = Convert.ToDouble(txt_MontoMaximo.Text.Trim());
                    obj_Patrocinador_DAL.iIdPatrocinador = Convert.ToInt32(txt_IdPatrocinador.Text.Trim());


                    if (txt_Correo.Text.Length == 0)
                    {
                        MessageBox.Show("Debe Ingresar una Direccion de Correo Electrónico");
                        txt_Correo.Focus();
                    }
                    else
                    {
                        ValEmail();

                        if (Valmail==true)
                        {
                            obj_Patrocinador_DAL.sCorreo = txt_Correo.Text.Trim();
                            obj_Patrocinador_BLL.Crear_Patrocinador(ref obj_Patrocinador_DAL, ref sMsjError);

                            if (sMsjError != string.Empty)
                            {
                                MessageBox.Show("Se presento un error al Insertar un nuevo Patrocinador.\n\nDesc.Error="
                                    + sMsjError, "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                            }
                            else
                            {
                                MessageBox.Show("Patrocinador Insertado Exitosamente", "Insert Exitoso", MessageBoxButton.OK, MessageBoxImage.Information);
                            }
                            PintarControles(); 
                        }
                       
                    }

                    

                }

            }
            else
            {
          

                if (txt_NombrePatrocinador.Text == string.Empty ||
                    txt_MontoMaximo.Text == string.Empty ||
                    txt_Correo.Text == string.Empty ||
                    txt_IdPatrocinador.Text == string.Empty
                    )
                {
                    MessageBox.Show("Debe rellenar lo campos con la información Solicitada", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    obj_Patrocinador_DAL.sNombre = txt_NombrePatrocinador.Text.Trim();
                    obj_Patrocinador_DAL.iIdEstado = Convert.ToInt32(cmb_Estado.SelectedValue.ToString());
                    obj_Patrocinador_DAL.iIdPeriodo = Convert.ToInt32(cmb_Periodo.SelectedValue.ToString());
                   
                    obj_Patrocinador_DAL.dMontoMaximo = Convert.ToDouble(txt_MontoMaximo.Text.Trim());
                    obj_Patrocinador_DAL.iIdPatrocinador = Convert.ToInt32(txt_IdPatrocinador.Text.Trim());

                    if (txt_Correo.Text.Length == 0)
                    {
                        MessageBox.Show("Debe Ingresar una Direccion de Correo Electrónico");
                        txt_Correo.Focus();
                    }
                    else
                    {
                        ValEmail();
                        obj_Patrocinador_DAL.sCorreo = txt_Correo.Text.Trim();
                        obj_Patrocinador_BLL.Modificar_Patrocinador(ref obj_Patrocinador_DAL, ref sMsjError);

                        if (sMsjError != string.Empty)
                        {
                            MessageBox.Show("Se presento un error al Modificar un nuevo Patrocinador.\n\nDesc.Error="
                                + sMsjError, "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                        }
                        else
                        {
                            MessageBox.Show("Patrocinador Modificado Exitosamente", "Update Exitoso", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                        PintarControles();
                    }
                    


                }
            }

        }
        #endregion

        #region PintarControles

        private void PintarControles()
        {
            if (obj_Patrocinador_DAL.cBandAxion == 'I')
            {

                Cargar_Combo();

                txt_NombrePatrocinador.Text = string.Empty;
                txt_MontoMaximo.Text =  string.Empty;
                txt_Correo.Text = string.Empty;
                txt_IdPatrocinador.Text = string.Empty;

            }
            else
            {

                Cargar_Combo();

                txt_IdPatrocinador.Text = obj_Patrocinador_DAL.iIdPatrocinador.ToString();
                txt_NombrePatrocinador.Text = obj_Patrocinador_DAL.sNombre.ToString();
                txt_MontoMaximo.Text = obj_Patrocinador_DAL.dMontoMaximo.ToString();
                txt_Correo.Text = obj_Patrocinador_DAL.sCorreo.ToString();
                txt_IdPatrocinador.IsEnabled = false;
                cmb_Estado.SelectedValue = obj_Patrocinador_DAL.iIdEstado.ToString();
                cmb_Periodo.SelectedValue = obj_Patrocinador_DAL.iIdPeriodo.ToString();

            }
        }
        #endregion

        #region Cargar Combos

        private void Cargar_Combo()
        {
            #region Variables
            string sMsjError = string.Empty;
            DataTable dtEstadoPatrocinador = new DataTable();
            DataTable dtPeriodo = new DataTable();

            #endregion

            //Llamo los métodos de cada tabla q necesito


            obj_EstaPatro_BLL.Listar_EstadoPatrocinado(ref dtEstadoPatrocinador, ref sMsjError);
            obj_Periodo_BLL.Listar_Periodo(ref dtPeriodo,ref sMsjError);



            if (sMsjError == string.Empty)
            {



                cmb_Estado.ItemsSource = null;
                cmb_Estado.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtEstadoPatrocinador });

                //In WPF use DisplayMemberPath and SelectedValuePath.

                cmb_Estado.SelectedValuePath = dtEstadoPatrocinador.Columns[0].ToString();
                cmb_Estado.DisplayMemberPath = dtEstadoPatrocinador.Columns[1].ToString();

                cmb_Periodo.ItemsSource = null;
                cmb_Periodo.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtPeriodo });

                //In WPF use DisplayMemberPath and SelectedValuePath.

                cmb_Periodo.SelectedValuePath = dtPeriodo.Columns[0].ToString();
                cmb_Periodo.DisplayMemberPath = dtPeriodo.Columns[1].ToString();


            }
            else
            {


                cmb_Estado.ItemsSource = null;
                cmb_Periodo.ItemsSource = null;

                MessageBox.Show("Se presento un error al Listar Patrocinadores.\n\nDesc.Error=" + sMsjError, "Error al listar",
                                 MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        #endregion
      


        #endregion


        private void btn_Guardar_Click(object sender, RoutedEventArgs e)
        {
            Guardar();
        }

       
    }
}
