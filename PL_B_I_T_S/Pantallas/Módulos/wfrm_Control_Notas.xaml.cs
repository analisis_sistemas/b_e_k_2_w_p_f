﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using System.Data;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;
using BLL_B_I_T_S.Catalogos_y_Mantenimiento;
using PL_B_I_T_S.Pantallas.Módulos;
using System.Collections.ObjectModel;

namespace PL_B_I_T_S.Pantallas.Módulos
{
    /// <summary>
    /// Interaction logic for wfrm_Control_Notas.xaml
    /// </summary>
    public partial class wfrm_Control_Notas : MetroWindow
    {
        public cls_Cursos_DAL obj_Cursos_DAL = new cls_Cursos_DAL();
        cls_Cursos_BLL obj_Cursos_BLL = new cls_Cursos_BLL();
        cls_EstadoCurso_BLL obj_EstadoCurso_BLL = new cls_EstadoCurso_BLL();

        public wfrm_Control_Notas()
        {
            InitializeComponent();
        }

        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                GlassHelper.ExtendGlass(this, -1, -1, -1, -1);
            }
            catch (Exception)
            {

                this.Background = Brushes.White;
            }

            Cargar_Combo();
        }

        private void btn_calcular_promedio_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btn_Guardar_Click(object sender, RoutedEventArgs e)
        {

        }

        #region Métodos

        #region Guardar

        //public void Guardar()
        //{

        //    string sMsjError = string.Empty;

        //    if (obj_Cursos_DAL.cBandAxion == 'I')
        //    {
        //        if (txt_IdEstudiante.Text == string.Empty ||
        //            txt_NombreCurso.Text == string.Empty ||
        //            txt_Nota.Text == string.Empty ||
        //            cmb_EstadoCurso.SelectedIndex == null)
        //        {
        //            MessageBox.Show("Debe realizar los campos con la información solicitada", "Error"
        //                , MessageBoxButton.OK, MessageBoxImage.Error);

        //        }
        //        else
        //        {

        //            obj_Cursos_DAL.sNombre = txt_NombreCurso.Text.Trim();
        //            obj_Cursos_DAL.iId_Estudiante = Convert.ToInt32(txt_IdEstudiante.Text.Trim());
        //            obj_Cursos_DAL.dNota = Convert.ToDecimal(txt_Nota.Text.Trim());
        //            obj_Cursos_DAL.iIdEstado = Convert.ToInt32(cmb_EstadoCurso.SelectedIndex);


        //            if (sMsjError != string.Empty)
        //            {
        //                MessageBox.Show("Se presento un error al Insertar un Nuevo Curso.\n\nDesc.Error="
        //                    + sMsjError, "Error", MessageBoxButton.OK, MessageBoxImage.Error);

        //            }
        //            else
        //            {
        //                MessageBox.Show("Curso Insertado Exitosamente", "Insert Exitoso", MessageBoxButton.OK, MessageBoxImage.Information);
        //            }
        //            PintarControles();
        //        }

        //    }
        //    else
        //    {
        //        if (txt_IdEstudiante.Text == string.Empty ||
        //            txt_NombreCurso.Text == string.Empty ||
        //            txt_Nota.Text == string.Empty ||
        //            cmb_EstadoCurso.SelectedIndex == null)
        //        {
        //            MessageBox.Show("Debe rellenar lo campos con la información Solicitada", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
        //        }
        //        else
        //        {


        //            obj_Cursos_DAL.sNombre = txt_NombreCurso.Text.Trim();
        //            obj_Cursos_DAL.iId_Estudiante = Convert.ToInt32(txt_IdEstudiante.Text.Trim());
        //            obj_Cursos_DAL.dNota = Convert.ToDecimal(txt_Nota.Text.Trim());
        //            obj_Cursos_DAL.iIdEstado = Convert.ToInt32(cmb_EstadoCurso.SelectedIndex);





        //        }
        //    }

        //}
        #endregion

         

        #region Cargar
        //private void CargarNotas()
        //{


        //    #region Variables
        //    string sMsjError = string.Empty;
        //    DataTable dtNotas = new DataTable();
        //    #endregion

        //    obj_co_BLL.Listar_Estudiante(ref dtEstudiante, ref sMsjError);

        //    if (sMsjError == string.Empty)
        //    {
        //        dgv_Estudiantes.ItemsSource = null;
        //        dgv_Estudiantes.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtEstudiante });

        //    }
        //    else
        //    {
        //        dgv_Estudiantes.ItemsSource = null;
        //        MessageBox.Show("Se presento un error al listar Activos.\n\nDesc.Error ="
        //                         + sMsjError, "Error al Listar", MessageBoxButton.OK, MessageBoxImage.Error);

        //        dgv_Estudiantes.ItemsSource = null;
        //    }
        //}
        #endregion      

        #region PintarControles

        private void PintarControles()
        {
            if (obj_Cursos_DAL.cBandAxion == 'I')
            {

                Cargar_Combo();


                txt_NombreCurso.Text = string.Empty;
                txt_IdEstudiante.Text = string.Empty;
                txt_Nota.Text = string.Empty;
                txt_Promedio.Text = string.Empty;



            }
            else
            {
                Cargar_Combo();

                txt_NombreCurso.Text = obj_Cursos_DAL.sNombre;
                txt_IdEstudiante.Text = obj_Cursos_DAL.iId_Estudiante.ToString();
                txt_Nota.Text = obj_Cursos_DAL.dNota.ToString();




            }
        }


        #endregion



        #region Cargar Combos

        private void Cargar_Combo()
        {
            #region Variables
            string sMsjError = string.Empty;
            DataTable dtEstados = new DataTable();


            #endregion

            //Llamo los métodos de cada tabla q necesito


            obj_EstadoCurso_BLL.Listar_EstadoCurso(ref dtEstados, ref sMsjError);




            if (sMsjError == string.Empty)
            {



                cmb_EstadoCurso.ItemsSource = null;
                cmb_EstadoCurso.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtEstados });

                //In WPF use DisplayMemberPath and SelectedValuePath.

                cmb_EstadoCurso.SelectedValuePath = dtEstados.Columns[0].ToString();
                cmb_EstadoCurso.DisplayMemberPath = dtEstados.Columns[1].ToString();




            }
            else
            {


                cmb_EstadoCurso.ItemsSource = null;


                MessageBox.Show("Se presento un error al Listar los Cursos.\n\nDesc.Error=" + sMsjError, "Error al listar",
                                 MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        #endregion

        private void txt_IdEstudiante_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int numeros = Convert.ToInt32(Convert.ToChar(e.Text));

            if (numeros >= 48 && numeros <= 57)
            {

                e.Handled = false;

            }
            else
            {

                e.Handled = true;
            }
        }
        #endregion

        private void txt_Nota_PreviewStylusSystemGesture(object sender, StylusSystemGestureEventArgs e)
        {

        }

        private void txt_Nota_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int numeros = Convert.ToInt32(Convert.ToChar(e.Text));

            if (numeros >= 48 && numeros <= 57)
            {

                e.Handled = false;

            }
            else
            {

                e.Handled = true;
            }
        }

        private void txt_NombreCurso_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int letra = Convert.ToInt32(Convert.ToChar(e.Text));

            if (letra >= 65 && letra <= 90 || letra >= 97 && letra <= 122)
            {
                e.Handled = false;
            }
            else e.Handled = true;
        }

        
    }
}
