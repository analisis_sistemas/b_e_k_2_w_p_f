﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using BLL_B_I_T_S.Logica_General;

using DAL_B_I_T_S.Catalogos_y_Mantenimiento;
using BLL_B_I_T_S.Catalogos_y_Mantenimiento;
using System.Data;

namespace PL_B_I_T_S.Pantallas.Módulos
{
    /// <summary>
    /// Interaction logic for wfrm_Registro_Becas.xaml
    /// </summary>
    public partial class wfrm_Registro_Becas : MetroWindow
    {
        #region VarGlobales
        public cls_Beca_DAL obj_Beca_DAL = new cls_Beca_DAL();
        cls_Beca_BLL obj_Beca_BLL = new cls_Beca_BLL();

        cls_Patrocinador_DAL obj_Patrocinador_DAL = new cls_Patrocinador_DAL();
        cls_Patrocinador_BLL obj_Patrocinador_BLL = new cls_Patrocinador_BLL();

        cls_Categoria_DAL obj_categoria_DAL = new cls_Categoria_DAL();
        cls_Categoria_BLL obj_categoria_BLL = new cls_Categoria_BLL();

        cls_Sub_Categoria_DAL obj_SubCategoria_DAL = new cls_Sub_Categoria_DAL();
        cls_SubCategoria_BLL obj_SubCategoria_BLL = new cls_SubCategoria_BLL();

        cls_Estado_Beca_DAL obj_estadoBeca_DAL = new cls_Estado_Beca_DAL();
        cls_EstadoBeca_BLL obj_estdoBeca_BLL = new cls_EstadoBeca_BLL();

        #endregion

        public wfrm_Registro_Becas()
        {
            InitializeComponent();
        }
        #region Eventos
        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                GlassHelper.ExtendGlass(this, -1, -1, -1, -1);
            }
            catch (Exception)
            {

                this.Background = Brushes.White;
            }
            Cargar_Combo();
            PintarControles();
            
        }

        private void txt_Nombre_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int letra = Convert.ToInt32(Convert.ToChar(e.Text));

            if (letra >= 65 && letra <= 90 || letra >= 97 && letra <= 122)
            {
                e.Handled = false;
            }
            else e.Handled = true;
        }

        private void txt_Porcentaje_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int numeros = Convert.ToInt32(Convert.ToChar(e.Text));

            if (numeros >= 48 && numeros <= 57)
            {

                e.Handled = false;

            }
            else
            {

                e.Handled = true;
            }
        }

        private void txt_Nota_Prom_min_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int numeros = Convert.ToInt32(Convert.ToChar(e.Text));

            if (numeros >= 48 && numeros <= 57)
            {

                e.Handled = false;

            }
            else
            {

                e.Handled = true;
            }
        }

        private void txt_idBeca_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int numeros = Convert.ToInt32(Convert.ToChar(e.Text));

            if (numeros >= 48 && numeros <= 57)
            {

                e.Handled = false;

            }
            else
            {

                e.Handled = true;
            }
        }
        #endregion

        #region Métodos

        #region Guardar

        public void Guardar()
        {

            string sMsjError = string.Empty;

            if (obj_Beca_DAL.cBandAxion == 'I')
            {
                if (txt_idBeca.Text == string.Empty ||
                    txt_Nombre.Text == string.Empty ||
                    txt_Porcentaje.Text == string.Empty ||
                    txt_Nota_Prom_min.Text == string.Empty
                    )
                {
                    MessageBox.Show("Debe realizar los campos con la información solicitada", "Error"
                        , MessageBoxButton.OK, MessageBoxImage.Error);

                }
                else
                {
                    obj_Beca_DAL.iIdBeca= Convert.ToInt32(txt_idBeca.Text.Trim());
                    obj_Beca_DAL.sNombre = txt_Nombre.Text.Trim();
                    obj_Beca_DAL.dPorcentajeDescuento = Convert.ToDouble(txt_Porcentaje.Text.Trim());
                    obj_Beca_DAL.dNotaMinima = Convert.ToDouble(txt_Nota_Prom_min.Text.Trim());
                    obj_Beca_DAL.iIdPatrocinador = Convert.ToInt32(cbx_Patrocinador.SelectedValue.ToString());
                    obj_Beca_DAL.iIdCategoria = Convert.ToInt32(cbx_Categoria.SelectedValue.ToString());
                    obj_Beca_DAL.iIdSubcategoria = Convert.ToInt32(cbx_Sub_Categoria.SelectedValue.ToString());
                    obj_Beca_DAL.iIdEstado = Convert.ToInt32(cbx_Estado.SelectedValue.ToString());
                    obj_Beca_BLL.Crear_Beca(ref obj_Beca_DAL, ref sMsjError);

                    if (sMsjError != string.Empty)
                    {
                        MessageBox.Show("Se presento un error al ingresar La beca.\n\nDesc.Error="
                            + sMsjError, "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                    }
                    else
                    {
                        MessageBox.Show("Beca ingresada Exitosamente", "Update Exitoso", MessageBoxButton.OK, MessageBoxImage.Information);
                    }

                    PintarControles();
                }

            }
            else
            {


                if (txt_idBeca.Text == string.Empty ||
                    txt_Nombre.Text == string.Empty ||
                    txt_Porcentaje.Text == string.Empty ||
                    txt_Nota_Prom_min.Text == string.Empty
                    )
                {
                    MessageBox.Show("Debe rellenar lo campos con la información Solicitada", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    obj_Beca_DAL.iIdBeca = Convert.ToInt32(txt_idBeca.Text.Trim());
                    obj_Beca_DAL.sNombre = txt_Nombre.Text.Trim();
                    obj_Beca_DAL.dPorcentajeDescuento = Convert.ToDouble(txt_Porcentaje.Text.Trim());
                    obj_Beca_DAL.dNotaMinima = Convert.ToDouble(txt_Nota_Prom_min.Text.Trim());
                    obj_Beca_DAL.iIdPatrocinador = Convert.ToInt32(cbx_Patrocinador.SelectedValue.ToString());
                    obj_Beca_DAL.iIdCategoria = Convert.ToInt32(cbx_Categoria.SelectedValue.ToString());
                    obj_Beca_DAL.iIdSubcategoria = Convert.ToInt32(cbx_Sub_Categoria.SelectedValue.ToString());
                    obj_Beca_DAL.iIdEstado = Convert.ToInt32(cbx_Estado.SelectedValue.ToString());
                    obj_Beca_BLL.Modificar_Beca(ref obj_Beca_DAL, ref sMsjError);

                    if (sMsjError != string.Empty)
                    {
                        MessageBox.Show("Se presento un error al Modificar la Beca.\n\nDesc.Error="
                            + sMsjError, "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                    }
                    else
                    {
                        MessageBox.Show("Beca Modificada Exitosamente", "Update Exitoso", MessageBoxButton.OK, MessageBoxImage.Information);
                    }

                    PintarControles();
                }
            }

        }
        #endregion

        #region PintarControles

        private void PintarControles()
        {
            if (obj_Beca_DAL.cBandAxion == 'I')
            {

                Cargar_Combo();

                txt_idBeca.Text = string.Empty;
                txt_Nombre.Text = string.Empty;
                txt_Porcentaje.Text = string.Empty;
                txt_Nota_Prom_min.Text = string.Empty;

            }
            else
            {

                Cargar_Combo();

                txt_idBeca.Text = obj_Beca_DAL.iIdBeca.ToString();
                txt_Nombre.Text = obj_Beca_DAL.sNombre.ToString();
                txt_Porcentaje.Text = obj_Beca_DAL.dPorcentajeDescuento.ToString();
                txt_Nota_Prom_min.Text = obj_Beca_DAL.dNotaMinima.ToString();
                txt_idBeca.IsEnabled = false;
                cbx_Patrocinador.SelectedValue = obj_Beca_DAL.iIdPatrocinador.ToString();
                cbx_Categoria.SelectedValue = obj_Beca_DAL.iIdCategoria.ToString();
                cbx_Sub_Categoria.SelectedValue = obj_Beca_DAL.iIdSubcategoria.ToString();
                cbx_Estado.SelectedValue = obj_Beca_DAL.iIdEstado.ToString();

            }
        }
        #endregion

        #region Cargar Combos

        private void Cargar_Combo()
        {
            #region Variables
            string sMsjError = string.Empty;
            DataTable dtPatrocinador = new DataTable();
            DataTable dtCategoria = new DataTable();
            DataTable dtSubCategoria = new DataTable();
            DataTable dtEstadoBeca = new DataTable();
            #endregion

            //Llamo los métodos de cada tabla q necesito


            obj_Patrocinador_BLL.Listar_Patrocinador_Activo(ref dtPatrocinador, ref sMsjError);
            obj_categoria_BLL.Listar_Categoria(ref dtCategoria, ref sMsjError);
            obj_SubCategoria_BLL.Listar_SubCategoria(ref dtSubCategoria, ref sMsjError);
            obj_estdoBeca_BLL.Listar_EstadoBeca(ref dtEstadoBeca, ref sMsjError);



            if (sMsjError == string.Empty)
            {



                cbx_Patrocinador.ItemsSource = null;
                cbx_Patrocinador.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtPatrocinador });

                //In WPF use DisplayMemberPath and SelectedValuePath.

                cbx_Patrocinador.SelectedValuePath = dtPatrocinador.Columns[0].ToString();
                cbx_Patrocinador.DisplayMemberPath = dtPatrocinador.Columns[1].ToString();

                cbx_Categoria.ItemsSource = null;
                cbx_Categoria.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtCategoria });
                cbx_Categoria.SelectedValuePath = dtCategoria.Columns[0].ToString();
                cbx_Categoria.DisplayMemberPath = dtCategoria.Columns[1].ToString();

                cbx_Sub_Categoria.ItemsSource = null;
                cbx_Sub_Categoria.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtSubCategoria });
                cbx_Sub_Categoria.SelectedValuePath = dtSubCategoria.Columns[0].ToString();
                cbx_Sub_Categoria.DisplayMemberPath = dtSubCategoria.Columns[1].ToString();

                cbx_Estado.ItemsSource = null;
                cbx_Estado.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = dtEstadoBeca });
                cbx_Estado.SelectedValuePath = dtEstadoBeca.Columns[0].ToString();
                cbx_Estado.DisplayMemberPath = dtEstadoBeca.Columns[1].ToString();
            }
            else
            {


                cbx_Patrocinador.ItemsSource = null;
                cbx_Categoria.ItemsSource = null;
                cbx_Sub_Categoria.ItemsSource = null;
                cbx_Estado.ItemsSource = null;

                MessageBox.Show("Se presento un error al Listar Patrocinadores.\n\nDesc.Error=" + sMsjError, "Error al listar",
                                 MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        #endregion

        private void btn_Atras_Click(object sender, RoutedEventArgs e)
        {
            Guardar();
        }



        #endregion



        

    }
}
