﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DAL_B_I_T_S.BD;
using BLL_B_I_T_S.BD;
using BLL_B_I_T_S.Logica_General;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;

namespace BLL_B_I_T_S.Catalogos_y_Mantenimiento
{
   public class cls_Patrocinador_BLL
    {

        #region Variables Globales
        cls_BD_DAL obj_BD_DAL = new cls_BD_DAL();
        cls_BD_BLL obj_BD_BLL = new cls_BD_BLL();
        cls_Parametros_BLL obj_Parametros_BLL = new cls_Parametros_BLL();
        #endregion

        #region Métodos
        #region Listar
        public void Listar_Patrocinador(ref DataTable dtPatrocinador, ref string sMsjError)
        {
            #region Variables Publicas

            obj_BD_DAL = new cls_BD_DAL();
            obj_BD_BLL = new cls_BD_BLL();
            DataTable dtParametros = new DataTable();

            #endregion

            obj_BD_BLL.Excecute_Fill("SP_Patrocinador_Listar", "TBL_PATROCINADOR", dtParametros, ref obj_BD_DAL);

            if (obj_BD_DAL.sMsjError == String.Empty)
            {
                sMsjError = string.Empty;
                dtPatrocinador = obj_BD_DAL.DS.Tables[0];

            }
            else
            {
                sMsjError = obj_BD_DAL.sMsjError;
                dtPatrocinador = new DataTable();
            }

        }

        public void Listar_Patrocinador_Activo(ref DataTable dtPatrocinador, ref string sMsjError)
        {
            #region Variables Publicas

            obj_BD_DAL = new cls_BD_DAL();
            obj_BD_BLL = new cls_BD_BLL();
            DataTable dtParametros = new DataTable();

            #endregion

            obj_BD_BLL.Excecute_Fill("SP_Patrocinador_Listar_Activo", "TBL_PATROCINADOR", dtParametros, ref obj_BD_DAL);

            if (obj_BD_DAL.sMsjError == String.Empty)
            {
                sMsjError = string.Empty;
                dtPatrocinador = obj_BD_DAL.DS.Tables[0];

            }
            else
            {
                sMsjError = obj_BD_DAL.sMsjError;
                dtPatrocinador = new DataTable();
            }

        }
        #endregion

        #region Filtrar
        //public void Filtrar_Estudiante(ref DataTable dtEstudiante, string sFiltro, ref string sMsjError)
        public void Filtrar_Patrocinador(ref DataTable dtPatrocinador, int iFiltro, ref string sMsjError)
        {
            #region Variables Publicas

            obj_BD_DAL = new cls_BD_DAL();
            obj_BD_BLL = new cls_BD_BLL();

            #endregion

            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();

            if (dtParametros.Columns.Count == 3)
            {
                dtParametros.Rows.Add("@idPatrocinador", 2, iFiltro);//Nombre id a filtrar,tipo de dato y donde capturo el id
                obj_BD_BLL.Excecute_Fill("SP_Patrocinador_Filtrar_X_ID", "TBL_PATROCINADOR", dtParametros, ref obj_BD_DAL);

            }
            if (obj_BD_DAL.sMsjError == string.Empty)
            {
                sMsjError = string.Empty;
                dtPatrocinador = obj_BD_DAL.DS.Tables[0];


            }
            else
            {
                sMsjError = obj_BD_DAL.sMsjError;
                dtPatrocinador = new DataTable();
            }
        }
        #endregion

        #region Eliminar

        public void Eliminar_Patrocinador(int iId, ref string sMsjError)
        {
            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();

            if (dtParametros.Columns.Count == 3)
            {

                dtParametros.Rows.Add("@idPatrocinador", 2, iId);
                obj_BD_BLL.ExecuteNonQuery("SP_Patrocinador_Eliminar", dtParametros, ref obj_BD_DAL);


                if (obj_BD_DAL.sMsjError == string.Empty)
                {
                    sMsjError = string.Empty;
                }
                else
                {
                    sMsjError = obj_BD_DAL.sMsjError;

                }
            }

            else
            {
                sMsjError = " Se presento un error al crear parametros";

            }

        }
        #endregion

        #region Crear
        public void Crear_Patrocinador(ref cls_Patrocinador_DAL obj_Patrocinador_DAL, ref string sMsjError)
        {
            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();
            if (dtParametros.Columns.Count == 3)
            {
                dtParametros.Rows.Add("@idPatrocinador", 2, obj_Patrocinador_DAL.iIdPatrocinador);
                dtParametros.Rows.Add("@nombre", 4, obj_Patrocinador_DAL.sNombre);
                dtParametros.Rows.Add("@montoMaximo", 6, obj_Patrocinador_DAL.dMontoMaximo);
                dtParametros.Rows.Add("@correoElectronico", 4, obj_Patrocinador_DAL.sCorreo);
                dtParametros.Rows.Add("@idPeriodo", 2, obj_Patrocinador_DAL.iIdPeriodo);
                dtParametros.Rows.Add("@idEstado", 2, obj_Patrocinador_DAL.iIdEstado);


            }
            obj_BD_BLL.ExecuteNonQuery("SP_Patrocinador_Insertar", dtParametros, ref obj_BD_DAL);//Si fuera identity uso scalar

            if (obj_BD_DAL.sMsjError != string.Empty)
            {
                sMsjError = string.Empty;
                obj_Patrocinador_DAL.cBandAxion = 'I';
                obj_Patrocinador_DAL.cBandEstAxion = 'F';
            }
            else
            {
                sMsjError = string.Empty;
                obj_Patrocinador_DAL.cBandAxion = 'U';
                obj_Patrocinador_DAL.cBandEstAxion = 'E';
            }
        }

        #endregion

        #region Modificar

        public void Modificar_Patrocinador(ref cls_Patrocinador_DAL obj_Patrocinador_DAL, ref string sMsjError)
        {
            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();

            if (dtParametros.Columns.Count == 3)
            {

                dtParametros.Rows.Add("@idPatrocinador", 2, obj_Patrocinador_DAL.iIdPatrocinador);
                dtParametros.Rows.Add("@nombre", 4, obj_Patrocinador_DAL.sNombre);
                dtParametros.Rows.Add("@montoMaximo", 6, obj_Patrocinador_DAL.dMontoMaximo);
                dtParametros.Rows.Add("@correoElectronico", 4, obj_Patrocinador_DAL.sCorreo);
                dtParametros.Rows.Add("@idPeriodo", 2, obj_Patrocinador_DAL.iIdPeriodo);
                dtParametros.Rows.Add("@idEstado", 2, obj_Patrocinador_DAL.iIdEstado);


            }
            obj_BD_BLL.ExecuteNonQuery("SP_Patrocinador_Modificar", dtParametros, ref obj_BD_DAL);

            if (obj_BD_DAL.sMsjError != string.Empty)
            {
                sMsjError = obj_BD_DAL.sMsjError;
                obj_Patrocinador_DAL.cBandAxion = 'U';
                obj_Patrocinador_DAL.cBandEstAxion = 'F';
            }
            else
            {
                sMsjError = string.Empty;
                obj_Patrocinador_DAL.cBandAxion = 'U';
                obj_Patrocinador_DAL.cBandEstAxion = 'E';

            }
        }


        #endregion
        #endregion

    }
}
