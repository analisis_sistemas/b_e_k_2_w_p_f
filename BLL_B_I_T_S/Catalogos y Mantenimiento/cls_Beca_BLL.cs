﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DAL_B_I_T_S.BD;
using BLL_B_I_T_S.BD;
using BLL_B_I_T_S.Logica_General;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;

namespace BLL_B_I_T_S.Catalogos_y_Mantenimiento
{
   public class cls_Beca_BLL
    {
        #region Variables Globales
        cls_BD_DAL obj_BD_DAL = new cls_BD_DAL();
        cls_BD_BLL obj_BD_BLL = new cls_BD_BLL();
        cls_Parametros_BLL obj_Parametros_BLL = new cls_Parametros_BLL();
        #endregion

        #region Métodos

        #region Listar
        public void Listar_Beca(ref DataTable dtBeca, ref string sMsjError)
        {
            #region Variables Publicas

            obj_BD_DAL = new cls_BD_DAL();
            obj_BD_BLL = new cls_BD_BLL();
            DataTable dtParametros = new DataTable();

            #endregion

            obj_BD_BLL.Excecute_Fill("SP_Becas_Listar", "TBL_BECAS", dtParametros, ref obj_BD_DAL);

            if (obj_BD_DAL.sMsjError == String.Empty)
            {
                sMsjError = string.Empty;
                dtBeca = obj_BD_DAL.DS.Tables[0];

            }
            else
            {
                sMsjError = obj_BD_DAL.sMsjError;
                dtBeca = new DataTable();
            }

        }
        #endregion

        #region Crear
        public void Crear_Beca(ref cls_Beca_DAL obj_Beca_DAL, ref string sMsjError)
        {
            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();
            if (dtParametros.Columns.Count == 3)
            {
                dtParametros.Rows.Add("@idBeca", 2, obj_Beca_DAL.iIdBeca);
                dtParametros.Rows.Add("@porcentajeDescuento", 6, obj_Beca_DAL.dPorcentajeDescuento);
                dtParametros.Rows.Add("@nombre", 4, obj_Beca_DAL.sNombre);
                dtParametros.Rows.Add("@notaMinima", 6, obj_Beca_DAL.dNotaMinima);
                dtParametros.Rows.Add("@idPatrocinador", 2, obj_Beca_DAL.iIdPatrocinador);
                dtParametros.Rows.Add("@idCategoria", 2, obj_Beca_DAL.iIdCategoria);
                dtParametros.Rows.Add("@idSubcategoria", 2, obj_Beca_DAL.iIdSubcategoria);
                dtParametros.Rows.Add("@idEstado", 2, obj_Beca_DAL.iIdEstado);
                


            }
            obj_BD_BLL.ExecuteNonQuery("SP_Becas_Insertar", dtParametros, ref obj_BD_DAL);//Si fuera identity uso scalar

            if (obj_BD_DAL.sMsjError != string.Empty)
            {
                sMsjError = string.Empty;
                obj_Beca_DAL.cBandAxion = 'I';
                obj_Beca_DAL.cBandEstAxion = 'F';
            }
            else
            {
                sMsjError = string.Empty;
                obj_Beca_DAL.cBandAxion = 'U';
                obj_Beca_DAL.cBandEstAxion = 'E';
            }
        }

        #endregion

        #region Modificar

        public void Modificar_Beca(ref cls_Beca_DAL obj_Beca_DAL, ref string sMsjError)
        {
            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();

            if (dtParametros.Columns.Count == 3)
            {

                dtParametros.Rows.Add("@idBeca", 2, obj_Beca_DAL.iIdBeca);
                dtParametros.Rows.Add("@porcentajeDescuento", 6, obj_Beca_DAL.dPorcentajeDescuento);
                dtParametros.Rows.Add("@nombre", 4, obj_Beca_DAL.sNombre);
                dtParametros.Rows.Add("@notaMinima", 6, obj_Beca_DAL.dNotaMinima);
                dtParametros.Rows.Add("@idPatrocinador", 2, obj_Beca_DAL.iIdPatrocinador);
                dtParametros.Rows.Add("@idCategoria", 2, obj_Beca_DAL.iIdCategoria);
                dtParametros.Rows.Add("@idSubcategoria", 2, obj_Beca_DAL.iIdSubcategoria);
                dtParametros.Rows.Add("@idEstado", 2, obj_Beca_DAL.iIdEstado);
            }
            obj_BD_BLL.ExecuteNonQuery("SP_Becas_Modificar", dtParametros, ref obj_BD_DAL);

            if (obj_BD_DAL.sMsjError != string.Empty)
            {
                sMsjError = obj_BD_DAL.sMsjError;
                obj_Beca_DAL.cBandAxion = 'U';
                obj_Beca_DAL.cBandEstAxion = 'F';
            }
            else
            {
                sMsjError = string.Empty;
                obj_Beca_DAL.cBandAxion = 'U';
                obj_Beca_DAL.cBandEstAxion = 'E';
                //obj_Estudiante_DAL.iIdEstudiante = Convert.ToInt32(obj_BD_DAL.sResultadoID);
            }
        }


        #endregion

    
        #endregion

    }
}
