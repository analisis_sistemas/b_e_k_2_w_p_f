﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DAL_B_I_T_S.BD;
using BLL_B_I_T_S.BD;
using BLL_B_I_T_S.Logica_General;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;

namespace BLL_B_I_T_S.Catalogos_y_Mantenimiento
{
   public class cls_Empleados_BLL
    {
        #region Variables Globales
        cls_BD_DAL obj_BD_DAL = new cls_BD_DAL();
        cls_BD_BLL obj_BD_BLL = new cls_BD_BLL();
        cls_Parametros_BLL obj_Parametros_BLL = new cls_Parametros_BLL();
        #endregion

        #region Métodos
        #region Listar
        public void Listar_Empleados(ref DataTable dtEmpleados, ref string sMsjError)
        {
            #region Variables Publicas

            obj_BD_DAL = new cls_BD_DAL();
            obj_BD_BLL = new cls_BD_BLL();
            DataTable dtParametros = new DataTable();

            #endregion

            obj_BD_BLL.Excecute_Fill("SP_Empleados_Listar", "TBL_EMPLEADOS", dtParametros, ref obj_BD_DAL);

            if (obj_BD_DAL.sMsjError == String.Empty)
            {
                sMsjError = string.Empty;
                dtEmpleados = obj_BD_DAL.DS.Tables[0];

            }
            else
            {
                sMsjError = obj_BD_DAL.sMsjError;
                dtEmpleados = new DataTable();
            }

        }
        #endregion

        #region Filtrar
        //public void Filtrar_Estudiante(ref DataTable dtEstudiante, string sFiltro, ref string sMsjError)
        public void Filtrar_Empleados(ref DataTable dtEmpleados, int iFiltro, ref string sMsjError)
        {
            #region Variables Publicas

            obj_BD_DAL = new cls_BD_DAL();
            obj_BD_BLL = new cls_BD_BLL();

            #endregion

            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();

            if (dtParametros.Columns.Count == 3)
            {
                dtParametros.Rows.Add("@idEmpleado", 2, iFiltro);//Nombre id a filtrar,tipo de dato y donde capturo el id
                obj_BD_BLL.Excecute_Fill("SP_Empleados_Filtrar", "TBL_EMPLEADOS", dtParametros, ref obj_BD_DAL);

            }
            if (obj_BD_DAL.sMsjError == string.Empty)
            {
                sMsjError = string.Empty;
                dtEmpleados = obj_BD_DAL.DS.Tables[0];


            }
            else
            {
                sMsjError = obj_BD_DAL.sMsjError;
                dtEmpleados = new DataTable();
            }
        }
        #endregion

        #region Eliminar
       // empleado no se debe eliminar por regla de negocio solo puede cambiar de estado
        //public void Eliminar_Empleados(string sId, ref string sMsjError)
        //{
        //    DataTable dtParametros = obj_Parametros_BLL.TablaParametros();

        //    if (dtParametros.Columns.Count == 3)
        //    {

        //        dtParametros.Rows.Add("@idEmpleado", 2, sId);
        //        obj_BD_BLL.ExecuteNonQuery("SP_Empleados_Eliminar", dtParametros, ref obj_BD_DAL);


        //        if (obj_BD_DAL.sMsjError == string.Empty)
        //        {
        //            sMsjError = string.Empty;
        //        }
        //        else
        //        {
        //            sMsjError = obj_BD_DAL.sMsjError;

        //        }
        //    }

        //    else
        //    {
        //        sMsjError = " Se presento un error al crear parametros";

        //    }

        //}
        #endregion

        #region Crear
        public void Crear_Empleados(ref cls_Empleados_DAL obj_Empleados_DAL, ref string sMsjError)
        {
            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();
            if (dtParametros.Columns.Count == 3)
            {
                dtParametros.Rows.Add("@idEmpleado", 2, obj_Empleados_DAL.iIdEmpleado);
                dtParametros.Rows.Add("@cedula", 5, obj_Empleados_DAL.sCedula);
                dtParametros.Rows.Add("@nombre", 5, obj_Empleados_DAL.sNombre);
                dtParametros.Rows.Add("@primerApellido", 5, obj_Empleados_DAL.sPrimerApellido);
                dtParametros.Rows.Add("@segundoApellido", 5, obj_Empleados_DAL.sSegundoApellido);
                dtParametros.Rows.Add("@idUsuario", 5, obj_Empleados_DAL.sIdUsuario);
                dtParametros.Rows.Add("@correoElectronico", 5, obj_Empleados_DAL.sCorreoElectronico);
                dtParametros.Rows.Add("@fechaNacimiento", 7, obj_Empleados_DAL.dtFechaNacimiento);
                dtParametros.Rows.Add("@idCampus", 2, obj_Empleados_DAL.iIdCampus);



            }
            obj_BD_BLL.ExecuteNonQuery("SP_Empleados_Insertar", dtParametros, ref obj_BD_DAL);

            if (obj_BD_DAL.sMsjError != string.Empty)
            {
                sMsjError = string.Empty;
                obj_Empleados_DAL.cBandAxion = 'I';
                obj_Empleados_DAL.cBandEstAxion = 'F';
            }
            else
            {
                sMsjError = string.Empty;
                obj_Empleados_DAL.cBandAxion = 'U';
                obj_Empleados_DAL.cBandEstAxion = 'E';
            }
        }

        #endregion

        #region Modificar

        public void Modificar_Empleados(ref cls_Empleados_DAL obj_Empleados_DAL, ref string sMsjError)
        {
            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();

            if (dtParametros.Columns.Count == 3)
            {

                dtParametros.Rows.Add("@idEmpleado", 2, obj_Empleados_DAL.iIdEmpleado);
                dtParametros.Rows.Add("@cedula", 5, obj_Empleados_DAL.sCedula);
                dtParametros.Rows.Add("@nombre", 5, obj_Empleados_DAL.sNombre);
                dtParametros.Rows.Add("@primerApellido", 5, obj_Empleados_DAL.sPrimerApellido);
                dtParametros.Rows.Add("@segundoApellido", 5, obj_Empleados_DAL.sSegundoApellido);
                dtParametros.Rows.Add("@idUsuario", 5, obj_Empleados_DAL.sIdUsuario);
                dtParametros.Rows.Add("@correoElectronico", 5, obj_Empleados_DAL.sCorreoElectronico);
                dtParametros.Rows.Add("@fechaNacimiento", 7, obj_Empleados_DAL.dtFechaNacimiento);
                dtParametros.Rows.Add("@idCampus", 2, obj_Empleados_DAL.iIdCampus);
            }
            obj_BD_BLL.ExecuteNonQuery("SP_Empleados_Modificar", dtParametros, ref obj_BD_DAL);

            if (obj_BD_DAL.sMsjError != string.Empty)
            {
                sMsjError = obj_BD_DAL.sMsjError;
                obj_Empleados_DAL.cBandAxion = 'U';
                obj_Empleados_DAL.cBandEstAxion = 'F';
            }
            else
            {
                sMsjError = string.Empty;
                obj_Empleados_DAL.cBandAxion = 'U';
                obj_Empleados_DAL.cBandEstAxion = 'E';
                //obj_Estudiante_DAL.iIdEstudiante = Convert.ToInt32(obj_BD_DAL.sResultadoID);
            }
        }


        #endregion
        #endregion

    }
}
