﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DAL_B_I_T_S.BD;
using BLL_B_I_T_S.BD;
using BLL_B_I_T_S.Logica_General;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;

namespace BLL_B_I_T_S.Catalogos_y_Mantenimiento
{
   public class cls_Respuestas_BLL
    {
        #region Variables Globales
        cls_BD_DAL obj_BD_DAL = new cls_BD_DAL();
        cls_BD_BLL obj_BD_BLL = new cls_BD_BLL();
        cls_Parametros_BLL obj_Parametros_BLL = new cls_Parametros_BLL();
        int j = 0;
        #endregion

        #region Métodos
        #region Listar
        public void Listar_Respuestas(ref DataTable dtRespuestas, ref string sMsjError)
        {
            #region Variables Publicas

            obj_BD_DAL = new cls_BD_DAL();
            obj_BD_BLL = new cls_BD_BLL();
            DataTable dtParametros = new DataTable();

            #endregion

            obj_BD_BLL.Excecute_Fill("SP_Respuestas_Listar", "TBL_RESPUESTAS", dtParametros, ref obj_BD_DAL);

            if (obj_BD_DAL.sMsjError == String.Empty)
            {
                sMsjError = string.Empty;
                dtRespuestas = obj_BD_DAL.DS.Tables[0];

            }
            else
            {
                sMsjError = obj_BD_DAL.sMsjError;
                dtRespuestas = new DataTable();
            }

        }
        #endregion

        #region Filtrar
        //public void Filtrar_Estudiante(ref DataTable dtEstudiante, string sFiltro, ref string sMsjError)
        public void Filtrar_Respuestas(ref DataTable dtRespuestas, int iFiltro, ref string sMsjError)
        {
            #region Variables Publicas

            obj_BD_DAL = new cls_BD_DAL();
            obj_BD_BLL = new cls_BD_BLL();

            #endregion

            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();

            if (dtParametros.Columns.Count == 3)
            {
                dtParametros.Rows.Add("@idRespuesta", 2, iFiltro);//Nombre id a filtrar,tipo de dato y donde capturo el id
                obj_BD_BLL.Excecute_Fill("SP_Respuestas_Filtrar", "TBL_RESPUESTAS", dtParametros, ref obj_BD_DAL);

            }
            if (obj_BD_DAL.sMsjError == string.Empty)
            {
                sMsjError = string.Empty;
                dtRespuestas = obj_BD_DAL.DS.Tables[0];


            }
            else
            {
                sMsjError = obj_BD_DAL.sMsjError;
                dtRespuestas = new DataTable();
            }
        }
        #endregion

        #region Eliminar

        public void Eliminar_Respuestas(string sId, ref string sMsjError)
        {
            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();

            if (dtParametros.Columns.Count == 3)
            {

                dtParametros.Rows.Add("@idRespuesta", 2, sId);
                obj_BD_BLL.ExecuteNonQuery("SP_Respuestas_Eliminar", dtParametros, ref obj_BD_DAL);


                if (obj_BD_DAL.sMsjError == string.Empty)
                {
                    sMsjError = string.Empty;
                }
                else
                {
                    sMsjError = obj_BD_DAL.sMsjError;

                }
            }

            else
            {
                sMsjError = " Se presento un error al crear parametros";

            }

        }
        #endregion

        #region Crear
        public void Crear_Respuestas(ref cls_Respuestas_DAL obj_Respuestas_DAL, ref string sMsjError)
        {
            
            for (int i = 0; i < obj_Respuestas_DAL.ls_Lista.Count; i++)
            {
            
                DataTable dtParametros = obj_Parametros_BLL.TablaParametros();
            if (dtParametros.Columns.Count == 3)
            {
                    dtParametros.Rows.Add("@respuesta", 5, obj_Respuestas_DAL.ls_Lista[j]);
                    dtParametros.Rows.Add("@idUsuario", 5, obj_Respuestas_DAL.sIdUsuario);
                    dtParametros.Rows.Add("@idPregunta", 2, obj_Respuestas_DAL.iIdPregunta);
            }

            string id = obj_BD_BLL.Excecute_Scalar("SP_Respuestas_Insertar", dtParametros, ref obj_BD_DAL);
            j += 1;
            obj_Respuestas_DAL.iIdPregunta += 1;
            }

            if (obj_BD_DAL.sMsjError != string.Empty)
            {
                sMsjError = string.Empty;
                obj_Respuestas_DAL.cBandAxion = 'I';
                obj_Respuestas_DAL.cBandEstAxion = 'F';
            }
            else
            {
                sMsjError = string.Empty;
                obj_Respuestas_DAL.cBandAxion = 'U';
                obj_Respuestas_DAL.cBandEstAxion = 'E';
            }
        }

        #endregion

        #region Modificar

        public void Modificar_Encargados(ref cls_Respuestas_DAL obj_Respuestas_DAL, ref string sMsjError)
        {
            //for (int i = 0; i < obj_Respuestas_DAL.ls_Lista.Count; i++)
            //{

            //    DataTable dtParametros = obj_Parametros_BLL.TablaParametros();
            //    if (dtParametros.Columns.Count == 3)
            //    {
            //        dtParametros.Rows.Add("@idRespuesta", 2, obj_Respuestas_DAL.iIdRespuesta);
            //        dtParametros.Rows.Add("@idtipoPregunta", 2, obj_Respuestas_DAL.iIdtipoPregunta);
            //        dtParametros.Rows.Add("@respuesta", 5, obj_Respuestas_DAL.sRespuesta);
            //        dtParametros.Rows.Add("@idUsuario", 5, obj_Respuestas_DAL.sIdUsuario);
            //        dtParametros.Rows.Add("@idPregunta", 2, obj_Respuestas_DAL.iIdPregunta);




            //        dtParametros.Rows.Add("@respuesta", 5, obj_Respuestas_DAL.ls_Lista[j]);
            //        dtParametros.Rows.Add("@idUsuario", 5, obj_Respuestas_DAL.sIdUsuario);
            //        dtParametros.Rows.Add("@idPregunta", 2, obj_Respuestas_DAL.iIdPregunta);
            //    }

            //    string id = obj_BD_BLL.Excecute_Scalar("SP_Respuestas_Insertar", dtParametros, ref obj_BD_DAL);
            //    j += 1;
            //    obj_Respuestas_DAL.iIdPregunta += 1;
            //}
            //obj_BD_BLL.ExecuteNonQuery("SP_Respuestas_Modificar", dtParametros, ref obj_BD_DAL);

            //if (obj_BD_DAL.sMsjError != string.Empty)
            //{
            //    sMsjError = obj_BD_DAL.sMsjError;
            //    obj_Respuestas_DAL.cBandAxion = 'U';
            //    obj_Respuestas_DAL.cBandEstAxion = 'F';
            //}
            //else
            //{
            //    sMsjError = string.Empty;
            //    obj_Respuestas_DAL.cBandAxion = 'U';
            //    obj_Respuestas_DAL.cBandEstAxion = 'E';
            //    obj_Estudiante_DAL.iIdEstudiante = Convert.ToInt32(obj_BD_DAL.sResultadoID);
            //}
        }


        #endregion
        #endregion
    }
}
