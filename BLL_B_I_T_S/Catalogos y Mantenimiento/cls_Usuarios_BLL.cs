﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL_B_I_T_S.BD;
using BLL_B_I_T_S.BD;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;
using BLL_B_I_T_S.Logica_General;
using System.Data;

namespace BLL_B_I_T_S.Catalogos_y_Mantenimiento
{
  public  class cls_Usuarios_BLL
  {
      #region Variables Globales
      cls_BD_DAL obj_BD_DAL = new cls_BD_DAL();
      cls_BD_BLL obj_BD_BLL = new cls_BD_BLL();
      cls_Parametros_BLL obj_Parametros_BLL = new cls_Parametros_BLL();
      #endregion

      #region Métodos
      #region Listar
      public void Listar_Usuario(ref DataTable dtUsuario, ref string sMsjError)
        {
            #region Variables Publicas

            obj_BD_DAL = new cls_BD_DAL();
            obj_BD_BLL = new cls_BD_BLL();
            DataTable dtParametros = new DataTable();

            #endregion

            obj_BD_BLL.Excecute_Fill("SP_Usuarios_Listar", "TBL_USUARIOS", dtParametros, ref obj_BD_DAL);

            if (obj_BD_DAL.sMsjError == String.Empty)
            {
                sMsjError = string.Empty;
                dtUsuario = obj_BD_DAL.DS.Tables[0];

            }
            else
            {
                sMsjError = obj_BD_DAL.sMsjError;
                dtUsuario = new DataTable();
            }

        }
        #endregion

        #region Filtrar
       
        public void Filtrar_Usuario(ref DataTable dtUsuario, int sFiltro, ref string sMsjError)
        {
            #region Variables Publicas

            obj_BD_DAL = new cls_BD_DAL();
            obj_BD_BLL = new cls_BD_BLL();

            #endregion

            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();

            if (dtParametros.Columns.Count == 3)
            {
                dtParametros.Rows.Add("@idUsuario", 4, sFiltro);
                obj_BD_BLL.Excecute_Fill("SP_Usuarios_Filtrar_X_ID", "TBL_USUARIOS", dtParametros, ref obj_BD_DAL);

            }
            if (obj_BD_DAL.sMsjError == string.Empty)
            {
                sMsjError = string.Empty;
                dtUsuario = obj_BD_DAL.DS.Tables[0];


            }
            else
            {
                sMsjError = obj_BD_DAL.sMsjError;
                dtUsuario = new DataTable();
            }
        }
        #endregion



        #region Crear
        public void Crear_Usuarios(ref cls_Usuarios_DAL obj_Usuarios_DAL, ref string sMsjError)
        {
            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();
            if (dtParametros.Columns.Count == 3)
            {
                dtParametros.Rows.Add("@idUsuario", 4, obj_Usuarios_DAL.sIdUsuario);
                dtParametros.Rows.Add("@nombre", 4, obj_Usuarios_DAL.sNombre);
                dtParametros.Rows.Add("@password", 4, obj_Usuarios_DAL.sPassword);
                dtParametros.Rows.Add("@idRol", 2, obj_Usuarios_DAL.iIdRol);
                dtParametros.Rows.Add("@idEstado",2, obj_Usuarios_DAL.iIdEstado);
               


            }
            obj_BD_BLL.ExecuteNonQuery("SP_Usuarios_Insertar", dtParametros, ref obj_BD_DAL);

            if (obj_BD_DAL.sMsjError != string.Empty)
            {
                sMsjError = string.Empty;
                obj_Usuarios_DAL.cBandAxion = 'I';
                obj_Usuarios_DAL.cBandEstAxion = 'F';
            }
            else
            {
                sMsjError = string.Empty;
                obj_Usuarios_DAL.cBandAxion = 'U';
                obj_Usuarios_DAL.cBandEstAxion = 'E';
            }
        }

        #endregion

        #region Modificar

        public void Modificar_Usuarios(ref cls_Usuarios_DAL obj_Usuarios_DAL, ref string sMsjError)
        {
            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();

            if (dtParametros.Columns.Count == 3)
            {

                dtParametros.Rows.Add("@idUsuario", 4, obj_Usuarios_DAL.sIdUsuario);
                dtParametros.Rows.Add("@nombre", 4, obj_Usuarios_DAL.sNombre);
                dtParametros.Rows.Add("@password", 4, obj_Usuarios_DAL.sPassword);
                dtParametros.Rows.Add("@idRol", 2, obj_Usuarios_DAL.iIdRol);
                dtParametros.Rows.Add("@idEstado", 2, obj_Usuarios_DAL.iIdEstado);
            }
            obj_BD_BLL.ExecuteNonQuery("SP_Usuarios_Modificar", dtParametros, ref obj_BD_DAL);

            if (obj_BD_DAL.sMsjError != string.Empty)
            {
                sMsjError = obj_BD_DAL.sMsjError;
                obj_Usuarios_DAL.cBandAxion = 'U';
                obj_Usuarios_DAL.cBandEstAxion = 'F';
            }
            else
            {
                sMsjError = string.Empty;
                obj_Usuarios_DAL.cBandAxion = 'U';
                obj_Usuarios_DAL.cBandEstAxion = 'E';
                //obj_Estudiante_DAL.iIdEstudiante = Convert.ToInt32(obj_BD_DAL.sResultadoID);
            }
        }


        #endregion
        #endregion
    }
}
