﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DAL_B_I_T_S.BD;
using BLL_B_I_T_S.BD;
using BLL_B_I_T_S.Logica_General;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;

namespace BLL_B_I_T_S.Catalogos_y_Mantenimiento
{
    public class cls_Estado_Expediente_BLL
    {
        #region Variables Globales
        cls_BD_DAL obj_BD_DAL = new cls_BD_DAL();
        cls_BD_BLL obj_BD_BLL = new cls_BD_BLL();
        cls_Parametros_BLL obj_Parametros_BLL = new cls_Parametros_BLL();
        #endregion

        #region Métodos

        #region Listar
        public void Listar_Estado_Expediente(ref DataTable dtEstadoExpediente, ref string sMsjError)
        {
            #region Variables Publicas

            obj_BD_DAL = new cls_BD_DAL();
            obj_BD_BLL = new cls_BD_BLL();
            DataTable dtParametros = new DataTable();

            #endregion

            obj_BD_BLL.Excecute_Fill("SP_Estado_Expediente_Listar", "TBL_ESTADOEXPEDIENTE", dtParametros, ref obj_BD_DAL);

            if (obj_BD_DAL.sMsjError == String.Empty)
            {
                sMsjError = string.Empty;
                dtEstadoExpediente = obj_BD_DAL.DS.Tables[0];

            }
            else
            {
                sMsjError = obj_BD_DAL.sMsjError;
                dtEstadoExpediente = new DataTable();
            }

        }
        #endregion





        #region Crear
        public void Crear_EstadoExpediente(ref cls_Estado_Expediente_DAL obj_Estado_Expediente_DAL, ref string sMsjError)
        {
            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();
            if (dtParametros.Columns.Count == 3)
            {
                dtParametros.Rows.Add("@idEstado", 2, obj_Estado_Expediente_DAL.iIdEstado);
                dtParametros.Rows.Add("@nombre", 4, obj_Estado_Expediente_DAL.sNombre);
                


            }
            obj_BD_BLL.ExecuteNonQuery("SP_Estado_Expediente_Insertar", dtParametros, ref obj_BD_DAL);//Si fuera identity uso scalar

            if (obj_BD_DAL.sMsjError != string.Empty)
            {
                sMsjError = string.Empty;
                obj_Estado_Expediente_DAL.cBandAxion = 'I';
                obj_Estado_Expediente_DAL.cBandEstAxion = 'F';
            }
            else
            {
                sMsjError = string.Empty;
                obj_Estado_Expediente_DAL.cBandAxion = 'U';
                obj_Estado_Expediente_DAL.cBandEstAxion = 'E';
            }
        }

        #endregion

        #region Modificar

        public void Modificar_Estado_Expediente(ref cls_Estado_Expediente_DAL obj_Estado_Expediente_DAL, ref string sMsjError)
        {
            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();

            if (dtParametros.Columns.Count == 3)
            {

                dtParametros.Rows.Add("@idEstado", 2, obj_Estado_Expediente_DAL.iIdEstado);
                dtParametros.Rows.Add("@nombre", 4, obj_Estado_Expediente_DAL.sNombre);
            }
            obj_BD_BLL.ExecuteNonQuery("SP_Estado_Expediente_Modificar", dtParametros, ref obj_BD_DAL);

            if (obj_BD_DAL.sMsjError != string.Empty)
            {
                sMsjError = obj_BD_DAL.sMsjError;
                obj_Estado_Expediente_DAL.cBandAxion = 'U';
                obj_Estado_Expediente_DAL.cBandEstAxion = 'F';
            }
            else
            {
                sMsjError = string.Empty;
                obj_Estado_Expediente_DAL.cBandAxion = 'U';
                obj_Estado_Expediente_DAL.cBandEstAxion = 'E';
                //obj_Estudiante_DAL.iIdEstudiante = Convert.ToInt32(obj_BD_DAL.sResultadoID);
            }
        }


        #endregion
        #endregion
    }
}
