﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DAL_B_I_T_S.BD;
using BLL_B_I_T_S.BD;
using BLL_B_I_T_S.Logica_General;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;

namespace BLL_B_I_T_S.Catalogos_y_Mantenimiento
{
   public class cls_Periodo_BLL
    {
        #region Variables Globales
        cls_BD_DAL obj_BD_DAL = new cls_BD_DAL();
        cls_BD_BLL obj_BD_BLL = new cls_BD_BLL();
        cls_Parametros_BLL obj_Parametros_BLL = new cls_Parametros_BLL();
        #endregion

        #region Métodos
        #region Listar
        public void Listar_Periodo(ref DataTable dtPeriodo, ref string sMsjError)
        {
            #region Variables Publicas

            obj_BD_DAL = new cls_BD_DAL();
            obj_BD_BLL = new cls_BD_BLL();
            DataTable dtParametros = new DataTable();

            #endregion

            obj_BD_BLL.Excecute_Fill("SP_Periodo_Listar", "TBL_PERIODO", dtParametros, ref obj_BD_DAL);

            if (obj_BD_DAL.sMsjError == String.Empty)
            {
                sMsjError = string.Empty;
                dtPeriodo = obj_BD_DAL.DS.Tables[0];

            }
            else
            {
                sMsjError = obj_BD_DAL.sMsjError;
                dtPeriodo = new DataTable();
            }

        }
        #endregion

        #region Filtrar
        //public void Filtrar_Estudiante(ref DataTable dtEstudiante, string sFiltro, ref string sMsjError)
        public void Filtrar_Periodo(ref DataTable dtPeriodo, int iFiltro, ref string sMsjError)
        {
            #region Variables Publicas

            obj_BD_DAL = new cls_BD_DAL();
            obj_BD_BLL = new cls_BD_BLL();

            #endregion

            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();

            if (dtParametros.Columns.Count == 3)
            {
                dtParametros.Rows.Add("@idPeriodo", 5, iFiltro);//Nombre id a filtrar,tipo de dato y donde capturo el id
                obj_BD_BLL.Excecute_Fill("SP_Periodo_Filtrar_X_ID", "TBL_PERIODO", dtParametros, ref obj_BD_DAL);

            }
            if (obj_BD_DAL.sMsjError == string.Empty)
            {
                sMsjError = string.Empty;
                dtPeriodo = obj_BD_DAL.DS.Tables[0];


            }
            else
            {
                sMsjError = obj_BD_DAL.sMsjError;
                dtPeriodo = new DataTable();
            }
        }
        #endregion

        #region Eliminar

        public void Eliminar_Periodo(string sId, ref string sMsjError)
        {
            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();

            if (dtParametros.Columns.Count == 3)
            {

                dtParametros.Rows.Add("@idPeriodo", 2, sId);
                obj_BD_BLL.ExecuteNonQuery("SP_Periodo_Eliminar", dtParametros, ref obj_BD_DAL);


                if (obj_BD_DAL.sMsjError == string.Empty)
                {
                    sMsjError = string.Empty;
                }
                else
                {
                    sMsjError = obj_BD_DAL.sMsjError;

                }
            }

            else
            {
                sMsjError = " Se presento un error al crear parametros";

            }

        }
        #endregion

        #region Crear
        public void Crear_Periodo(ref cls_Periodo_DAL obj_Periodo_DAL, ref string sMsjError)
        {
            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();
            if (dtParametros.Columns.Count == 3)
            {
                dtParametros.Rows.Add("@idPeriodo", 2, obj_Periodo_DAL.iIdPeriodo);
                dtParametros.Rows.Add("@nombre", 5, obj_Periodo_DAL.sNombre);
               



            }
            obj_BD_BLL.Excecute_Scalar("SP_Periodo_Insertar", dtParametros, ref obj_BD_DAL);

            if (obj_BD_DAL.sMsjError != string.Empty)
            {
                sMsjError = string.Empty;
                obj_Periodo_DAL.cBandAxion = 'I';
                obj_Periodo_DAL.cBandEstAxion = 'F';
            }
            else
            {
                sMsjError = string.Empty;
                obj_Periodo_DAL.cBandAxion = 'U';
                obj_Periodo_DAL.cBandEstAxion = 'E';
            }
        }

        #endregion

        #region Modificar

        public void Modificar_Periodo(ref cls_Periodo_DAL obj_Periodo_DAL, ref string sMsjError)
        {
            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();

            if (dtParametros.Columns.Count == 3)
            {

                dtParametros.Rows.Add("@idPeriodo", 2, obj_Periodo_DAL.iIdPeriodo);
                dtParametros.Rows.Add("@nombre", 5, obj_Periodo_DAL.sNombre);

            }
            obj_BD_BLL.ExecuteNonQuery("SP_Periodo_Modificar", dtParametros, ref obj_BD_DAL);

            if (obj_BD_DAL.sMsjError != string.Empty)
            {
                sMsjError = obj_BD_DAL.sMsjError;
                obj_Periodo_DAL.cBandAxion = 'U';
                obj_Periodo_DAL.cBandEstAxion = 'F';
            }
            else
            {
                sMsjError = string.Empty;
                obj_Periodo_DAL.cBandAxion = 'U';
                obj_Periodo_DAL.cBandEstAxion = 'E';
                //obj_Estudiante_DAL.iIdEstudiante = Convert.ToInt32(obj_BD_DAL.sResultadoID);
            }
        }


        #endregion
        #endregion
    }
}
