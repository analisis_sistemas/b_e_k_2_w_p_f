﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DAL_B_I_T_S.BD;
using BLL_B_I_T_S.BD;
using BLL_B_I_T_S.Logica_General;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;

namespace BLL_B_I_T_S.Catalogos_y_Mantenimiento
{
   public class cls_Cursos_BLL
    {
        #region Variables Globales
        cls_BD_DAL obj_BD_DAL = new cls_BD_DAL();
        cls_BD_BLL obj_BD_BLL = new cls_BD_BLL();
        cls_Parametros_BLL obj_Parametros_BLL = new cls_Parametros_BLL();
        #endregion

        #region Métodos
        #region Listar
        public void Listar_Cursos(ref DataTable dtCursos, ref string sMsjError)
        {
            #region Variables Publicas

            obj_BD_DAL = new cls_BD_DAL();
            obj_BD_BLL = new cls_BD_BLL();
            DataTable dtParametros = new DataTable();

            #endregion

            obj_BD_BLL.Excecute_Fill("SP_Cursos_Listar", "TBL_CURSOS", dtParametros, ref obj_BD_DAL);

            if (obj_BD_DAL.sMsjError == String.Empty)
            {
                sMsjError = string.Empty;
                dtCursos = obj_BD_DAL.DS.Tables[0];

            }
            else
            {
                sMsjError = obj_BD_DAL.sMsjError;
                dtCursos = new DataTable();
            }

        }
        #endregion

        #region Filtrar
        //public void Filtrar_Estudiante(ref DataTable dtEstudiante, string sFiltro, ref string sMsjError)
        public void Filtrar_Cursos(ref DataTable dtCursos, int iFiltro, ref string sMsjError)
        {
            #region Variables Publicas

            obj_BD_DAL = new cls_BD_DAL();
            obj_BD_BLL = new cls_BD_BLL();

            #endregion

            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();

            if (dtParametros.Columns.Count == 3)
            {
                dtParametros.Rows.Add("@idEstudiante", 2, iFiltro);//Nombre id a filtrar,tipo de dato y donde capturo el id
                obj_BD_BLL.Excecute_Fill("SP_Cursos_Filtrar", "TBL_CURSOS", dtParametros, ref obj_BD_DAL);

            }
            if (obj_BD_DAL.sMsjError == string.Empty)
            {
                sMsjError = string.Empty;
                dtCursos = obj_BD_DAL.DS.Tables[0];


            }
            else
            {
                sMsjError = obj_BD_DAL.sMsjError;
                dtCursos = new DataTable();
            }
        }
        #endregion

        #region Eliminar

        public void Eliminar_Cursos(string sId, ref string sMsjError)
        {
            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();

            if (dtParametros.Columns.Count == 3)
            {

                dtParametros.Rows.Add("@idCurso", 2, sId);
                obj_BD_BLL.ExecuteNonQuery("SP_Cursos_Eliminar", dtParametros, ref obj_BD_DAL);


                if (obj_BD_DAL.sMsjError == string.Empty)
                {
                    sMsjError = string.Empty;
                }
                else
                {
                    sMsjError = obj_BD_DAL.sMsjError;

                }
            }

            else
            {
                sMsjError = " Se presento un error al crear parametros";

            }

        }
        #endregion

        #region Crear
        public void Crear_Cursos(ref cls_Cursos_DAL obj_Cursos_DAL, ref string sMsjError)
        {
            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();
            if (dtParametros.Columns.Count == 3)
            {
                dtParametros.Rows.Add("@nombre", 5, obj_Cursos_DAL.sNombre);
                dtParametros.Rows.Add("@idEstudiante", 2, obj_Cursos_DAL.iId_Estudiante);
                dtParametros.Rows.Add("@idEstado", 2, obj_Cursos_DAL.iIdEstado);
                dtParametros.Rows.Add("@nota", 2, obj_Cursos_DAL.dNota);

            }
            obj_BD_BLL.Excecute_Scalar("SP_Cursos_Insertar", dtParametros, ref obj_BD_DAL);

            if (obj_BD_DAL.sMsjError != string.Empty)
            {
                sMsjError = string.Empty;
                obj_Cursos_DAL.cBandAxion = 'I';
                obj_Cursos_DAL.cBandEstAxion = 'F';
            }
            else
            {
                sMsjError = string.Empty;
                obj_Cursos_DAL.cBandAxion = 'U';
                obj_Cursos_DAL.cBandEstAxion = 'E';
            }
        }

        #endregion

        #region Modificar

        public void Modificar_Activos(ref cls_Cursos_DAL obj_Cursos_DAL, ref string sMsjError)
        {
            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();

            if (dtParametros.Columns.Count == 3)
            {

                dtParametros.Rows.Add("@nombre", 5, obj_Cursos_DAL.sNombre);
                dtParametros.Rows.Add("@idEstudiante", 2, obj_Cursos_DAL.iId_Estudiante);
                dtParametros.Rows.Add("@idEstado", 2, obj_Cursos_DAL.iIdEstado);
                dtParametros.Rows.Add("@nota", 2, obj_Cursos_DAL.dNota);


            }
            obj_BD_BLL.ExecuteNonQuery("SP_Cursos_Modificar", dtParametros, ref obj_BD_DAL);

            if (obj_BD_DAL.sMsjError != string.Empty)
            {
                sMsjError = obj_BD_DAL.sMsjError;
                obj_Cursos_DAL.cBandAxion = 'U';
                obj_Cursos_DAL.cBandEstAxion = 'F';
            }
            else
            {
                sMsjError = string.Empty;
                obj_Cursos_DAL.cBandAxion = 'U';
                obj_Cursos_DAL.cBandEstAxion = 'E';
                //obj_Estudiante_DAL.iIdEstudiante = Convert.ToInt32(obj_BD_DAL.sResultadoID);
            }
        }


        #endregion
        #endregion
    }
}
