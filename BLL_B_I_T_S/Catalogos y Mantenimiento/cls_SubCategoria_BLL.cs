﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DAL_B_I_T_S.BD;
using BLL_B_I_T_S.BD;
using BLL_B_I_T_S.Logica_General;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;


namespace BLL_B_I_T_S.Catalogos_y_Mantenimiento
{
   public class cls_SubCategoria_BLL
    {
        #region Variables Globales
        cls_BD_DAL obj_BD_DAL = new cls_BD_DAL();
        cls_BD_BLL obj_BD_BLL = new cls_BD_BLL();
        cls_Parametros_BLL obj_Parametros_BLL = new cls_Parametros_BLL();
        #endregion

        #region Métodos
        #region Listar
        public void Listar_SubCategoria(ref DataTable dtSubCategoria, ref string sMsjError)
        {
            #region Variables Publicas

            obj_BD_DAL = new cls_BD_DAL();
            obj_BD_BLL = new cls_BD_BLL();
            DataTable dtParametros = new DataTable();

            #endregion

            obj_BD_BLL.Excecute_Fill("SP_SubCategorias_Listar", "TBL_SUBCATEGORIA", dtParametros, ref obj_BD_DAL);

            if (obj_BD_DAL.sMsjError == String.Empty)
            {
                sMsjError = string.Empty;
                dtSubCategoria = obj_BD_DAL.DS.Tables[0];

            }
            else
            {
                sMsjError = obj_BD_DAL.sMsjError;
                dtSubCategoria = new DataTable();
            }

        }
        #endregion

        #region Filtrar
        //public void Filtrar_Estudiante(ref DataTable dtEstudiante, string sFiltro, ref string sMsjError)
        public void Filtrar_SubCategoria(ref DataTable dtSubCategoria, int iFiltro, ref string sMsjError)
        {
            #region Variables Publicas

            obj_BD_DAL = new cls_BD_DAL();
            obj_BD_BLL = new cls_BD_BLL();

            #endregion

            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();

            if (dtParametros.Columns.Count == 3)
            {
                dtParametros.Rows.Add("@idSubcategoria", 5, iFiltro);//Nombre id a filtrar,tipo de dato y donde capturo el id
                obj_BD_BLL.Excecute_Fill("SP_SubCategorias_Filtrar_X_ID", "TBL_SUBCATEGORIA", dtParametros, ref obj_BD_DAL);

            }
            if (obj_BD_DAL.sMsjError == string.Empty)
            {
                sMsjError = string.Empty;
                dtSubCategoria = obj_BD_DAL.DS.Tables[0];


            }
            else
            {
                sMsjError = obj_BD_DAL.sMsjError;
                dtSubCategoria = new DataTable();
            }
        }
        #endregion

        #region Eliminar

        public void Eliminar_SubCategoria(string sId, ref string sMsjError)
        {
            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();

            if (dtParametros.Columns.Count == 3)
            {

                dtParametros.Rows.Add("@idSubCategoria", 2, sId);
                obj_BD_BLL.ExecuteNonQuery("SP_SubCategorias_Eliminar", dtParametros, ref obj_BD_DAL);


                if (obj_BD_DAL.sMsjError == string.Empty)
                {
                    sMsjError = string.Empty;
                }
                else
                {
                    sMsjError = obj_BD_DAL.sMsjError;

                }
            }

            else
            {
                sMsjError = " Se presento un error al crear parametros";

            }

        }
        #endregion

        #region Crear
        public void Crear_SubCategoria(ref cls_Sub_Categoria_DAL obj_SubCategoria_DAL, ref string sMsjError)
        {
            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();
            if (dtParametros.Columns.Count == 3)
            {
                dtParametros.Rows.Add("@idSubCategoria", 2, obj_SubCategoria_DAL.iIdSubcategoria);
                dtParametros.Rows.Add("@nombre", 5, obj_SubCategoria_DAL.sNombre);
                dtParametros.Rows.Add("@porcentaje", 2, obj_SubCategoria_DAL.iPorcentaje);



            }
            obj_BD_BLL.ExecuteNonQuery("SP_SubCategorias_Insertar", dtParametros, ref obj_BD_DAL);

            if (obj_BD_DAL.sMsjError != string.Empty)
            {
                sMsjError = string.Empty;
                obj_SubCategoria_DAL.cBandAxion = 'I';
                obj_SubCategoria_DAL.cBandEstAxion = 'F';
            }
            else
            {
                sMsjError = string.Empty;
                obj_SubCategoria_DAL.cBandAxion = 'U';
                obj_SubCategoria_DAL.cBandEstAxion = 'E';
            }
        }

        #endregion

        #region Modificar

        public void Modificar_SubCategoria(ref cls_Sub_Categoria_DAL obj_SubCategoria_DAL, ref string sMsjError)
        {
            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();

            if (dtParametros.Columns.Count == 3)
            {

                dtParametros.Rows.Add("@idSubCategoria", 2, obj_SubCategoria_DAL.iIdSubcategoria);
                dtParametros.Rows.Add("@nombre", 5, obj_SubCategoria_DAL.sNombre);
                dtParametros.Rows.Add("@porcentaje", 2, obj_SubCategoria_DAL.iPorcentaje);

            }
            obj_BD_BLL.ExecuteNonQuery("SP_SubCategorias_Modificar", dtParametros, ref obj_BD_DAL);

            if (obj_BD_DAL.sMsjError != string.Empty)
            {
                sMsjError = obj_BD_DAL.sMsjError;
                obj_SubCategoria_DAL.cBandAxion = 'U';
                obj_SubCategoria_DAL.cBandEstAxion = 'F';
            }
            else
            {
                sMsjError = string.Empty;
                obj_SubCategoria_DAL.cBandAxion = 'U';
                obj_SubCategoria_DAL.cBandEstAxion = 'E';
                //obj_Estudiante_DAL.iIdEstudiante = Convert.ToInt32(obj_BD_DAL.sResultadoID);
            }
        }


        #endregion
        #endregion
    }
}
