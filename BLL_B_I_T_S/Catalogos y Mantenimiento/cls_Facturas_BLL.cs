﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DAL_B_I_T_S.BD;
using BLL_B_I_T_S.BD;
using BLL_B_I_T_S.Logica_General;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;

namespace BLL_B_I_T_S.Catalogos_y_Mantenimiento
{
   public class cls_Facturas_BLL
    {
        #region Variables Globales
        cls_BD_DAL obj_BD_DAL = new cls_BD_DAL();
        cls_BD_BLL obj_BD_BLL = new cls_BD_BLL();
        cls_Parametros_BLL obj_Parametros_BLL = new cls_Parametros_BLL();
        #endregion

        #region Métodos
        #region Listar
        public void Listar_Facturas(ref DataTable dtFacturas, ref string sMsjError)
        {
            #region Variables Publicas

            obj_BD_DAL = new cls_BD_DAL();
            obj_BD_BLL = new cls_BD_BLL();
            DataTable dtParametros = new DataTable();

            #endregion

            obj_BD_BLL.Excecute_Fill("SP_Facturas_Listar", "TBL_FACTURAS", dtParametros, ref obj_BD_DAL);

            if (obj_BD_DAL.sMsjError == String.Empty)
            {
                sMsjError = string.Empty;
                dtFacturas = obj_BD_DAL.DS.Tables[0];

            }
            else
            {
                sMsjError = obj_BD_DAL.sMsjError;
                dtFacturas = new DataTable();
            }

        }
        #endregion

        #region Filtrar
        //public void Filtrar_Estudiante(ref DataTable dtEstudiante, string sFiltro, ref string sMsjError)
        public void Filtrar_Facturas(ref DataTable dtFacturas, int iFiltro, ref string sMsjError)
        {
            #region Variables Publicas

            obj_BD_DAL = new cls_BD_DAL();
            obj_BD_BLL = new cls_BD_BLL();

            #endregion

            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();

            if (dtParametros.Columns.Count == 3)
            {
                dtParametros.Rows.Add("@idFactura", 2, iFiltro);//Nombre id a filtrar,tipo de dato y donde capturo el id
                obj_BD_BLL.Excecute_Fill("SP_Facturas_Filtrar_X_ID", "TBL_FACTURAS", dtParametros, ref obj_BD_DAL);

            }
            if (obj_BD_DAL.sMsjError == string.Empty)
            {
                sMsjError = string.Empty;
                dtFacturas = obj_BD_DAL.DS.Tables[0];


            }
            else
            {
                sMsjError = obj_BD_DAL.sMsjError;
                dtFacturas = new DataTable();
            }
        }
        #endregion

        #region Eliminar

        public void Eliminar_Facturas(string sId, ref string sMsjError)
        {
            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();

            if (dtParametros.Columns.Count == 3)
            {

                dtParametros.Rows.Add("@idFactura", 2, sId);
                obj_BD_BLL.ExecuteNonQuery("SP_Facturas_Eliminar", dtParametros, ref obj_BD_DAL);


                if (obj_BD_DAL.sMsjError == string.Empty)
                {
                    sMsjError = string.Empty;
                }
                else
                {
                    sMsjError = obj_BD_DAL.sMsjError;

                }
            }

            else
            {
                sMsjError = " Se presento un error al crear parametros";

            }

        }
        #endregion

        #region Crear
        public void Crear_Facturas(ref cls_Facturas_DAL obj_Facturas_DAL, ref string sMsjError)
        {
            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();
            if (dtParametros.Columns.Count == 3)
            {
                dtParametros.Rows.Add("@idFactura", 2, obj_Facturas_DAL.iIdFactura);
                dtParametros.Rows.Add("@cantidadEstudiantes", 2, obj_Facturas_DAL.iCantidadEstudiantes);
              
                dtParametros.Rows.Add("@monto", 6, obj_Facturas_DAL.dMonto);
                dtParametros.Rows.Add("@fechaCobro", 7, obj_Facturas_DAL.dtfechaCobro);
                dtParametros.Rows.Add("@fechaPago", 7, obj_Facturas_DAL.dtfechaPago);
                dtParametros.Rows.Add("@comprobantePago", 2, obj_Facturas_DAL.iComprobantePago);
                dtParametros.Rows.Add("@idEstado", 2, obj_Facturas_DAL.iIdEstado);
                dtParametros.Rows.Add("@idPatrocinador", 2, obj_Facturas_DAL.iIdPatrocinador);
               


            }
            obj_BD_BLL.Excecute_Scalar("SP_Facturas_Insertar", dtParametros, ref obj_BD_DAL);

            if (obj_BD_DAL.sMsjError != string.Empty)
            {
                sMsjError = string.Empty;
                obj_Facturas_DAL.cBandAxion = 'I';
                obj_Facturas_DAL.cBandEstAxion = 'F';
            }
            else
            {
                sMsjError = string.Empty;
                obj_Facturas_DAL.cBandAxion = 'U';
                obj_Facturas_DAL.cBandEstAxion = 'E';
            }
        }

        #endregion

        #region Modificar

        public void ModificarFacturas(ref cls_Facturas_DAL obj_Facturas_DAL, ref string sMsjError)
        {
            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();

            if (dtParametros.Columns.Count == 3)
            {

                dtParametros.Rows.Add("@idFactura", 2, obj_Facturas_DAL.iIdFactura);
                dtParametros.Rows.Add("@cantidadEstudiantes", 2, obj_Facturas_DAL.iCantidadEstudiantes);
                dtParametros.Rows.Add("@monto", 6, obj_Facturas_DAL.dMonto);
                dtParametros.Rows.Add("@fechaCobro", 7, obj_Facturas_DAL.dtfechaCobro);
                dtParametros.Rows.Add("@fechaPago", 7, obj_Facturas_DAL.dtfechaPago);
                dtParametros.Rows.Add("@comprobantePago", 2, obj_Facturas_DAL.iComprobantePago);
                dtParametros.Rows.Add("@idEstado", 2, obj_Facturas_DAL.iIdEstado);
                dtParametros.Rows.Add("@idPatrocinador", 2, obj_Facturas_DAL.iIdPatrocinador);
              
            }
            obj_BD_BLL.ExecuteNonQuery("SP_Facturas_Modificar", dtParametros, ref obj_BD_DAL);

            if (obj_BD_DAL.sMsjError != string.Empty)
            {
                sMsjError = obj_BD_DAL.sMsjError;
                obj_Facturas_DAL.cBandAxion = 'U';
                obj_Facturas_DAL.cBandEstAxion = 'F';
            }
            else
            {
                sMsjError = string.Empty;
                obj_Facturas_DAL.cBandAxion = 'U';
                obj_Facturas_DAL.cBandEstAxion = 'E';
                //obj_Estudiante_DAL.iIdEstudiante = Convert.ToInt32(obj_BD_DAL.sResultadoID);
            }
        }


        #endregion
        #endregion
    }
}
