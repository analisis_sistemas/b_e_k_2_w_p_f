﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DAL_B_I_T_S.BD;
using BLL_B_I_T_S.BD;
using BLL_B_I_T_S.Logica_General;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;

namespace BLL_B_I_T_S.Catalogos_y_Mantenimiento
{

   
    public  class cls_Estudiante_BLL
    {
        #region Variables Globales
        cls_BD_DAL obj_BD_DAL = new cls_BD_DAL();
        cls_BD_BLL obj_BD_BLL = new cls_BD_BLL();
        cls_Parametros_BLL obj_Parametros_BLL = new cls_Parametros_BLL();
        #endregion

        #region Métodos

        #region Listar
        public void Listar_Estudiante(ref DataTable dtEstudiante, ref string sMsjError)
        {
            #region Variables Publicas

            obj_BD_DAL = new cls_BD_DAL();
            obj_BD_BLL = new cls_BD_BLL();
            DataTable dtParametros = new DataTable();

            #endregion

            obj_BD_BLL.Excecute_Fill("SP_Expediente_Estudiante_Listar", "TBL_EXPEDIENTE_ESTUDIANTES", dtParametros, ref obj_BD_DAL);

            if (obj_BD_DAL.sMsjError == String.Empty)
            {
                sMsjError = string.Empty;
                dtEstudiante = obj_BD_DAL.DS.Tables[0];

            }
            else
            {
                sMsjError = obj_BD_DAL.sMsjError;
                dtEstudiante = new DataTable();
            }

        }
        #endregion

        #region Filtrar
       
        public void Filtrar_Estudiante(ref DataTable dtEstudiante, int iFiltro, ref string sMsjError)
        {
            #region Variables Publicas

            obj_BD_DAL = new cls_BD_DAL();
            obj_BD_BLL = new cls_BD_BLL();

            #endregion

            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();

            if (dtParametros.Columns.Count == 3)
            {
                dtParametros.Rows.Add("@idEstudiante", 2, iFiltro);//Nombre id a filtrar,tipo de dato y donde capturo el id
                obj_BD_BLL.Excecute_Fill("SP_Expediente_Estudiante_Filtrar_X_ID", "TBL_EXPEDIENTE_ESTUDIANTES", dtParametros, ref obj_BD_DAL);

            }
            if (obj_BD_DAL.sMsjError == string.Empty)
            {
                sMsjError = string.Empty;
                dtEstudiante = obj_BD_DAL.DS.Tables[0];


            }
            else
            {
                sMsjError = obj_BD_DAL.sMsjError;
                dtEstudiante = new DataTable();
            }
        }
        #endregion

        #region Eliminar

        //public void Eliminar_Estudiante(string sId, ref string sMsjError)
        //{
        //    DataTable dtParametros = obj_Parametros_BLL.TablaParametros();

        //    if (dtParametros.Columns.Count == 3)
        //    {

        //        dtParametros.Rows.Add("@identificacion", 4, sId);
        //        obj_BD_BLL.ExecuteNonQuery("SP_ELIMINAR_ESTUDIANTE", dtParametros, ref obj_BD_DAL);


        //        if (obj_BD_DAL.sMsjError == string.Empty)
        //        {
        //            sMsjError = string.Empty;
        //        }
        //        else
        //        {
        //            sMsjError = obj_BD_DAL.sMsjError;

        //        }
        //    }

        //    else
        //    {
        //        sMsjError = " Se presento un error al crear parametros";

        //    }

        //}
        #endregion //No se va a eliminar Expedientes

        #region Crear
        public void Crear_Estudiante(ref cls_Estudiante_DAL obj_Estudiante_DAL, ref string sMsjError)
        {
            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();
            if (dtParametros.Columns.Count == 3)
            {
                dtParametros.Rows.Add("@idEstudiante", 2, obj_Estudiante_DAL.iIdEstudiante);
                dtParametros.Rows.Add("@nombre", 4, obj_Estudiante_DAL.sNombre);
                dtParametros.Rows.Add("@primerApellido", 4, obj_Estudiante_DAL.sPrimerApellido);
                dtParametros.Rows.Add("@segundoApellido", 4, obj_Estudiante_DAL.sSegundoApellido);
                dtParametros.Rows.Add("@identificacion", 4, obj_Estudiante_DAL.sIdentificacion);
                dtParametros.Rows.Add("@direccion", 4, obj_Estudiante_DAL.sDireccion);
                dtParametros.Rows.Add("@telefono", 2, obj_Estudiante_DAL.iTelefono);
                dtParametros.Rows.Add("@fechaNacimiento", 7, obj_Estudiante_DAL.dFechaNacimiento);
                dtParametros.Rows.Add("@genero", 1, obj_Estudiante_DAL.cGenero);
                dtParametros.Rows.Add("@nacionalidad", 4, obj_Estudiante_DAL.sNacionalidad);
                dtParametros.Rows.Add("@correoElectronico", 4, obj_Estudiante_DAL.sCorreo);
                dtParametros.Rows.Add("@nivel_academico", 4, obj_Estudiante_DAL.sNivel_Academico);
                dtParametros.Rows.Add("@grado", 2, obj_Estudiante_DAL.iGrado);
                dtParametros.Rows.Add("@total_costo",6,obj_Estudiante_DAL.dTotalCosto);
                dtParametros.Rows.Add("@idCampus", 2, obj_Estudiante_DAL.iIdCampus);
                dtParametros.Rows.Add("@idBeca", 2, obj_Estudiante_DAL.iIdBeca);
                dtParametros.Rows.Add("@idEstado", 2, obj_Estudiante_DAL.iIdEstado);
                dtParametros.Rows.Add("@condicionCasa", 4, obj_Estudiante_DAL.sTipoCasa);
                dtParametros.Rows.Add("@personasNucleo", 2, obj_Estudiante_DAL.iCantNucleoFam);
                dtParametros.Rows.Add("@cantidadPersonasLaboran", 2, obj_Estudiante_DAL.iCantTrabajador);
                dtParametros.Rows.Add("@rangoSalario", 4, obj_Estudiante_DAL.sIngresoMesBruto);
                dtParametros.Rows.Add("@ingresos", 6, obj_Estudiante_DAL.dIngresoMesNeto);
                dtParametros.Rows.Add("@gastos", 6, obj_Estudiante_DAL.dGastoMensual);
         

            }
            obj_BD_BLL.ExecuteNonQuery("SP_Expediente_Estudiante_Insertar", dtParametros, ref obj_BD_DAL);//Si fuera identity uso scalar

            if (obj_BD_DAL.sMsjError != string.Empty)
            {
                sMsjError = string.Empty;
                obj_Estudiante_DAL.cBandAxion = 'I';
                obj_Estudiante_DAL.cBandEstAxion = 'F';
            }
            else
            {
                sMsjError = string.Empty;
                obj_Estudiante_DAL.cBandAxion = 'U';
                obj_Estudiante_DAL.cBandEstAxion = 'E';
            }
        }

        #endregion

        #region Modificar

        public void Modificar_Estudiante(ref cls_Estudiante_DAL obj_Estudiante_DAL, ref string sMsjError)
        {
            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();

            if (dtParametros.Columns.Count == 3)
            {

                dtParametros.Rows.Add("@idEstudiante", 2, obj_Estudiante_DAL.iIdEstudiante);
                dtParametros.Rows.Add("@nombre", 4, obj_Estudiante_DAL.sNombre);
                dtParametros.Rows.Add("@primerApellido", 4, obj_Estudiante_DAL.sPrimerApellido);
                dtParametros.Rows.Add("@segundoApellido", 4, obj_Estudiante_DAL.sSegundoApellido);
                dtParametros.Rows.Add("@identificacion", 4, obj_Estudiante_DAL.sIdentificacion);
                dtParametros.Rows.Add("@direccion", 4, obj_Estudiante_DAL.sDireccion);
                dtParametros.Rows.Add("@telefono", 2, obj_Estudiante_DAL.iTelefono);
                dtParametros.Rows.Add("@fechaNacimiento", 7, obj_Estudiante_DAL.dFechaNacimiento);
                dtParametros.Rows.Add("@genero", 1, obj_Estudiante_DAL.cGenero);
                dtParametros.Rows.Add("@nacionalidad", 4, obj_Estudiante_DAL.sNacionalidad);
                dtParametros.Rows.Add("@correoElectronico", 4, obj_Estudiante_DAL.sCorreo);
                dtParametros.Rows.Add("@nivel_academico", 4, obj_Estudiante_DAL.sNivel_Academico);
                dtParametros.Rows.Add("@grado", 2, obj_Estudiante_DAL.iGrado);
                dtParametros.Rows.Add("@total_costo", 6, obj_Estudiante_DAL.dTotalCosto);
                dtParametros.Rows.Add("@idCampus", 2, obj_Estudiante_DAL.iIdCampus);
                dtParametros.Rows.Add("@idBeca", 2, obj_Estudiante_DAL.iIdBeca);
                dtParametros.Rows.Add("@idEstado", 2, obj_Estudiante_DAL.iIdEstado);
                dtParametros.Rows.Add("@condicionCasa", 4, obj_Estudiante_DAL.sTipoCasa);
                dtParametros.Rows.Add("@personasNucleo", 2, obj_Estudiante_DAL.iCantNucleoFam);
                dtParametros.Rows.Add("@cantidadPersonasLaboran", 2, obj_Estudiante_DAL.iCantTrabajador);
                dtParametros.Rows.Add("@rangoSalario", 4, obj_Estudiante_DAL.sIngresoMesBruto);
                dtParametros.Rows.Add("@ingresos", 6, obj_Estudiante_DAL.dIngresoMesNeto);
                dtParametros.Rows.Add("@gastos", 6, obj_Estudiante_DAL.dGastoMensual);
    
            }
            obj_BD_BLL.ExecuteNonQuery("SP_Expediente_Estudiante_Modificar", dtParametros, ref obj_BD_DAL);

            if (obj_BD_DAL.sMsjError != string.Empty)
            {
                sMsjError = obj_BD_DAL.sMsjError;
                obj_Estudiante_DAL.cBandAxion = 'U';
                obj_Estudiante_DAL.cBandEstAxion = 'F';
            }
            else
            {
                sMsjError = string.Empty;
                obj_Estudiante_DAL.cBandAxion = 'U';
                obj_Estudiante_DAL.cBandEstAxion = 'E';
                //obj_Estudiante_DAL.iIdEstudiante = Convert.ToInt32(obj_BD_DAL.sResultadoID);
            }
        }


        #endregion
        #endregion
    }
}
