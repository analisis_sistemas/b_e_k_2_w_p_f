﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DAL_B_I_T_S.BD;
using BLL_B_I_T_S.BD;
using BLL_B_I_T_S.Logica_General;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;

namespace BLL_B_I_T_S.Catalogos_y_Mantenimiento
{
   public class cls_EstadoFacturas_BLL
   {
       #region Variables Globales
       cls_BD_DAL obj_BD_DAL = new cls_BD_DAL();
       cls_BD_BLL obj_BD_BLL = new cls_BD_BLL();
       cls_Parametros_BLL obj_Parametros_BLL = new cls_Parametros_BLL();
       #endregion

       #region Métodos
       #region Listar
       public void Listar_EstadoFactura(ref DataTable dtEstadoFactura, ref string sMsjError)
       {
           #region Variables Publicas

           obj_BD_DAL = new cls_BD_DAL();
           obj_BD_BLL = new cls_BD_BLL();
           DataTable dtParametros = new DataTable();

           #endregion

           obj_BD_BLL.Excecute_Fill("SP_Estado_Facturas_Listar", "TBL_ESTADOFACTURA", dtParametros, ref obj_BD_DAL);

           if (obj_BD_DAL.sMsjError == String.Empty)
           {
               sMsjError = string.Empty;
               dtEstadoFactura = obj_BD_DAL.DS.Tables[0];

           }
           else
           {
               sMsjError = obj_BD_DAL.sMsjError;
               dtEstadoFactura = new DataTable();
           }

       }
       #endregion

       #region Filtrar
       //public void Filtrar_Estudiante(ref DataTable dtEstudiante, string sFiltro, ref string sMsjError)
       public void Filtrar_EstadoBeca(ref DataTable dtEstadoFactura, int iFiltro, ref string sMsjError)
       {
           #region Variables Publicas

           obj_BD_DAL = new cls_BD_DAL();
           obj_BD_BLL = new cls_BD_BLL();

           #endregion

           DataTable dtParametros = obj_Parametros_BLL.TablaParametros();

           if (dtParametros.Columns.Count == 3)
           {
               dtParametros.Rows.Add("@idEstado", 2, iFiltro);//Nombre id a filtrar,tipo de dato y donde capturo el id
               obj_BD_BLL.Excecute_Fill("SP_Estado_Facturas_Filtrar", "TBL_ESTADOFACTURA", dtParametros, ref obj_BD_DAL);

           }
           if (obj_BD_DAL.sMsjError == string.Empty)
           {
               sMsjError = string.Empty;
               dtEstadoFactura = obj_BD_DAL.DS.Tables[0];


           }
           else
           {
               sMsjError = obj_BD_DAL.sMsjError;
               dtEstadoFactura = new DataTable();
           }
       }
       #endregion


       #endregion
    }
}
