﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DAL_B_I_T_S.BD;
using BLL_B_I_T_S.BD;
using BLL_B_I_T_S.Logica_General;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;

namespace BLL_B_I_T_S.Catalogos_y_Mantenimiento
{
   public class cls_Campus_BLL
    {
        #region Variables Globales
        cls_BD_DAL obj_BD_DAL = new cls_BD_DAL();
        cls_BD_BLL obj_BD_BLL = new cls_BD_BLL();
        cls_Parametros_BLL obj_Parametros_BLL = new cls_Parametros_BLL();
        #endregion

        #region Métodos

        #region Listar
        public void Listar_Campus(ref DataTable dtCampus, ref string sMsjError)
        {
            #region Variables Publicas

            obj_BD_DAL = new cls_BD_DAL();
            obj_BD_BLL = new cls_BD_BLL();
            DataTable dtParametros = new DataTable();

            #endregion

            obj_BD_BLL.Excecute_Fill("SP_Campus_Listar", "TBL_CAMPUS", dtParametros, ref obj_BD_DAL);

            if (obj_BD_DAL.sMsjError == String.Empty)
            {
                sMsjError = string.Empty;
                dtCampus = obj_BD_DAL.DS.Tables[0];

            }
            else
            {
                sMsjError = obj_BD_DAL.sMsjError;
                dtCampus = new DataTable();
            }

        }
        #endregion

        #region Filtrar

        public void Filtrar_Campus(ref DataTable dtCampus, string sFiltro, ref string sMsjError)
        {
            #region Variables Publicas

            obj_BD_DAL = new cls_BD_DAL();
            obj_BD_BLL = new cls_BD_BLL();

            #endregion

            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();

            if (dtParametros.Columns.Count == 3)
            {
                dtParametros.Rows.Add("@nombre", 4, sFiltro);//Nombre id a filtrar,tipo de dato y donde capturo el id
                obj_BD_BLL.Excecute_Fill("SP_Campus_Modificar", "TBL_CAMPUS", dtParametros, ref obj_BD_DAL);

            }
            if (obj_BD_DAL.sMsjError == string.Empty)
            {
                sMsjError = string.Empty;
                dtCampus = obj_BD_DAL.DS.Tables[0];


            }
            else
            {
                sMsjError = obj_BD_DAL.sMsjError;
                dtCampus = new DataTable();
            }
        }
        #endregion

        #region Eliminar

        public void Eliminar_Campus(int iId, ref string sMsjError)
        {
            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();

            if (dtParametros.Columns.Count == 3)
            {

                dtParametros.Rows.Add("@idCampus", 2, iId);
                obj_BD_BLL.ExecuteNonQuery("SP_Campus_Eliminar", dtParametros, ref obj_BD_DAL);


                if (obj_BD_DAL.sMsjError == string.Empty)
                {
                    sMsjError = string.Empty;
                }
                else
                {
                    sMsjError = obj_BD_DAL.sMsjError;

                }
            }

            else
            {
                sMsjError = " Se presento un error al crear parametros";

            }

        }

        #endregion 

        #region Crear
        public void Crear_Campus(ref cls_Campus_DAL obj_Campus_DAL, ref string sMsjError)
        {
            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();
            if (dtParametros.Columns.Count == 3)
            {
                dtParametros.Rows.Add("@idCampus", 2, obj_Campus_DAL.iIdCampus);
                dtParametros.Rows.Add("@nombre", 4, obj_Campus_DAL.sNombre);
                dtParametros.Rows.Add("@IDInstitucion", 9, obj_Campus_DAL.sIDInstitucion);
                


            }
            obj_BD_BLL.ExecuteNonQuery("SP_Campus_Insertar", dtParametros, ref obj_BD_DAL);//Si fuera identity uso scalar

            if (obj_BD_DAL.sMsjError != string.Empty)
            {
                sMsjError = string.Empty;
                obj_Campus_DAL.cBandAxion = 'I';
                obj_Campus_DAL.cBandEstAxion = 'F';
            }
            else
            {
                sMsjError = string.Empty;
                obj_Campus_DAL.cBandAxion = 'U';
                obj_Campus_DAL.cBandEstAxion = 'E';
            }
        }

        #endregion

        #region Modificar

        public void Modificar_Campus(ref cls_Campus_DAL obj_Campus_DAL, ref string sMsjError)
        {
            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();

            if (dtParametros.Columns.Count == 3)
            {

                dtParametros.Rows.Add("@idCampus", 2, obj_Campus_DAL.iIdCampus);
                dtParametros.Rows.Add("@nombre", 4, obj_Campus_DAL.sNombre);
                dtParametros.Rows.Add("@IDInstitucion", 9, obj_Campus_DAL.sIDInstitucion);
            }
            obj_BD_BLL.ExecuteNonQuery("SP_Campus_Modificar", dtParametros, ref obj_BD_DAL);

            if (obj_BD_DAL.sMsjError != string.Empty)
            {
                sMsjError = obj_BD_DAL.sMsjError;
                obj_Campus_DAL.cBandAxion = 'U';
                obj_Campus_DAL.cBandEstAxion = 'F';
            }
            else
            {
                sMsjError = string.Empty;
                obj_Campus_DAL.cBandAxion = 'U';
                obj_Campus_DAL.cBandEstAxion = 'E';
                //obj_Estudiante_DAL.iIdEstudiante = Convert.ToInt32(obj_BD_DAL.sResultadoID);
            }
        }


        #endregion

        #endregion
    }
}
