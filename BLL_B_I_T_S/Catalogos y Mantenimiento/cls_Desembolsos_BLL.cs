﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DAL_B_I_T_S.BD;
using BLL_B_I_T_S.BD;
using BLL_B_I_T_S.Logica_General;
using DAL_B_I_T_S.Catalogos_y_Mantenimiento;

namespace BLL_B_I_T_S.Catalogos_y_Mantenimiento
{
   public class cls_Desembolsos_BLL
    {

        #region Variables Globales
        cls_BD_DAL obj_BD_DAL = new cls_BD_DAL();
        cls_BD_BLL obj_BD_BLL = new cls_BD_BLL();
        cls_Parametros_BLL obj_Parametros_BLL = new cls_Parametros_BLL();
        #endregion

        #region Métodos
        #region Listar
        public void Listar_Desembolsos(ref DataTable dtDesembolsos, ref string sMsjError)
        {
            #region Variables Publicas

            obj_BD_DAL = new cls_BD_DAL();
            obj_BD_BLL = new cls_BD_BLL();
            DataTable dtParametros = new DataTable();

            #endregion

            obj_BD_BLL.Excecute_Fill("SP_Desembolsos_Listar", "TBL_DESEMBOLSOS", dtParametros, ref obj_BD_DAL);

            if (obj_BD_DAL.sMsjError == String.Empty)
            {
                sMsjError = string.Empty;
                dtDesembolsos = obj_BD_DAL.DS.Tables[0];

            }
            else
            {
                sMsjError = obj_BD_DAL.sMsjError;
                dtDesembolsos = new DataTable();
            }

        }
        #endregion

        #region Filtrar
        //public void Filtrar_Estudiante(ref DataTable dtEstudiante, string sFiltro, ref string sMsjError)
        public void Filtrar_Desembolsos(ref DataTable dtDesembolso, int iFiltro, ref string sMsjError)
        {
            #region Variables Publicas

            obj_BD_DAL = new cls_BD_DAL();
            obj_BD_BLL = new cls_BD_BLL();

            #endregion

            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();

            if (dtParametros.Columns.Count == 3)
            {
                dtParametros.Rows.Add("@id_Desembolso", 2, iFiltro);//Nombre id a filtrar,tipo de dato y donde capturo el id
                obj_BD_BLL.Excecute_Fill("SP_Desembolsos_Filtrar", "TBL_DESEMBOLSOS", dtParametros, ref obj_BD_DAL);

            }
            if (obj_BD_DAL.sMsjError == string.Empty)
            {
                sMsjError = string.Empty;
                dtDesembolso = obj_BD_DAL.DS.Tables[0];


            }
            else
            {
                sMsjError = obj_BD_DAL.sMsjError;
                dtDesembolso = new DataTable();
            }
        }
        #endregion

        #region Eliminar

        public void Eliminar_Desembolso(string sId, ref string sMsjError)
        {
            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();

            if (dtParametros.Columns.Count == 3)
            {

                dtParametros.Rows.Add("@idDesembolso", 2, sId);
                obj_BD_BLL.ExecuteNonQuery("SP_Desembolsos_Eliminar", dtParametros, ref obj_BD_DAL);


                if (obj_BD_DAL.sMsjError == string.Empty)
                {
                    sMsjError = string.Empty;
                }
                else
                {
                    sMsjError = obj_BD_DAL.sMsjError;

                }
            }

            else
            {
                sMsjError = " Se presento un error al crear parametros";

            }

        }
        #endregion

        #region Crear
        public void Crear_Desembolso(ref cls_Desembolsos_DAL obj_Desembolsos_DAL, ref string sMsjError)
        {
            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();
            if (dtParametros.Columns.Count == 3)
            {
                dtParametros.Rows.Add("@idDesembolso", 2, obj_Desembolsos_DAL.iIdDesembolso);
                dtParametros.Rows.Add("@idEstudiante", 2, obj_Desembolsos_DAL.iIdEstudiante);
                dtParametros.Rows.Add("@montoDesembolsado", 6, obj_Desembolsos_DAL.dMonto_Desembolsado);
                dtParametros.Rows.Add("@montoSubvencion", 6, obj_Desembolsos_DAL.dMonto_Subvencion);
                dtParametros.Rows.Add("@@montoEstudiante", 6, obj_Desembolsos_DAL.dMonto_Estudiante);
                dtParametros.Rows.Add("@fechaDesembolsado", 7, obj_Desembolsos_DAL.dtFecha_Desembolso);
                dtParametros.Rows.Add("@idEstado", 2, obj_Desembolsos_DAL.iIdEstado);
                dtParametros.Rows.Add("@idFactura", 2, obj_Desembolsos_DAL.iIdFactura);



            }
            obj_BD_BLL.Excecute_Scalar("SP_Desembolsos_Insertar", dtParametros, ref obj_BD_DAL);

            if (obj_BD_DAL.sMsjError != string.Empty)
            {
                sMsjError = string.Empty;
                obj_Desembolsos_DAL.cBandAxion = 'I';
                obj_Desembolsos_DAL.cBandEstAxion = 'F';
            }
            else
            {
                sMsjError = string.Empty;
                obj_Desembolsos_DAL.cBandAxion = 'U';
                obj_Desembolsos_DAL.cBandEstAxion = 'E';
            }
        }

        #endregion

        #region Modificar

        public void Modificar_Activos(ref cls_Desembolsos_DAL obj_Desembolsos_DAL, ref string sMsjError)
        {
            DataTable dtParametros = obj_Parametros_BLL.TablaParametros();

            if (dtParametros.Columns.Count == 3)
            {
                dtParametros.Rows.Add("@idDesembolso", 2, obj_Desembolsos_DAL.iIdDesembolso);
                dtParametros.Rows.Add("@idEstudiante", 2, obj_Desembolsos_DAL.iIdEstudiante);
                dtParametros.Rows.Add("@montoDesembolsado", 6, obj_Desembolsos_DAL.dMonto_Desembolsado);
                dtParametros.Rows.Add("@montoSubvencion", 6, obj_Desembolsos_DAL.dMonto_Subvencion);
                dtParametros.Rows.Add("@@montoEstudiante", 6, obj_Desembolsos_DAL.dMonto_Estudiante);
                dtParametros.Rows.Add("@fechaDesembolsado", 7, obj_Desembolsos_DAL.dtFecha_Desembolso);
                dtParametros.Rows.Add("@idEstado", 2, obj_Desembolsos_DAL.iIdEstado);
                dtParametros.Rows.Add("@idFactura", 2, obj_Desembolsos_DAL.iIdFactura);
            }
            obj_BD_BLL.ExecuteNonQuery("SP_Desembolsos_Modificar", dtParametros, ref obj_BD_DAL);

            if (obj_BD_DAL.sMsjError != string.Empty)
            {
                sMsjError = obj_BD_DAL.sMsjError;
                obj_Desembolsos_DAL.cBandAxion = 'U';
                obj_Desembolsos_DAL.cBandEstAxion = 'F';
            }
            else
            {
                sMsjError = string.Empty;
                obj_Desembolsos_DAL.cBandAxion = 'U';
                obj_Desembolsos_DAL.cBandEstAxion = 'E';
                //obj_Estudiante_DAL.iIdEstudiante = Convert.ToInt32(obj_BD_DAL.sResultadoID);
            }
        }


        #endregion
        #endregion

    }
}
