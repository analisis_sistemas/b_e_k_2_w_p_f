﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using DAL_B_I_T_S.Correo;

namespace BLL_B_I_T_S.Correo
{
    public class cls_Correo_BLL
    {

        public bool enviaMail(ref cls_Correo_DAL obj_Correo)
        {

            //una validación 
            if (obj_Correo.sTo.Trim().Equals("") || obj_Correo.sMessage.Trim().Equals("") || obj_Correo.sSubject.Trim().Equals(""))
            {
                obj_Correo.sError = "El mail, el asunto y el mensaje son obligatorios";
                return false;
            }

            try
            {
                /*creamos un objeto MailMessage
                  este objeto recibe quien envia el mail,
                  la direccion de procedencia, el asunto y el mensaje*/
                MailMessage mEmail = new MailMessage(obj_Correo.sFrom, obj_Correo.sTo, obj_Correo.sSubject, obj_Correo.sMessage);

                //almaceno en un arreglo los correos de los destinatarios separados por coma
                String[] direcciones = obj_Correo.sTo.Split(',');
                //Las recorro y guardo 
                foreach (string dire in direcciones)
                {
                    mEmail.To.Add(dire);
                }

                mEmail.From = new MailAddress(obj_Correo.sFrom,"B.I.T.S");
                mEmail.Subject = obj_Correo.sSubject;
                mEmail.Body = obj_Correo.sMessage;

                if (obj_Correo.lsArchivo != null)
                {
                    //agregado de archivo
                    foreach (string archivo in obj_Correo.lsArchivo)
                    {
                        //comprobamos si existe el archivo y lo agregamos a los adjuntos
                        if (System.IO.File.Exists(@archivo))
                            mEmail.Attachments.Add(new Attachment(@archivo));
                    }
                }

                //creo objeto SmtpClient recibe el servidor que utilizaremos como smtp
                SmtpClient cliente = new SmtpClient("smtp.gmail.com", 587);
                //le definimos si es conexión ssl
                cliente.EnableSsl = true;
                //agregamos nuestro usuario y pass de gmail
                cliente.Credentials = new NetworkCredential(obj_Correo.sDE, obj_Correo.sPASS);
                //enviamos el mail
                cliente.Send(mEmail);
                cliente.Dispose();
                //regresamos true
                return true;
            }
            catch (Exception ex)
            {
                //si ocurre un error regresamos false y el error
                obj_Correo.sError = "Ocurrio un error : \n " + ex.Message;

                return false;
            }


        }
    }
}

