﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL_B_I_T_S.BD;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace BLL_B_I_T_S.BD
{
   public class cls_BD_BLL
    {

        #region Conexión

        #region Traer Conexión
       public void Traer_CNX(ref cls_BD_DAL obj_BD_DAL)
       {
           try
           {
               obj_BD_DAL.sCadenaCNX = ConfigurationManager.ConnectionStrings["WIN_AUT"].ToString();

               obj_BD_DAL.SQL_CNX = new SqlConnection(obj_BD_DAL.sCadenaCNX);

               obj_BD_DAL.sMsjError = string.Empty;

           }
           catch (Exception ex)
           {

               obj_BD_DAL.sMsjError = ex.Message.ToString().Trim();
           }

       }
        #endregion

        #region Abrir Conexión
        public void Abrir_CNX(ref cls_BD_DAL obj_BD_DAL)
        {
            try
            {
                if (obj_BD_DAL.SQL_CNX.State == ConnectionState.Closed)
                {
                    obj_BD_DAL.SQL_CNX.Open();
                    obj_BD_DAL.sMsjError = string.Empty;
                }
                else
                {
                    obj_BD_DAL.sMsjError = "La Conexión ya se encuentra Abierta";
                }
            }
            catch (Exception ex)
            {

                obj_BD_DAL.sMsjError = ex.Message.ToString().Trim();
            }
        }
        #endregion


        #region Cerrar Conexión
        public void Cerrar_CNX(ref cls_BD_DAL obj_BD_DAL)
        {
            try
            {
                if (obj_BD_DAL.SQL_CNX.State != ConnectionState.Closed)
                {
                    obj_BD_DAL.SQL_CNX.Close();
                    obj_BD_DAL.sMsjError = string.Empty;
                }
                else
                {
                    obj_BD_DAL.sMsjError = "La conexión ya se encuentra Cerrada";
                }
            }
            catch (Exception ex)
            {

                obj_BD_DAL.sMsjError = ex.Message.ToString().Trim();
            }
        }
        #endregion

        #endregion

        #region Excecute

        #region Excecute NonQuery

        public void ExecuteNonQuery(string sNomSP, DataTable dtParametros, ref cls_BD_DAL obj_BD_DAL)
        {
            try
            {
                Traer_CNX(ref obj_BD_DAL);
                Abrir_CNX(ref obj_BD_DAL);

                if (obj_BD_DAL.sMsjError == string.Empty)
                {
                    obj_BD_DAL.SQL_CMD = new SqlCommand(sNomSP, obj_BD_DAL.SQL_CNX);

                    #region Parametros

                    foreach (DataRow dr in dtParametros.Rows)
                    {
                        SqlParameter SqlParametro = new SqlParameter(dr[0].ToString(), dr[2]);

                        switch (Convert.ToInt16(dr[1].ToString()))
                        {
                            case 1:
                                SqlParametro.SqlDbType = SqlDbType.Char;
                                break;

                            case 2:
                                SqlParametro.SqlDbType = SqlDbType.Int;
                                break;

                            case 3:
                                SqlParametro.SqlDbType = SqlDbType.VarChar;
                                break;

                            case 4:
                                SqlParametro.SqlDbType = SqlDbType.NVarChar;
                                break;
                            case 5:
                                SqlParametro.SqlDbType = SqlDbType.NChar;
                                break;

                            case 6:
                                SqlParametro.SqlDbType = SqlDbType.Decimal;
                                break;

                            case 7:
                                SqlParametro.SqlDbType = SqlDbType.DateTime;
                                break;

                            case 8:
                                SqlParametro.SqlDbType = SqlDbType.Money;
                                break;

                            case 9:
                                SqlParametro.SqlDbType = SqlDbType.SmallInt;
                                break;
                            default:
                                SqlParametro.SqlDbType = SqlDbType.VarChar;
                                break;
                        }

                        obj_BD_DAL.SQL_CMD.Parameters.Add(SqlParametro);
                    }
                    #endregion

                    obj_BD_DAL.SQL_CMD.CommandType = CommandType.StoredProcedure;

                    obj_BD_DAL.SQL_CMD.ExecuteNonQuery();

                    obj_BD_DAL.sMsjError = string.Empty;
                }


            }
            catch (SqlException sEx)
            {

                obj_BD_DAL.sMsjError = sEx.Message.ToString().Trim();
            }
            finally
            {
                if (obj_BD_DAL.SQL_CNX != null)
                {
                    if (obj_BD_DAL.SQL_CNX.State != ConnectionState.Closed)
                    {
                        obj_BD_DAL.SQL_CNX.Close();
                    }

                    obj_BD_DAL.SQL_CNX.Dispose();
                }

            }

        }

        #endregion

        #region Excecute Scalar

        public string Excecute_Scalar(string sNomSP, DataTable dtParametros, ref cls_BD_DAL obj_BD_DAL)
        {
            try
            {
                Traer_CNX(ref obj_BD_DAL);
                Abrir_CNX(ref obj_BD_DAL);

                string id = string.Empty;

                if (obj_BD_DAL.sMsjError == string.Empty)
                {
                    obj_BD_DAL.SQL_CMD = new SqlCommand(sNomSP, obj_BD_DAL.SQL_CNX);

                    #region Parametros

                    foreach (DataRow dr in dtParametros.Rows)
                    {
                        SqlParameter SqlParametro = new SqlParameter(dr[0].ToString(), dr[2]);

                        switch (Convert.ToInt16(dr[1].ToString()))
                        {
                            case 1:
                                SqlParametro.SqlDbType = SqlDbType.Char;
                                break;

                            case 2:
                                SqlParametro.SqlDbType = SqlDbType.Int;
                                break;

                            case 3:
                                SqlParametro.SqlDbType = SqlDbType.VarChar;
                                break;

                            case 4:
                                SqlParametro.SqlDbType = SqlDbType.NVarChar;
                                break;
                            case 5:
                                SqlParametro.SqlDbType = SqlDbType.NChar;
                                break;

                            case 6:
                                SqlParametro.SqlDbType = SqlDbType.Decimal;
                                break;

                            case 7:
                                SqlParametro.SqlDbType = SqlDbType.DateTime;
                                break;

                            case 8:
                                SqlParametro.SqlDbType = SqlDbType.Money;
                                break;
                            case 9:
                                SqlParametro.SqlDbType = SqlDbType.SmallInt;
                                break;
                        }

                        obj_BD_DAL.SQL_CMD.Parameters.Add(SqlParametro);
                    }
                    #endregion

                    obj_BD_DAL.SQL_CMD.CommandType = CommandType.StoredProcedure;



                    id = obj_BD_DAL.SQL_CMD.ExecuteScalar().ToString();

                    
                    obj_BD_DAL.sMsjError = string.Empty;

                    
                }
                return id;
                
            }
            catch (SqlException SqlEX)
            {

                obj_BD_DAL.sMsjError = SqlEX.Message.ToString().Trim();
                return string.Empty;
            }
            finally
            {
                if (obj_BD_DAL.SQL_CNX != null)
                {
                    if (obj_BD_DAL.SQL_CNX.State != ConnectionState.Closed)
                    {
                        obj_BD_DAL.SQL_CNX.Close();
                    }

                    obj_BD_DAL.SQL_CNX.Dispose();
                }

            }
        }
        #endregion

        #region Excecute Fill

        public void Excecute_Fill(string sNomSP, string sNomTabla, DataTable dtParmetros, ref cls_BD_DAL obj_BD_DAL)
        {
            try
            {
                Traer_CNX(ref obj_BD_DAL);
                Abrir_CNX(ref obj_BD_DAL);
                if (obj_BD_DAL.sMsjError == string.Empty)
                {
                    obj_BD_DAL.SQL_DA = new SqlDataAdapter(sNomSP, obj_BD_DAL.SQL_CNX);

                    #region Parametros

                    foreach (DataRow dr in dtParmetros.Rows)
                    {
                        SqlParameter SqlParametro = new SqlParameter(dr[0].ToString(), dr[2]);

                        switch (Convert.ToInt16(dr[1].ToString()))
                        {
                            case 1:
                                SqlParametro.SqlDbType = SqlDbType.Char;
                                break;

                            case 2:
                                SqlParametro.SqlDbType = SqlDbType.Int;
                                break;

                            case 3:
                                SqlParametro.SqlDbType = SqlDbType.VarChar;
                                break;

                            case 4:
                                SqlParametro.SqlDbType = SqlDbType.NVarChar;
                                break;
                            case 5:
                                SqlParametro.SqlDbType = SqlDbType.NChar;
                                break;

                            case 6:
                                SqlParametro.SqlDbType = SqlDbType.Decimal;
                                break;

                            case 7:
                                SqlParametro.SqlDbType = SqlDbType.DateTime;
                                break;

                            case 8:
                                SqlParametro.SqlDbType = SqlDbType.Money;
                                break;
                            case 9:
                                SqlParametro.SqlDbType = SqlDbType.SmallInt;
                                break;
                        }

                        obj_BD_DAL.SQL_DA.SelectCommand.Parameters.Add(SqlParametro);
                    }
                    #endregion

                    obj_BD_DAL.SQL_DA.SelectCommand.CommandType = CommandType.StoredProcedure;

                    obj_BD_DAL.SQL_DA.Fill(obj_BD_DAL.DS, sNomTabla);

                    obj_BD_DAL.sMsjError = string.Empty;
                }
            }
            catch (SqlException SqlEX)
            {

                obj_BD_DAL.sMsjError = SqlEX.Message.ToString().Trim();
            }
            finally
            {
                if (obj_BD_DAL.SQL_CNX != null)
                {
                    if (obj_BD_DAL.SQL_CNX.State != ConnectionState.Closed)
                    {
                        obj_BD_DAL.SQL_CNX.Close();
                    }

                    obj_BD_DAL.SQL_CNX.Dispose();
                }

            }

        }

        #endregion

        #endregion
    }
}
