﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace BLL_B_I_T_S.Logica_General
{
  public  class cls_Parametros_BLL
    {
      public DataTable TablaParametros()
      {

          try
          {
              DataTable dtParametros = new DataTable("Parámetros");

              dtParametros.Columns.Add("Nombre Parámetro");

              dtParametros.Columns.Add("Tipo Parámetro", typeof(int));

              dtParametros.Columns.Add("Valor Parámetro");

              return dtParametros;

          }
          catch (Exception)
          {

              return new DataTable();
          }
      }
    }
}
